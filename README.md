# GeneaQuilt 2

GeneaQuilt is a new visualization technique for representing large
genealogies of up to several thousand individuals. The visualization
takes the form of a diagonally-filled matrix, where rows are
individuals and columns are nuclear families. The GeneaQuilts system
includes an overview, a timeline, search and filtering components, and
a new interaction technique called Bring & Slide that allows fluid
navigation in very large genealogies.

Website: http://www.aviz.fr/geneaquilts/

GeneaQuilt 2 is an industrial refactoring of GeneaQuilt version 1.

Origin repository: https://github.com/jdfekete/geneaquilt 
 
 
## Developing environment

GeneaQuit project uses strictly Eclipse Kepler, Java 6, GIT and WindowBuilder.

Use another Java version have risks (many differences in Swing, Eclipse, WindowBuilder...).

CAUTION: as said in Eclipse wiki (https://wiki.eclipse.org/Eclipse/Installation): "A Java 6 JRE/JDK is recommended for Eclipse 4.3.". 
To launch Eclipse Kepler with JDK 6 when default Java is another one, In ECLIPSE_HOME/eclipse.ini, add the following lines:
-vm
/usr/java/jdk6/bin/java
(... -vmargs)


## Build

GeneaQuilt uses Ant and generates a geneaquilt-x.y.z folder which contains:

- geneaquilt.jar : full software.
- geneaquilt-core.jar : only compiled class without any library.
- geneaquilt-core-source.jar : sources.


## Command Line

# License
GeneaQuilt V2 (2015) is released under the CeCILL V2.1 license.

GeneaQuilt V1 (2010-2014) was released under the "Simplified BSD License" and GNU General Public License for the file `GUIUtils.java`. 


# History
Version 2 of GeneaQuilt was writed by Klaus Hamberger (EHESS) and Christian Pierre Momon (DEVINSY).

Version 1 of GeneaQuilt was writed by Jean-Daniel Fekete and Pierre Dragicevic, in Aviz laboratory (INRIA).
 