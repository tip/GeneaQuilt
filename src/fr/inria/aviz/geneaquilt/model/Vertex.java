/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import edu.umd.cs.piccolo.PNode;

/**
 * The Class Vertex.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public abstract class Vertex
{
    /** Unicode character for FEMALE (♀) */
    public static final String UNICODE_FEMALE = "\u2640";

    /** Unicode character for MALE (♂) */
    public static final String UNICODE_MALE = "\u2642";

    /** Unicode character for SEXLESS (⚪) */
    public static final String UNICODE_SEXLESS = "\u26AA";

    /** Unicode character for DIVORCE (⚮) */
    public static final String UNICODE_DIVORCE = "\u26AE";

    /** Unicode character for UNMARRIED (⚯) */
    public static final String UNICODE_UNMARRIED_PARTNERSHIP = "\u26AF";

    /** Unicode character for MARRIED (⚭) */
    public static final String UNICODE_MARIAGE = "\u26AD";

    /** Unicode Character for HETERO (⚤) */
    public static final String UNICODE_HETERO = "\u26A4";

    private PNode node;
    private double x;
    private String id;
    private int layer = -1;
    protected DateRange dateRange;
    private boolean dateRangeInvalid = true;
    private Map<String, Object> props = new TreeMap<String, Object>();

    /**
     * Creates a vertex.
     */
    public Vertex()
    {
    }

    /**
     * Creates the node.
     * 
     * @return the p node
     */
    protected abstract PNode createNode();

    /**
     * Unreference the pointed node.
     */
    public void deleteNode()
    {
        this.node = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object other)
    {
        boolean result;

        if (other instanceof Vertex)
        {
            Vertex vertex = (Vertex) other;
            result = vertex.getId().equals(this.id);
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Return all the properties with the specified key.
     * 
     * @param key
     *            the key
     * @return a table of properties with the name
     */
    public List<Object> getAllProperties(final String key)
    {
        List<Object> result;

        Object value = this.props.get(key);
        if (value == null)
        {
            result = Collections.emptyList();
        }
        else
        {
            result = new ArrayList<Object>(1);
            result.add(value);
            int count = 2;
            for (String k2 = key + "." + count; (value = this.props.get(k2)) != null; k2 = key + "." + count)
            {
                result.add(value);
            }
        }

        //
        return result;
    }

    /**
     * Gets the component.
     * 
     * @return the component
     */
    public int getComponent()
    {
        int result;

        Integer value = (Integer) getProperty("COMP");
        if (value == null)
        {
            result = -1;
        }
        else
        {
            result = value.intValue();
        }

        //
        return result;
    }

    /**
     * Returns the specified property as a date value.
     * 
     * @param key
     *            the property name
     * @return the value
     */
    public DateRange getDatePropery(final String key)
    {
        DateRange result;

        result = (DateRange) getProperty(key);

        //
        return result;
    }

    /**
     * Gets the date range.
     * 
     * @return Returns the range of dates of this vertex.
     */
    public DateRange getDateRange()
    {
        DateRange result;

        updateMinMaxDate();
        result = this.dateRange;

        //
        return result;
    }

    /**
     * Gets the doi.
     * 
     * @return the DOI
     */
    public double getDOI()
    {
        double result;

        result = getDoubleProperty("DOI", Double.POSITIVE_INFINITY);

        //
        return result;
    }

    /**
     * Returns the specified property as a double value.
     * 
     * @param key
     *            the property name
     * @param def
     *            the default value if the property is undefined
     * @return the value
     */
    public double getDoubleProperty(final String key, final double def)
    {
        double result;

        Double prop = (Double) getProperty(key);
        if (prop == null)
        {
            result = def;
        }
        else
        {
            result = prop.doubleValue();
        }

        //
        return result;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public String getId()
    {
        return this.id;
    }

    /**
     * Gets the label.
     * 
     * @return a displayable label
     */
    public abstract String getLabel();

    /**
     * Gets the layer.
     * 
     * @return the layer
     */
    public int getLayer()
    {
        return this.layer;
    }

    /**
     * Gets the node.
     * 
     * @return the node
     */
    public PNode getNode()
    {
        PNode result;

        if (this.node == null)
        {
            this.node = createNode();
        }

        result = this.node;

        //
        return result;
    }

    /**
     * Returns the property with the specified key as an object.
     * 
     * @param key
     *            the key
     * @return the value
     */
    public Object getProperty(final String key)
    {
        Object result;

        result = this.props.get(key);

        //
        return result;
    }

    /**
     * Gets the props.
     * 
     * @return the props
     */
    public Map<String, Object> getProps()
    {
        return this.props;
    }

    /**
     * Returns the property with the specified key as a string.
     * 
     * @param key
     *            the key
     * @return the value
     */
    public String getStringProperty(final String key)
    {
        String result;

        Object value = this.props.get(key);
        if (value == null)
        {
            result = null;
        }
        else
        {
            return value.toString();
        }

        //
        return result;
    }

    /**
     * Gets the x.
     * 
     * @return the x
     */
    public double getX()
    {
        return this.x;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int result;

        result = this.id.hashCode();

        //
        return result;
    }

    /**
     * Finds the specified pattern in the field or all the fields if field is
     * null.
     * 
     * @param pattern
     *            the pattern
     * @param field
     *            the field name or null
     * @return true if the pattern matches
     */
    public boolean matches(final Pattern pattern, final String field)
    {
        if (pattern == null)
        {
            return false;
        }
        else
        {
            if (field == null)
            {
                for (Object value : this.props.values())
                {
                    if (value != null)
                    {
                        String valueNonNull = value.toString();
                        Matcher matcher = pattern.matcher(valueNonNull);
                        if (matcher.find())
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                Object v = getProperty(field);
                if (v == null)
                {
                    return false;
                }

                Matcher matcher = pattern.matcher(v.toString());

                if (matcher.find())
                {
                    return true;
                }
                else
                {
                    for (int cnt = 2; true; cnt++)
                    {
                        v = getProperty(field + "." + cnt);
                        if (v == null)
                        {
                            return false;
                        }
                        matcher = pattern.matcher(v.toString());
                        if (matcher.find())
                        {
                            return true;
                        }
                    }
                }
            }
        }

        //
        return false;
    }

    /**
     * Removes the specified property.
     * 
     * WARNING: returns always null, count is unused, etc.
     * 
     * @param key
     *            the property name
     * @return the old value
     */
    public Object removeProperty(final String key)
    {
        Object result;

        result = this.props.remove(key);
        if (result instanceof DateRange)
        {
            this.dateRangeInvalid = true;
        }

        if (result != null)
        {
            int count = 2;

            boolean ended = false;
            String k2 = key + "." + count;
            while (!ended)
            {
                k2 = key + "." + count;
                Object removedObject = this.props.remove(k2);

                if (removedObject == null)
                {
                    ended = true;
                    result = null;
                }
            }
        }

        //
        return result;
    }

    /**
     * Searches for the specified text in the specified field.
     * 
     * @param text
     *            the text to search
     * @param field
     *            the property or null for all the properties
     * @return true if the text is found
     */
    public boolean search(final String text, final String field)
    {
        if (text == null || text.length() == 0)
        {
            return false;
        }
        else
        {
            if (field == null)
            {
                for (Object v : this.props.values())
                {
                    if (v == null)
                    {
                        continue;
                    }
                    String value = v.toString();
                    if (value.contains(text))
                    {
                        return true;
                    }
                }
            }
            else
            {
                Object v = getProperty(field);
                if (v == null)
                {
                    return false;
                }
                boolean f = v.toString().contains(text);
                if (f)
                {
                    return f;
                }
                for (int cnt = 2; true; cnt++)
                {
                    v = getProperty(field + "." + cnt);
                    if (v == null)
                    {
                        return false;
                    }
                    else
                    {
                        if (v.toString().contains(text))
                        {
                            return true;
                        }
                    }
                }

            }
        }

        //
        return false;
    }

    /**
     * Sets the component.
     * 
     * @param component
     *            the component to set
     */
    public void setComponent(final int component)
    {
        setProperty("COMP", Integer.valueOf(component), 0);
    }

    /**
     * Sets the date of the specified attribut (e.g. DEAT, BIRT)
     * 
     * @param attribute
     *            name of the attribute
     * @param date
     *            date value
     */
    public void setDate(final String attribute, final DateRange date)
    {
        if (!StringUtils.equals(attribute, "CHAN"))
        {
            setProperty(attribute, date);
            this.dateRangeInvalid = true;
        }
    }

    /**
     * Sets the date of the specified attribute (e.g. DEAT, BIRT)
     * 
     * @param attribute
     *            name of the attribute
     * @param value
     *            date value
     */
    public void setDate(final String attribute, final String value)
    {
        if (!StringUtils.equals(attribute, "CHAN"))
        {
            DateRange date = new DateRange(value);
            setProperty(attribute, date);
            this.dateRangeInvalid = true;
        }
    }

    /**
     * Sets the doi.
     * 
     * @param value
     *            the dOI to set
     */
    public void setDOI(final double value)
    {
        setProperty("DOI", new Double(value), 0);
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(final String id)
    {
        this.id = id;
    }

    /**
     * Sets the layer.
     * 
     * @param layer
     *            the layer to set
     */
    public void setLayer(final int layer)
    {
        this.layer = layer;
        setProperty("LAYER", Integer.valueOf(layer), 0);
    }

    /**
     * Sets the node.
     * 
     * @param node
     *            the node to set
     */
    public void setNode(final PNode node)
    {
        this.node = node;
    }

    /**
     * Sets the specified property, returning the old value or null.
     * 
     * @param key
     *            the key
     * @param value
     *            the new value
     * @return the real key
     */
    public String setProperty(final String key, final Object value)
    {
        String result;

        int count = 2;
        result = key;
        while (this.props.get(result) != null)
        {
            result = key + "." + count;
            count += 1;
        }
        this.props.put(result, value);

        //
        return result;
    }

    /**
     * Set the nth property with the specified key.
     * 
     * @param key
     *            the key
     * @param value
     *            the value
     * @param count
     *            the property number
     * @return the old property value
     */
    public Object setProperty(final String key, final Object value, final int count)
    {
        Object result;

        if (count == 0)
        {
            result = this.props.put(key, value);
        }
        else
        {
            result = this.props.put(key + "." + count, value);
        }

        //
        return result;
    }

    /**
     * Sets the x.
     * 
     * @param x
     *            the x to set
     */
    public void setX(final double x)
    {
        this.x = x;
    }

    /**
     * Update min max date.
     */
    protected void updateMinMaxDate()
    {
        if (this.dateRangeInvalid)
        {
            if (this.dateRange == null)
            {
                this.dateRange = new DateRange();
            }

            this.dateRange.setInvalid();

            for (Entry<String, Object> object : this.props.entrySet())
            {
                if (object.getValue() instanceof DateRange)
                {
                    DateRange date = (DateRange) object.getValue();
                    this.dateRange.union(date);
                }
            }
            this.dateRangeInvalid = false;
        }
    }
}
