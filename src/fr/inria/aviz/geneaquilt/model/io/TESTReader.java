/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class TESTReader.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class TESTReader
{
    private Map<String, Vertex> vertex;
    private Network net;

    /**
     * Instantiates a new TEST reader.
     */
    public TESTReader()
    {
    }

    /**
     * Find vertex.
     * 
     * @param id
     *            the id
     * @return the vertex
     */
    private Vertex findVertex(final String id)
    {
        Vertex result;

        result = this.vertex.get(id);
        if (result == null)
        {
            if (id.startsWith("F"))
            {
                result = new Fam();
            }
            else
            {
                Indi indi = new Indi();
                indi.setName(id);
                result = indi;
            }
            result.setId(id);
            this.vertex.put(id, result);
            this.net.addVertex(result);
        }

        //
        return result;
    }

    /**
     * Load the file.
     * 
     * @param filename
     *            the filename
     * @return the network or null
     */
    public Network load(final String filename)
    {
        Network result;

        try
        {
            URL url = GEDReader.class.getClassLoader().getResource(filename);
            InputStream bin = url.openStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(bin, "UTF-8"));

            result = new Network();
            this.vertex = new HashMap<String, Vertex>();

            String line;
            while ((line = in.readLine()) != null)
            {
                String[] fields = line.split(" ");
                Vertex vertex = findVertex(fields[0]);
                for (int fieldIndex = 1; fieldIndex < fields.length; fieldIndex++)
                {
                    Vertex other = findVertex(fields[fieldIndex]);
                    Edge edge = new Edge(other.getId(), vertex.getId());
                    edge.setFromVertex(other);
                    edge.setToVertex(vertex);
                    this.net.addEdge(edge, other, vertex);
                }
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            result = null;
        }

        //
        return result;
    }
}
