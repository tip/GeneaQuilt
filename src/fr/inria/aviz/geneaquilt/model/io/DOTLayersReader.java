/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class DOTLayersReader.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class DOTLayersReader
{
    private final Logger logger = LoggerFactory.getLogger(DOTLayersReader.class);

    private static boolean debug = false;

    /**
     * Reads a layer file when it exists, associating a layer and X position to
     * each vertex.
     * 
     * @param network
     *            the network
     * @return true if it has been loaded, false otherwise
     */
    public boolean load(final Network network)
    {
        try
        {
            DOTWriter writer = new DOTWriter(network);
            String msg = "Computing layers…";
            if (network.getEdgeCount() > 5000)
            {
                msg += " (this may take a while!)";
            }
            GUIUtils.updateComputationMessage(msg);

            writer.setBare(true);
            String arg;
            File tmp = null;
            if (debug)
            {
                tmp = File.createTempFile("quilt", ".dot");
                PrintWriter pw = new PrintWriter(tmp);
                writer.write(pw);
                pw.close();
                arg = "dot -Tplain " + tmp.getAbsolutePath();
            }
            else
            {
                arg = "dot -Tplain";
            }

            Process proc;
            try
            {
                proc = Runtime.getRuntime().exec(arg);
            }
            catch (IOException exception)
            {
                JOptionPane.showMessageDialog(null,
                        "Cannot find the 'dot' program. The generations will not be correctly assigned.\n\nTo fix this problem, make sure GraphViz is installed and the dot executable is accessible from "
                                + System.getProperty("user.dir") + "\nThen, delete the .lyr file and relaunch GeneaQuilts.", "Error", JOptionPane.ERROR_MESSAGE);
                this.logger.warn("Cannot find the dot program");
                return false;
            }

            InputStreamReader isr = new InputStreamReader(proc.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            if (!debug)
            {
                writer.write(new PrintWriter(proc.getOutputStream()));
            }

            if (tmp != null)
            {
                this.logger.info("Generated DOT file available at {}", tmp.getAbsolutePath());
                // tmp.delete();
            }

            String line;
            TreeSet<Double> ranks = new TreeSet<Double>();
            HashMap<Vertex, Double> vertexY = new HashMap<Vertex, Double>();
            while ((line = in.readLine()) != null)
            {
                String fields[] = line.split(" ");
                if (fields.length < 5 || !fields[0].equals("node"))
                {
                    continue;
                }
                String id = fields[1];
                if (id.startsWith("\"") && id.endsWith("\""))
                {
                    id = id.substring(1, id.length() - 1);
                }
                double x = Double.parseDouble(fields[2]);
                Double y = Double.valueOf(fields[3]);
                Vertex vertex = network.getVertex(id);
                if (vertex != null)
                {
                    vertex.setX(x);
                    vertexY.put(vertex, y);
                    ranks.add(y);
                }
                else
                {
                    this.logger.warn("Invalid node id={}", id);
                }
            }
            double[] r = new double[ranks.size()];
            int i = 0;
            for (Double d : ranks)
            {
                r[i++] = d.doubleValue();
            }
            for (Entry<Vertex, Double> entry : vertexY.entrySet())
            {
                int l = Arrays.binarySearch(r, entry.getValue().doubleValue());
                if (l < 0)
                {
                    this.logger.error("Unexpeced layer not found for {}", entry.getValue());
                    l = -l + 1;
                }
                network.setVertexLayer(entry.getKey(), l);
            }
            // network.fixLayers();
        }
        catch (Exception exception)
        {
            this.logger.debug("Cannot read layers", exception);
            return false;
        }

        //
        return true;
    }

    /**
     * Checks if is debug.
     * 
     * @return the debug
     */
    public static boolean isDebug()
    {
        return debug;
    }

    /**
     * Sets the debug.
     * 
     * @param d
     *            the debug to set
     */
    public static void setDebug(final boolean d)
    {
        debug = d;
    }
}
