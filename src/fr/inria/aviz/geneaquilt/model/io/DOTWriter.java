/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeMap;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class DOTWriter.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class DOTWriter
{
    private Network network;
    private boolean bare;
    private TreeMap<Object, Collection<Vertex>> ranks;
    /** Key for generation data */
    public static String GENERATION_KEY = "GENERATION";

    /**
     * Instantiates a writer from a network.
     * 
     * @param network
     *            the network
     */
    public DOTWriter(final Network network)
    {
        this.network = network;
    }

    /**
     * Adds the rank.
     * 
     * @param source
     *            the source
     */
    private void addRank(final Vertex source)
    {
        Object gen = source.getProperty(GENERATION_KEY);
        if (gen != null)
        {
            Collection<Vertex> sameRank = this.ranks.get(gen);
            if (sameRank == null)
            {
                sameRank = new HashSet<Vertex>();
                this.ranks.put(gen, sameRank);
            }
            sameRank.add(source);
        }
    }

    /**
     * Gets the generation key.
     * 
     * @return the generationKey
     */
    public String getGenerationKey()
    {
        return GENERATION_KEY;
    }

    /**
     * Checks if is bare.
     * 
     * @return the bare
     */
    public boolean isBare()
    {
        return this.bare;
    }

    /**
     * Sets the bare.
     * 
     * @param bare
     *            the bare to set
     */
    public void setBare(final boolean bare)
    {
        this.bare = bare;
    }

    /**
     * Writes in the specified printwriter.
     * 
     * @param out
     *            the print writer
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void write(final PrintWriter out) throws IOException
    {
        this.ranks = new TreeMap<Object, Collection<Vertex>>();
        out.println("digraph genealogy {");
        if (this.bare)
        {
            out.println("ranksep=1;");
            out.println("node [shape=point,width=0,height=0,label=\"\"];");
            out.println("edge [style=invis];");
        }
        else
        {
            out.println("node [shape=box];");
        }

        HashSet<Vertex> vertices = new HashSet<Vertex>(this.network.getVertices());
        if (!this.bare)
        {
            for (Vertex vertex : vertices)
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    out.println(vertex.getId() + " [label=\"" + indi.getName() + "\"];");
                }
            }
            vertices.clear();
        }

        for (Edge edge : this.network.getEdges())
        {
            Vertex source = this.network.getSource(edge);
            Vertex dest = this.network.getDest(edge);

            addRank(source);
            addRank(dest);
            vertices.remove(source);
            vertices.remove(dest);
            out.print('"');
            out.print(source.getId());
            out.print('"');
            out.print("->");
            out.print('"');
            out.print(dest.getId());
            out.print('"');
            out.print(';');
            out.println();
        }

        for (Vertex vertex : vertices)
        {
            addRank(vertex);
            out.print('"');
            out.print(vertex.getId());
            out.print('"');
            out.print(';');
            out.println();
        }

        for (Collection<Vertex> sameRank : this.ranks.values())
        {
            if (sameRank.size() != 1)
            {
                out.print("{ rank=same;");
                for (Vertex vertex : sameRank)
                {
                    out.print(' ');
                    out.print('"');
                    out.print(vertex.getId());
                    out.print('"');
                }
                out.println(";}");
            }
        }

        out.println('}');
        out.close();
        this.ranks = null;
    }

    /**
     * Writes in the specified file.
     * 
     * @param filename
     *            the file name
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void write(final String filename) throws IOException
    {
        PrintWriter out = new PrintWriter(filename);
        try
        {
            write(out);
        }
        finally
        {
            out.close();
        }
    }

    /**
     * Sets the generation key.
     * 
     * @param generationKey
     *            the generationKey to set
     */
    public static void setGenerationKey(final String generationKey)
    {
        GENERATION_KEY = generationKey;
    }
}
