/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class TIPReader.
 * 
 * See http://www.kintip.net/content/view/74/ and
 * http://www.kintip.net/content/view/76/21/
 * 
 * @author Jean-Daniel Fekete
 */
public class TIPReader
{

    /**
     * Instantiates a new TIP reader.
     */
    private TIPReader()
    {
    }

    /**
     * Gets the fam.
     * 
     * @param source
     *            the source
     * @param wife
     *            the wife
     * @param husb
     *            the husb
     * @return the fam
     */
    private static Fam getFam(final Network source, final String wife, final String husb)
    {
        Fam result;

        String id = "F" + (wife == null ? "unknown" : wife) + "_" + (husb == null ? "unkown" : husb);
        result = (Fam) source.getVertex(id);
        if (result == null)
        {
            result = new Fam();
            result.setId(id);
            result.setHusb(husb);
            result.setWife(wife);
            source.addVertex(result);

            if (husb != null)
            {
                Edge edge = new Edge(id, husb);
                if (!source.containsEdge(edge))
                {
                    Indi husbIndi = (Indi) source.getVertex(husb);
                    source.addEdge(edge, result, husbIndi);
                    husbIndi.addFams(id);
                    edge.setFromVertex(result);
                    edge.setToVertex(husbIndi);
                }
            }

            if (wife != null)
            {
                Edge edge = new Edge(id, wife);
                if (!source.containsEdge(edge))
                {
                    Indi wifeIndi = (Indi) source.getVertex(wife);
                    if (wifeIndi == null)
                    {
                        wifeIndi = new Indi();
                        wifeIndi.setId(wife);
                        source.addVertex(wifeIndi);
                    }
                    source.addEdge(edge, result, wifeIndi);
                    wifeIndi.addFams(id);
                    edge.setFromVertex(result);
                    edge.setToVertex(wifeIndi);
                }
            }
        }

        //
        return result;
    }

    /**
     * Load the file.
     * 
     * @param filename
     *            the filename
     * @return the network or null
     */
    public static Network load(final String filename)
    {
        Network result;

        try
        {
            FileReader fin = new FileReader(filename);
            BufferedReader in = new BufferedReader(fin);
            result = new Network();

            String line;
            int lineNum = 0;
            while ((line = in.readLine()) != null)
            {
                try
                {
                    lineNum += 1;
                    if (line.startsWith("*"))
                    {
                        // comment, ignore for no
                        continue;
                    }
                    String[] field = line.split("\t");
                    if (field.length < 4)
                    {
                        System.err.println("Invalid line :" + line);
                        continue;
                    }
                    int type = Integer.parseInt(field[0]);
                    String id = field[1].intern();
                    Indi indi;
                    switch (type)
                    {
                        case 0:
                            indi = new Indi();
                            indi.setId(id);
                            if ("0".equals(field[2]))
                            {
                                indi.setSex("M");
                            }
                            else if ("1".equals(field[2]))
                            {
                                indi.setSex("F");
                            }
                            // 2 is unkown
                            indi.setName(field[3]);
                            result.addVertex(indi);
                        break;
                        case 1:
                            indi = (Indi) result.getVertex(id);
                            if (indi == null)
                            {
                                System.err.println("Invalid id at line " + lineNum + line);
                                continue;
                            }
                            String other = field[2].intern();
                            int rel = Integer.parseInt(field[3]);
                            switch (rel)
                            {
                                case 0:
                                    indi.setProperty("FATHER", other);
                                break;
                                case 1:
                                    indi.setProperty("MOTHER", other);
                                break;
                                case 2:
                                    indi.setProperty("SPOUSE", other);
                                break;
                                default:
                                    System.err.println("Invalid relation at line " + lineNum + line);
                            }
                        break;
                        case 2:
                            // parse property
                            String prop = field[2];
                            String val = field[3];
                            if ("GENERATION".equals(prop))
                            {
                                int gen = Integer.parseInt(val.trim());
                                result.getVertex(id).setLayer(2 * gen);
                            }
                        // TODO
                        break;
                        default:
                            System.err.println("Invalid type at line " + lineNum + line);
                    }
                }
                catch (NumberFormatException exception)
                {
                    System.err.println("Invalid int at line " + lineNum + line);
                }
            }
            in.close();
            fin.close();

            ArrayList<Vertex> vertices = new ArrayList<Vertex>(result.getVertices());

            for (Vertex vertex : vertices)
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    String father = (String) indi.getProperty("FATHER");
                    String mother = (String) indi.getProperty("MOTHER");
                    if (father != null || mother != null)
                    {
                        Fam fam = getFam(result, mother, father);
                        fam.addChild(indi.getId());
                        indi.setFamc(fam.getId());
                        Edge edge = new Edge(indi.getId(), fam.getId());
                        if (!result.containsEdge(edge))
                        {
                            result.addEdge(edge, indi, fam);
                            edge.setFromVertex(indi);
                            edge.setToVertex(fam);
                        }
                    }

                    String spouse = (String) indi.getProperty("SPOUSE");
                    if (spouse != null)
                    {
                        Indi spouseIndi = (Indi) result.getVertex(spouse);
                        assert (spouseIndi != null);
                        Fam fam = getFam(result, spouse, indi.getId());
                        Edge edge = new Edge(fam.getId(), spouse);
                        if (!result.containsEdge(edge))
                        {
                            result.addEdge(edge, fam, spouseIndi);
                            spouseIndi.addFams(fam.getId());
                            edge.setFromVertex(fam);
                            edge.setToVertex(spouseIndi);
                        }
                        edge = new Edge(fam.getId(), indi.getId());
                        if (!result.containsEdge(edge))
                        {
                            result.addEdge(edge, fam, indi);
                            indi.addFams(fam.getId());
                            edge.setFromVertex(fam);
                            edge.setToVertex(indi);
                        }
                    }
                }
            }

            // if (network != null) {
            // DOTWriter writer = new DOTWriter(network);
            // writer.setBare(true);
            // try {
            // writer.write("debug.dot");
            // }
            // catch(Exception exception) {
            // e.printStackTrace();
            // }
            // }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            result = null;
        }

        //
        return result;
    }
}
