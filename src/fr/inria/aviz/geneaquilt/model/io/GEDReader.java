/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import org.gedcom4j.model.Family;
import org.gedcom4j.model.FamilyChild;
import org.gedcom4j.model.FamilySpouse;
import org.gedcom4j.model.Individual;
import org.gedcom4j.model.IndividualAttribute;
import org.gedcom4j.model.IndividualEvent;
import org.gedcom4j.model.PersonalName;
import org.gedcom4j.model.StringWithCustomTags;
import org.gedcom4j.parser.GedcomParser;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class GEDReader allows reading from a GEDCOM file.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class GEDReader
{

    /**
     * Instantiates a new GED reader.
     */
    private GEDReader()
    {
    }

    /**
     * Loads a GEDCOM file.
     * 
     * @param filename
     *            the file name
     * @return a network or null
     */
    public static Network load(final String filename)
    {
        Network result;

        GedcomParser parser = new GedcomParser();
        result = new Network();
        try
        {
            parser.load(filename);

            for (Individual individual : parser.gedcom.individuals.values())
            {
                Indi indi = new Indi();
                indi.setId(individual.xref);
                result.addVertex(indi);
                for (PersonalName personalName : individual.names)
                {
                    indi.setProperty("NAME", personalName.basic);
                    if (personalName.givenName != null)
                    {
                        indi.setProperty("NAME.GIVN", personalName.givenName);
                    }
                    if (personalName.surname != null)
                    {
                        indi.setProperty("NAME.SURN", personalName.surname);
                    }
                    if (personalName.nickname != null)
                    {
                        indi.setProperty("NAME.NICK", personalName.surname);
                    }
                }

                for (StringWithCustomTags tag : individual.aliases)
                {
                    indi.setProperty("NAME.ALIAS", tag.trim());
                }

                for (FamilyChild familyChild : individual.familiesWhereChild)
                {
                    indi.setFamc(familyChild.family.xref); // should take the
                                                           // fist?
                }

                for (FamilySpouse familySpouse : individual.familiesWhereSpouse)
                {
                    indi.addFams(familySpouse.family.xref);
                }

                if (individual.sex != null)
                {
                    indi.setSex(individual.sex.trim());
                }

                for (IndividualAttribute attribute : individual.attributes)
                {
                    indi.setProperty(attribute.type.tag, attribute.description.value);
                }

                for (IndividualEvent event : individual.events)
                {
                    String tag = event.type.tag;
                    if (event.address != null)
                    {
                        StringBuffer sb = new StringBuffer();
                        for (String s : event.address.lines)
                        {
                            sb.append(s);
                        }
                        indi.setProperty(tag + ".ADDR", sb.toString());
                    }

                    if (event.age != null)
                    {
                        indi.setProperty(tag + ".AGE", event.age.trim());
                    }

                    if (event.cause != null)
                    {
                        indi.setProperty(tag + ".CAUSE", event.cause.trim());
                    }

                    if (event.date != null)
                    {
                        indi.setDate(tag + ".DATE", event.date.trim());
                    }

                    if (event.description != null)
                    {
                        indi.setProperty(tag + ".DESC", event.description.trim());
                    }
                }
            }

            for (Family family : parser.gedcom.families.values())
            {
                Fam fam = new Fam();
                fam.setId(family.xref);
                result.addVertex(fam);

                if (family.husband != null)
                {
                    fam.setHusb(family.husband.xref);
                }

                if (family.wife != null)
                {
                    fam.setWife(family.wife.xref);
                }
            }

            for (Vertex vertex : result.getVertices())
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    if (indi.getFamc() != null)
                    {
                        Vertex fam = result.getVertex(indi.getFamc());
                        if (fam != null)
                        {
                            Edge edge = new Edge(vertex.getId(), indi.getFamc());
                            if (!result.containsEdge(edge))
                            {
                                result.addEdge(edge, indi, fam);
                                edge.setFromVertex(vertex);
                                edge.setToVertex(fam);
                            }
                        }
                    }
                    if (indi.getFams() != null)
                    {
                        for (String fid : indi.getFams())
                        {
                            Vertex fam = result.getVertex(fid);
                            if (fam != null)
                            {
                                Edge edge = new Edge(fam.getId(), vertex.getId());
                                if (!result.containsEdge(edge))
                                {
                                    result.addEdge(edge, fam, indi);
                                    edge.setFromVertex(fam);
                                    edge.setToVertex(indi);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            result = null;
        }

        //
        return result;
    }
}
