/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.BufferedReader;
import java.io.FileReader;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class PEDReader.
 * 
 * See http://pngu.mgh.harvard.edu/~purcell/plink/data.shtml
 * 
 * @author Jean-Daniel Fekete
 */
public class PEDReader
{
    private Network network;
    private String[] tmpLine = new String[7];
    private static final String DELIMS = " \t";

    /**
     * Instantiates a new PED reader.
     */
    public PEDReader()
    {
    }

    /**
     * Find fam.
     * 
     * @param family
     *            the f
     * @param wife
     *            the wife
     * @param husband
     *            the husband
     * @return the fam
     */
    private Fam findFam(final String family, final String wife, final String husband)
    {
        Fam result;

        Indi wifeIndi = null;
        Indi husbandIndi = null;

        String targetWife;
        if (wife.equals("0") || wife.equals("."))
        {
            targetWife = null;
        }
        else
        {
            wifeIndi = findIndi(family, wife);
            targetWife = wife;
        }

        String targetHusband;
        if (husband.equals("0") || husband.equals("."))
        {
            targetHusband = null;
        }
        else
        {
            husbandIndi = findIndi(family, husband);
            targetHusband = husband;
        }

        if ((targetWife == null) && (targetHusband == null))
        {
            result = null;
        }
        else
        {
            String id = "F" + family + "_" + (targetWife == null ? "unknown" : targetWife) + "_" + (targetHusband == null ? "unkown" : targetHusband);
            result = (Fam) this.network.getVertex(id);
            if (result == null)
            {
                result = new Fam();
                result.setId(id);
                result.setHusb(husbandIndi.getId());
                husbandIndi.addFams(result.getId());
                result.setWife(wifeIndi.getId());
                wifeIndi.addFams(result.getId());
                this.network.addVertex(result);
            }
        }

        //
        return result;
    }

    /**
     * Find indi.
     * 
     * @param fam
     *            the fam
     * @param id
     *            the id
     * @return the indi
     */
    private Indi findIndi(final String fam, final String id)
    {
        Indi result;

        if (id.equals("0") || id.equals("."))
        {
            result = null;
        }
        else
        {
            String targetId = "I" + fam + "_" + id;
            result = (Indi) this.network.getVertex(targetId);
            if (result == null)
            {
                result = new Indi();
                result.setId(targetId);
                this.network.addVertex(result);
            }
        }

        //
        return result;
    }

    /**
     * Gets the sex.
     * 
     * @param source
     *            the source
     * @return the sex
     */
    private String getSex(final String source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else if (source.equals("1"))
        {
            result = "M";
        }
        if (source.equals("2"))
        {
            result = "F";
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Loads a network.
     * 
     * @param filename
     *            the file name
     * @return a network
     */
    public Network load(final String filename)
    {
        try
        {
            FileReader fin = new FileReader(filename);
            BufferedReader in = new BufferedReader(fin);
            this.network = new Network();

            String line;
            int lineNum = 0;
            while ((line = in.readLine()) != null)
            {
                lineNum += 1;
                String[] field = split(line);
                if (field == null)
                {
                    continue;
                }
                String f = field[0];
                Indi indi = findIndi(f, field[1]);
                assert (indi.getFamc() == null);
                indi.setProperty("FAMILY", field[0]);
                indi.setName(field[1]);
                indi.setSex(getSex(field[4]));
                if (field[5] != null)
                {
                    indi.setProperty("PHENOTYPE", field[5]);
                }
                if (field[6] != null)
                {
                    indi.setProperty("DATA", field[6]);
                }
                Fam fam = findFam(f, field[3], field[2]);
                if (fam != null)
                {
                    indi.setFamc(fam.getId());
                    fam.addChild(indi.getId());
                }
            }
            in.close();
            fin.close();
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }

        for (Vertex vertex : this.network.getVertices())
        {
            if (vertex instanceof Indi)
            {
                Indi indi = (Indi) vertex;
                if (indi.getFamc() != null)
                {
                    Vertex fam = this.network.getVertex(indi.getFamc());
                    if (fam != null)
                    {
                        Edge edge = new Edge(vertex.getId(), indi.getFamc());
                        if (!this.network.containsEdge(edge))
                        {
                            this.network.addEdge(edge, indi, fam);
                            edge.setFromVertex(vertex);
                            edge.setToVertex(fam);
                        }
                    }
                }
                if (indi.getFams() != null)
                {
                    for (String fid : indi.getFams())
                    {
                        Vertex fam = this.network.getVertex(fid);
                        if (fam != null)
                        {
                            Edge edge = new Edge(fam.getId(), vertex.getId());
                            if (!this.network.containsEdge(edge))
                            {
                                this.network.addEdge(edge, fam, indi);
                                edge.setFromVertex(fam);
                                edge.setToVertex(indi);
                            }
                        }
                    }
                }
            }
        }

        //
        return this.network;
    }

    /**
     * Split.
     * 
     * @param source
     *            the source
     * @return the string[]
     */
    private String[] split(final String source)
    {
        String line = source.trim();
        if (line.length() == 0 || line.startsWith("#") || line.equals("end"))
        {
            return null;
        }
        else
        {
            int last = skipDelims(line, 0);
            for (int index = 0; index < 6; index++)
            {
                int next = nextDelim(line, last);
                if (next == -1)
                {
                    this.tmpLine[index++] = line.substring(last);
                    while (index < 7)
                    {
                        this.tmpLine[index++] = null;
                    }
                    return this.tmpLine;
                }
                else
                {
                    this.tmpLine[index] = line.substring(last, next);
                }
                last = skipDelims(line, next + 1);
            }
            this.tmpLine[6] = line.substring(last);

            return this.tmpLine;
        }
    }

    /**
     * Next delim.
     * 
     * @param line
     *            the line
     * @param offset
     *            the offset
     * @return the int
     */
    private static int nextDelim(final String line, final int offset)
    {
        int len = line.length();
        for (int index = offset; index < len; index++)
        {
            char character = line.charAt(index);
            if (DELIMS.indexOf(character) != -1)
            {
                return index;
            }
        }
        return -1;
    }

    /**
     * Skip delims.
     * 
     * @param line
     *            the line
     * @param offset
     *            the offset
     * @return the int
     */
    private static int skipDelims(final String line, final int offset)
    {
        int result;

        result = offset;
        while (DELIMS.indexOf(line.charAt(offset)) != -1)
        {
            result += 1;
        }

        //
        return result;
    }
}
