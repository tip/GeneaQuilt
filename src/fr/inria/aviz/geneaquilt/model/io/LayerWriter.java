/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.IOException;
import java.io.PrintWriter;

import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class LayerWriter.
 * 
 * @author Jean-Daniel Fekete
 */
public class LayerWriter
{
    private Network network;

    /**
     * Creates a LayerWriter for a specified network.
     * 
     * @param network
     *            the network
     */
    public LayerWriter(final Network network)
    {
        this.network = network;
    }

    /**
     * Write the layers into a specified PrintWriter.
     * 
     * @param out
     *            the writer
     * @throws IOException
     *             if a writing error occurs
     */
    public void write(final PrintWriter out) throws IOException
    {
        for (Vertex vertex : this.network.getVertices())
        {
            out.print(vertex.getId());
            out.print(" ");
            out.print(vertex.getX());
            out.print(" ");
            out.print(vertex.getLayer());
            out.println();
        }
    }

    /**
     * Writes the layers into a specified file.
     * 
     * @param filename
     *            the file name
     * @throws IOException
     *             if a writing error occurs
     */
    public void write(final String filename) throws IOException
    {
        PrintWriter out = new PrintWriter(filename);
        try
        {
            write(out);
        }
        finally
        {
            out.close();
        }
    }
}
