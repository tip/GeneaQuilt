/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class LayersReader.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class LayersReader
{
    private final Logger logger = LoggerFactory.getLogger(LayersReader.class);

    /**
     * Returns true if the layer file exists.
     * 
     * @param basefile
     *            the basefile
     * @return true if the layer file exists
     */
    public boolean layerFileExists(final String basefile)
    {
        boolean result;

        int index = basefile.lastIndexOf('.');
        if (index == -1)
        {
            this.logger.warn("No file extension");
            result = false;
        }
        else
        {
            String layername = basefile.substring(0, index) + ".lyr";
            result = (new File(layername)).exists();
        }

        //
        return result;
    }

    /**
     * Reads a layer file when it exists, associating a layer and X position to
     * each vertex.
     * 
     * @param basefile
     *            the GED file name
     * @param network
     *            the network
     * @return true if it has been loaded, false otherwise
     */
    public boolean load(final String basefile, final Network network)
    {
        boolean result;

        int index = basefile.lastIndexOf('.');
        if (index == -1)
        {
            this.logger.warn("No file extension");
            result = false;
        }
        else
        {
            String layername = basefile.substring(0, index) + ".lyr";
            FileReader fin = null;
            BufferedReader in = null;
            try
            {
                fin = new FileReader(layername);
                in = new BufferedReader(fin);

                String line;
                TreeSet<Double> ranks = new TreeSet<Double>();
                Map<Vertex, Double> vertexY = new HashMap<Vertex, Double>();
                while ((line = in.readLine()) != null)
                {
                    String fields[] = line.split(" ");
                    String id = fields[0];
                    if (id.startsWith("\"") && id.endsWith("\""))
                    {
                        id = id.substring(1, id.length() - 1);
                    }
                    double x = Double.parseDouble(fields[1]);
                    Double y = Double.valueOf(fields[2]);
                    Vertex vertex = network.getVertex(id);
                    if (vertex != null)
                    {
                        vertex.setX(x);
                        vertexY.put(vertex, y);
                        ranks.add(y);
                    }
                    else
                    {
                        this.logger.warn("Cannot find vertex with id={}", id);
                    }
                }
                double[] ranks2 = new double[ranks.size()];
                int i = 0;
                for (Double rank : ranks)
                {
                    ranks2[i++] = rank.doubleValue();
                }

                for (Entry<Vertex, Double> entry : vertexY.entrySet())
                {
                    int layerIndex = Arrays.binarySearch(ranks2, entry.getValue().doubleValue());
                    if (layerIndex < 0)
                    {
                        this.logger.error("Unexpected layer not found for {}", entry.getValue());
                        layerIndex = -layerIndex + 1;
                    }
                    network.setVertexLayer(entry.getKey(), layerIndex);
                }
                // network.fixLayers();
                in.close();
                fin.close();

                result = true;
            }
            catch (FileNotFoundException exception)
            {
                this.logger.warn("Layer file does not exist");
                result = false;
            }
            catch (Exception exception)
            {
                this.logger.info("Cannot open layer file", exception);
                result = false;
            }
        }

        //
        return result;
    }
}
