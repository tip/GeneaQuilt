/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class JSONWriter.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class JSONWriter
{
    Network network;
    /** Key for generation data */
    public static String GENERATION_KEY = "GENERATION";
    private static Pattern INTEGER = Pattern.compile("[0-9]+");
    private static Pattern JSON_CONTROLS = Pattern.compile("[\"|\\|\b|\n|\r|\t]");

    /**
     * Creates a writer from a network.
     * 
     * @param network
     *            the network
     */
    public JSONWriter(final Network network)
    {
        this.network = network;
    }

    /**
     * Prints the.
     * 
     * @param out
     *            the out
     * @param object
     *            the object
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void print(final PrintWriter out, final Object object) throws IOException
    {
        if (object == null)
        {
            out.print("null");
        }
        else if (object instanceof String)
        {
            String string = (String) object;
            Matcher matcher = INTEGER.matcher(string);
            if (matcher.matches())
            {
                out.print(string);
            }
            else
            {
                printName(out, string);
            }
        }
        else
        {
            out.print(object.toString());
        }
    }

    /**
     * Prints the name.
     * 
     * @param out
     *            the out
     * @param name
     *            the name
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void printName(final PrintWriter out, final String name) throws IOException
    {
        int last = 0;
        out.print('"');
        Matcher matcher = JSON_CONTROLS.matcher(name);

        while (matcher.find())
        {
            String pre = name.substring(last, matcher.end() - 1);
            out.print(pre);
            out.print('\\');
            last = matcher.end() - 1;
        }
        out.print(name.substring(last));
        out.print('"');
    }

    /**
     * Prints the tag.
     * 
     * @param out
     *            the out
     * @param name
     *            the name
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void printTag(final PrintWriter out, final String name) throws IOException
    {
        printName(out, name);
        out.print(':');
    }

    /**
     * Writes in the specified printwriter.
     * 
     * @param out
     *            the print writer
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void write(final PrintWriter out) throws IOException
    {
        HashMap<Vertex, Integer> nodeIndex = new HashMap<Vertex, Integer>(this.network.getVertexCount());
        out.print("{");

        printTag(out, "nodes");
        out.print('[');

        boolean first = true;
        for (Vertex vertex : this.network.getVertices())
        {
            if (first)
            {
                first = false;
            }
            else
            {
                out.print(',');
            }
            out.print('{');
            nodeIndex.put(vertex, Integer.valueOf(nodeIndex.size()));
            if (vertex instanceof Indi)
            {
                Indi indi = (Indi) vertex;
                printTag(out, "type");
                printName(out, "indi");
                String f = indi.getFamc();
                if (f != null)
                {
                    out.print(',');
                    printTag(out, "famc");
                    printName(out, f);
                }
                if (indi.getFams() != null && !indi.getFams().isEmpty())
                {
                    out.print(',');
                    printTag(out, "fams");
                    out.print('[');
                    boolean first2 = true;
                    for (String o : indi.getFams())
                    {
                        if (first2)
                        {
                            first2 = false;
                        }
                        else
                        {
                            out.print(',');
                        }
                        printName(out, o);
                    }
                    out.print(']');
                }
                // printTag(out, "id");
                // printName(out, indi.getId());
                // out.print(',');
                // printTag(out, "name");
                // printName(out, indi.getName());
            }
            else
            {
                // Fam fam = (Fam) v;
                printTag(out, "type");
                printName(out, "fam");
                out.print(',');
                printTag(out, "ID");
                printName(out, vertex.getId());
            }
            out.print(',');
            printTag(out, "x");
            out.print(vertex.getX());
            out.print(',');
            printTag(out, "y");
            out.print(vertex.getLayer());

            for (Map.Entry<String, Object> entry : vertex.getProps().entrySet())
            {
                out.print(',');
                printTag(out, entry.getKey());
                print(out, entry.getValue());
            }
            out.print('}');
        }
        out.print(']');
        out.print(',');
        printTag(out, "links");
        out.print('[');

        first = true;
        for (Edge entry : this.network.getEdges())
        {
            Vertex source = this.network.getSource(entry);
            Vertex dest = this.network.getDest(entry);

            if (first)
            {
                first = false;
            }
            else
            {
                out.print(',');
            }
            out.print('{');
            printTag(out, "source");
            out.print(nodeIndex.get(source));
            out.print(',');
            printTag(out, "target");
            out.print(nodeIndex.get(dest));
            out.print('}');
        }

        out.println("]}");
        out.close();
    }

    /**
     * Writes in the specified file.
     * 
     * @param filename
     *            the file name
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void write(final String filename) throws IOException
    {
        PrintWriter out = new PrintWriter(filename);
        try
        {
            write(out);
        }
        finally
        {
            out.close();
        }
    }
}
