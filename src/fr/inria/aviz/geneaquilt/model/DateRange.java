/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import fr.inria.aviz.geneaquilt.model.util.GeneaQuiltUtils;

/**
 * The Class <b>DateRange</b> is the implementation of a GEDCOM date with a
 * precision unit of one second.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class DateRange implements Comparable<DateRange>
{
    /** Number of unit per second. */
    public static final long SECOND = 1;

    /** Number of unit per minute. */
    public static final long MINUTE = SECOND * 60;

    /** Number of unit per hour. */
    public static final long HOUR = MINUTE * 60;

    /** Number of unit per day. */
    public static final long DAY = HOUR * 24;

    /** Number of unit per hour. */
    public static final long YEAR = 31558150L;

    /** Minimum date */
    public static final long MIN_INF = Long.MIN_VALUE / 2;

    /** Maximum date */
    public static final long MAX_INF = Long.MAX_VALUE / 2;
    private String text;
    private String mode;
    private long start;
    private long end;
    private boolean approximated;
    private boolean calculated;
    private boolean estimated;
    private boolean interpolated;

    private static final Calendar CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    private static final Map<String, Integer> MONTH = new HashMap<String, Integer>();
    private static final String[] MONTHS = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
    static
    {
        CALENDAR.setLenient(true);
        for (int index = 0; index < MONTHS.length; index++)
        {
            MONTH.put(MONTHS[index], Integer.valueOf(index));
        }
    }

    /**
     * Instantiates a new date range.
     */
    public DateRange()
    {
        setInvalid();
    }

    /**
     * Instantiates a new copy of the specified date.
     * 
     * @param other
     *            the other date
     */
    public DateRange(final DateRange other)
    {
        this.text = other.text;
        this.mode = other.mode;
        this.start = other.start;
        this.end = other.end;
    }

    /**
     * Creates a precisely date.
     * 
     * @param value
     *            the precise value
     */
    public DateRange(final long value)
    {
        this.start = this.end = value;
    }

    /**
     * Creates a range.
     * 
     * @param start
     *            start range
     * @param end
     *            end range
     */
    public DateRange(final long start, final long end)
    {
        this.start = start;
        this.end = end;
    }

    /**
     * Creates a date from a GEDCom syntax.
     * 
     * @param dateString
     *            the string specifying the date
     */
    public DateRange(final String dateString)
    {
        try
        {
            parse(dateString);
        }
        catch (ParseException exception)
        {
            System.err.println("Couldn't parse date " + dateString);
            // exception.printStackTrace();
        }
    }

    /**
     * Clears the date, making as long as possible.
     */
    public void clear()
    {
        this.start = MIN_INF;
        this.end = MAX_INF;
        this.text = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final DateRange range)
    {
        int result;

        if (!isValid())
        {
            if (range.isValid())
            {
                result = 1;
            }
            else
            {
                result = 0;
            }
        }
        else if (!range.isValid())
        {
            result = -1;
        }
        else if (range.getStart() > getEnd())
        {
            result = 1;
        }
        else if (getStart() > range.getEnd())
        {
            result = -1;
        }
        else if (getStart() == range.getStart() && getEnd() == range.getEnd())
        {
            result = 0;
        }
        else
        {
            result = (int) Math.signum(range.getCenter() - getCenter());
        }

        //
        return result;
    }

    /**
     * Computes the distance between this date and the specified date.
     * 
     * @param range
     *            the date
     * 
     * @return a distance, computed as the cartesian distance between the starts
     *         and the ends.
     */
    public double distance(final DateRange range)
    {
        double result;

        if ((range == null) || (!range.isValid()) || (!isValid()))
        {
            result = Double.POSITIVE_INFINITY;
        }
        else
        {
            long ds = this.start - range.start;
            long de = this.end - range.end;
            result = Math.hypot(ds, de);
        }

        //
        return result;
    }

    /**
     * Computes the distance between this date and the specified date.
     * 
     * @param range
     *            the date
     * @return a distance, computed as the cartesian distance between the starts
     *         and the ends.
     */
    public double distanceToCenter(final DateRange range)
    {
        double result;

        if ((range == null) || (!range.isValid()) || (!isValid()))
        {
            result = Double.POSITIVE_INFINITY;
        }
        else
        {
            result = Math.abs(getCenter() - range.getCenter());
        }

        //
        return result;
    }

    /**
     * Format end.
     * 
     * @return the end time formated.
     */
    public String formatEnd()
    {
        return format(this.end);
    }

    // public static DateRange createGregorian(int year) {
    // return new DateRange(yearFloor(year), yearCeil(year));
    // }

    // public static DateRange createGregorian(int month, int year) {
    // calendar.set(year, month, 0);
    // long floor = calendar.getTimeInMillis()/1000;
    // calendar.set(year, month, 30);
    // long ceil = calendar.getTimeInMillis()/1000;
    // return new DateRange(floor, ceil);
    // }

    // public static DateRange createGregorian(int day, int month, int year) {
    // calendar.set(year, month, day);
    // long floor = calendar.getTimeInMillis()/1000;
    // calendar.set(year, month, day+1);
    // long ceil = calendar.getTimeInMillis()/1000;
    // return new DateRange(floor, ceil);
    // }

    /**
     * Format start.
     * 
     * @return the start time formated.
     */
    public String formatStart()
    {
        return format(this.start);
    }

    /**
     * Gets the center.
     * 
     * @return the center of the interval except when it is unbound in one
     *         direction
     */
    public long getCenter()
    {
        long result;

        if (!isValid())
        {
            result = MIN_INF;
        }
        else if (this.start == MIN_INF)
        {
            result = this.end;
        }
        else if (this.end == MAX_INF)
        {
            result = this.start;
        }
        else
        {
            result = (this.start + this.end) / 2;
        }

        //
        return result;
    }

    /**
     * Gets the end.
     * 
     * @return the end
     */
    public long getEnd()
    {
        return this.end;
    }

    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode()
    {
        return this.mode;
    }

    /**
     * Gets the start.
     * 
     * @return the start
     */
    public long getStart()
    {
        return this.start;
    }

    /**
     * Gets the text.
     * 
     * @return the text
     */
    public String getText()
    {
        return this.text;
    }

    /**
     * Computes the intersection of this date with the specified one.
     * 
     * @param other
     *            the other date
     */
    public void intersection(final DateRange other)
    {
        if (other.isValid())
        {
            this.start = Math.max(other.getStart(), this.start);
            this.end = Math.min(other.getEnd(), this.end);
            this.text = null;
        }

    }

    /**
     * Checks if is approximated.
     * 
     * @return the approximated
     */
    public boolean isApproximated()
    {
        return this.approximated;
    }

    /**
     * Checks if is calculated.
     * 
     * @return the calculated
     */
    public boolean isCalculated()
    {
        return this.calculated;
    }

    /**
     * Checks if is estimated.
     * 
     * @return the estimated
     */
    public boolean isEstimated()
    {
        return this.estimated;
    }

    /**
     * Checks if is interpolated.
     * 
     * @return the interpolated
     */
    public boolean isInterpolated()
    {
        return this.interpolated;
    }

    /**
     * Checks if is max.
     * 
     * @return true if the range is maximal
     */
    public boolean isMax()
    {
        boolean result;

        if ((this.start == MIN_INF) && (this.end == MAX_INF))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is valid.
     * 
     * @return true of the date range is valid
     */
    public boolean isValid()
    {
        boolean result;

        if (this.start <= this.end)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        return result;
    }

    /**
     * Parses a date formated string.
     * 
     * @param dateString
     *            the string specifying the date
     * @throws ParseException
     *             if the syntax is not recognized
     */
    public final void parse(final String dateString) throws ParseException
    {
        this.text = dateString;
        this.mode = null;

        String[] field = dateString.split(" +");
        for (int fieldIndex = 0; fieldIndex < field.length; fieldIndex++)
        {
            field[fieldIndex] = field[fieldIndex].toUpperCase();
        }

        int fieldIndex = 0;
        if (GeneaQuiltUtils.equalsAny(field[fieldIndex], "ABT", "about", "EST", "BEF", "AFT", "BET"))
        {
            this.mode = field[fieldIndex];
            fieldIndex += 1;
            if (this.mode.equals("BET"))
            {
                for (int index = fieldIndex; index < field.length; index++)
                {
                    if ((field[index] != null) && (field[index].equals("AND")))
                    {
                        parse(field, fieldIndex, index - fieldIndex);
                        // TODO parse(field, i+1, field.length-i);
                        return;
                    }
                }
            }
        }
        int n = field.length - fieldIndex;
        parse(field, fieldIndex, n);
    }

    /**
     * Parses the.
     * 
     * @param field
     *            the field
     * @param f
     *            the f
     * @param n
     *            the n
     * @throws ParseException
     *             the parse exception
     */
    private void parse(final String[] field, final int f, final int n) throws ParseException
    {
        try
        {
            if (n == 3)
            {
                // Day month year.
                int month = parseMonth(field[f + 1]);
                int[] day = null;
                if (month < 0)
                {
                    month = parseMonth(field[f]);
                    if (month >= 0)
                    {
                        day = parseInt(field[f + 1]);
                    }
                }
                else
                {
                    day = parseInt(field[f]);
                }
                int[] y = parseInt(field[f + 2]);
                if (month >= 0 && y != null && day != null)
                {
                    if (y.length == 1)
                    {
                        int year = y[0];
                        CALENDAR.set(year, month, day[0]);
                        this.end = CALENDAR.getTimeInMillis() / 1000;
                        CALENDAR.set(year, month, day[0]);
                        this.start = CALENDAR.getTimeInMillis() / 1000;
                    }
                    else
                    {
                        int year1 = y[0];
                        int year2 = y[1];
                        CALENDAR.set(year2, month, day[0]);
                        this.end = CALENDAR.getTimeInMillis() / 1000;
                        CALENDAR.set(year1, month, day[0]);
                        this.start = CALENDAR.getTimeInMillis() / 1000;
                    }
                }
                else
                {
                    throw new ParseException("Invalid date format", 0);
                }
            }
            else if (n == 2)
            {
                int month = parseMonth(field[f]);
                int[] y = parseInt(field[f + 1]);
                if (month != -1 && y != null)
                {
                    if (y.length == 1)
                    {
                        int year = y[0];
                        CALENDAR.set(year, month + 1, 0);
                        this.end = CALENDAR.getTimeInMillis() / 1000;
                        // Calendar should be lenient for month-1.
                        CALENDAR.set(year, month, 1);
                        this.start = CALENDAR.getTimeInMillis() / 1000;
                    }
                    else
                    {
                        int year1 = y[0];
                        int year2 = y[1];
                        CALENDAR.set(year2, month + 1, 0);
                        this.end = CALENDAR.getTimeInMillis() / 1000;
                        CALENDAR.set(year1, month, 1);
                        this.start = CALENDAR.getTimeInMillis() / 1000;
                    }
                }
                else
                {
                    throw new ParseException("Invalid date format", 0);
                }
            }
            else if (n == 1)
            {
                int[] y = parseInt(field[f]);
                if (y != null)
                {
                    if (y.length == 2)
                    {
                        int year1 = y[0];
                        int year2 = y[1];
                        CALENDAR.set(year2, 11, 31);
                        this.end = CALENDAR.getTimeInMillis() / 1000;
                        CALENDAR.set(year1, 0, 1);
                        this.start = CALENDAR.getTimeInMillis() / 1000;
                    }
                    else
                    {
                        int year = y[0];
                        CALENDAR.set(year, 11, 31);
                        this.end = CALENDAR.getTimeInMillis() / 1000;
                        CALENDAR.set(year, 0, 1);
                        this.start = CALENDAR.getTimeInMillis() / 1000;
                    }
                }
                else
                {
                    throw new ParseException("Invalid date format", 0);
                }
            }
        }
        catch (NumberFormatException exception)
        {
            throw new ParseException("Invalid format", 0);
        }
    }

    /**
     * Sets the approximated.
     * 
     * @param approximated
     *            the approximated to set
     */
    public void setApproximated(final boolean approximated)
    {
        this.approximated = approximated;
    }

    /**
     * Sets the calculated.
     * 
     * @param calculated
     *            the calculated to set
     */
    public void setCalculated(final boolean calculated)
    {
        this.calculated = calculated;
    }

    /**
     * Sets the end.
     * 
     * @param end
     *            the end to set
     */
    public void setEnd(final long end)
    {
        this.end = end;
        this.text = null;
    }

    /**
     * Sets the estimated.
     * 
     * @param estimated
     *            the estimated to set
     */
    public void setEstimated(final boolean estimated)
    {
        this.estimated = estimated;
    }

    /**
     * Sets the interpolated.
     * 
     * @param interpolated
     *            the interpolated to set
     */
    public void setInterpolated(final boolean interpolated)
    {
        this.interpolated = interpolated;
    }

    /**
     * Sets the date to invalid with end = MIN_INF and start = MAX_INF. final
     */
    public void setInvalid()
    {
        this.start = MAX_INF;
        this.end = MIN_INF;
        this.text = null;

    }

    /**
     * Sets the start.
     * 
     * @param start
     *            the start to set
     */
    public void setStart(final long start)
    {
        this.start = start;
        this.text = null;
    }

    /**
     * Sets the year as a plus/minus 6 month range around jan 1st.
     * 
     * @param year
     *            the year
     */
    public void setYear(final int year)
    {
        CALENDAR.set(year, 6, 15);
        this.end = CALENDAR.getTimeInMillis() / 1000;
        CALENDAR.roll(Calendar.YEAR, false);
        this.start = CALENDAR.getTimeInMillis() / 1000;
        this.text = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        String result;

        if (this.text == null)
        {
            result = formatStart();
            if (this.start != this.end)
            {
                result = "[" + result + ", " + formatEnd() + "]";
            }
        }
        else
        {
            if (isValid())
            {
                result = this.text;
            }
            else
            {
                result = "??" + this.text;
            }
        }

        //
        return result;
    }

    /**
     * Computes the union of this date with the specified one.
     * 
     * @param other
     *            the other date
     */
    public void union(final DateRange other)
    {
        if (other.isValid())
        {
            this.start = Math.min(other.getStart(), this.start);
            this.end = Math.max(other.getEnd(), this.end);
            this.text = null;
        }
    }

    /**
     * Format.
     * 
     * @param timeInSeconds
     *            the time in seconds
     * @return the time in secodns formated.
     */
    public static String format(final long timeInSeconds)
    {
        String result;

        if (timeInSeconds <= MIN_INF)
        {
            result = "-INF";
        }
        else if (timeInSeconds >= MAX_INF)
        {
            result = "INF";
        }
        else
        {
            CALENDAR.setTimeInMillis(timeInSeconds * 1000);
            int day = CALENDAR.get(Calendar.DAY_OF_MONTH);
            int month = CALENDAR.get(Calendar.MONTH);
            int year = CALENDAR.get(Calendar.YEAR);
            result = Integer.toString(day) + " " + MONTHS[month] + " " + Integer.toString(year);
        }

        //
        return result;
    }

    /**
     * Returns the Year/Month/Day of the specified date value in seconds.
     * 
     * @param value
     *            the value
     * @param fields
     *            optional array of at least 3 ints
     * @return the fields filled with the values
     */
    public static int[] getYMD(final long value, final int[] fields)
    {
        int[] result;

        if (fields == null)
        {
            result = new int[3];
        }
        else
        {
            result = fields;
        }
        CALENDAR.setTimeInMillis(value * 1000);
        result[0] = CALENDAR.get(Calendar.YEAR);
        result[1] = CALENDAR.get(Calendar.MONDAY);
        result[2] = CALENDAR.get(Calendar.DAY_OF_MONTH);

        //
        return result;
    }

    /**
     * Parses the int.
     * 
     * @param source
     *            the source
     * @return the int[]
     */
    private static int[] parseInt(final String source)
    {
        int[] result;

        try
        {
            int separatorIndex = source.indexOf('/');

            if (separatorIndex != -1)
            {
                result = new int[2];
                String y1 = source.substring(0, separatorIndex);
                String y2 = source.substring(separatorIndex + 1);
                result[0] = Integer.parseInt(y1);
                result[1] = Integer.parseInt(y2);
            }
            else
            {
                result = new int[1];
                result[0] = Integer.parseInt(source);
            }
        }
        catch (NumberFormatException exception)
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Parses the month.
     * 
     * @param month
     *            the month
     * @return the int
     */
    private static int parseMonth(final String month)
    {
        int result;

        Integer montIndex = MONTH.get(month);
        if (montIndex == null)
        {
            result = -1;
        }
        else
        {
            result = montIndex.intValue();
        }

        //
        return result;
    }

    /**
     * Returns the date of the ending year day for the specified date value.
     * 
     * @param value
     *            the value
     * @return the end of the year
     */
    public static long yearCeil(final long value)
    {
        CALENDAR.setTimeInMillis(value * 1000);
        int year = CALENDAR.get(Calendar.YEAR);
        CALENDAR.set(year + 1, 0, 1);
        return CALENDAR.getTimeInMillis() / 1000;
    }

    /**
     * Returns the date value for January first of the specified year.
     * 
     * @param year
     *            the year
     * @return the date value for January first
     */
    public static long yearFloor(final int year)
    {
        CALENDAR.set(year, 0, 1);
        return CALENDAR.getTimeInMillis() / 1000;
    }

    /**
     * Returns the date of the starting year day for the specified date value.
     * 
     * @param value
     *            the value
     * @return the start of the year
     */
    public static long yearFloor(final long value)
    {
        CALENDAR.setTimeInMillis(value * 1000);
        int year = CALENDAR.get(Calendar.YEAR);
        return yearFloor(year);
    }
}
