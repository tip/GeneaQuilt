/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.nodes.PSemanticText;

/**
 * The Class Indi.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class Indi extends Vertex
{
    private List<String> fams;
    private String famc;
    /** Use that attribute for names */
    private transient String label;

    /**
     * Creates a new Indi.
     */
    public Indi()
    {
    }

    /**
     * Adds a fams.
     * 
     * @param family
     *            the fams
     */
    public void addFams(final String family)
    {
        if (this.fams == null)
        {
            this.fams = new ArrayList<String>();
        }

        if (!this.fams.contains(family))
        {
            this.fams.add(family);
        }
    }

    /* (non-Javadoc)
     * @see fr.inria.aviz.geneaquilt.model.Vertex#createNode()
     */
    @Override
    protected PNode createNode()
    {
        return new PIndi(this);
    }

    /**
     * Find birth.
     * 
     * @return a birth date, maybe invalid, never null
     */
    public DateRange findBirth()
    {
        DateRange result;

        result = getBirth();
        if (result == null)
        {
            result = new DateRange();
            result.clear();
            setDate("BIRT.DATE", result);
        }

        //
        return result;
    }

    /**
     * Find burial.
     * 
     * @return a burial date, maybe invalid, never null
     */
    public DateRange findBurial()
    {
        DateRange result;

        result = getBurial();
        if (result == null)
        {
            result = new DateRange();
            result.clear();
            setDate("BURI.DATE", result);
        }

        //
        return result;
    }

    /**
     * Find christening.
     * 
     * @return a christening date, maybe invalid, never null
     */
    public DateRange findChristening()
    {
        DateRange result;

        result = getChristening();
        if (result == null)
        {
            result = new DateRange();
            result.clear();
            setDate("CHR.DATE", result);
        }

        //
        return result;
    }

    /**
     * Find death.
     * 
     * @return a death date, maybe invalid, never null
     */
    public DateRange findDeath()
    {
        DateRange result;

        result = getDeath();
        if (result == null)
        {
            result = new DateRange();
            result.clear();
            setDate("DEAT.DATE", result);
        }

        //
        return result;
    }

    /**
     * Gets the birth.
     * 
     * @return the birth
     */
    public DateRange getBirth()
    {
        DateRange result;

        Object date = getProperty("BIRT.DATE");
        if (date == null)
        {
            result = null;
        }
        else
        {
            result = (DateRange) date;
        }

        //
        return result;
    }

    /**
     * Gets the burial.
     * 
     * @return the burial date
     */
    public DateRange getBurial()
    {
        DateRange result;

        Object burial = getProperty("BURI.DATE");
        if (burial == null)
        {
            result = null;
        }
        else
        {
            result = (DateRange) burial;
        }

        //
        return result;
    }

    /**
     * Gets the christening.
     * 
     * @return the christening date
     */
    public DateRange getChristening()
    {
        DateRange result;

        Object date = getProperty("CHR.DATE");
        if (date == null)
        {
            result = null;
        }
        else
        {
            result = (DateRange) date;
        }

        //
        return result;
    }

    /**
     * Gets the death.
     * 
     * @return the death
     */
    public DateRange getDeath()
    {
        DateRange result;

        Object death = getProperty("DEAT.DATE");
        if (death == null)
        {
            result = null;
        }
        else
        {
            result = (DateRange) death;
        }

        //
        return result;
    }

    /**
     * Gets the famc.
     * 
     * @return the famc
     */
    public String getFamc()
    {
        return this.famc;
    }

    /**
     * Gets the fams.
     * 
     * @return the fams
     */
    public List<String> getFams()
    {
        List<String> result;

        if (this.fams == null)
        {
            result = Collections.EMPTY_LIST;
        }
        else
        {
            result = this.fams;
        }

        //
        return result;
    }

    /**
     * Gets the given.
     * 
     * @return the given name
     */
    public String getGiven()
    {
        return getStringProperty("NAME.GIVN");
    }

    /**
     * Gets the label.
     * 
     * @return a suitable label
     */
    @Override
    public String getLabel()
    {
        String result;

        if (this.label == null)
        {
            result = getSexString() + getName();
        }
        else
        {
            result = this.label;
        }

        //
        return result;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName()
    {
        return getStringProperty("NAME");
    }

    /**
     * Gets the sex.
     * 
     * @return the sex
     */
    public String getSex()
    {
        return getStringProperty("SEX");
    }

    /**
     * Gets the sex string.
     * 
     * @return the unicode string related to the sex
     */
    public String getSexString()
    {
        String result;

        if ("M".equalsIgnoreCase(getSex()))
        {
            result = UNICODE_MALE + " ";
        }
        else if ("F".equalsIgnoreCase(getSex()))
        {
            result = UNICODE_FEMALE + " ";
        }
        else
        {
            result = "    ";
        }

        //
        return result;
    }

    /**
     * Gets the surname.
     * 
     * @return the surname
     */
    public String getSurname()
    {
        return getStringProperty("NAME.SURN");
    }

    /**
     * Sets the birth.
     * 
     * @param birth
     *            the birth to set
     */
    public void setBirth(final String birth)
    {
        setDate("BIRT.DATE", birth);
    }

    /**
     * Sets the death.
     * 
     * @param death
     *            the death to set
     */
    public void setDeath(final String death)
    {
        setDate("DEAT.DATE", death);
    }

    /**
     * Sets the famc.
     * 
     * @param famc
     *            the famc to set
     */
    public void setFamc(final String famc)
    {
        this.famc = famc;
    }

    /**
     * Sets the fams.
     * 
     * @param fams
     *            the fams to set
     */
    public void setFams(final List<String> fams)
    {
        this.fams = fams;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setId(final String id)
    {
        super.setId(id);
        setProperty("ID", id);
    }

    /**
     * Change the label.
     * 
     * @param prop
     *            the new label by
     */
    public void setLabelBy(final String prop)
    {
        String old = this.label;

        Object label = getProperty(prop);
        if (label == null)
        {
            label = getName();
        }
        if (label == null)
        {
            this.label = getSexString();
        }
        else
        {
            this.label = getSexString() + label.toString();
        }
        if (getNode() != null && (old == null || !old.equals(this.label)))
        {
            ((PSemanticText) getNode()).setText(this.label);
        }
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the name to set
     */
    public void setName(final String name)
    {
        setProperty("NAME", name);
    }

    /**
     * Sets the sex.
     * 
     * @param sex
     *            the sex to set
     */
    public void setSex(final String sex)
    {
        setProperty("SEX", sex);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        String result;

        result = "Indi[" + getId() + ":" + getName() + "]";

        //
        return result;
    }
}
