/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import org.apache.commons.lang3.StringUtils;

import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;

/**
 * The Class Edge.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class Edge
{
    private PEdge node;
    private String from;
    private String to;
    private Vertex fromVertex;
    private Vertex toVertex;

    /**
     * Creates an edge with the name of the from and to vertices.
     * 
     * @param from
     *            the from vertex name
     * @param to
     *            the to vertex name
     */
    public Edge(final String from, final String to)
    {
        this.from = from;
        this.to = to;
    }

    /**
     * Creates the node.
     * 
     * @return the p edge
     */
    protected PEdge createNode()
    {
        return new PEdge(this);
    }

    /**
     * Unreference the node.
     */
    public void deleteNode()
    {
        this.node = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj)
    {
        boolean result;

        if (obj == this)
        {
            result = true;
        }
        else if ((obj instanceof Edge) && (((Edge) obj).from.equals(this.from)) && (((Edge) obj).to.equals(this.to)))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Gets the from.
     * 
     * @return the from
     */
    public String getFrom()
    {
        return this.from;
    }

    /**
     * Gets the from vertex.
     * 
     * @return the fromVertex
     */
    public Vertex getFromVertex()
    {
        return this.fromVertex;
    }

    /**
     * Gets the node.
     * 
     * @return the node
     */
    public PEdge getNode()
    {
        PEdge result;

        if (this.node == null)
        {
            this.node = createNode();
        }

        result = this.node;

        //
        return result;
    }

    /**
     * Gets the sex.
     * 
     * @return the sex of either of the vertices
     */
    public String getSex()
    {
        String result;

        result = StringUtils.defaultString(this.fromVertex.getStringProperty("SEX"), result = this.toVertex.getStringProperty("SEX"));

        //
        return result;
    }

    /**
     * Gets the to.
     * 
     * @return the to
     */
    public String getTo()
    {
        return this.to;
    }

    /**
     * Gets the to vertex.
     * 
     * @return the toVertex
     */
    public Vertex getToVertex()
    {
        return this.toVertex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return this.from.hashCode() + 31 * this.to.hashCode();
    }

    /**
     * Sets the from.
     * 
     * @param from
     *            the from to set
     */
    public void setFrom(final String from)
    {
        this.from = from;
    }

    /**
     * Sets the from vertex.
     * 
     * @param fromVertex
     *            the fromVertex to set
     */
    public void setFromVertex(final Vertex fromVertex)
    {
        this.fromVertex = fromVertex;
    }

    /**
     * Sets the to.
     * 
     * @param to
     *            the to to set
     */
    public void setTo(final String to)
    {
        this.to = to;
    }

    /**
     * Sets the to vertex.
     * 
     * @param toVertex
     *            the toVertex to set
     */
    public void setToVertex(final Vertex toVertex)
    {
        this.toVertex = toVertex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        String result;

        result = "Edge[" + this.from + "->" + this.to + "]";

        //
        return result;
    }
}
