/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import java.util.Comparator;

/**
 * The Class Event.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 * @version $Revision$
 */
public class VertexComparator implements Comparator<Vertex>
{
    public enum Sorting
    {
        NONE,
        ID,
        DATERANGE,
        DATERANGE_START,
        COMPONENT,
        COMPONENT_DATERANGE,
    }

    private Sorting sorting;

    /**
     * Instantiates a new vertex comparator.
     */
    public VertexComparator()
    {
        this.sorting = Sorting.COMPONENT_DATERANGE;
    }

    /**
     * Instantiates a new vertex comparator.
     * 
     * @param sorting
     *            the sorting
     */
    public VertexComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final Vertex alpha, final Vertex bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     * 
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final Long alpha, final Long bravo)
    {
        int result;

        if (alpha == null)
        {
            result = -1;
        }
        else if (bravo == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     * 
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final String alpha, final String bravo)
    {
        int result;

        if (alpha == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     * 
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final Vertex alpha, final Vertex bravo, final Sorting sorting)
    {
        int result;

        //
        switch (sorting)
        {
            case NONE:
                result = 0;
            break;

            case ID:
            default:
            {
                result = compare(getId(alpha), getId(bravo));
            }
            break;

            case COMPONENT:
            {
                result = getComponent(alpha) - getComponent(bravo);
            }
            break;

            case DATERANGE:
            {
                DateRange alphaValue = alpha.getDateRange();
                DateRange bravoValue = bravo.getDateRange();
                result = -alphaValue.compareTo(bravoValue);
            }
            break;

            case DATERANGE_START:
            {
                result = compare(getStart(alpha), getStart(bravo));
            }
            break;

            case COMPONENT_DATERANGE:
            {
                result = compare(alpha, bravo, Sorting.COMPONENT);

                if (result == 0)
                {
                    result = compare(alpha, bravo, Sorting.DATERANGE);
                }
            }
            break;
        }

        //
        return result;
    }

    /**
     * Gets the component.
     * 
     * @param source
     *            the source
     * @return the component
     */
    public static int getComponent(final Vertex source)
    {
        int result;

        if (source == null)
        {
            result = -1;
        }
        else
        {
            result = source.getComponent();
        }

        //
        return result;
    }

    /**
     * Gets the id.
     * 
     * @param source
     *            the source
     * @return the id
     */
    public static String getId(final Vertex source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getId();
        }

        //
        return result;
    }

    /**
     * Gets the start.
     * 
     * @param source
     *            the source
     * @return the start
     */
    public static Long getStart(final Vertex source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            DateRange dateRange = source.getDateRange();

            if (dateRange.isValid())
            {
                result = dateRange.getStart();
            }
            else
            {
                result = null;
            }
        }

        //
        return result;
    }
}
