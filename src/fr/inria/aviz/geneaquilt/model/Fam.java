/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;

/**
 * The Class Fam.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class Fam extends Vertex
{
    private List<String> children;

    /**
     * Instantiates a new family.
     */
    public Fam()
    {
    }

    /**
     * Adds a child.
     * 
     * @param child
     *            the child
     */
    public void addChild(final String child)
    {
        if (child != null)
        {
            if (this.children == null)
            {
                this.children = new ArrayList<String>();
            }

            if (!this.children.contains(child))
            {
                this.children.add(child);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PNode createNode()
    {
        return new PFam(this);
    }

    /**
     * Find marriage.
     * 
     * @return the marriage date as a date, never null, maybe invalid
     */
    public DateRange findMarriage()
    {
        DateRange range = getMarriage();
        if (range == null)
        {
            range = new DateRange();
            range.clear();
            setMarriage(range);
        }
        return range;
    }

    /**
     * Gets the child.
     * 
     * @return the child
     */
    public List<String> getChild()
    {
        List<String> result;

        if (this.children == null)
        {
            result = Collections.EMPTY_LIST;
        }
        else
        {
            result = this.children;
        }

        //
        return result;
    }

    /**
     * Gets the husb.
     * 
     * @return the husb
     */
    public String getHusb()
    {
        return (String) getProperty("HUSB");
    }

    /**
     * Gets the label.
     * 
     * @return the label to use
     */
    @Override
    public String getLabel()
    {
        // return UNICODE_MARIAGE;//FIXME
        // return " "+UNICODE_MALE+UNICODE_FEMALE+" ";
        return " F ";
    }

    /**
     * Gets the marriage.
     * 
     * @return the marriage date or null
     */
    public DateRange getMarriage()
    {
        return (DateRange) getProperty("MARR.DATE");
    }

    /**
     * Gets the wife.
     * 
     * @return the wife
     */
    public String getWife()
    {
        return (String) getProperty("WIFE");
    }

    /**
     * Sets the chil.
     * 
     * @param child
     *            the child to set
     */
    public void setChil(final List<String> child)
    {
        this.children = child;
    }

    /**
     * Sets the husb.
     * 
     * @param husb
     *            the husb to set
     */
    public void setHusb(final String husb)
    {
        setProperty("HUSB", husb);
    }

    /**
     * Sets the marriage date.
     * 
     * @param date
     *            the date
     */
    public void setMarriage(final DateRange date)
    {
        setProperty("MARR.DATE", date);
    }

    /**
     * Sets the wife.
     * 
     * @param wife
     *            the wife to set
     */
    public void setWife(final String wife)
    {
        setProperty("WIFE", wife);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        String result;

        result = "Fam[" + getId() + "]";

        //
        return result;
    }
}
