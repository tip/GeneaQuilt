/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import fr.inria.aviz.geneaquilt.model.algorithms.BFSCycleFinder;

/**
 * The Class Network.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class Network extends DirectedSparseGraph<Vertex, Edge>
{
    private static final long serialVersionUID = -8274529837949754540L;

    private final Logger logger = LoggerFactory.getLogger(Network.class);

    private String name;
    private List<Set<Vertex>> components;
    private Map<String, Vertex> index;
    private int maxLayer = -1;
    private int minLayer = -1;
    private boolean minMaxUpdated = false;
    private Set<Object> selection;
    private Set<Edge> cycles;

    /**
     * Instantiates a new network.
     */
    public Network()
    {
        this.index = new HashMap<String, Vertex>();
        this.selection = new HashSet<Object>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addVertex(final Vertex vertex)
    {
        boolean result;

        this.index.put(vertex.getId(), vertex);

        result = super.addVertex(vertex);

        //
        return result;
    }

    /**
     * Returns the ascendant of edge (dest).
     * 
     * @param edge
     *            the edge
     * @return the ascendant
     */
    public Vertex getAscendant(final Edge edge)
    {
        Vertex result;

        result = getDest(edge);

        //
        return result;
    }

    /**
     * Returns the number of Ascendants of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * 
     * @return the number of ascendants
     */
    public int getAscendantCount(final Vertex vertex)
    {
        int result;

        result = getSuccessorCount(vertex);

        //
        return result;
    }

    /**
     * Returns the Ascendants of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * 
     * @return the ascendants
     */
    public Collection<Vertex> getAscendants(final Vertex vertex)
    {
        Collection<Vertex> result;

        result = getSuccessors(vertex);

        //
        return result;
    }

    /**
     * Gets the component count.
     * 
     * @return the number of components
     */
    public int getComponentCount()
    {
        int result;

        result = getComponents().size();

        //
        return result;
    }

    /**
     * Gets the components.
     * 
     * @return the connected components of this graph
     */
    public List<Set<Vertex>> getComponents()
    {
        List<Set<Vertex>> result;

        if (this.components == null)
        {
            this.logger.debug("Computing connected components");
            WeakComponentClusterer<Vertex, Edge> clusterer = new WeakComponentClusterer<Vertex, Edge>();
            Set<Set<Vertex>> ret = clusterer.transform(this);
            this.components = new ArrayList<Set<Vertex>>(ret);
            this.logger.debug("Sorting connected components");
            Collections.sort(this.components, new Comparator<Set<Vertex>>()
            {
                @Override
                public int compare(final Set<Vertex> s1, final Set<Vertex> s2)
                {
                    return s2.size() - s1.size();
                }
            });
            int index = 0;
            for (Set<Vertex> comp : this.components)
            {
                this.logger.debug("ComponentSize[{}]={}", index, comp.size());
                for (Vertex vertex : comp)
                {
                    vertex.setComponent(index);
                }
                index += 1;
            }
        }

        result = this.components;

        //
        return result;
    }

    /**
     * Returns the component set containing the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * @return the set or null
     */
    public Set<Vertex> getComponentSet(final Vertex vertex)
    {
        for (Set<Vertex> set : getComponents())
        {
            if (set.contains(vertex))
            {
                return set;
            }
        }

        //
        return null;
    }

    /**
     * Gets the cycles.
     * 
     * @return Returns the set of edges causing cycles
     */
    public Set<Edge> getCycles()
    {
        Set<Edge> result;

        if (this.cycles == null)
        {
            BFSCycleFinder<Vertex, Edge> cycleFinder = new BFSCycleFinder<Vertex, Edge>(this);
            this.cycles = new HashSet<Edge>();
            for (Set<Vertex> comp : getComponents())
            {
                this.cycles.addAll(cycleFinder.findCycles(comp));
            }
        }

        result = this.cycles;

        //
        return result;
    }

    /**
     * Returns the descendant of edge (source).
     * 
     * @param edge
     *            the edge
     * @return the descendant
     */
    public Vertex getDescendant(final Edge edge)
    {
        return getSource(edge);
    }

    /**
     * Returns the number of Descendants of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * @return the number of descendants
     */
    public int getDescendantCount(final Vertex vertex)
    {
        return getPredecessorCount(vertex);
    }

    /**
     * Returns the Descendants of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * @return the descendants
     */
    public Collection<Vertex> getDescendants(final Vertex vertex)
    {
        return getPredecessors(vertex);
    }

    /**
     * Gets the max layer.
     * 
     * @return the maxLayer
     */
    public int getMaxLayer()
    {
        int result;

        updateMinMax();

        result = this.maxLayer;

        //
        return result;
    }

    /**
     * Gets the min layer.
     * 
     * @return the minLayer
     */
    public int getMinLayer()
    {
        int result;

        updateMinMax();

        result = this.minLayer;

        //
        return result;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Returns the parents of a specified individual.
     * 
     * @param indi
     *            the individual
     * @return a collection of up to two parents
     */
    public List<Indi> getParents(final Indi indi)
    {
        List<Indi> result = new ArrayList<Indi>(2);

        for (Vertex family : getAscendants(indi))
        {
            assert (family instanceof Fam);
            for (Vertex parent : getAscendants(family))
            {
                result.add((Indi) parent);
            }
        }

        //
        return result;
    }

    /**
     * Returns the collection of spouses of this individual.
     * 
     * @param indi
     *            the individual
     * @return the spouses
     */
    public Collection<Indi> getSpouses(final Indi indi)
    {
        Collection<Indi> result;

        int count = getDescendantCount(indi);
        if (count == 0)
        {
            result = Collections.emptyList();
        }
        else
        {
            result = new ArrayList<Indi>(count);
            for (Vertex f : getDescendants(indi))
            {
                assert (f instanceof Fam);
                for (Vertex spouse : getAscendants(f))
                {
                    if (spouse != indi)
                    {
                        result.add((Indi) spouse);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Returns the vertex with the specified id.
     * 
     * @param id
     *            the id
     * @return a vertex or null
     */
    public Vertex getVertex(final String id)
    {
        Vertex result;

        result = this.index.get(id);

        //
        return result;
    }

    /**
     * Returns the layer of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * @return the vertex's layer
     */
    public int getVertexLayer(final Vertex vertex)
    {
        int result;

        result = vertex.getLayer();

        //
        return result;
    }

    /**
     * Checks if is layer computed.
     * 
     * @return the layerComputed
     */
    public boolean isLayerComputed()
    {
        boolean result;

        updateMinMax();
        result = this.maxLayer >= 0;

        //
        return result;
    }

    /**
     * Check if a vertex is orphan (no successor).
     * 
     * @param vertex
     *            the vertex
     * @return true/false
     */
    public boolean isOrphan(final Vertex vertex)
    {
        boolean result;

        result = getAscendantCount(vertex) == 0;

        //
        return result;
    }

    /**
     * Check if a vertex is sterile (no predecessor).
     * 
     * @param vertex
     *            the vertex
     * @return true/false
     */
    public boolean isSterile(final Vertex vertex)
    {
        boolean result;

        if (getDescendantCount(vertex) == 0)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Offsets the component by the specified offset.
     * 
     * @param dv
     *            the offset
     * @param comp
     *            the component
     */
    public void offsetLayer(final int dv, final Collection<Vertex> comp)
    {
        if (dv != 0)
        {
            for (Vertex vertex : comp)
            {
                int layer = vertex.getLayer() + dv;
                vertex.setLayer(layer);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeVertex(final Vertex vertex)
    {
        boolean result;

        this.index.remove(vertex.getId());
        result = super.removeVertex(vertex);

        //
        return result;
    }

    /**
     * Reset the layers to -1.
     */
    public void resetLayers()
    {
        for (Vertex vertex : getVertices())
        {
            vertex.setLayer(-1);
        }
        this.maxLayer = -1;
        this.minLayer = -1;
        this.minMaxUpdated = false;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the new name
     */
    public void setName(final String name)
    {
        this.name = name;
    }

    /**
     * Selects/unselects the specified edge.
     * 
     * @param edge
     *            the edge
     * @param sel
     *            selected or not
     */
    public void setSelected(final Edge edge, final boolean sel)
    {
        // e.setSelected(sel);
        if (sel)
        {
            this.selection.add(edge);
        }
        else
        {
            this.selection.remove(edge);
        }
    }

    /**
     * Sets the layer of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * @param layer
     *            the layer
     */
    public void setVertexLayer(final Vertex vertex, final int layer)
    {
        vertex.setLayer(layer);
        if (this.minMaxUpdated)
        {
            this.minLayer = Math.min(layer, this.minLayer);
            this.maxLayer = Math.max(layer, this.maxLayer);
        }
    }

    /**
     * Update min max.
     */
    private void updateMinMax()
    {
        if (!this.minMaxUpdated)
        {
            int min = Integer.MAX_VALUE;
            int max = -1;
            for (Vertex vertex : getVertices())
            {
                int layer = vertex.getLayer();
                min = Math.min(min, layer);
                max = Math.max(max, layer);
            }
            this.minLayer = min;
            this.maxLayer = max;
            this.minMaxUpdated = true;
        }
    }

    /**
     * Recompute the min and max layer values, forcing if necessary.
     * 
     * @param force
     *            true to force the recomputation
     */
    public void updateMinMax(final boolean force)
    {
        this.minMaxUpdated &= !force;
        updateMinMax();
    }
}
