/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.algorithms;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.uci.ics.jung.graph.DirectedGraph;

/**
 * The Class <b>BFSCycleFinder</b> implements the algorithm to break cycles in
 * graphs described in: Gansner, E. R., Koutsofios, E., North, S. C., and Vo, K.
 * 1993. A Technique for Drawing Directed Graphs. IEEE Trans. Softw. Eng. 19, 3
 * (Mar. 1993), 214-230
 * 
 * @param <V>
 *            the vertex class
 * @param <E>
 *            the edge class
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class BFSCycleFinder<V, E>
{
    protected DirectedGraph<V, E> graph;
    protected Set<E> cycles;

    /**
     * Instantiates a new BFS cycle finder from the specified graph and
     * connected component.
     * 
     * @param graph
     *            the graph
     */
    public BFSCycleFinder(final DirectedGraph<V, E> graph)
    {
        this.graph = graph;
    }

    /**
     * Dfs.
     * 
     * @param v
     *            the v
     * @param mark
     *            the mark
     * @param onStack
     *            the on stack
     */
    private void dfs(final V v, final Set<V> mark, final Set<V> onStack)
    {
        if (!mark.contains(v))
        {
            mark.add(v);
            onStack.add(v);
            for (E edge : this.graph.getOutEdges(v))
            {
                V w = this.graph.getDest(edge);
                if (onStack.contains(w))
                {
                    this.cycles.add(edge);
                    w = this.graph.getSource(edge);
                }
                else
                {
                    // mark.remove(w);
                    dfs(w, mark, onStack);
                }
            }
            onStack.remove(v);
        }
    }

    /**
     * Finds all the cyclic edges on the specified weak connected component.
     * 
     * @param comp
     *            the component or null for the whole graph
     * @return a collection of edges to invert
     */
    public Set<E> findCycles(final Collection<V> comp)
    {
        Set<E> result;

        Collection<V> validComp;
        if (comp == null)
        {
            validComp = this.graph.getVertices();
        }
        else
        {
            validComp = comp;
        }
        this.cycles = new HashSet<E>();
        Set<V> mark = new HashSet<V>();
        Set<V> onStack = new HashSet<V>();

        for (V v : validComp)
        {
            dfs(v, mark, onStack);
        }

        result = this.cycles;

        //
        return result;
    }
}
