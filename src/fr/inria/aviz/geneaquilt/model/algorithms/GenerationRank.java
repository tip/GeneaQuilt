/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class GenerationRank.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class GenerationRank extends AbstractAlgorithm
{
    private final Logger logger = LoggerFactory.getLogger(GenerationRank.class);
    private Set<Vertex> treeNode = new HashSet<Vertex>();
    private Set<Edge> treeEdge = new HashSet<Edge>();

    /**
     * Creates a GenerationRank.
     * 
     * @param network
     *            the network
     */
    public GenerationRank(final Network network)
    {
        super(network);
    }

    /**
     * Adds the tree edge.
     * 
     * @param edge
     *            the edge
     */
    private void addTreeEdge(final Edge edge)
    {
        assert (!this.treeEdge.contains(edge));

        this.treeEdge.add(edge);
        this.treeNode.add(this.network.getSource(edge));
        this.treeNode.add(this.network.getDest(edge));
    }

    /**
     * Assign layers.
     */
    private void assignLayers()
    {
        Set<Edge> cycles = this.network.getCycles();
        this.logger.debug("Cyclic edges: {}", cycles.size());
        try
        {
            // for (Edge e : network.getEdges())
            // e.setInverted(cycles.contains(e));
            for (Edge edge : cycles)
            {
                this.network.removeEdge(edge);
                this.network.addEdge(edge, edge.getToVertex(), edge.getFromVertex());
            }

            this.network.resetLayers();
            for (Set<Vertex> comp : this.network.getComponents())
            {
                assignLayers(comp);
            }
            int min = this.network.getMinLayer();
            if (min != 0)
            {
                this.network.offsetLayer(-min, this.network.getVertices());
            }
            this.treeNode.clear();
            this.treeEdge.clear();
        }
        finally
        {
            for (Edge edge : cycles)
            {
                this.network.removeEdge(edge);
                this.network.addEdge(edge, edge.getFromVertex(), edge.getToVertex());
            }
        }
    }

    /**
     * Assign layers.
     * 
     * @param comp
     *            the comp
     */
    private void assignLayers(final Set<Vertex> comp)
    {
        initRank(comp);
        feasibleTree(comp);
        int min = comp.size();
        for (Vertex vertex : comp)
        {
            min = Math.min(min, vertex.getLayer());
        }
        if ((min % 2) == 1)
        {
            min -= 1;
        }
        this.network.offsetLayer(-min, comp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void compute()
    {
        assignLayers();
    }

    /**
     * Feasible tree.
     * 
     * @param comp
     *            the comp
     */
    private void feasibleTree(final Set<Vertex> comp)
    {
        if (comp.size() > 1)
        {
            while (tightTree(comp) < comp.size())
            {
                Edge edge = null;
                for (Vertex vertex : comp)
                {
                    for (Edge otherEdge : this.network.getInEdges(vertex))
                    {
                        if (!this.treeEdge.contains(otherEdge) && incident(otherEdge) != null && ((edge == null) || (slack(otherEdge) < slack(edge))))
                        {
                            edge = otherEdge;
                        }
                    }
                }
                if (edge != null)
                {
                    int delta = slack(edge);
                    if (delta == 0)
                    {
                        this.logger.error("Unexpected tight node");
                    }
                    else
                    {
                        if (incident(edge) == this.network.getDescendant(edge))
                        {
                            delta = -delta;// CHECK
                        }
                        this.network.offsetLayer(delta, this.treeNode);
                    }
                }
            }
        }
    }

    /**
     * Incident.
     * 
     * @param edge
     *            the edge
     * @return the vertex
     */
    private Vertex incident(final Edge edge)
    {
        Vertex result;

        Vertex source = this.network.getSource(edge);
        Vertex dest = this.network.getDest(edge);
        if (this.treeNode.contains(source))
        {
            if (this.treeNode.contains(dest))
            {
                result = null;
            }
            else
            {
                result = source;
            }
        }
        else if (this.treeNode.contains(dest))
        {
            result = dest;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Inits the rank.
     * 
     * @param comp
     *            the comp
     */
    private void initRank(final Set<Vertex> comp)
    {
        LinkedList<Vertex> queue = new LinkedList<Vertex>();
        HashSet<Vertex> processed = new HashSet<Vertex>();
        int counter = 0;

        for (Vertex vertex : comp)
        {
            if (this.network.isOrphan(vertex))
            {
                queue.add(vertex);
            }
        }

        while (!queue.isEmpty())
        {
            Vertex vertex = queue.removeFirst();
            processed.add(vertex);
            counter += 1;

            int layer;
            if (vertex instanceof Fam)
            {
                layer = 1;
            }
            else
            {
                layer = 0;
            }

            for (Vertex parent : this.network.getAscendants(vertex))
            {
                layer = Math.max(layer, parent.getLayer() + 1);
            }
            vertex.setLayer(layer);
            for (Vertex descendant : this.network.getDescendants(vertex))
            {
                if (processed.containsAll(this.network.getAscendants(descendant)))
                {
                    queue.addLast(descendant);
                }
            }
        }
        assert (counter == comp.size());
    }

    /**
     * Slack.
     * 
     * @param edge
     *            the edge
     * @return the int
     */
    private int slack(final Edge edge)
    {
        return this.network.getSource(edge).getLayer() - this.network.getDest(edge).getLayer() - 1;
    }

    /**
     * Tight tree.
     * 
     * @param comp
     *            the comp
     * @return the int
     */
    private int tightTree(final Set<Vertex> comp)
    {
        int result;

        this.treeNode.clear();
        this.treeEdge.clear();
        for (Vertex vertex : comp)
        {
            treeSearch(vertex, comp.size());
            if (!this.treeEdge.isEmpty())
            {
                break;
            }
        }

        result = this.treeNode.size();

        //
        return result;
    }

    /**
     * Tree search.
     * 
     * @param vertex
     *            the vertex
     * @param n
     *            the n
     * @return true, if successful
     */
    private boolean treeSearch(final Vertex vertex, final int n)
    {
        for (Edge edge : this.network.getOutEdges(vertex))
        {
            Vertex head = this.network.getDest(edge);
            if (!this.treeNode.contains(head) && slack(edge) == 0)
            {
                addTreeEdge(edge);
                if (this.treeEdge.size() == n - 1 || treeSearch(head, n))
                {
                    return true;
                }
            }
        }

        for (Edge edge : this.network.getInEdges(vertex))
        {
            Vertex tail = this.network.getSource(edge);
            if (!this.treeNode.contains(tail) && slack(edge) == 0)
            {
                addTreeEdge(edge);
                if (this.treeEdge.size() == n - 1 || treeSearch(tail, n))
                {
                    return true;
                }
            }
        }
        return false;
    }

    // private int depth(Vertex v) {
    // int max = 0;
    // for (Vertex d : network.getDescendants(v)) {
    // max = Math.max(max, depth(d));
    // }
    // return max + 1;
    // }
    //
    // private int assignLayer(Vertex first) {
    // int min = network.getVertexCount();
    // first.setLayer(min);
    // LinkedList<Vertex> queue = new LinkedList<Vertex>();
    // queue.addLast(first);
    //
    // while (! queue.isEmpty()) {
    // Vertex v = queue.removeFirst();
    // int l = v.getLayer();
    // for (Vertex d : network.getDescendants(v)) {
    // if (d.getLayer()!=-1) {// already assigned
    // if (d.getLayer() > l)
    // continue;
    // else
    // logger.debug("Fixing up layer of "+d
    // +" from "+d.getLayer()+" to "+(l+1));
    // }
    // d.setLayer(l+1);
    // logger.debug("Vertex "+d+"="+Integer.toString(l+1));
    // queue.addLast(d);
    // }
    // }
    // return min;
    // }
}
