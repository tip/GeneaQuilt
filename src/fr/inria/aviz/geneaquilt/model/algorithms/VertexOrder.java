/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import fr.inria.aviz.geneaquilt.model.DateRange;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class VertexOrder.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class VertexOrder extends AbstractAlgorithm
{
    /**
     * The Class BirthOrder.
     */
    public static class BirthOrder implements Comparator<Vertex>
    {

        /* (non-Javadoc)
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(final Vertex alpha, final Vertex bravo)
        {
            int result;

            if (alpha.getComponent() != bravo.getComponent())
            {
                result = alpha.getComponent() - bravo.getComponent();
            }
            else
            {
                DateRange alphaValue = alpha.getDateRange();
                DateRange bravoValue = bravo.getDateRange();
                result = -alphaValue.compareTo(bravoValue);
            }

            //
            return result;
        }
    }

    private Vertex[][] layers;

    private static final BirthOrder birthOrder = new BirthOrder();

    // private HashMap<Vertex,Integer> bestOrder;
    // private int bestCrossings;
    /**
     * Creates the vertex orderer.
     * 
     * @param network
     *            the network
     */
    public VertexOrder(final Network network)
    {
        super(network);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void compute()
    {
        init();
        // bestOrder = computeOrder();
        // TODO Auto-generated method stub
    }

    /**
     * Inits the.
     */
    private void init()
    {
        ArrayList<Vertex>[] layersBuffer = new ArrayList[this.network.getMaxLayer() + 1];

        for (int layerIndex = 0; layerIndex < layersBuffer.length; layerIndex++)
        {
            layersBuffer[layerIndex] = new ArrayList<Vertex>();
        }

        for (Vertex vertex : this.network.getVertices())
        {
            int layer = vertex.getLayer();
            layersBuffer[layer].add(vertex);
        }

        this.layers = new Vertex[this.network.getMaxLayer() + 1][];

        for (int layerIndex = 0; layerIndex < layersBuffer.length; layerIndex++)
        {
            Vertex[] layer = layersBuffer[layerIndex].toArray(null);
            this.layers[layerIndex] = layer;
        }

        sortLayer(this.layers[0], birthOrder);
    }

    /**
     * Sets the order.
     * 
     * @param layer
     *            the new order
     */
    private void setOrder(final Vertex[] layer)
    {
        for (int index = 0; index < layer.length; index++)
        {
            layer[index].setX(index);
        }
    }

    //
    // private int getOrder(Vertex v) {
    // return (int)v.getX();
    // }
    //
    // private HashMap<Vertex,Integer> computeOrder() {
    // HashMap<Vertex,Integer> order = new HashMap<Vertex,
    // Integer>(network.getVertexCount());
    // for (int l = 0; l < layers.length; l++) {
    // Vertex[] layer = layers[l];
    // layers[l] = layer;
    // for (int i = 0; i < layer.length; i++) {
    // order.put(layer[i], Integer.valueOf(i));
    // }
    // }
    // return order;
    // }

    /**
     * Sort layer.
     * 
     * @param layer
     *            the layer
     * @param order
     *            the order
     */
    private void sortLayer(final Vertex[] layer, final Comparator<Vertex> order)
    {
        Arrays.sort(layer, order);
        setOrder(layer);
    }

    // private int minimizeCrossings(int startpass, int endpass) {
    // int currentCross = Integer.MAX_VALUE;
    // int bestCross = currentCross;
    // HashMap<Vertex,Integer> savedOrder = null;
    //
    // if (startpass > 1) {
    // bestCross = currentCross = computeCrossings();
    // savedOrder = computeOrder();
    // }
    //
    // for (int pass = startpass; pass <= endpass; pass++) {
    //
    // }
    //
    // return bestCross;
    // }

    // private int computeCrossings() {
    // int count = 0;
    // for (int l = network.getMinLayer(); l <= network.getMaxLayer(); l++) {
    // count += computeCrossings(l);
    // }
    // return count;
    // }

    // private int computeCrossings(int l) {
    // int cross = 0;
    // int max = 0;
    // Vertex[] layer = layers[l];
    // int[] count = new int[layer.length+1];
    //
    // for (int top = 0; top < layer.length; top++) {
    // Vertex v = layer[top];
    // if (max > 0) {
    // for (Vertex w : network.getDescendants(v)) {
    // for (int k = getOrder(w)+1; k <= max; k++) {
    // cross += count[k];
    // }
    // }
    // }
    // for (Vertex w : network.getDescendants(v)) {
    // int inv = getOrder(w);
    // if (inv > max) max = inv;
    // count[inv]++;
    // }
    // }
    //
    // return cross;
    // }
}
