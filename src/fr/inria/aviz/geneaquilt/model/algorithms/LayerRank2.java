/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.algorithms;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class LayerRank2.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class LayerRank2 extends AbstractAlgorithm
{
    private final Logger logger = LoggerFactory.getLogger(LayerRank2.class);
    private Map<Vertex, Integer> levelPred;
    private Map<Vertex, Integer> levelSucc;
    private int depth;

    /**
     * Creates athe layer ranker.
     * 
     * @param network
     *            the network
     */
    public LayerRank2(final Network network)
    {
        super(network);
        this.levelPred = new HashMap<Vertex, Integer>();
        this.levelSucc = new HashMap<Vertex, Integer>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void compute()
    {
        Map<Vertex, Integer> map = computeLayers();
        for (Entry<Vertex, Integer> entry : map.entrySet())
        {
            this.network.setVertexLayer(entry.getKey(), entry.getValue().intValue());
        }
    }

    /**
     * Computes and assign the layers.
     * 
     * @return the layer assignment
     */
    public Map<Vertex, Integer> computeLayers()
    {
        Set<Edge> cycles = this.network.getCycles();
        this.logger.debug("Cyclic edges: {}", cycles.size());
        try
        {
            for (Edge edge : cycles)
            {
                this.network.removeEdge(edge);
            }
            this.levelPred.clear();
            this.levelSucc.clear();
            for (Vertex vertex : this.network.getVertices())
            {
                if (this.network.isOrphan(vertex))
                {
                    setDepthPred(vertex);
                }
                if (this.network.isSterile(vertex))
                {
                    setDepthSucc(vertex);
                }
            }
            this.depth = 0;
            Map<Vertex, Integer> layers = new HashMap<Vertex, Integer>();
            for (Vertex vertex : this.network.getVertices())
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    int k = getLevelPred(indi) + getLevelSucc(indi);
                    assert (k >= 0);
                    layers.put(indi, Integer.valueOf(k));
                    if (k > this.depth)
                    {
                        this.depth = k;
                    }
                }
            }
            for (Vertex vertex : this.network.getVertices())
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    int k = (this.depth - layers.get(indi).intValue()) / 2 + getLevelPred(indi);
                    layers.put(indi, Integer.valueOf(k));
                }
            }
            for (Vertex vertex : this.network.getVertices())
            {
                if (vertex instanceof Fam)
                {
                    Fam fam = (Fam) vertex;
                    int k = famLayer(fam, layers);
                    layers.put(fam, Integer.valueOf(k));
                }
            }
            return layers;
        }
        finally
        {
            for (Edge edge : cycles)
            {
                this.network.addEdge(edge, edge.getFromVertex(), edge.getToVertex());
            }
        }
    }

    /**
     * Fam layer.
     * 
     * @param fam
     *            the fam
     * @param layers
     *            the layers
     * @return the int
     */
    private int famLayer(final Fam fam, final Map<Vertex, Integer> layers)
    {
        int result;

        result = this.depth + 1;
        for (Vertex vertex : this.network.getPredecessors(fam))
        {
            result = Math.min(result, layers.get(vertex).intValue() - 1);
        }
        if (result == this.depth + 1)
        {
            result = 0;
            for (Vertex vertex : this.network.getSuccessors(fam))
            {
                result = Math.max(result, layers.get(vertex).intValue() + 1);
            }
        }

        //
        return result;
    }

    // /**
    // * Returns the layer associated with the specified vertex.
    // * @param v the vertex
    // * @return the layer
    // */
    // public int getLayer(Vertex v) {
    // if (layers == null)
    // computeLayers();
    // Integer i = layers.get(v);
    // if (i == null)
    // return -1;
    // return i.intValue();
    // }

    /**
     * Gets the level pred.
     * 
     * @param vertex
     *            the vertex
     * @return the level pred
     */
    private int getLevelPred(final Vertex vertex)
    {
        int result;

        Integer level = this.levelPred.get(vertex);
        if (level == null)
        {
            result = -1;
        }
        else
        {
            result = level.intValue();
        }

        //
        return result;
    }

    /**
     * Gets the level succ.
     * 
     * @param vertex
     *            the vertex
     * @return the level succ
     */
    private int getLevelSucc(final Vertex vertex)
    {
        int result;

        Integer level = this.levelSucc.get(vertex);
        if (level == null)
        {
            result = -1;
        }
        else
        {
            result = level.intValue();
        }

        //
        return result;
    }

    /**
     * Sets the depth pred.
     * 
     * @param vertex
     *            the new depth pred
     */
    private void setDepthPred(final Vertex vertex)
    {
        Stack<Indi> stack = new Stack<Indi>();
        Stack<Integer> dstack = new Stack<Integer>();
        if (vertex instanceof Fam)
        {
            Fam fam = (Fam) vertex;
            for (Vertex w : this.network.getPredecessors(fam))
            {
                stack.push((Indi) w);
                dstack.push(2);
                dstack.push(0);
            }
        }
        else
        {
            stack.push((Indi) vertex);
            dstack.push(0);
            dstack.push(0);
        }
        while (!stack.isEmpty())
        {
            Indi indi = stack.pop();
            int d = dstack.pop().intValue();
            int i = dstack.pop().intValue();
            if (getLevelPred(indi) > d - i)
            {
                continue;
            }
            setLevelPred(indi, d);
            for (Indi s : this.network.getSpouses(indi))
            {
                stack.push(s);
                dstack.push(1);
                dstack.push(d);
            }

            for (Vertex f : this.network.getPredecessors(indi))
            {
                for (Vertex w : this.network.getPredecessors(f))
                {
                    stack.push((Indi) w);
                    dstack.push(0);
                    dstack.push(d + 2);
                }
            }
        }
    }

    /**
     * Sets the depth succ.
     * 
     * @param vertex
     *            the new depth succ
     */
    private void setDepthSucc(final Vertex vertex)
    {
        Stack<Indi> stack = new Stack<Indi>();
        Stack<Integer> dstack = new Stack<Integer>();
        if (vertex instanceof Fam)
        {
            Fam fam = (Fam) vertex;
            for (Vertex w : this.network.getSuccessors(fam))
            {
                stack.push((Indi) w);
                dstack.push(0);
                dstack.push(2);
            }
        }
        else
        {
            stack.push((Indi) vertex);
            dstack.push(0);
            dstack.push(0);
        }
        while (!stack.isEmpty())
        {
            Indi indi = stack.pop();
            int d = dstack.pop().intValue();
            int i = dstack.pop().intValue();
            if (getLevelSucc(indi) > d - i)
            {
                continue;
            }
            setLevelSucc(indi, d);
            for (Indi s : this.network.getSpouses(indi))
            {
                stack.push(s);
                dstack.push(d);
                dstack.push(1);
            }

            for (Vertex f : this.network.getSuccessors(indi))
            {
                for (Vertex w : this.network.getSuccessors(f))
                {
                    stack.push((Indi) w);
                    dstack.push(0);
                    dstack.push(d + 2);
                }
            }
        }
    }

    /**
     * Sets the level pred.
     * 
     * @param vertex
     *            the vertex
     * @param d
     *            the d
     */
    private void setLevelPred(final Vertex vertex, final int d)
    {
        // logger.debug("setLevelPred("{},{})", vertex, d);
        this.levelPred.put(vertex, Integer.valueOf(d));
    }

    /**
     * Sets the level succ.
     * 
     * @param vertex
     *            the vertex
     * @param d
     *            the d
     */
    private void setLevelSucc(final Vertex vertex, final int d)
    {
        // logger.debug("setLevelPred("{},{})", vertex, d);
        this.levelSucc.put(vertex, Integer.valueOf(d));
    }
}
