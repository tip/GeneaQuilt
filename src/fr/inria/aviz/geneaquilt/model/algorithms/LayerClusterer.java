/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.algorithms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.map.HashedMap;

import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.Tree;

/**
 * The Class LayerClusterer.
 * 
 * @param <V>
 *            vertex class
 * @param <E>
 *            edge class
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class LayerClusterer<V, E> implements Transformer<DirectedGraph<V, E>, List<Set<V>>>
{
    private Map<V, Integer> rank;
    private int maxRank;
    private DirectedGraph<V, E> graph;

    /**
     * Assign rank.
     * 
     * @param v
     *            the v
     * @param vRank
     *            the v rank
     */
    private void assignRank(final V v, final int vRank)
    {
        for (V child : this.graph.getPredecessors(v))
        {
            Integer o = this.rank.get(child);
            int newRank;
            if (o != null)
            {
                int oldRank = o.intValue();
                newRank = Math.max(oldRank, vRank + 1);
            }
            else
            {
                newRank = vRank + 1;
            }

            this.rank.put(child, Integer.valueOf(newRank));
            if (newRank > this.maxRank)
            {
                this.maxRank = newRank;
            }
            assignRank(child, newRank);
        }
    }

    /**
     * Feasible tree.
     * 
     * @param comp
     *            the comp
     * @return the tree
     */
    protected Tree<V, E> feasibleTree(final Collection<V> comp)
    {
        return null;
    }

    /**
     * Inits the rank.
     * 
     * @param comp
     *            the comp
     */
    protected void init_rank(final Collection<V> comp)
    {
        for (V v : comp)
        {
            if (this.graph.getSuccessorCount(v) == 0)
            {
                setRoot(v);
            }
        }
    }

    /**
     * Assign ranks to a connected component.
     * 
     * @param graph
     *            the graph
     * @param comp
     *            the component
     * @return an ordered list of vertices with the same rank
     */
    public List<Set<V>> rank(final DirectedGraph<V, E> graph, final Collection<V> comp)
    {
        this.graph = graph;
        this.rank = new HashedMap<V, Integer>();
        this.maxRank = 0;
        // Tree<V, E> tree = feasibleTree(comp);
        // TODO
        return null;
    }

    /**
     * Sets the root.
     * 
     * @param v
     *            the new root
     */
    private void setRoot(final V v)
    {
        this.rank.put(v, Integer.valueOf(0));
        assignRank(v, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Set<V>> transform(final DirectedGraph<V, E> graph)
    {
        List<Set<V>> result;

        WeakComponentClusterer<V, E> cluster = new WeakComponentClusterer<V, E>();
        Set<Set<V>> components = cluster.transform(graph);
        result = new ArrayList<Set<V>>();
        for (Set<V> comp : components)
        {
            List<Set<V>> r = rank(graph, comp);
            for (int index = 0; index < r.size(); index++)
            {
                Set<V> set = r.get(index);
                if (result.size() <= index)
                {
                    result.add(set);
                }
                else
                {
                    result.get(index).addAll(set);
                }
            }
        }

        //
        return result;
    }
}
