/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.model.algorithms;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * 
 * The Class LayerRank<Vertex,Edge>.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class LayerRank extends AbstractAlgorithm
{
    /** Name of the algorithm */
    public static final String LAYER = "layer";

    private Map<Vertex, Integer> rank = new HashMap<Vertex, Integer>();
    private Set<Vertex> unprocessed = new HashSet<Vertex>();
    private int maxRank = -1;

    // private Set<Vertex> movedup = new HashSet<Vertex>();

    /**
     * Constructor which initializes the algorithm.
     * 
     * @param g
     *            the graph whose nodes are to be analyzed
     */
    public LayerRank(final Network g)
    {
        super(g);
    }

    /**
     * Assign rank.
     * 
     * @param v
     *            the v
     * @param level
     *            the level
     * @return the int
     */
    private int assignRank(final Vertex v, final int level)
    {
        int result;

        result = level;
        for (Vertex child : this.network.getPredecessors(v))
        {
            Integer o = this.rank.get(child);
            int newRank;
            if (o != null)
            {
                int oldRank = o.intValue();
                newRank = Math.max(oldRank, level + 1);
            }
            else
            {
                newRank = level + 1;
            }
            this.rank.put(child, Integer.valueOf(newRank));
            this.unprocessed.remove(child);
            if (newRank > result)
            {
                result = newRank;
            }
            assignRank(child, newRank);
        }

        if (result > this.maxRank)
        {
            this.maxRank = result;
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void compute()
    {
        computeLayers();
    }

    /**
     * Compute layers.
     */
    private void computeLayers()
    {
        Collection<Vertex> vertices = this.network.getVertices();
        this.rank.clear();
        this.unprocessed.clear();
        this.unprocessed.addAll(vertices);
        int maxLayers = 0;
        // Vertex longestRoot = null;

        for (Vertex vertex : vertices)
        {
            if (this.network.getSuccessorCount(vertex) == 0)
            {
                int layers = setRoot(vertex);
                if (layers > maxLayers)
                {
                    maxLayers = layers;
                    // longestRoot = v;
                }
            }
        }

        if (!this.unprocessed.isEmpty())
        {
            // loops
            System.err.println("Loops found in graph");
            Vertex best = null;
            int lowest = this.maxRank + 1;
            for (Vertex vertex : this.unprocessed)
            {
                // Look for the lowest ranked neighbor to cut
                for (Vertex other : this.network.getPredecessors(vertex))
                {
                    Integer i = this.rank.get(other);
                    if (i != null && i.intValue() < lowest)
                    {
                        best = other;
                        lowest = i.intValue();
                    }
                }
                // cut at V
                if (best != null)
                {
                    this.rank.put(vertex, Integer.valueOf(lowest - 1));
                }
                else
                {
                    this.rank.put(vertex, Integer.valueOf(0));
                }
            }
        }

        for (Vertex vertex : vertices)
        {
            if (this.network.getPredecessorCount(vertex) == 0)
            {
                setTop(vertex);
            }
        }
        // bumpUp(vertices);
    }

    /**
     * Gets the graph.
     * 
     * @return the graph
     */
    public Network getGraph()
    {
        return this.network;
    }

    // private int maxPred(Vertex v) {
    // int m = -1;
    // for (Vertex child : graph.getPredecessors(v)) {
    // m = Math.max(m, rank.get(child).intValue());
    // }
    // return m;
    // }

    /**
     * Gets the max rank.
     * 
     * @return the maxRank
     */
    public int getMaxRank()
    {
        return this.maxRank;
    }

    // private void bumpUp(Collection<Vertex> vertices) {
    // Vertex[] sorted = new Vertex[vertices.size()];
    // Comparator<Vertex> comparator = new Comparator<Vertex>() {
    // public int compare(Vertex o1, Vertex o2) {
    // return rank.get(o2).intValue()-rank.get(o1).intValue();
    // }
    // };
    //
    // vertices.toArray(sorted);
    // //TreeSet<Vertex> heap = new TreeSet<Vertex>(
    // //MapBinaryHeap<Vertex> heap = new MapBinaryHeap<Vertex>(
    // Arrays.sort(sorted, comparator);
    // for (int n = 0; n < sorted.length; n++) {
    // Vertex child = sorted[n];
    // int oldRank = rank.get(child).intValue();
    // int newRank = minPred(child)-1;
    // if (newRank > 0 && oldRank != newRank) {
    // if (newRank < oldRank)
    // assert(false);
    // System.out.println("Moved "+child+" from "+oldRank+" to "+newRank);
    // rank.put(child, Integer.valueOf(newRank));
    // int index = Arrays.binarySearch(sorted, child, comparator);
    // if (index < 0) {
    // index = -index-1;
    // }
    // System.arraycopy(sorted, index, sorted, index+1, n-index);
    // sorted[index] = child;
    // n = index;
    // }
    // }
    // }

    /**
     * Returns the rank of the specified vertex.
     * 
     * @param vertex
     *            the vertex
     * @return the rank or -1
     */
    public int getRank(final Vertex vertex)
    {
        int result;

        Integer value = this.rank.get(vertex);
        if (value == null)
        {
            result = -1;
        }
        else
        {
            result = value.intValue();
        }

        //
        return result;
    }

    /**
     * Min pred.
     * 
     * @param vertex
     *            the vertex
     * @return the int
     */
    private int minPred(final Vertex vertex)
    {
        int result;

        result = Integer.MAX_VALUE;

        for (Vertex child : this.network.getPredecessors(vertex))
        {
            result = Math.min(result, this.rank.get(child).intValue());
        }

        if (result == Integer.MAX_VALUE)
        {
            result = -1;
        }

        //
        return result;
    }

    /**
     * Sets the root.
     * 
     * @param vertex
     *            the vertex
     * @return the int
     */
    private int setRoot(final Vertex vertex)
    {
        int result;

        int rankValue;
        if (vertex instanceof Indi)
        {
            rankValue = 0;
        }
        else
        {
            rankValue = 1;
        }

        this.rank.put(vertex, Integer.valueOf(rankValue));
        this.unprocessed.remove(vertex);
        System.out.println("Root: " + vertex);

        result = assignRank(vertex, rankValue);

        //
        return result;
    }

    /**
     * Sets the top.
     * 
     * @param vertex
     *            the new top
     */
    private void setTop(final Vertex vertex)
    {
        for (Vertex parent : this.network.getSuccessors(vertex))
        {
            int oldRank = this.rank.get(parent).intValue();
            int newRank = minPred(parent) - 1;
            if (newRank > oldRank)
            {
                this.rank.put(parent, Integer.valueOf(newRank));
            }
            setTop(parent);
        }
    }
}
