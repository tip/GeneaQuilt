/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.gui.GeneaQuiltGUI;
import fr.inria.aviz.geneaquilt.model.GeneaQuiltException;
import fr.inria.aviz.geneaquilt.model.util.GeneaQuiltUtils;

/**
 * The Class GeneaQuilt.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeneaQuilt
{
    private static Logger logger = LoggerFactory.getLogger(GeneaQuilt.class);

    /**
     * The main method.
     * 
     * @param args
     *            the arguments
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws GeneaQuiltException
     *             the genea quilt exception
     */
    public static void main(final String[] args) throws IOException, GeneaQuiltException
    {
        //
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            /* (non-Javadoc)
             * @see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
             */
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\nTo fix this problem, run GeneaQuilts from the command line:\njava -jar -Xms256m geneaquilt-x.x.x.jar\n\nIf you still get the same error, increase the value 256 above.";
                }
                else
                {
                    message = "An error occured: " + exception.getClass() + "(" + exception.getMessage() + ")";
                }

                System.err.println(message);
                exception.printStackTrace();
            }
        });

        // Configure log.
        File loggerConfig = new File("log4j.properties");
        if (loggerConfig.exists())
        {
            PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
            logger.info("Dedicated log configuration done.");
            logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
        }
        else
        {
            BasicConfigurator.configure();
            logger.info("Basic log configuration done.");
            logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
        }

        // Manage parameters.
        if ((args.length == 1) && (GeneaQuiltUtils.equalsAny(args[0], "-h", "-help", "--help")))
        {
            System.out.println("geneaquilt [ -h | -help | --help | fileToLoad]");
        }
        else
        {
            // Display information about extra unused parameters.
            if (args.length > 1)
            {
                logger.warn("WARNING: Only one genealogy file can be loaded at a time. Loading the first one…");
            }

            //
            File file;
            if (ArrayUtils.isEmpty(args))
            {
                file = null;
            }
            else
            {
                file = new File(args[0]);

            }

            if ((file == null) || (file.exists()))
            {
                GeneaQuiltGUI.instance().run();
                GeneaQuiltGUI.instance().setFile(file);
            }
            else
            {
                logger.error("The file {} does not exist. Quitting the application.", file.getAbsoluteFile());
                System.exit(0);
            }
        }
    }
}
