/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.net.Net;

import edu.umd.cs.piccolo.util.PDebug;
import fr.inria.aviz.geneaquilt.gui.help.AboutDialog;
import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.quiltview.GeneaQuiltPanel;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.VertexComparator.Sorting;
import fr.inria.aviz.geneaquilt.model.io.DOTLayersReader;
import fr.inria.aviz.geneaquilt.model.io.DOTWriter;
import fr.inria.aviz.geneaquilt.model.io.GEDReader;
import fr.inria.aviz.geneaquilt.model.io.JSONWriter;
import fr.inria.aviz.geneaquilt.model.io.LayerWriter;
import fr.inria.aviz.geneaquilt.model.io.PEDReader;
import fr.inria.aviz.geneaquilt.model.io.TIPReader;
import fr.inria.aviz.geneaquilt.model.util.GeneaQuiltConvert;

/**
 * The Class GeneaQuiltWindow.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeneaQuiltWindow extends JFrame
{
    private static final long serialVersionUID = -6461791243403233382L;

    private final Logger logger = LoggerFactory.getLogger(GeneaQuiltWindow.class);

    private static final String DEFAULT_TITLE = "Genealogy Quilt";

    private File currentFile;

    private JPanel contentPane;
    private GeneaQuiltPanel geneaQuiltPanel;

    /**
     * Create the frame.
     */
    public GeneaQuiltWindow()
    {
        super(DEFAULT_TITLE);

        setIconImage(Toolkit.getDefaultToolkit().getImage(GeneaQuiltWindow.class.getResource("/fr/inria/aviz/geneaquilt/gui/favicon-32x32.png")));
        setTitle("GeneaQuilt 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 593, 482);

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnFile = new JMenu("File");
        mnFile.setMnemonic('F');
        menuBar.add(mnFile);

        JSeparator separator_3 = new JSeparator();
        mnFile.add(separator_3);

        JSeparator separator_2 = new JSeparator();
        mnFile.add(separator_2);

        JMenuItem mntmOpen = new JMenuItem("Open…");
        mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        mntmOpen.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Open…
                try
                {
                    // GeneaQuiltWindow.this.currentFile
                    File startDirectory;
                    if ((GeneaQuiltWindow.this.currentFile == null) && (new File("data/").exists()))
                    {
                        startDirectory = new File("data/");
                    }
                    else
                    {
                        startDirectory = GeneaQuiltWindow.this.currentFile;
                    }

                    File selectedFile = OpenFileSelector.showSelectorDialog(GeneaQuiltWindow.this, startDirectory);

                    if (selectedFile != null)
                    {
                        GUIUtils.beginLongComputation(GeneaQuiltWindow.this, "Loading file…");
                        GeneaQuiltWindow.this.currentFile = selectedFile;
                        Network loadedNetwork;
                        loadedNetwork = load(selectedFile);
                        GUIUtils.endLongComputation(GeneaQuiltWindow.this);

                        if (loadedNetwork == null)
                        {
                            GeneaQuiltWindow.this.logger.error("Couldn't read the network");
                            JOptionPane.showMessageDialog(GeneaQuiltWindow.this, "Could not read the file. Make sure its format is correct.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        else
                        {
                            GeneaQuiltWindow.this.remove(GeneaQuiltWindow.this.geneaQuiltPanel);

                            GeneaQuiltWindow.this.setTitle(DEFAULT_TITLE + ": " + selectedFile);

                            GeneaQuiltWindow.this.geneaQuiltPanel = new GeneaQuiltPanel(GeneaQuiltWindow.this.contentPane, loadedNetwork);
                            GeneaQuiltWindow.this.getContentPane().add(GeneaQuiltWindow.this.geneaQuiltPanel, BorderLayout.CENTER);

                            GeneaQuiltWindow.this.validate();
                        }
                    }
                }
                catch (Exception exception)
                {
                    //
                    exception.printStackTrace();

                    //
                    String title = "Error computerum est";
                    String message = "Error occured during working: " + exception.getMessage();

                    //
                    JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        mnFile.add(mntmOpen);

        JMenuItem mntmClose = new JMenuItem("Close");
        mntmClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
        mntmClose.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Close.
                remove(GeneaQuiltWindow.this.geneaQuiltPanel);
                getContentPane().add(new GeneaQuiltPanel(GeneaQuiltWindow.this.contentPane, null), BorderLayout.CENTER);
                GeneaQuiltWindow.this.currentFile = null;
                setTitle(DEFAULT_TITLE);
                validate();
            }
        });

        JMenuItem mntmOpenByPuck_1 = new JMenuItem("Open by Puck…");
        mntmOpenByPuck_1.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Open by Puck.
                try
                {
                    // GeneaQuiltWindow.this.currentFile
                    File startDirectory;
                    if ((GeneaQuiltWindow.this.currentFile == null) && (new File("data/").exists()))
                    {
                        startDirectory = new File("data/");
                    }
                    else
                    {
                        startDirectory = GeneaQuiltWindow.this.currentFile;
                    }

                    File selectedFile = OpenPuckFileSelector.showSelectorDialog(GeneaQuiltWindow.this, startDirectory);

                    if (selectedFile != null)
                    {
                        GUIUtils.beginLongComputation(GeneaQuiltWindow.this, "Loading file by Puck…");
                        GeneaQuiltWindow.this.currentFile = selectedFile;
                        Net net = PuckManager.loadNet(selectedFile);
                        Network loadedNetwork = GeneaQuiltConvert.convertToGeneaQuilt(net);
                        GUIUtils.endLongComputation(GeneaQuiltWindow.this);

                        if (loadedNetwork == null)
                        {
                            GeneaQuiltWindow.this.logger.error("Couldn't read the network");
                            JOptionPane.showMessageDialog(GeneaQuiltWindow.this, "Could not read the file. Make sure its format is correct.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        else
                        {
                            loadedNetwork.setName(FilenameUtils.getBaseName(selectedFile.getName()));

                            GeneaQuiltWindow.this.remove(GeneaQuiltWindow.this.geneaQuiltPanel);

                            GeneaQuiltWindow.this.setTitle(DEFAULT_TITLE + ": " + selectedFile);

                            GeneaQuiltWindow.this.geneaQuiltPanel = new GeneaQuiltPanel(GeneaQuiltWindow.this.contentPane, loadedNetwork);
                            GeneaQuiltWindow.this.getContentPane().add(GeneaQuiltWindow.this.geneaQuiltPanel, BorderLayout.CENTER);

                            GeneaQuiltWindow.this.validate();
                        }
                    }

                }
                catch (Exception exception)
                {
                    //
                    exception.printStackTrace();

                    //
                    String title = "Error computerum est";
                    String message = "Error occured during working: " + exception.getMessage();

                    //
                    JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        mnFile.add(mntmOpenByPuck_1);

        JMenuItem mntmOpenByPuck = new JMenuItem("Open by Puck as Event…");
        mntmOpenByPuck.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Open by Puck as Event
                try
                {
                    // GeneaQuiltWindow.this.currentFile
                    File startDirectory;
                    if ((GeneaQuiltWindow.this.currentFile == null) && (new File("data/").exists()))
                    {
                        startDirectory = new File("data/");
                    }
                    else
                    {
                        startDirectory = GeneaQuiltWindow.this.currentFile;
                    }

                    File selectedFile = OpenPuckFileSelector.showSelectorDialog(GeneaQuiltWindow.this, startDirectory);

                    if (selectedFile != null)
                    {
                        GUIUtils.beginLongComputation(GeneaQuiltWindow.this, "Loading file by Puck…");
                        GeneaQuiltWindow.this.currentFile = selectedFile;

                        Net net = PuckManager.loadNet(selectedFile);
                        Network loadedNetwork = GeneaQuiltConvert.convertToEventQuilt(net);

                        GUIUtils.endLongComputation(GeneaQuiltWindow.this);

                        if (loadedNetwork == null)
                        {
                            GeneaQuiltWindow.this.logger.error("Couldn't read the network");
                            JOptionPane.showMessageDialog(GeneaQuiltWindow.this, "Could not read the file. Make sure its format is correct.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        else
                        {
                            loadedNetwork.setName(FilenameUtils.getBaseName(selectedFile.getName()));
                            GeneaQuiltWindow.this.remove(GeneaQuiltWindow.this.geneaQuiltPanel);

                            GeneaQuiltWindow.this.setTitle(DEFAULT_TITLE + ": " + selectedFile);

                            GeneaQuiltWindow.this.geneaQuiltPanel = new GeneaQuiltPanel(GeneaQuiltWindow.this.contentPane, loadedNetwork, Sorting.DATERANGE_START);
                            GeneaQuiltWindow.this.getContentPane().add(GeneaQuiltWindow.this.geneaQuiltPanel, BorderLayout.CENTER);

                            GeneaQuiltWindow.this.validate();
                        }
                    }

                }
                catch (Exception exception)
                {
                    // PanelPanelPanel
                    exception.printStackTrace();

                    //
                    String title = "Error computerum est";
                    String message = "Error occured during working: " + exception.getMessage();

                    //
                    JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        mnFile.add(mntmOpenByPuck);

        JSeparator separator_4 = new JSeparator();
        mnFile.add(separator_4);

        JMenuItem mntmExportToJson = new JMenuItem("Export to JSON…");
        mntmExportToJson.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Menu Export to JSON.
                if (GeneaQuiltWindow.this.currentFile != null)
                {
                    saveAsJSON(GeneaQuiltWindow.this.geneaQuiltPanel.getNetwork());
                }
            }
        });
        mnFile.add(mntmExportToJson);

        JMenuItem mntmExportToDot = new JMenuItem("Export to DOT…");
        mntmExportToDot.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Menu Export to DOT.
                if (GeneaQuiltWindow.this.currentFile != null)
                {
                    saveAsDOT(GeneaQuiltWindow.this.geneaQuiltPanel.getNetwork());
                }
            }
        });
        mnFile.add(mntmExportToDot);

        JMenuItem mntmExportSelectionTodot = new JMenuItem("Export selection to DOT…");
        mntmExportSelectionTodot.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Menu Export selection to DOT.
                if (GeneaQuiltWindow.this.currentFile != null)
                {
                    saveAsDOT(GeneaQuiltWindow.this.geneaQuiltPanel.getQuiltManager().getSelectionManager().getSelectedNetwork());
                }
            }
        });
        mnFile.add(mntmExportSelectionTodot);

        JSeparator separator_5 = new JSeparator();
        mnFile.add(separator_5);
        mnFile.add(mntmClose);

        JSeparator separator = new JSeparator();
        mnFile.add(separator);

        JMenuItem mntmQuit = new JMenuItem("Quit");
        mntmQuit.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Quit.
                GeneaQuiltGUI.instance().exit();
            }
        });
        mntmQuit.setMnemonic('Q');
        mnFile.add(mntmQuit);

        JMenu mnOperation = new JMenu("Operation");
        menuBar.add(mnOperation);

        JMenuItem mntmStatistics = new JMenuItem("Statistics");
        mntmStatistics.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // General.
                JOptionPane.showMessageDialog(GeneaQuiltWindow.this, GeneaQuiltWindow.this.geneaQuiltPanel.getStats(), "Stats", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        mnOperation.add(mntmStatistics);

        JMenu mnSettings = new JMenu("Settings");
        menuBar.add(mnSettings);

        JCheckBoxMenuItem chckbxmntmFrameRate = new JCheckBoxMenuItem("Frame Rate");
        chckbxmntmFrameRate.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                //
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    PDebug.debugPrintFrameRate = true;
                }
                else
                {
                    PDebug.debugPrintFrameRate = false;
                }
            }
        });

        JCheckBoxMenuItem chckbxmntmKeepDotFile = new JCheckBoxMenuItem("Keep DOT File");
        chckbxmntmKeepDotFile.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                //
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    DOTLayersReader.setDebug(true);
                }
                else
                {
                    DOTLayersReader.setDebug(false);
                }
            }
        });
        mnSettings.add(chckbxmntmKeepDotFile);
        mnSettings.add(chckbxmntmFrameRate);

        JCheckBoxMenuItem chckbxmntmThreadBugs = new JCheckBoxMenuItem("Thread bugs");
        chckbxmntmThreadBugs.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                //
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    PDebug.debugThreads = true;
                }
                else
                {
                    PDebug.debugThreads = false;
                }
            }
        });
        mnSettings.add(chckbxmntmThreadBugs);

        JCheckBoxMenuItem chckbxmntmTextOutlines = new JCheckBoxMenuItem("Text Outlines");
        chckbxmntmTextOutlines.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                //
                GeneaQuiltWindow.this.geneaQuiltPanel.getCanvas().repaint();
            }
        });
        mnSettings.add(chckbxmntmTextOutlines);

        JMenu mnHelp = new JMenu("Help");
        mnHelp.setMnemonic('H');
        menuBar.add(mnHelp);

        JMenuItem mntmBrowseGeneaquiltWebsite = new JMenuItem("Browse GeneaQuilt website");
        mntmBrowseGeneaquiltWebsite.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Browse GeneaQuilt website.
                String HELP_URL = "http://www.aviz.fr/geneaquilts/";
                try
                {
                    if ((Desktop.isDesktopSupported()) && (Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)))
                    {
                        java.net.URI uri = new java.net.URI(HELP_URL);
                        Desktop.getDesktop().browse(uri);
                    }
                    else
                    {
                        JMenuItem source = (JMenuItem) event.getSource();
                        if (source != null)
                        {
                            source.setEnabled(false);
                        }
                    }
                }
                catch (java.io.IOException exception)
                {
                    exception.printStackTrace();
                    System.err.println("The system cannot find the URL specified: [" + HELP_URL + "]");
                    JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
                }
                catch (java.net.URISyntaxException exception)
                {
                    exception.printStackTrace();
                    System.out.println("Illegal character in path [" + HELP_URL + "]");
                    JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        mnHelp.add(mntmBrowseGeneaquiltWebsite);

        JSeparator separator_1 = new JSeparator();
        mnHelp.add(separator_1);

        JMenuItem mntmAbout = new JMenuItem("About GeneaQuilt 2");
        mntmAbout.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // About Dialog.
                try
                {
                    AboutDialog dialog = new AboutDialog();
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();

                    //
                    String title = "Error computerum est";
                    String message = "Error occured during working: " + exception.getMessage();

                    //
                    JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        mnHelp.add(mntmAbout);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(this.contentPane);

        this.geneaQuiltPanel = new GeneaQuiltPanel(this.contentPane, (Network) null);
        this.contentPane.add(this.geneaQuiltPanel, BorderLayout.CENTER);

        // this.quiltPane = new GeneaQuiltOldPanel(parent, network);
        // this.contentPane.add(quiltPane, BorderLayout.CENTER);

        // this.generalTab = new FooPanel();
        // this.tabbedPane.addTab("General", null, this.generalTab, null);

        // ////////////////////////////:::
        resizeAndCenterWindow(this);
    }

    /**
     * Save as DOT.
     * 
     * @param network
     *            the network
     */
    void saveAsDOT(final Network network)
    {
        if (this.currentFile != null)
        {
            JFileChooser jexportfile = new JFileChooser(this.currentFile.getParentFile());

            jexportfile.addChoosableFileFilter(new FileFilter()
            {
                /**
                 * 
                 */
                @Override
                public boolean accept(final File file)
                {
                    boolean result;

                    if (file.isDirectory())
                    {
                        result = true;
                    }
                    else
                    {
                        result = file.getName().endsWith(".dot");
                    }

                    //
                    return result;
                }

                /**
                 * 
                 */
                @Override
                public String getDescription()
                {
                    return "Choose a DOT filename";
                }
            });

            String defaultFilename = this.currentFile.getAbsolutePath() + File.separator + this.currentFile.getName().substring(0, this.currentFile.getName().length() - 4) + ".dot";
            jexportfile.setSelectedFile(new File(defaultFilename));
            int ret = jexportfile.showSaveDialog(this);
            if (ret == JFileChooser.APPROVE_OPTION)
            {
                String targetFilename = jexportfile.getSelectedFile().getAbsolutePath();
                DOTWriter writer = new DOTWriter(network);
                // writer.setBare(true);
                try
                {
                    writer.write(targetFilename);
                }
                catch (Exception exception)
                {
                    JOptionPane.showMessageDialog(this, exception.getMessage());
                }
            }
        }
    }

    /**
     * Save as JSON.
     * 
     * @param network
     *            the network
     */
    private void saveAsJSON(final Network network)
    {
        if (network != null)
        {
            JFileChooser jexportfile = new JFileChooser(this.currentFile.getParentFile());
            jexportfile.addChoosableFileFilter(new FileFilter()
            {
                /**
                 * 
                 */
                @Override
                public boolean accept(final File file)
                {
                    boolean result;

                    if (file.isDirectory())
                    {
                        result = true;
                    }
                    else
                    {
                        result = file.getName().endsWith(".json");
                    }

                    //
                    return result;
                }

                /**
                 * 
                 */
                @Override
                public String getDescription()
                {
                    return "Choose a JSON filename";
                }
            });
            String defaultFilename = this.currentFile.getAbsolutePath() + File.separator + this.currentFile.getName().substring(0, this.currentFile.getName().length() - 4) + ".json";
            jexportfile.setSelectedFile(new File(defaultFilename));
            int ret = jexportfile.showSaveDialog(GeneaQuiltWindow.this);
            if (ret == JFileChooser.APPROVE_OPTION)
            {
                String targetFilename = jexportfile.getSelectedFile().getAbsolutePath();
                JSONWriter writer = new JSONWriter(network);
                // writer.setBare(true);
                try
                {
                    writer.write(targetFilename);
                }
                catch (Exception exception)
                {
                    JOptionPane.showMessageDialog(GeneaQuiltWindow.this, exception.getMessage());
                }
            }
        }
    }

    /**
     * Save layer.
     */
    private void saveLayer()
    {
        try
        {
            if (this.currentFile != null)
            {
                int last = this.currentFile.getAbsolutePath().lastIndexOf('.');
                String lyerfile;
                if (last == -1)
                {
                    lyerfile = this.currentFile.getAbsolutePath() + ".lyr";
                }
                else
                {
                    lyerfile = this.currentFile.getAbsolutePath().substring(0, last) + ".lyr";
                }
                File file = new File(lyerfile);
                if ((!file.exists())
                        || (JOptionPane.showConfirmDialog(this, "File " + this.currentFile.getAbsolutePath() + " already exists.", "Replace", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION))
                {
                    LayerWriter writer = new LayerWriter(this.geneaQuiltPanel.getNetwork());
                    writer.write(lyerfile);
                }
            }
        }
        catch (IOException exception)
        {
            this.logger.error("Cannot write layer file", exception);
        }
    }

    /**
     * Load.
     * 
     * @param source
     *            the source
     * @return the network
     * @throws PuckException
     *             the puck exception
     */
    private static Network load(final File source) throws PuckException
    {
        Network result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            if (StringUtils.endsWithIgnoreCase(source.getAbsolutePath(), ".tip"))
            {
                result = TIPReader.load(source.getAbsolutePath());
            }
            else if (StringUtils.endsWithIgnoreCase(source.getAbsolutePath(), ".ped"))
            {
                PEDReader reader = new PEDReader();
                result = reader.load(source.getAbsolutePath());
            }
            else
            {
                result = GEDReader.load(source.getAbsolutePath());
            }

            if (result != null)
            {
                result.setName(FilenameUtils.getBaseName(source.getName()));
            }
        }

        //
        return result;
    }

    /**
     * Resize and center window.
     * 
     * @param window
     *            the window
     */
    private static void resizeAndCenterWindow(final JFrame window)
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        int width = (int) (screenSize.width * GraphicsConstants.MAIN_WINDOW_SIZE);
        int height = (int) (screenSize.height * GraphicsConstants.MAIN_WINDOW_SIZE);

        if (width > GraphicsConstants.MAIN_WINDOW_MAX_WIDTH)
        {
            width = GraphicsConstants.MAIN_WINDOW_MAX_WIDTH;
        }

        if (height > GraphicsConstants.MAIN_WINDOW_MAX_HEIGHT)
        {
            height = GraphicsConstants.MAIN_WINDOW_MAX_HEIGHT;
        }

        window.setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2, width, height);
    }
}
