/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.geom.Rectangle2D;

import edu.umd.cs.piccolo.nodes.PText;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.Printer;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;

/**
 * <b>PSemanticText</b> draws text as a rectangle when it is below readable
 * size. It also makes the rectangle big enough to be seen when its Paint
 * attribute is not null.
 * 
 * @author Pierre Dragicevic
 */
public class PSemanticText extends PText
{
    private static final long serialVersionUID = -7511987184819017654L;
    private Rectangle2D.Float smallTextBounds = new Rectangle2D.Float();
    private static final float EFFECTIVE_Y0 = 0f;// 0.4f;
    private static final float EFFECTIVE_H = 1f;// 0.5f;

    private static float minimumScreenWidth = 3;
    private static float minimumScreenHeight = 3;
    protected double greekThreshold2;

    private static Color backgroundColor = new Color(1, 1, 1, 0.1f);

    private static Color outlineColor = new Color(1, 1, 1, 1f);

    // static PAffineTransform outlineShifts[][] = new PAffineTransform[3][3];
    // static {
    // for (int x=0; x<3; x++)
    // for (int y=0; y<3; y++) {
    // outlineShifts[x][y] = new
    // PAffineTransform(AffineTransform.getTranslateInstance((x-1)/1.0,
    // (y-1)/1.0));
    // }
    // }

    /**
     * Creates a ptext with tunable effective height
     * 
     * @param label
     */
    public PSemanticText(final String label)
    {
        super(label);
        this.greekThreshold2 = super.getGreekThreshold();
        setGreekThreshold(0);
    }

    /**
	 * 
	 */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        float scale = (float) PrintConstants.instance.getScale(paintContext);
        float screenFontSize = getFont().getSize() * scale;
        if (getTextPaint() != null && (Printer.isPrinting() || screenFontSize > this.greekThreshold2))
        {
            super.paint(paintContext);
        }
        else
        {
            Graphics2D graphic = paintContext.getGraphics();
            if (getPaint() == null)
            {
                graphic.setColor(GraphicsConstants.SMALL_TEXT_COLOR);
                graphic.fill(this.smallTextBounds);
            }
            else
            {
                graphic.setPaint(getPaint());
                float height = (float) (this.smallTextBounds.getHeight() * scale);
                float width = (float) (this.smallTextBounds.getWidth() * scale);
                if (height >= minimumScreenHeight && width > minimumScreenWidth)
                {
                    graphic.fill(this.smallTextBounds);
                }
                else
                {
                    // Grow the rectangle
                    float h2 = Math.max((float) this.smallTextBounds.getHeight(), minimumScreenHeight / scale);
                    float w2 = Math.max((float) this.smallTextBounds.getWidth(), minimumScreenWidth / scale);
                    graphic.fillRect((int) (this.smallTextBounds.getCenterX() - w2 / 2), (int) (this.smallTextBounds.getCenterY() - h2 / 2), (int) (w2 + 0.5f), (int) (h2 + 0.5f));
                }

            }
        }
    }

    /**
     * 
     * @param paintContext
     */
    public void paintWithOutline(final PPaintContext paintContext)
    {

        float scale = (float) PrintConstants.instance.getScale(paintContext);
        float screenFontSize = getFont().getSize() * scale;
        if (getTextPaint() != null && (Printer.isPrinting() || screenFontSize > this.greekThreshold))
        {

            Graphics2D graphic = paintContext.getGraphics();
            if (getPaint() == null)
            {
                graphic.setColor(backgroundColor);
                graphic.fillRect((int) (this.smallTextBounds.getX()), (int) (this.smallTextBounds.getY() + 4), // FIXME
                        (int) (this.smallTextBounds.getWidth()), (int) (this.smallTextBounds.getHeight() - 6)); // FIXME
            }

            Paint oldTextPaint = getTextPaint();
            setTextPaint(outlineColor);

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (x != 0 || y != 0)
                    {
                        // paintContext.pushTransform(outlineShifts[x][y]);
                        graphic.translate(x - 1, y - 1);
                        super.paint(paintContext);
                        graphic.translate(-x + 1, -y + 1);
                        // paintContext.popTransform(outlineShifts[x][y]);
                    }
                }
            }

            super.paint(paintContext);
            setTextPaint(oldTextPaint);
            super.paint(paintContext);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void recomputeLayout()
    {
        super.recomputeLayout();
        Rectangle2D bounds = getBoundsReference();
        if (bounds != null && this.smallTextBounds != null)
        {
            this.smallTextBounds.setFrame(bounds.getX(), bounds.getY() + (int) (EFFECTIVE_Y0 * bounds.getHeight()), bounds.getWidth(), (int) (EFFECTIVE_H * bounds.getHeight()));
        }
    }
}
