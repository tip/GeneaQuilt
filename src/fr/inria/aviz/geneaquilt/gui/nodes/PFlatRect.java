/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
/*
 * Copyright (c) 2008-2010, Piccolo2D project, http://piccolo2d.org Copyright
 * (c) 1998-2008, University of Maryland All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * None of the name of the University of Maryland, the name of the Piccolo2D
 * project, or the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;

/**
 * The Class PFlatRect.
 * 
 * This is a simple node that draws a "3D" rectangle within the bounds of the
 * node. Drawing a 3D rectangle in a zooming environment is a little tricky
 * because if you just use the regular (Java2D) 3D rectangle, the 3D borders get
 * scaled, and that is ugly. This version always draws the 3D border at fixed 2
 * pixel width.
 * 
 * @author Ben Bederson
 */
public class PFlatRect extends PNode
{
    private static final long serialVersionUID = 1L;
    private Color borderColor;
    private transient GeneralPath path = null;
    private transient Stroke stroke = new BasicStroke(1);

    /**
     * Constructs a simple P3DRect with empty bounds and a black stroke.
     */
    public PFlatRect()
    {
    }

    /**
     * Constructs a P3DRect with the bounds provided.
     * 
     * @param x
     *            left of bounds
     * @param y
     *            top of bounds
     * @param width
     *            width of bounds
     * @param height
     *            height of bounds
     */
    public PFlatRect(final double x, final double y, final double width, final double height)
    {
        this();
        setBounds(x, y, width, height);
    }

    /**
     * Constructs a P3DRect with the provided bounds.
     * 
     * @param bounds
     *            bounds to assigned to the P3DRect
     */
    public PFlatRect(final Rectangle2D bounds)
    {
        this(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
    }

    /**
     * Paints this rectangle with shaded edges. Making it appear to stand out of
     * the page as normal 3D buttons do.
     * 
     * @param paintContext
     *            context in which the paiting should occur
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {

        int oldQuality = paintContext.getRenderQuality();
        paintContext.setRenderQuality(PPaintContext.HIGH_QUALITY_RENDERING);

        // lazy init:
        if (this.stroke == null)
        {
            this.stroke = new BasicStroke(0);
        }

        if (this.path == null)
        {
            this.path = new GeneralPath();
        }

        final Graphics2D graphic = paintContext.getGraphics();

        final double x = getX();
        final double y = getY();
        final double width = getWidth() - 1;
        final double height = getHeight() - 1;
        final double scaleX = graphic.getTransform().getScaleX();
        final double scaleY = graphic.getTransform().getScaleY();
        final double dx = (float) (1.0 / scaleX);
        final double dy = (float) (1.0 / scaleY);
        final PBounds bounds = getBounds();

        graphic.setPaint(getPaint());
        graphic.fill(bounds);
        graphic.setStroke(this.stroke);

        this.path.reset();
        this.path.moveTo((float) (x + width), (float) y);
        this.path.lineTo((float) x, (float) y);
        this.path.lineTo((float) x, (float) (y + height));
        graphic.setPaint(this.borderColor);
        graphic.draw(this.path);

        this.path.reset();
        this.path.moveTo((float) (x + width), (float) (y + dy));
        this.path.lineTo((float) (x + dx), (float) (y + dy));
        this.path.lineTo((float) (x + dx), (float) (y + height));
        graphic.setPaint(this.borderColor);
        graphic.draw(this.path);

        this.path.reset();
        this.path.moveTo((float) (x + width), (float) y);
        this.path.lineTo((float) (x + width), (float) (y + height));
        this.path.lineTo((float) x, (float) (y + height));
        graphic.setPaint(this.borderColor);
        graphic.draw(this.path);

        this.path.reset();
        this.path.moveTo((float) (x + width - dx), (float) (y + dy));
        this.path.lineTo((float) (x + width - dx), (float) (y + height - dy));
        this.path.lineTo((float) x, (float) (y + height - dy));
        graphic.setPaint(this.borderColor);
        graphic.draw(this.path);

        paintContext.setRenderQuality(oldQuality);
    }

    /**
     * Changes the paint that will be used to draw this rectangle. This paint is
     * used to shade the edges of the rectangle.
     * 
     * @param newPaint
     *            the color to use for painting this rectangle
     */
    @Override
    public void setPaint(final Paint newPaint)
    {
        super.setPaint(newPaint);

        if (newPaint instanceof Color)
        {
            final Color color = (Color) newPaint;
            this.borderColor = color.darker().darker();
        }
    }
}
