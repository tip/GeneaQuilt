/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.Graphics2D;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.Printer;
import fr.inria.aviz.geneaquilt.gui.quiltview.hull.HullBin;
import fr.inria.aviz.geneaquilt.gui.util.PiccoloUtils;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;
import fr.inria.aviz.geneaquilt.model.Indi;

/**
 * An <b>IndiGeneration</b> is a container that lays out its children (Indi) in
 * a vertical fashion and draws an horizontal grid that spans over all the
 * families.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class IndiGeneration extends PNode implements HullBin
{
    private static final long serialVersionUID = 7440748819481052360L;

    private Logger logger = LoggerFactory.getLogger(IndiGeneration.class);

    private int hullBinIndex;

    /**
     * Creates a Generation from a layer of individuals.
     * 
     * @param layer
     *            the layer
     */
    public IndiGeneration(final List<Indi> layer)
    {
        for (Indi individual : layer)
        {
            addChild(individual.getNode());
        }
        // setPaint(Color.LIGHT_GRAY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fullPaint(final PPaintContext paintContext)
    {

        if (getVisible() && fullIntersects(paintContext.getLocalClip()))
        {
            // -- Paint in normal scale.

            if (Printer.isPrinting() || PrintConstants.instance.getScale(paintContext) > 0.1)
            {
                super.fullPaint(paintContext);
            }
            else
            {
                // -- Paint in small scale

                // paintContext.pushTransform(getTransform());
                paintContext.pushTransparency(getTransparency());

                Graphics2D graphic = paintContext.getGraphics();
                graphic.setColor(GraphicsConstants.INDI_GENERATION_COLOR);
                graphic.fill(getBoundsReference());

                // paint only highlighted labels (string search)
                for (Object childReference : getChildrenReference())
                {
                    PNode node = (PNode) childReference;
                    if (node instanceof PSemanticText && ((PSemanticText) node).getPaint() != null)
                    {
                        node.fullPaint(paintContext);
                    }
                }

                paintContext.popTransparency(getTransparency());
                // paintContext.popTransform(getTransform());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getHullBinIndex()
    {
        return this.hullBinIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void layoutChildren()
    {
        final double lineSpacing = GraphicsConstants.INDI_LINE_SPACING;
        double height = 0;
        double width = 0;
        for (Object childReference : getChildrenReference())
        {
            PNode child = (PNode) childReference;
            // child.setGlobalTranslation(
            // new Point2D.Double(
            // getX(),
            // getY()+h));
            PiccoloUtils.setLocation(child, getX(), getY() + height, true);
            if (child.getVisible())
            {
                PBounds bounds = child.getFullBoundsReference();
                height += bounds.getHeight() * lineSpacing;
                width = Math.max(width, bounds.getWidth());
                if (child instanceof PSemanticText)
                {
                    ((PSemanticText) child).recomputeLayout();
                }
            }
        }

        setBounds(getX(), getY(), width / 2, height / 2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHullBinIndex(final int index)
    {
        this.hullBinIndex = index;
    }
}
