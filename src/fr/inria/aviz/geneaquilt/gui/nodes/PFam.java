/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import edu.umd.cs.piccolo.nodes.PText;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.Printer;
import fr.inria.aviz.geneaquilt.gui.quiltview.hull.HullBin;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class <code>PFam</code> is a Piccolo object representing a Family.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class PFam extends PText implements PVertex, HullBin
{
    private static final long serialVersionUID = -1181680752302748723L;

    private Fam fam;
    private Color bordercolor;
    private int hullBinIndex;
    private static final BasicStroke STROKE = new BasicStroke(1); // FIXME

    /**
     * Creates a PFam from a specified Fam.
     * 
     * @param fam
     *            the Fam
     */
    public PFam(final Fam fam)
    {
        super(fam.getLabel());
        this.fam = fam;
        setTextPaint(GraphicsConstants.FAM_COLOR);
        setFont(GraphicsConstants.FAM_FONT);
        setStrokePaint(GraphicsConstants.instance.famBorderColor());
    }

    /**
     * Gets the fam.
     * 
     * @return the fam
     */
    public Fam getFam()
    {
        return this.fam;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getHullBinIndex()
    {
        return this.hullBinIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Vertex getVertex()
    {
        return this.fam;
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.nodes.PText#paint(edu.umd.cs.piccolo.util.PPaintContext)
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        double save = getGreekThreshold();
        try
        {
            if (Printer.isPrinting())
            {
                setGreekThreshold(0);
            }
            super.paint(paintContext);
            if (this.bordercolor != null)
            {
                Graphics2D graphic = paintContext.getGraphics();
                graphic.setColor(this.bordercolor);
                graphic.setStroke(STROKE);
                graphic.draw(getBoundsReference());
            }
        }
        finally
        {
            if (Printer.isPrinting())
            {
                setGreekThreshold(save);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHullBinIndex(final int index)
    {
        this.hullBinIndex = index;
    }

    /**
     * Sets the stroke paint.
     * 
     * @param bordercolor
     *            color
     */
    public void setStrokePaint(final Color bordercolor)
    {
        this.bordercolor = bordercolor;
    }
}
