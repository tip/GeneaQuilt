/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.geom.Rectangle2D;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;

/**
 * This component redraws Indi labels that need to be redrawn on top of
 * selections, with a white outline so they are more readable.
 * 
 * @author dragicevic
 * 
 */
public class TextOutlineManager extends PNode
{
    private static final long serialVersionUID = 6442905836031588805L;

    private QuiltManager quilt;
    private boolean enabled = false;

    /**
     * Instantiates a new text outline manager.
     * 
     * @param quilt
     *            the quilt
     */
    public TextOutlineManager(final QuiltManager quilt)
    {
        this.quilt = quilt;
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.PNode#getBoundsReference()
     */
    @Override
    public PBounds getBoundsReference()
    {
        return this.quilt.getBoundsReference();
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.PNode#getFullBoundsReference()
     */
    @Override
    public PBounds getFullBoundsReference()
    {
        return this.quilt.getFullBoundsReference();
    }

    /**
     * Checks if is enabled.
     * 
     * @return true, if is enabled
     */
    public boolean isEnabled()
    {
        return this.enabled;
    }

    /**
     * TODO: comment.
     * 
     * @param paintContext
     *            the paint context
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        if (this.enabled)
        {
            if (paintContext.getRenderQuality() == PPaintContext.HIGH_QUALITY_RENDERING && this.quilt.getSelectionManager().getSelections().size() > 0)
            {
                Rectangle2D paintclip = paintContext.getLocalClip();
                PBounds selectionbounds = this.quilt.getSelectionManager().getHighlightManager().getFullBoundsReference();
                if (selectionbounds.intersects(paintclip))
                {
                    paintContext.pushTransform(getTransform());
                    IndiGeneration generation;
                    for (int generationIndex = 0; generationIndex < this.quilt.getIndiGenerations().length; generationIndex++)
                    {
                        generation = this.quilt.getIndiGenerations()[generationIndex];
                        if (generation.getFullBoundsReference().createIntersection(selectionbounds).intersects(paintclip))
                        {
                            PIndi indi;
                            for (Object object : generation.getChildrenReference())
                            {
                                if (object instanceof PIndi)
                                {
                                    indi = (PIndi) object;
                                    // FIXME: Don't draw if there is no
                                    // highlight on
                                    // top of the indi
                                    paintContext.pushTransform(indi.getTransform());
                                    indi.paintWithOutline(paintContext);
                                    paintContext.popTransform(indi.getTransform());
                                }
                            }
                        }

                    }
                    paintContext.popTransform(getTransform());
                }
            }
        }
    }

    /**
     * Sets the enabled.
     * 
     * @param enabled
     *            the new enabled
     */
    public void setEnabled(final boolean enabled)
    {
        this.enabled = enabled;
    }
}
