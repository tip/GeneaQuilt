/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PAffineTransform;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.Printer;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;

/**
 * The Class <b>PIsoShape</b> show a shape centered and scaled to fit the
 * specified bounds. The scale is the same in X and Y.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class PIsoShape extends PNode
{
    private static final long serialVersionUID = -3111060167233054931L;

    private Shape shape;

    /**
     * Circle shape
     */
    public static final Shape CIRCLE = new Ellipse2D.Double(0, 0, 10, 10);

    /**
     * Creates a PIsoShape with the specified shape.
     * 
     * IMPORTANT: The shape bounds must be within (0, 0, 10, 10). This allows to
     * specify margins.
     * 
     * @param shape
     *            the shape
     */
    public PIsoShape(final Shape shape)
    {
        this.shape = shape;
    }

    /**
     * @return the shape
     */
    public Shape getShape()
    {
        return this.shape;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        if (getPaint() != null)
        {
            final Graphics2D graphic = paintContext.getGraphics();
            graphic.setPaint(getPaint());
            paintIsoShape(paintContext, this.shape, getBoundsReference());
        }
    }

    /**
     * 
     * @param shape
     */
    public void setShape(final Shape shape)
    {
        this.shape = shape;
    }

    /**
     * Temporary fix. This allows PathHighlight to render on top of this node
     * using the same shape.
     * 
     * @param paintContext
     *            the paint context
     * @param shape
     *            the shape to paint
     * @param bounds
     *            the bounding box where the shape should be drawn
     */
    public static void paintIsoShape(final PPaintContext paintContext, final Shape shape, final PBounds bounds)
    {
        Rectangle2D sb = shape.getBounds2D();
        double w = Math.min(bounds.getWidth(), bounds.getHeight());
        double minSize = paintContext.getRenderQuality() == PPaintContext.LOW_QUALITY_RENDERING ? 1 : 0.25f;

        // don't draw edges in small scales. This will accelerate the rendering
        // of
        // the overview.
        if (Printer.isPrinting() || PrintConstants.instance.getScale(paintContext) * w > minSize)
        {
            PAffineTransform at = new PAffineTransform();

            double sx = bounds.getWidth() / 10;// sb.getWidth();
            double sy = bounds.getHeight() / 10;// sb.getHeight();
            double s = Math.min(sx, sy);
            // at.scaleAboutPoint(s, b.getCenterX(), b.getCenterY());
            at.translate(bounds.getCenterX(), bounds.getCenterY());
            at.scale(s, s);
            at.translate(-sb.getCenterX(), -sb.getCenterY());

            if (Printer.isPrinting())
            {
                Graphics2D graphic = paintContext.getGraphics();
                graphic = (Graphics2D) graphic.create();
                graphic.transform(at);
                graphic.fill(shape);
                graphic.dispose();
            }
            else
            {
                paintContext.pushTransform(at);
                paintContext.getGraphics().fill(shape);
                paintContext.popTransform(at);
            }
        }
    }
}
