/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.hull.Hull;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.SelectionManager;
import fr.inria.aviz.geneaquilt.gui.util.PiccoloUtils;
import fr.inria.aviz.geneaquilt.model.DateRange;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;
import fr.inria.aviz.geneaquilt.model.VertexComparator;
import fr.inria.aviz.geneaquilt.model.VertexComparator.Sorting;
import fr.inria.aviz.geneaquilt.model.algorithms.GenerationRank;
import fr.inria.aviz.geneaquilt.model.io.DOTWriter;

/**
 * The class Generation.
 * 
 * @author Jean-Daniel Fekete
 * 
 *         TODO Fix the layers, Fam/Indi levels get mixed-up
 */
public class QuiltManager extends PNode
{
    /**
     * The Class BestMatch.
     */
    private static class BestMatch
    {
        /** */
        public int layer1;

        /** */
        public int layer2;

        /** */
        public int delta;
    }

    /**
     * The Class InternalVertexComparator.
     */
    private static class InternalVertexComparator implements Comparator<Vertex>
    {
        private boolean changed = false;

        /* (non-Javadoc)
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(final Vertex alpha, final Vertex bravo)
        {
            int result;

            result = alpha.getComponent() - bravo.getComponent();
            if (result == 0)
            {
                result = (int) Math.signum(alpha.getX() - bravo.getX());
                if (result > 0)
                {
                    this.changed = true;
                }
            }

            //
            return result;
        }
    }

    private static final long serialVersionUID = 5685429618863274697L;

    private Logger logger = LoggerFactory.getLogger(QuiltManager.class);

    private Network network;
    public VertexComparator.Sorting sorting;
    private List<Indi>[] individualGenerations;
    private List<Fam>[] familyGenerations;
    private IndiGeneration[] indiGeneration;
    private FamGeneration[] famGeneration;
    private int layerCount;
    private double xPad = 10;
    private double yPad = 10;
    private PNode grids;
    private SelectionManager selectionManager;

    private boolean familyFirst;

    private Hull hull;

    /** The comparator used to order the vertices in Y. */
    public static final InternalVertexComparator COMPARATOR = new InternalVertexComparator();

    private Line2D lastLine = new Line2D.Double();

    /**
     * Creates a Quilt manager and checks for sanity.
     * 
     * @param source
     *            the network
     */
    public QuiltManager(final Network source)
    {
        this(source, Sorting.NONE);
    }

    /**
     * Creates a Quilt manager and checks for sanity.
     * 
     * @param source
     *            the network
     * @param vertexSorting
     *            the vertex sorting
     */
    public QuiltManager(final Network source, final Sorting vertexSorting)
    {
        this.network = source;
        this.sorting = vertexSorting;

        if (source != null)
        {

            // network.breakCycles();
            assignLayers();
            if (source.isLayerComputed())
            {
                this.logger.info("Layers already assigned");
                if (vertexSorting == Sorting.NONE)
                {
                    updateSortedLayers();
                }
                else
                {
                    sortVertexInGenerations();
                }
            }
            else
            {
                sortLayers();
            }

            addChildren();

            // The following line is optional (only for debug)
            // addChild(hull);
        }
    }

    /**
     * Adds the children.
     */
    protected void addChildren()
    {
        this.grids = new PNode();
        addChild(this.grids);
        createGenerations();

        PNode edges = new PNode();
        for (Edge edge : this.network.getEdges())
        {
            edges.addChild(edge.getNode());
        }
        addChild(edges);

        this.hull = new Hull(this);
        this.hull.createBins();
    }

    /**
     * Adds the grid line.
     * 
     * @param parent
     *            the parent
     * @param line
     *            the line
     */
    protected void addGridLine(final PNode parent, final Line2D line)
    {
        PSemanticPath path = newGridLine(line);
        if (path != null)
        {
            parent.addChild(path);
        }
    }

    /**
     * Assign layers.
     */
    private void assignLayers()
    {
        this.logger.debug("Entering assignLayers");
        if (!this.network.isLayerComputed())
        {
            GenerationRank rank = new GenerationRank(this.network);
            rank.compute();
        }
        // LayerRank layer = new LayerRank(network);
        // layer.computeRanks();
        // for (Vertex v : network.getVertices()) {
        // network.setVertexLayer(v, layer.getRank(v));
        // if (v.getLayer() < 0)
        // LOG.error("Vertex "+v+" has no layer");
        // }
        // }

        this.layerCount = this.network.getMaxLayer() + 1;
        fixLayers();
        // fixLayersByDate();
        this.individualGenerations = new ArrayList[(this.layerCount + 1) / 2];
        this.familyGenerations = new ArrayList[(this.layerCount + 1) / 2];
        for (Vertex vertex : this.network.getVertices())
        {
            int layerIndex = vertex.getLayer() / 2;
            if (layerIndex < 0)
            {
                this.logger.error("Negative layer");
                layerIndex = 0;
            }
            if (vertex instanceof Indi)
            {
                Indi indi = (Indi) vertex;
                if (layerIndex >= this.individualGenerations.length)
                {
                    List<Indi>[] buffer = new ArrayList[layerIndex + 1];
                    System.arraycopy(this.individualGenerations, 0, buffer, 0, this.individualGenerations.length);
                    this.individualGenerations = buffer;
                }
                List<Indi> individuals = this.individualGenerations[layerIndex];

                if (individuals == null)
                {
                    individuals = new ArrayList<Indi>();
                    this.individualGenerations[layerIndex] = individuals;
                    if (vertex.getLayer() == 0)
                    {
                        this.familyFirst = false;
                    }
                }
                individuals.add(indi);
            }
            else if (vertex instanceof Fam)
            {
                Fam fam = (Fam) vertex;

                if (layerIndex >= this.familyGenerations.length)
                {
                    List<Fam>[] buffer = new ArrayList[layerIndex + 1];
                    System.arraycopy(this.familyGenerations, 0, buffer, 0, this.familyGenerations.length);
                    this.familyGenerations = buffer;
                }

                List<Fam> families = this.familyGenerations[layerIndex];

                if (families == null)
                {
                    families = new ArrayList<Fam>();
                    this.familyGenerations[layerIndex] = families;
                    if (vertex.getLayer() == 0)
                    {
                        this.familyFirst = true;
                    }
                }
                families.add(fam);
            }
            else
            {
                this.logger.error("Unexpected type of vertex {}", vertex);
            }
        }

        this.logger.debug("Leaving assignLayers");
    }

    /**
     * Compute best match.
     * 
     * @param layerDate
     *            the layer date
     * @return the best match
     */
    private BestMatch computeBestMatch(final List<SortedMap<Integer, DateRange>> layerDate)
    {
        BestMatch result;

        this.logger.debug("Computing best match for {} layers", layerDate.size());
        result = new BestMatch();
        int bestLayer1 = -1;
        int bestLayer2 = -1;
        int bestDelta = 0;
        long bestDist = Long.MAX_VALUE;

        int n = layerDate.size();

        for (int layer1 = 0; layer1 < n && bestDist != 0; layer1++)
        {
            for (int layer2 = layer1 + 1; layer2 < n; layer2++)
            {
                // LOGGER.debug("computing distance between ("+layer1+", "+layer2+")");
                long dist = computeDistance(layerDate.get(layer1), layerDate.get(layer2), result);
                // LOGGER.debug(" distance="+dist);
                if (dist < bestDist)
                {
                    // LOGGER.debug(" best so far");
                    bestDist = dist;
                    bestLayer1 = layer1;
                    bestLayer2 = layer2;
                    bestDelta = result.delta;
                    if (bestDist == 0)
                    {
                        break;
                    }
                }
            }
        }

        if (bestLayer1 == -1)
        {
            result = null;
        }
        else
        {
            result.layer1 = bestLayer1;
            result.layer2 = bestLayer2;
            result.delta = bestDelta;
        }
        // LOGGER.debug("Best distance is "+bestDist);

        //
        return result;
    }

    /**
     * Compute distance.
     * 
     * @param l1
     *            the l 1
     * @param l2
     *            the l 2
     * @param bestMatch
     *            the best match
     * @return the long
     */
    private long computeDistance(final SortedMap<Integer, DateRange> l1, final SortedMap<Integer, DateRange> l2, final BestMatch bestMatch)
    {
        long result;

        result = Long.MAX_VALUE;
        if ((l1 == null) || (l2 == null))
        {
            bestMatch.delta = 0;
        }
        else
        {
            Iterator<Entry<Integer, DateRange>> iter1 = l1.entrySet().iterator(), iter2 = l2.entrySet().iterator();
            Entry<Integer, DateRange> e1 = null, e2 = null;
            boolean need1 = true, need2 = true;
            while (true)
            {
                if (need1)
                {
                    if (iter1.hasNext())
                    {
                        e1 = iter1.next();
                        need1 = false;
                    }
                    else
                    {
                        break;
                    }
                }
                if (need2)
                {
                    if (iter2.hasNext())
                    {
                        e2 = iter2.next();
                        need2 = false;
                    }
                    else
                    {
                        break;
                    }
                }
                int layer1 = e1.getKey().intValue();
                DateRange dr1 = e1.getValue();
                int layer2 = e2.getKey().intValue();
                DateRange dr2 = e2.getValue();

                if (!dr1.isValid())
                {
                    need1 = true;
                }
                else if (!dr2.isValid())
                {
                    need2 = true;
                }
                else
                {
                    long rel = dr1.getCenter() - dr2.getCenter();
                    long dist = Math.abs(rel);
                    if (dist < result)
                    {
                        result = dist;
                        bestMatch.delta = layer1 - layer2;
                        if (dist == 0)
                        {
                            break;
                        }
                    }
                    if (rel < 0)
                    {
                        need1 = true;
                    }
                    else
                    {
                        need2 = true;
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Compute layer date.
     * 
     * @param compCount
     *            the comp count
     * @return the list
     */
    private List<SortedMap<Integer, DateRange>> computeLayerDate(final int compCount)
    {
        ArrayList<SortedMap<Integer, DateRange>> layerDate = new ArrayList<SortedMap<Integer, DateRange>>(compCount);
        for (int compIndex = 0; compIndex < compCount; compIndex++)
        {
            SortedMap<Integer, DateRange> l = new TreeMap<Integer, DateRange>();
            Set<Vertex> components = getComponent(compIndex);
            for (Vertex vertex : components)
            {
                if (vertex instanceof Fam)
                {
                    // Only align indi generations.
                    continue;
                }

                if (vertex.getProperty(DOTWriter.GENERATION_KEY) != null)
                {
                    // Don't move components containing nodes with a specified
                    // generation.
                    l.clear();
                    break;
                }

                DateRange dateRange = vertex.getDateRange();
                if (!dateRange.isValid())
                {
                    continue;
                }

                Integer layer = Integer.valueOf(vertex.getLayer());
                DateRange dr = l.get(layer);
                if (dr == null)
                {
                    dr = new DateRange(dateRange);
                    l.put(layer, dr);
                }
                else
                {
                    dr.union(dateRange);
                }
            }
            if (l.isEmpty())
            {
                layerDate.add(null);
            }
            else
            {
                layerDate.add(l);
            }
        }
        return layerDate;
    }

    /**
     * Creates the fam lines.
     * 
     * @param g
     *            the g
     * @return the p node
     */
    private PNode createFamLines(final int g)
    {
        PNode result;

        this.lastLine.setLine(0, 0, 0, 0);
        double lineSpacingOffset = (1 - GraphicsConstants.INDI_LINE_SPACING) * 0.5;
        FamGeneration gen = this.famGeneration[g];
        if (gen == null)
        {
            result = null;
        }
        else
        {
            // PBounds bounds = gen.getFullBounds();
            HashMap<PNode, PBounds> famBounds = new HashMap<PNode, PBounds>();
            PBounds prevBounds = null;
            for (Object object : gen.getChildrenReference())
            {
                PFam pfam = (PFam) object;
                if (pfam.getVisible())
                {
                    PBounds bounds = pfam.getFullBounds();
                    Fam fam = pfam.getFam();
                    for (Edge edge : this.network.getIncidentEdges(fam))
                    {
                        PBounds edgeBounds = edge.getNode().getFullBoundsReference();
                        double offset = lineSpacingOffset * edgeBounds.getHeight();
                        // bounds.add(edgeBounds.x, edgeBounds.y + offset);
                        // bounds.add(edgeBounds.x + edgeBounds.width,
                        // edgeBounds.y
                        // + edgeBounds.height + offset);
                        bounds.add(edgeBounds.x, edgeBounds.y + offset);
                        bounds.add(edgeBounds.x + edgeBounds.width, edgeBounds.y + edgeBounds.height - offset);
                    }
                    famBounds.put(pfam, bounds);
                    if (prevBounds == null)
                    {
                        prevBounds = bounds;
                    }
                }
            }

            if (prevBounds == null)
            {
                result = null;
            }
            else
            {
                result = new PNode();
                Line2D.Double line = new Line2D.Double(prevBounds.getMinX(), prevBounds.getMinY(), prevBounds.getMinX(), prevBounds.getMaxY());
                addGridLine(result, line);

                for (int childIndex = 1; childIndex < gen.getChildrenCount(); childIndex++)
                {
                    PFam pfam = (PFam) gen.getChild(childIndex);
                    if (pfam.getVisible())
                    {
                        PBounds bounds = famBounds.get(pfam);
                        line.x1 = bounds.getMinX();
                        line.y1 = Math.min(prevBounds.getMinY(), bounds.getMinY());

                        line.x2 = bounds.getMinX();
                        line.y2 = Math.max(prevBounds.getMaxY(), bounds.getMaxY());
                        // (prevB.getCenterX() + b.getCenterX())/2;

                        addGridLine(result, line);
                        prevBounds = bounds;
                    }
                }
                line.setLine(prevBounds.getMaxX(), prevBounds.getMinY(), prevBounds.getMaxX(), prevBounds.getMaxY());
                addGridLine(result, line);
            }
        }

        //
        return result;
    }

    /**
     * Creates the generations.
     */
    private void createGenerations()
    {
        this.indiGeneration = new IndiGeneration[this.individualGenerations.length];
        this.famGeneration = new FamGeneration[this.familyGenerations.length];

        for (int generationIndex = 0; generationIndex < this.individualGenerations.length; generationIndex++)
        {
            List<Indi> layer = this.individualGenerations[generationIndex];
            if (layer != null)
            {
                IndiGeneration generation = new IndiGeneration(layer);
                this.indiGeneration[generationIndex] = generation;
                addChild(generation);
            }
        }

        for (int generationIndex = 0; generationIndex < this.familyGenerations.length; generationIndex++)
        {
            List<Fam> layer = this.familyGenerations[generationIndex];
            if (layer != null)
            {
                FamGeneration generation = new FamGeneration(layer);
                this.famGeneration[generationIndex] = generation;
                addChild(generation);
            }
        }
    }

    /**
     * Creates the indi lines.
     * 
     * @param generationIndex
     *            the generation index
     * @return the p node
     */
    private PNode createIndiLines(final int generationIndex)
    {
        PNode result;

        this.lastLine.setLine(0, 0, 0, 0);
        double lineSpacingOffset = (1 - GraphicsConstants.INDI_LINE_SPACING) * 0.5;
        IndiGeneration gen = this.indiGeneration[generationIndex];
        if (gen == null)
        {
            result = null;
        }
        else
        {
            PBounds generationBounds = gen.getFullBounds();
            HashMap<PNode, PBounds> indiBounds = new HashMap<PNode, PBounds>();
            PBounds prevBounds = null;
            for (Object childReference : gen.getChildrenReference())
            {
                PIndi pindi = (PIndi) childReference;
                if (pindi.getVisible())
                {
                    PBounds individualBounds = pindi.getFullBounds();
                    Indi indi = pindi.getIndi();
                    for (Edge edge : this.network.getIncidentEdges(indi))
                    {
                        PBounds edgeBounds = edge.getNode().getFullBoundsReference();
                        individualBounds.add(edgeBounds);
                        generationBounds.add(edgeBounds);
                    }
                    indiBounds.put(pindi, individualBounds);
                    if (prevBounds == null)
                    {
                        prevBounds = individualBounds;
                    }
                }
            }
            if (prevBounds == null)
            {
                result = null;
            }
            else
            {
                result = new PNode();
                Line2D.Double line = new Line2D.Double(prevBounds.getMinX(), prevBounds.getMinY() + lineSpacingOffset * prevBounds.getHeight(), prevBounds.getMaxX(), prevBounds.getMinY()
                        + lineSpacingOffset * prevBounds.getHeight());
                addGridLine(result, line);
                for (int childIndex = 1; childIndex < gen.getChildrenCount(); childIndex++)
                {
                    PIndi pindi = (PIndi) gen.getChild(childIndex);
                    if (pindi.getVisible())
                    {
                        PBounds individualBounds = indiBounds.get(pindi);
                        line.x1 = Math.min(prevBounds.getMinX(), individualBounds.getMinX());
                        line.y1 = line.y2 = individualBounds.getMinY() + lineSpacingOffset * individualBounds.getHeight();

                        line.x2 = Math.max(prevBounds.getMaxX(), individualBounds.getMaxX());
                        line.y2 = individualBounds.getMinY() + lineSpacingOffset * individualBounds.getHeight();
                        // (prevB.getCenterY() + b.getCenterY())/2;
                        addGridLine(result, line);
                        prevBounds = individualBounds;
                    }
                }
                line.setLine(prevBounds.getMinX(), prevBounds.getMaxY() - lineSpacingOffset * prevBounds.getHeight(), prevBounds.getMaxX(),
                        prevBounds.getMaxY() - lineSpacingOffset * prevBounds.getHeight());
                addGridLine(result, line);
            }
        }

        //
        return result;
    }

    /**
     * Distance.
     * 
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the double
     */
    private double distance(final DateRange alpha, final DateRange bravo)
    {
        double result;

        if (alpha == null || bravo == null)
        {
            result = Double.POSITIVE_INFINITY;
        }
        else
        {
            result = alpha.distanceToCenter(bravo);
        }

        //
        return result;
    }

    /**
     * Fix layers.
     */
    private void fixLayers()
    {
        int compCount = getComponentCount();
        if (compCount >= 2)
        {
            this.logger.debug("Fixing layers");
            int[] depth = new int[compCount];
            Vertex[] last = new Vertex[compCount];
            int deepest = -1;
            int maxDepth = 0;
            for (int componentIndex = 0; componentIndex < compCount; componentIndex++)
            {
                int max = 0;
                int min = this.layerCount;
                for (Vertex vertex : getComponent(componentIndex))
                {
                    int layerIndex = vertex.getLayer();
                    if (layerIndex > max)
                    {
                        max = vertex.getLayer();
                        last[componentIndex] = vertex;
                    }
                    else if (layerIndex < min)
                    {
                        min = layerIndex;
                    }
                }
                depth[componentIndex] = max - min + 1;
                if (depth[componentIndex] > maxDepth)
                {
                    deepest = componentIndex;
                    maxDepth = depth[componentIndex];
                }
            }

            boolean lastIsFamily = (last[deepest] instanceof Fam);
            for (int componentIndex = 0; componentIndex < compCount; componentIndex++)
            {
                if (componentIndex == deepest)
                {
                    continue;
                }
                if (lastIsFamily != (last[componentIndex] instanceof Fam))
                {
                    for (Vertex vertex : getComponent(componentIndex))
                    {
                        vertex.setLayer(vertex.getLayer() - 1);
                    }
                }
            }
        }
    }

    /**
     * Fix layers by date.
     */
    private void fixLayersByDate()
    {
        int compCount = getComponentCount();
        if (compCount >= 2)
        {
            this.logger.debug("Fixing layers by date");
            List<SortedMap<Integer, DateRange>> layerDate = computeLayerDate(compCount);
            List<Set<Vertex>> layerVertex = new ArrayList<Set<Vertex>>(this.network.getComponents());
            // Filter out components with no date
            // for (int i = 0; i < layerDate.size(); i++) {
            // if (layerDate.get(i) == null) {
            // layerVertex.remove(i);
            // layerDate.remove(i);
            // LOGGER.debug("Removed layer "+i+" with no date");
            // i--;
            // }
            // }

            while (layerDate.size() > 1)
            {
                BestMatch bestMatch = computeBestMatch(layerDate);
                if (bestMatch == null)
                {
                    // No more improvements.
                    break;
                }
                this.logger.debug("Merging best match bewteen layers({}, {}) with delta {}", bestMatch.layer1, bestMatch.layer2, bestMatch.delta);
                mergeLayers(layerDate, bestMatch, layerVertex);
            }

            this.logger.debug("Ending with {} disconnected components", layerDate.size());
            fixMinMax(layerDate, layerVertex);
        }
    }

    /**
     * Fix min max.
     * 
     * @param layerDate
     *            the layer date
     * @param vertexLayers
     *            the vertex layers
     */
    private void fixMinMax(final List<SortedMap<Integer, DateRange>> layerDate, final List<Set<Vertex>> vertexLayers)
    {
        this.logger.debug("Fixing min and max layers");
        boolean bumpToMax = true;

        assert (layerDate.size() == vertexLayers.size());
        int maxCount = this.layerCount;
        int[] min = new int[layerDate.size()];
        int[] max = new int[layerDate.size()];
        int index = 0;
        for (Set<Vertex> vertices : vertexLayers)
        {
            min[index] = Integer.MAX_VALUE;
            max[index] = Integer.MIN_VALUE;

            for (Vertex vertex : vertices)
            {
                if (vertex.getProperty(DOTWriter.GENERATION_KEY) != null)
                {
                    min[index] = 0;
                    max[index] = maxCount - 1;
                    break;
                }
                int layerIndex = vertex.getLayer();
                min[index] = Math.min(min[index], layerIndex);
                max[index] = Math.max(max[index], layerIndex);
            }

            int count = (max[index] - min[index]) + 1;

            if (count > maxCount)
            {
                maxCount = count;
            }

            if (min[index] < 0)
            {
                this.logger.debug("Layer {} has min={}", index, min[index]);
            }

            index += 1;
        }
        this.logger.debug("New layers count {} (was {})", maxCount, this.layerCount);
        for (int layerIndex = 0; layerIndex < layerDate.size(); layerIndex++)
        {
            Set<Vertex> vertices = vertexLayers.get(layerIndex);
            int offset;
            if (bumpToMax)
            {
                offset = maxCount - max[layerIndex] - 1;
            }
            else
            {
                offset = -min[layerIndex];
            }
            if ((min[layerIndex] + offset) < 0)
            {
                offset = -min[layerIndex];
            }
            this.logger.debug("Moving comp {} by {}", layerIndex, offset);
            if (offset != 0)
            {
                for (Vertex vertex : vertices)
                {
                    vertex.setLayer(vertex.getLayer() + offset);
                }
            }
        }
        this.layerCount = maxCount;
    }

    /**
     * Gets the component.
     * 
     * @param component
     *            the component
     * @return the component
     */
    private Set<Vertex> getComponent(final int component)
    {
        return this.network.getComponents().get(component);
    }

    /**
     * Gets the component count.
     * 
     * @return the component count
     */
    private int getComponentCount()
    {
        return this.network.getComponentCount();
    }

    /**
     * Gets the fam generations.
     * 
     * @return the family generations
     */
    public FamGeneration[] getFamGenerations()
    {
        return this.famGeneration;
    }

    /**
     * Gets the hull.
     * 
     * @return the hull
     */
    public Hull getHull()
    {
        return this.hull;
    }

    /**
     * Gets the indi generations.
     * 
     * @return the individual generations
     */
    public IndiGeneration[] getIndiGenerations()
    {
        return this.indiGeneration;
    }

    /**
     * Gets the layer count.
     * 
     * @return the layerCount
     */
    public int getLayerCount()
    {
        return this.layerCount;
    }

    /**
     * Gets the network.
     * 
     * @return the network
     */
    public Network getNetwork()
    {
        return this.network;
    }

    /**
     * Gets the selection manager.
     * 
     * @return the selection manager
     */
    public SelectionManager getSelectionManager()
    {
        return this.selectionManager;
    }

    /**
     * Inits the positions.
     * 
     * @param layers
     *            the layers
     */
    private void initPositions(final Vertex[][] layers)
    {
        for (int layerIndex = 0; layerIndex < this.layerCount; layerIndex++)
        {
            Vertex[] layer = layers[layerIndex];
            for (int subLayerIndex = 0; subLayerIndex < layer.length; subLayerIndex++)
            {
                layer[subLayerIndex].setX(subLayerIndex);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void layoutChildren()
    {
        if (this.network != null)
        {

            double x = 0;
            double y = 0;
            if (this.familyFirst)
            {
                for (int generationIndex = 0; generationIndex < this.famGeneration.length; generationIndex++)
                {
                    FamGeneration fam = this.famGeneration[generationIndex];
                    if (fam != null)
                    {
                        PiccoloUtils.setLocation(fam, x, y, true);
                        x += fam.getFullBoundsReference().getWidth() + this.xPad * 0.5;
                        y += fam.getFullBoundsReference().getHeight() + this.yPad;
                    }
                    IndiGeneration indi = this.indiGeneration[generationIndex];
                    if (indi != null)
                    {
                        PiccoloUtils.setLocation(indi, x, y, true);
                        x += indi.getWidth() + this.xPad * 0.5;
                        y += indi.getFullBoundsReference().getHeight() + this.yPad;
                    }
                }
            }
            else
            {
                for (int generationIndex = 0; generationIndex < this.famGeneration.length; generationIndex++)
                {
                    IndiGeneration indi = this.indiGeneration[generationIndex];
                    if (indi != null)
                    {
                        x -= this.xPad * 0.5;
                        y -= this.yPad * 1;
                        PiccoloUtils.setLocation(indi, x, y, true);
                        y += indi.getFullBoundsReference().getHeight() + this.xPad * 0.5;
                        x += indi.getFullBoundsReference().getWidth() + this.yPad * 0.5;
                    }
                    FamGeneration fam = this.famGeneration[generationIndex];
                    if (fam != null)
                    {
                        y -= this.yPad * 0.5;
                        PiccoloUtils.setLocation(fam, x, y, true);
                        x += fam.getFullBoundsReference().getWidth() + this.xPad;
                        y += fam.getFullBoundsReference().getHeight() + this.yPad;
                    }
                }
            }

            for (Edge edge : this.network.getEdges())
            {
                edge.getNode().updateBounds();
            }
            this.grids.removeAllChildren();
            for (int generationIndex = 0; generationIndex < this.indiGeneration.length; generationIndex++)
            {
                PNode grid = createIndiLines(generationIndex);
                if (grid != null)
                {
                    this.grids.addChild(grid);
                }
            }
            for (int generationIndex = 0; generationIndex < this.famGeneration.length; generationIndex++)
            {
                PNode grid = createFamLines(generationIndex);
                if (grid != null)
                {
                    this.grids.addChild(grid);
                }
            }

            setBounds(0, 0, x, y);

            this.hull.updateShape();

            /* Optional -- only needed if hull is added to the scenegraph for debugging */
            this.hull.setBounds(0, 0, x, y);
        }
    }

    /**
     * Merge layers.
     * 
     * @param layerDate
     *            the layer date
     * @param bestMatch
     *            the best match
     * @param layerVertex
     *            the layer vertex
     */
    private void mergeLayers(final List<SortedMap<Integer, DateRange>> layerDate, final BestMatch bestMatch, final List<Set<Vertex>> layerVertex)
    {
        SortedMap<Integer, DateRange> l = layerDate.get(bestMatch.layer1);
        for (Entry<Integer, DateRange> e : layerDate.get(bestMatch.layer2).entrySet())
        {
            Integer nl = Integer.valueOf(e.getKey().intValue() + bestMatch.delta);
            assert (nl.intValue() >= 0);
            DateRange dr = l.get(nl);
            if (dr == null)
            {
                l.put(nl, e.getValue());
            }
            else
            {
                dr.union(e.getValue()); // merge
            }
        }
        layerDate.remove(bestMatch.layer2);

        // update the vertex layers
        Set<Vertex> comp1 = new HashSet<Vertex>(layerVertex.get(bestMatch.layer1));
        for (Vertex vertex : layerVertex.get(bestMatch.layer2))
        {
            int layer = vertex.getLayer() + bestMatch.delta;
            vertex.setLayer(layer);
            comp1.add(vertex);
        }
        layerVertex.remove(bestMatch.layer2);
    }

    /**
     * New grid line.
     * 
     * @param line
     *            the line
     * @return the p semantic path
     */
    protected PSemanticPath newGridLine(final Line2D line)
    {
        PSemanticPath result;

        if (line.equals(this.lastLine))
        {
            result = null;
        }
        else
        {
            this.lastLine.setLine(line);
            result = new PSemanticPath(line);
            result.setMinimumScreenStrokeWidth(1);
            result.setStroke(GraphicsConstants.instance.gridStroke());
            result.setStrokePaint(GraphicsConstants.instance.gridColor());
            result.setSmallStrokePaint(GraphicsConstants.GRID_COLOR_SMALL, 0.4f);
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paint(final PPaintContext aPaintContext)
    {
        super.paint(aPaintContext);
    }

    /**
     * Cleanup and rebuild the network.
     */
    public void rebuild()
    {
        for (Vertex vertex : this.network.getVertices())
        {
            vertex.deleteNode();
        }

        for (Edge edge : this.network.getEdges())
        {
            edge.deleteNode();
        }

        removeAllChildren();

        addChildren();
    }

    /**
     * Selects the list of vertices.
     * 
     * @param vertices
     *            the list
     */
    public void select(final Collection<Vertex> vertices)
    {
        SelectionManager selManager = getSelectionManager();
        int colorindex = selManager.getNextSelectionColorIndex();
        for (Vertex vertex : vertices)
        {
            selManager.setNextSelectionColorIndex(colorindex);
            Selection currentSelection = selManager.select(vertex.getNode());
            if (currentSelection != null)
            {
                currentSelection.setHighlightMode(Selection.HighlightMode.HIGHLIGHT_NONE);
            }
        }
    }

    /**
     * Sets the selection manager.
     * 
     * @param selectionManager
     *            the new selection manager
     */
    public void setSelectionManager(final SelectionManager selectionManager)
    {
        this.selectionManager = selectionManager;
    }

    /**
     * Sort layers.
     */
    private void sortLayers()
    {
        Vertex[][] layers = new Vertex[this.layerCount][];

        int famOffset;
        if (this.familyFirst)
        {
            famOffset = 0;
        }
        else
        {
            famOffset = 1;
        }

        for (int generationIndex = 0; generationIndex < this.familyGenerations.length; generationIndex++)
        {
            if (this.familyGenerations[generationIndex] == null)
            {
                continue;
            }
            Vertex[] layer = new Vertex[this.familyGenerations[generationIndex].size()];
            this.familyGenerations[generationIndex].toArray(layer);
            layers[2 * generationIndex + famOffset] = layer;
        }

        for (int generationIndex = 0; generationIndex < this.individualGenerations.length; generationIndex++)
        {
            if (this.individualGenerations[generationIndex] == null)
            {
                continue;
            }
            Vertex[] layer = new Vertex[this.individualGenerations[generationIndex].size()];
            this.individualGenerations[generationIndex].toArray(layer);
            layers[2 * generationIndex + 1 - famOffset] = layer;
        }

        // initialize the positions
        initPositions(layers);
        boolean changed = true;
        int maxIter = 100;
        while (changed & maxIter-- > 0)
        {
            for (int generationIndex = 0; generationIndex < (this.layerCount - 1); generationIndex++)
            {
                updateBarycenterUp(layers[generationIndex]);
            }

            for (int generationIndex = this.layerCount - 1; generationIndex > 1; generationIndex--)
            {
                updateBarycenterDown(layers[generationIndex]);
            }

            COMPARATOR.changed = false;

            for (int generationIndex = 0; generationIndex < this.layerCount; generationIndex++)
            {
                Arrays.sort(layers[generationIndex], COMPARATOR);
            }
            changed = COMPARATOR.changed;
            initPositions(layers);
        }

        // for (int g = 0; g < family.length; g++) {
        // if (family[g] == null) continue;
        // Vertex[] layer = layers[2*g+famOffset];
        // List<Fam> fam = family[g];
        // for (int i = 0; i < layer.length; i++) {
        // fam.set(i, (Fam)layer[i]);
        // }
        // }
        //
        // for (int g = 0; g < individual.length; g++) {
        // if (individual[g]==null) continue;
        // Vertex[] layer = layers[2*g+1-famOffset];
        // List<Indi> indi = individual[g];
        // for (int i = 0; i < layer.length; i++) {
        // indi.set(i, (Indi)layer[i]);
        // }
        // }
        updateSortedLayers();
    }

    /**
     * This method sorts vertex items. Useful in case of Event Quilt.
     */
    public void sortVertexInGenerations()
    {
        this.logger.debug("sorting with " + this.sorting);

        if ((this.sorting != null) && (this.sorting != Sorting.NONE))
        {
            Comparator<Vertex> comparator = new VertexComparator(this.sorting);

            for (int generationIndex = 0; generationIndex < this.individualGenerations.length; generationIndex++)
            {
                List<Indi> generation = this.individualGenerations[generationIndex];
                if (generation != null)
                {
                    Collections.sort(generation, comparator);

                    if (this.logger.isDebugEnabled())
                    {
                        for (Indi indi : generation)
                        {
                            if (indi.getDateRange() != null)
                            {
                                this.logger.debug("I: {} {}", generationIndex, indi.getDateRange().toString());
                            }
                        }
                    }
                }
            }

            for (int generationIndex = 0; generationIndex < this.familyGenerations.length; generationIndex++)
            {
                List<Fam> generation = this.familyGenerations[generationIndex];
                if (generation != null)
                {
                    Collections.sort(generation, comparator);

                    if (this.logger.isDebugEnabled())
                    {
                        for (Fam fam : generation)
                        {
                            if (fam.getDateRange() != null)
                            {
                                this.logger.debug("F: {} {}", generationIndex, fam.getDateRange().toString());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Update barycenter down.
     * 
     * @param layer
     *            the layer
     */
    private void updateBarycenterDown(final Vertex[] layer)
    {
        for (int layerIndex = 0; layerIndex < layer.length; layerIndex++)
        {
            Vertex vertex = layer[layerIndex];
            double barycenter = barycenter(this.network.getSuccessors(vertex));
            if (barycenter != Double.NaN)
            {
                vertex.setX(barycenter);
            }
        }
    }

    /**
     * Update barycenter up.
     * 
     * @param layer
     *            the layer
     */
    private void updateBarycenterUp(final Vertex[] layer)
    {
        for (int layerIndex = 0; layerIndex < layer.length; layerIndex++)
        {
            Vertex vertex = layer[layerIndex];
            double barycenter = barycenter(this.network.getPredecessors(vertex));
            if (barycenter != Double.NaN)
            {
                vertex.setX(barycenter);
            }
        }
    }

    /**
     * Update sorted layers.
     */
    private void updateSortedLayers()
    {
        for (int generationIndex = 0; generationIndex < this.familyGenerations.length; generationIndex++)
        {
            if (this.familyGenerations[generationIndex] == null)
            {
                continue;
            }
            List<Fam> fam = this.familyGenerations[generationIndex];
            Collections.sort(fam, COMPARATOR);
        }

        for (int generationIndex = 0; generationIndex < this.individualGenerations.length; generationIndex++)
        {
            if (this.individualGenerations[generationIndex] == null)
            {
                continue;
            }
            List<Indi> indi = this.individualGenerations[generationIndex];
            Collections.sort(indi, COMPARATOR);
        }
    }

    /**
     * Barycenter.
     * 
     * @param vertices
     *            the vertices
     * @return the double
     */
    private static double barycenter(final Collection<Vertex> vertices)
    {
        double result;

        double sum = 0;
        int count = 0;

        for (Vertex vertex : vertices)
        {
            sum += vertex.getX();
            count += 1;
        }
        if (count == 0)
        {
            result = Double.NaN;
        }
        else
        {
            result = sum / count;
        }

        //
        return result;
    }
}
