/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.SelectionManager;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.model.DateRange;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The class TimeLine.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class TimeLine extends PNode implements ChangeListener
{
    private static final long serialVersionUID = -8201608031817672578L;

    private QuiltManager manager;
    private PNode areaVisiblePNode;
    private PCanvas canvas;
    private PCanvas viewedCanvas;
    private PropertyChangeListener changeListener;
    private DateRange fullRange;
    private DateRange visibleRange;
    private PropertyChangeSupport propChange;
    private Font font = new Font("SansSerif", Font.PLAIN, 12);
    private Line2D.Double line = new Line2D.Double();
    private PBasicInputEventHandler pickEventHandler = new PBasicInputEventHandler()
    {
        private Collection<Vertex> selected;

        /**
		 * 
		 */
        @Override
        public void mouseClicked(final PInputEvent event)
        {
            if (event.getClickCount() == 1)
            {
                this.selected = drag(event.getPosition());
            }
            else if (this.selected != null)
            {
                clearHighlights();
                TimeLine.this.manager.select(this.selected);
            }

        }
    };

    /** prop. visibleRange */
    public static final String PROP_VISIBLE_RANGE = "visibleRange";

    /**
     * Creates a TimeLine from a manager.
     * 
     * @param manager
     *            the manager
     */
    public TimeLine(final QuiltManager manager)
    {
        this.manager = manager;
        // setPaint(new Color(200, 200, 200, 20));
        this.changeListener = new PropertyChangeListener()
        {
            @Override
            public void propertyChange(final PropertyChangeEvent evt)
            {
                updateFromViewed();
            }
        };

        this.areaVisiblePNode = new PFlatRect();
        this.areaVisiblePNode.setPaint(new Color(0.5f, 0.5f, 1f, 0.3f));
        this.areaVisiblePNode.setTransparency(.5f);
        this.areaVisiblePNode.setBounds(0, 0, 100, getHeight());

        addChild(this.areaVisiblePNode);
        this.propChange = new PropertyChangeSupport(this);
    }

    /**
     * Adds the property change listener.
     * 
     * @param listener
     *            the listener
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(java.beans.PropertyChangeListener)
     */
    @Override
    public void addPropertyChangeListener(final PropertyChangeListener listener)
    {
        this.propChange.addPropertyChangeListener(listener);
    }

    /**
     * Adds the property change listener.
     * 
     * @param propertyName
     *            the property name
     * @param listener
     *            the listener
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(java.lang.String,
     *      java.beans.PropertyChangeListener)
     */
    @Override
    public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener)
    {
        this.propChange.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Clear highlights.
     */
    void clearHighlights()
    {
        for (Vertex vertex : this.manager.getNetwork().getVertices())
        {
            if (vertex instanceof Indi)
            {
                Indi indi = (Indi) vertex;
                indi.getNode().setPaint(null);
            }
        }
    }

    /**
     * Compute full range.
     * 
     * @return the date range
     */
    DateRange computeFullRange()
    {
        DateRange result;

        if (this.manager.getNetwork() == null)
        {
            result = null;
        }
        else
        {
            result = new DateRange();
            for (Vertex vertex : this.manager.getNetwork().getVertices())
            {
                result.union(vertex.getDateRange());
            }
        }

        return result;
    }

    /**
     * Compute visible range.
     * 
     * @return the date range
     */
    DateRange computeVisibleRange()
    {
        DateRange result;

        PBounds viewBounds = this.viewedCanvas.getCamera().getViewBounds();
        PBounds fullBounds = this.viewedCanvas.getLayer().getFullBoundsReference();
        viewBounds.y = fullBounds.y;
        viewBounds.height = fullBounds.height;
        result = new DateRange();
        result.setInvalid();
        ArrayList<PNode> picked = new ArrayList<PNode>();
        this.viewedCanvas.getLayer().findIntersectingNodes(viewBounds, picked);
        for (PNode node : picked)
        {
            if (node instanceof PFam)
            {
                PFam pfam = (PFam) node;
                result.union(pfam.getFam().getDateRange());
            }
            else if (node instanceof PIndi)
            {
                PIndi pindi = (PIndi) node;
                result.union(pindi.getIndi().getDateRange());
            }
            // else if (n instanceof PEdge) {
            // PEdge pedge = (PEdge) n;
            // visible.union(pedge.getEdge().getDateRange());
            // }
        }

        return result;
    }

    /**
     * Connects to the specified canvas with the specified manager.
     * 
     * @param viewedCanvas
     *            the viewed canvas
     * @param canvas
     *            my canvas
     */
    public void connect(final PCanvas viewedCanvas, final PCanvas canvas)
    {
        this.viewedCanvas = viewedCanvas;
        this.viewedCanvas.getCamera().addPropertyChangeListener(this.changeListener);
        this.canvas = canvas;
        this.canvas.getCamera().addPropertyChangeListener(this.changeListener);
        canvas.addInputEventListener(this.pickEventHandler);
    }

    /**
     * Disconnects from the watched canvas.
     */
    public void disconnect()
    {
        this.viewedCanvas.getCamera().removePropertyChangeListener(this.changeListener);
        this.canvas.getCamera().removePropertyChangeListener(this.changeListener);
        this.canvas.removeInputEventListener(this.pickEventHandler);
    }

    /**
     * Selects the object under the specified position.
     * 
     * @param position
     *            the position
     * @return the collection of selected vertices
     */
    public Collection<Vertex> drag(final Point2D position)
    {
        PBounds bb = new PBounds(position.getX() - 1, getY(), 2, getHeight());
        HashSet<Vertex> verts = new HashSet<Vertex>();
        pick(bb, verts);
        if (!verts.isEmpty())
        {
            SelectionManager selManager = getSelectionManager();
            ArrayList<Vertex> selection = new ArrayList<Vertex>();
            Color selColor = GUIUtils.multiplyAlpha(selManager.getNextSelectionColor(), 0.7f);
            for (Vertex vertex : this.manager.getNetwork().getVertices())
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    // if (indi.search(text, field)) {
                    if (verts.contains(indi))
                    {
                        PNode pindi = indi.getNode();
                        pindi.setPaint(selColor);
                        selection.add(indi);
                    }
                    else
                    {
                        PNode pindi = indi.getNode();
                        pindi.setPaint(null);
                    }
                }
            }
            if (!selection.isEmpty())
            {
                Vertex first = selection.get(0);
                this.viewedCanvas.getCamera().animateViewToPanToBounds(first.getNode().getFullBounds(), 200);
            }
        }
        return verts;
    }

    /**
     * Fire property change.
     * 
     * @param propertyName
     *            the property name
     * @param oldValue
     *            the old value
     * @param newValue
     *            the new value
     * @see java.beans.PropertyChangeSupport#firePropertyChange(java.lang.String,
     *      java.lang.Object, java.lang.Object)
     */
    public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue)
    {
        this.propChange.firePropertyChange(propertyName, oldValue, newValue);
    }

    /**
     * Gets the font.
     * 
     * @return the font
     */
    public Font getFont()
    {
        return this.font;
    }

    /**
     * Gets the full range.
     * 
     * @return the full range of dates
     */
    public DateRange getFullRange()
    {
        DateRange result;

        if (this.fullRange == null)
        {
            this.fullRange = computeFullRange();
        }

        result = this.fullRange;

        //
        return result;
    }

    /**
     * Gets the property change listeners.
     * 
     * @return the property change listeners
     * @see java.beans.PropertyChangeSupport#getPropertyChangeListeners()
     */
    public PropertyChangeListener[] getPropertyChangeListeners()
    {
        return this.propChange.getPropertyChangeListeners();
    }

    /**
     * Gets the property change listeners.
     * 
     * @param propertyName
     *            the property name
     * @return the property listeners
     * @see java.beans.PropertyChangeSupport#getPropertyChangeListeners(java.lang.String)
     */
    public PropertyChangeListener[] getPropertyChangeListeners(final String propertyName)
    {
        return this.propChange.getPropertyChangeListeners(propertyName);
    }

    /**
     * Gets the selection manager.
     * 
     * @return the selection manager
     */
    SelectionManager getSelectionManager()
    {
        return this.manager.getSelectionManager();
    }

    /**
     * Gets the visible range.
     * 
     * @return the visibleRange
     */
    public DateRange getVisibleRange()
    {
        return this.visibleRange;
    }

    /**
     * Checks for listeners.
     * 
     * @param propertyName
     *            the property name
     * @return true if it has listeners on the specified property
     * @see java.beans.PropertyChangeSupport#hasListeners(java.lang.String)
     */
    public boolean hasListeners(final String propertyName)
    {
        return this.propChange.hasListeners(propertyName);
    }

    // private double computeScale() {
    // if (fullRange == null || !fullRange.isValid())
    // return 1;
    // return getWidth() / (fullRange.getEnd() - fullRange.getStart());
    // }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        super.paint(paintContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintAfterChildren(final PPaintContext paintContext)
    {
        paintTicks(paintContext);
        paintVertices(paintContext);
    }

    /**
     * Paint ticks.
     * 
     * @param paintContext
     *            the paint context
     */
    private void paintTicks(final PPaintContext paintContext)
    {
        getFullRange();
        if ((this.fullRange != null) && (this.fullRange.isValid()))
        {
            int[] start = DateRange.getYMD(this.fullRange.getStart(), null);
            int[] end = DateRange.getYMD(this.fullRange.getEnd(), null);
            int step = computeStep(start[0], end[0], (int) (getWidth() / 100));

            if (step != 0)
            {
                double scale = getWidth() / (this.fullRange.getEnd() - this.fullRange.getStart());
                double offset = this.fullRange.getStart();

                Graphics2D graphics = paintContext.getGraphics();
                graphics.setColor(Color.BLUE);
                graphics.setFont(this.font);
                FontMetrics fonteMetrics = graphics.getFontMetrics();
                this.line.y1 = getBoundsReference().getMinY() + fonteMetrics.getHeight();
                this.line.y2 = getBoundsReference().getMaxY();

                String s = Integer.toString(start[0]);
                Rectangle2D sb = fonteMetrics.getStringBounds(s, graphics);
                float sx = (float) (getX());
                float sy = (float) (getY() + fonteMetrics.getHeight());
                graphics.drawString(s, sx, sy);
                double rightExtent = (getX() + sb.getMaxX());

                int y0 = (start[0] % step);
                if (y0 != 0)
                {
                    y0 = step - y0;
                }
                for (int y = start[0] + y0; y <= end[0]; y += step)
                {
                    long d = DateRange.yearFloor(y);
                    // assert(d >= fullRange.getStart());
                    // assert(d <= fullRange.getEnd());
                    this.line.x1 = this.line.x2 = (d - offset) * scale + getX();
                    graphics.draw(this.line);
                    s = Integer.toString(y);
                    sb = fonteMetrics.getStringBounds(s, graphics);
                    sx = (float) (this.line.x1 - sb.getCenterX());
                    if (sx >= rightExtent)
                    {
                        graphics.drawString(s, sx, sy);
                        rightExtent = (float) (this.line.x1 + sb.getCenterX());
                    }
                }
                s = Integer.toString(end[0]);
                sb = fonteMetrics.getStringBounds(s, graphics);
                sx = (float) (getBoundsReference().getMaxX() - sb.getMaxX());
                if (sx >= rightExtent)
                {
                    graphics.drawString(s, sx, sy);
                }

                this.line.y1 = getBoundsReference().getCenterY();
                step /= 5;
                if (step != 0)
                {
                    y0 = (start[0] % step);
                    if (y0 != 0)
                    {
                        y0 = step - y0;
                    }
                    for (int y = start[0] + y0; y <= end[0]; y += step)
                    {
                        long d = DateRange.yearFloor(y);
                        // assert(d >= fullRange.getStart());
                        // assert(d <= fullRange.getEnd());
                        this.line.x1 = this.line.x2 = (d - offset) * scale + getX();
                        graphics.draw(this.line);
                    }
                    // s = Integer.toString(y);
                    // sb = fm.getStringBounds(s, g);
                    // sx = (float)(line.x1 - sb.getCenterX());
                    // if (sx >= rightExtent) {
                    // g.drawString(s, sx, sy);
                    // rightExtent = (float)(line.x1 + sb.getCenterX());
                    // }
                }
            }
        }
    }

    /**
     * Paint vertices.
     * 
     * @param paintContext
     *            the paint context
     */
    private void paintVertices(final PPaintContext paintContext)
    {
        getFullRange();
        if ((this.fullRange != null) && (this.fullRange.isValid()))
        {
            double scale = getWidth() / (this.fullRange.getEnd() - this.fullRange.getStart());
            double offset = this.fullRange.getStart();
            double scaley = (getHeight() - 20) / this.manager.getLayerCount();
            Graphics2D graphics = paintContext.getGraphics();
            graphics.setColor(Color.BLACK);
            for (Vertex vertex : this.manager.getNetwork().getVertices())
            {
                DateRange dateRange = vertex.getDateRange();
                if ((dateRange != null) && (dateRange.isValid()))
                {
                    this.line.y1 = this.line.y2 = 20 + vertex.getLayer() * scaley + getY();
                    this.line.x1 = (dateRange.getStart() - offset) * scale + getX();
                    this.line.x2 = (dateRange.getEnd() - offset) * scale + getX();
                    graphics.draw(this.line);
                }
            }
            graphics.setStroke(new BasicStroke(3));
            SelectionManager selManager = this.manager.getSelectionManager();
            for (Selection selection : selManager.getSelections())
            {
                PNode node = selection.getSelectedObject();
                if (node instanceof PVertex)
                {
                    PVertex pv = (PVertex) node;
                    Vertex vertex = pv.getVertex();
                    DateRange dateRange = vertex.getDateRange();
                    if ((dateRange != null) && (dateRange.isValid()))
                    {
                        this.line.y1 = this.line.y2 = 20 + vertex.getLayer() * scaley + getY();
                        this.line.x1 = (dateRange.getStart() - offset) * scale + getX();
                        this.line.x2 = (dateRange.getEnd() - offset) * scale + getX();
                        graphics.setColor(selection.getStrongColor());
                        graphics.draw(this.line);
                    }
                }
            }
        }
    }

    /**
     * Returns all the vertices that pick the selected box.
     * 
     * @param box
     *            the bounding box to pick
     * @param pick
     *            a list of vertices to add to or null
     * @return a list of vertices
     */
    public Collection<Vertex> pick(final PBounds box, final Collection<Vertex> pick)
    {
        Collection<Vertex> result;

        getFullRange();
        if (this.fullRange == null || !this.fullRange.isValid())
        {
            result = null;
        }
        else
        {
            if (pick == null)
            {
                result = new ArrayList<Vertex>();
            }
            else
            {
                result = pick;
            }

            double scale = getWidth() / (this.fullRange.getEnd() - this.fullRange.getStart());
            double offset = this.fullRange.getStart();
            double scaley = (getHeight() - 20) / this.manager.getLayerCount();
            for (Vertex vertex : this.manager.getNetwork().getVertices())
            {
                DateRange dateRange = vertex.getDateRange();

                if ((dateRange != null) && (dateRange.isValid()))
                {
                    this.line.y1 = this.line.y2 = 20 + vertex.getLayer() * scaley + getY();
                    this.line.x1 = (dateRange.getStart() - offset) * scale + getX();
                    this.line.x2 = (dateRange.getEnd() - offset) * scale + getX();
                    if (this.line.intersects(box))
                    {
                        result.add(vertex);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * This method will get called when the viewed canvas changes.
     * 
     * @param event
     *            the property change event
     */
    public void propertyChange(final PropertyChangeEvent event)
    {
        updateFromViewed();
    }

    /**
     * Removes the property change listener.
     * 
     * @param listener
     *            the listener
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(java.beans.PropertyChangeListener)
     */
    @Override
    public void removePropertyChangeListener(final PropertyChangeListener listener)
    {
        this.propChange.removePropertyChangeListener(listener);
    }

    /**
     * Removes the property change listener.
     * 
     * @param propertyName
     *            the property name
     * @param listener
     *            the listener
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(java.lang.String,
     *      java.beans.PropertyChangeListener)
     */
    @Override
    public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener)
    {
        this.propChange.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Sets the font.
     * 
     * @param font
     *            the font to set
     */
    public void setFont(final Font font)
    {
        this.font = font;
        invalidatePaint();
    }

    /**
     * Sets the visible range.
     * 
     * @param visibleRange
     *            the visibleRange to set
     */
    public void setVisibleRange(final DateRange visibleRange)
    {
        if (!visibleRange.equals(this.visibleRange))
        {
            DateRange old = this.visibleRange;
            this.visibleRange = visibleRange;
            firePropertyChange(PROP_VISIBLE_RANGE, old, visibleRange);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stateChanged(final ChangeEvent e)
    {
        updateSelection((SelectionManager) e.getSource());
    }

    /**
     * This method gets the state of the viewed canvas and updates the TimeLine
     * view. This can be called from outside code
     */
    public void updateFromViewed()
    {
        PBounds bounds = this.canvas.getCamera().getBounds();
        setBounds(bounds);
        DateRange fullRange = getFullRange();
        if (fullRange == null || !fullRange.isValid())
        {
            this.areaVisiblePNode.setVisible(false);
            fullRange = null; // TODO BUG fullRange is local attribute.
        }
        else
        {
            double scale = getWidth() / (fullRange.getEnd() - fullRange.getStart());
            double offset = fullRange.getStart();
            setVisibleRange(computeVisibleRange());
            if (this.visibleRange.isValid())
            {
                assert (this.visibleRange.getStart() >= fullRange.getStart());
                assert (this.visibleRange.getEnd() <= fullRange.getEnd());
                double xmin = (this.visibleRange.getStart() - offset) * scale + getX();
                double xmax = (this.visibleRange.getEnd() - offset) * scale + getX();
                this.areaVisiblePNode.setVisible(true);
                this.areaVisiblePNode.setBounds(xmin, 0, xmax - xmin, getHeight());
            }
            else
            {
                this.areaVisiblePNode.setVisible(false);
            }
        }
    }

    /**
     * Update selection.
     * 
     * @param selManager
     *            the sel manager
     */
    protected void updateSelection(final SelectionManager selManager)
    {
        invalidatePaint();
    }

    /**
     * Compute step.
     * 
     * @param s
     *            the s
     * @param e
     *            the e
     * @param max
     *            the max
     * @return the int
     */
    private static int computeStep(final int s, final int e, final int max)
    {
        int result;

        int dy = e - s;
        if (dy <= 1)
        {
            result = 1;
        }
        else
        {
            result = 1;
            int mul = 5;
            while ((dy / result) > max)
            {
                result *= mul;
                if (mul == 5)
                {
                    mul = 2;
                }
                else
                {
                    mul = 5;
                }
            }
        }

        //
        return result;
    }

    /**
     * Draw string.
     * 
     * @param graphic
     *            the graphic
     * @param s
     *            the s
     * @param x
     *            the x
     * @param y
     *            the y
     * @return the double
     */
    private static double drawString(final Graphics2D graphic, final String s, final double x, final double y)
    {
        double result;

        FontMetrics fm = graphic.getFontMetrics();
        Rectangle2D sb = fm.getStringBounds(s, graphic);
        graphic.drawString(s, (float) (x - sb.getCenterX()), (float) (y + sb.getHeight()));
        result = x + sb.getCenterX();

        //
        return result;
    }
}
