/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;

import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.quiltview.Printer;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;

/**
 * A PPath that allows to set a minimum stroke screen width and a different
 * stroke color when its screen width is below a certain size.
 * 
 * @author dragice
 */
public class PSemanticPath extends PPath
{
    private static final long serialVersionUID = 751645831912289223L;

    /**
     * 
     * <b>StrokeCache</b> implements a cache of strokes
     */
    /*public static class StrokeCache {
    	private Hashtable<Integer, Stroke> cache = new Hashtable<Integer, Stroke>();
    	private final int widthAccurracy = 1;
    	/ **
         * Returns a stroke with the specified width.
    	 * @param width the width
    	 * @return a stroke
    	 * /
    	public Stroke getStrokeWithWidth(float width) {
    		int key = (int)(width * widthAccurracy);
    		Stroke s = cache.get(Integer.valueOf(key));
    		if (s != null)
    			return s;
    		s = new BasicStroke(width);
    		cache.put(Integer.valueOf(key), s);
    	 	return s;
    	} 
    }*/

    // private static final StrokeCache strokeCache = new StrokeCache();
    private static Stroke lastStroke = null;
    private static float lastStrokeWidth = -1;

    private float normalStrokeWidth = 1;
    private float minimumScreenStrokeWidth = 1;
    private Paint smallStrokePaint = null;
    private float smallStrokePaintScale = 0;

    /**
     * Creates a PSemanticPath.
     */
    public PSemanticPath()
    {
        super();
        updateNormalStrokeWidth();
    }

    /**
     * Creates a PSemanticPath with the specified shape.
     * 
     * @param ppath
     *            the shape
     */
    public PSemanticPath(final Shape ppath)
    {
        super(ppath);
        updateNormalStrokeWidth();
    }

    /**
     * Gets the scale for larger strokes.
     * 
     * @return the scale for larger strokes
     */
    public float getScaleForLargerStrokes()
    {
        return this.minimumScreenStrokeWidth / this.normalStrokeWidth;
    }

    /**
     * Gets the small stroke paint.
     * 
     * @return the small stroke paint
     */
    public Paint getSmallStrokePaint()
    {
        return this.smallStrokePaint;
    }

    /**
     * Gets the small stroke paint scale.
     * 
     * @return the small stroke paint scale
     */
    public float getSmallStrokePaintScale()
    {
        return this.smallStrokePaintScale;
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.nodes.PPath#paint(edu.umd.cs.piccolo.util.PPaintContext)
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {

        Paint paint = getPaint();
        Graphics2D graphic = paintContext.getGraphics();

        if (paint != null)
        {
            graphic.setPaint(paint);
            graphic.fill(getPathReference());
        }

        float scale = (float) PrintConstants.instance.getScale(paintContext);
        Paint strokePaint;
        if (this.smallStrokePaint == null || scale >= this.smallStrokePaintScale)
        {
            strokePaint = getStrokePaint();
        }
        else
        {
            strokePaint = getSmallStrokePaint();
        }

        if (getStroke() != null && strokePaint != null)
        {

            float screenStrokeWidth = this.normalStrokeWidth * scale;

            if (Printer.isPrinting() || screenStrokeWidth >= this.minimumScreenStrokeWidth)
            {
                graphic.setPaint(strokePaint);
                if (Printer.isPrinting())
                {
                    graphic.setStroke(GraphicsConstants.NULL_WIDTH_STROKE);
                }
                else
                {
                    graphic.setStroke(getStroke());
                }
                graphic.draw(getPathReference());
            }
            else
            {
                if (Printer.isPrinting() || this.minimumScreenStrokeWidth == 1 && paintContext.getRenderQuality() == PPaintContext.LOW_QUALITY_RENDERING)
                {
                    // Special case: if no antialiasing and stroke width of 1,
                    // we can use a stroke
                    // width of zero.
                    graphic.setPaint(strokePaint);
                    graphic.setStroke(GraphicsConstants.NULL_WIDTH_STROKE);
                    graphic.draw(getPathReference());
                }
                else
                {
                    graphic.setPaint(strokePaint);
                    graphic.setStroke(getStrokeWithWidth(this.minimumScreenStrokeWidth / scale));
                    graphic.draw(getPathReference());
                }
            }
        }
    }

    /**
     * Sets the minimum screen stroke width.
     * 
     * @param minimumScreenStrokeWidth
     *            the new minimum screen stroke width
     */
    public void setMinimumScreenStrokeWidth(final float minimumScreenStrokeWidth)
    {
        this.minimumScreenStrokeWidth = minimumScreenStrokeWidth;
    }

    /**
     * Sets the small stroke paint.
     * 
     * @param paint
     *            the paint
     * @param scale
     *            the scale
     */
    public void setSmallStrokePaint(final Paint paint, final float scale)
    {
        this.smallStrokePaint = paint;
        this.smallStrokePaintScale = scale;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStroke(final Stroke stroke)
    {
        super.setStroke(stroke);
        updateNormalStrokeWidth();
    }

    /**
     * Update normal stroke width.
     */
    protected void updateNormalStrokeWidth()
    {
        if (getStroke() instanceof BasicStroke)
        {
            this.normalStrokeWidth = ((BasicStroke) getStroke()).getLineWidth();
        }
    }

    /**
     * Gets the stroke with width.
     * 
     * @param width
     *            the width
     * @return the stroke with width
     */
    private static Stroke getStrokeWithWidth(final float width)
    {
        Stroke result;

        if (lastStrokeWidth == width)
        {
            result = lastStroke;
        }
        else
        {
            lastStroke = new BasicStroke(width);
            result = lastStroke;
        }

        //
        return result;
    }
}
