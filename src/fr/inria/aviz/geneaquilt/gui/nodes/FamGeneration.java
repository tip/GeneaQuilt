/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.util.List;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import fr.inria.aviz.geneaquilt.gui.util.PiccoloUtils;
import fr.inria.aviz.geneaquilt.model.Fam;

/**
 * The Class FamGeneration.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class FamGeneration extends PNode
{
    private static final long serialVersionUID = 955943703495687506L;

    /**
     * Creates a Generation from a layer of individuals
     * 
     * @param layer
     *            the layer
     */
    public FamGeneration(final List<Fam> layer)
    {
        for (Fam fam : layer)
        {
            addChild(fam.getNode());
        }
        setPaint(GraphicsConstants.FAM_GENERATION_COLOR);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void layoutChildren()
    {
        double width = 0;
        double height = 0;
        for (Object childReference : getChildrenReference())
        {
            PNode child = (PNode) childReference;
            // child.setGlobalTranslation(
            // new Point2D.Double(
            // getX()+w,
            // getY()));
            PiccoloUtils.setLocation(child, getX() + width, getY(), true);
            if (child.getVisible())
            {
                PBounds bounds = child.getFullBoundsReference();
                width += bounds.getWidth();
                height = Math.max(height, bounds.getHeight());
            }
        }
        setBounds(getX(), getY(), width, height);
    }
}
