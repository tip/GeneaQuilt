/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class PEdge.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class PEdge extends PIsoShape
{
    private static final long serialVersionUID = -7449604428246852895L;

    private Edge edge;
    private static final Ellipse2D.Double circle = new Ellipse2D.Double(1, 1, 8, 8);
    private static final Rectangle2D.Double rectangle = new Rectangle2D.Double(1.5, 1.5, 7, 7);

    private static final Polygon triangle = new Polygon(new int[] { 1, 5, 9 }, new int[] { 9, 2, 9 }, 3);

    // private static final PBounds rectangle = new PBounds(1.5, 1.5, 7, 7);

    /**
     * Creates a PEdge.
     * 
     * @param edge
     *            the edge
     */
    public PEdge(final Edge edge)
    {
        super(null);

        if (edge == null)
        {
            throw new IllegalArgumentException("Null parameter detected.");
        }
        else if (edge.getSex() == null)
        {
            setShape(rectangle);
        }
        else if (edge.getSex().equals("M"))
        {
            setShape(triangle);
        }
        else if (edge.getSex().equals("F"))
        {
            setShape(circle);
        }
        else
        {
            setShape(rectangle);
        }

        setBounds(0, 0, 10, 10);
        this.edge = edge;
        setPaint(GraphicsConstants.instance.edgeColor());
    }

    /**
     * Gets the edge.
     * 
     * @return the edge
     */
    public Edge getEdge()
    {
        return this.edge;
    }

    // /**
    // * Indicate that the bounds are volatile for this group
    // */
    // public boolean getBoundsVolatile() {
    // return true;
    // }

    /**
     * Computes the position according to the position of the vertices.
     */
    public void updateBounds()
    {
        double x;
        double y;
        double w;
        double h;
        Vertex from = this.edge.getFromVertex();
        Vertex to = this.edge.getToVertex();
        if (from instanceof Indi)
        {
            x = to.getNode().getFullBoundsReference().getX();
            w = to.getNode().getFullBoundsReference().getWidth();
            y = from.getNode().getFullBoundsReference().getY();
            h = from.getNode().getFullBoundsReference().getHeight();
        }
        else
        {
            x = from.getNode().getFullBoundsReference().getX();
            w = from.getNode().getFullBoundsReference().getWidth();
            y = to.getNode().getFullBoundsReference().getY();
            h = to.getNode().getFullBoundsReference().getHeight();
        }

        if (w <= 0)
        {
            w = 0.0000000001;
        }
        if (h <= 0)
        {
            h = 0.0000000001;
        }
        setBounds(x, y, w, h);
        setVisible(from.getNode().getVisible() && to.getNode().getVisible());
    }
}
