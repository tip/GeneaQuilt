/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.nodes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;

import edu.umd.cs.piccolo.util.PPaintContext;

/**
 * The Class <b>GraphicsConstants</b> defines graphic constants.
 * 
 * @author Pierre Dragicevic
 */
public class GraphicsConstants
{
    /** */
    public static GraphicsConstants instance = new GraphicsConstants();

    /** Ratio between window size and screen size. */
    public static final float MAIN_WINDOW_SIZE = 0.9f;

    /** Needed because very slow if too large. */
    public static final int MAIN_WINDOW_MAX_WIDTH = 1200;

    /** Needed because very slow if too large. */
    public static final int MAIN_WINDOW_MAX_HEIGHT = 1000;

    /** Ratio between birdseye window size and main window size. */
    public static final float BIRDSEYE_VIEW_SIZE = 0.4f;

    public static final Color GRID_COLOR_SMALL = new Color(0.9f, 0.9f, 0.9f, 1f);

    /** Height of individual names, width of family names. */
    public static final double CELL_SIZE = 14;

    /** */
    public static final Color INDI_COLOR = Color.BLACK;
    public static final Font INDI_FONT = new Font("Helvetica", Font.PLAIN, 12);
    public static final double INDI_LINE_SPACING = 0.85;
    public static final Color INDI_GENERATION_COLOR = new Color(0.75f, 0.75f, 0.75f, 1f);

    /** */
    public static final Color FAM_COLOR = Color.BLACK;
    public static final Font FAM_FONT = new Font("Helvetica", Font.PLAIN, 10);
    public static final Color FAM_GENERATION_COLOR = new Color(0.85f, 0.85f, 0.85f, 1f);

    /** */
    public static final float SELECTION_HIGHLIGHT_WIDTH = 3;
    public static final Stroke SELECTION_STROKE = new BasicStroke(SELECTION_HIGHLIGHT_WIDTH);

    /** FIXME */
    public static final float PATH_HIGHLIGHT_WIDTH = 9;

    /** FIXME */
    public static final Stroke PATH_HIGHLIGHT_STROKE = new BasicStroke(PATH_HIGHLIGHT_WIDTH, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);

    /** In pixels. */
    public static final float PATH_HIGHLIGHT_MULTICOLOR_SPACING = 5;

    /** */
    public static final Color[] SELECTION_COLORS = new Color[] { new Color(1f, 0f, 0f), new Color(0.2f, 0.2f, 1f), new Color(0f, 0.8f, 0f), new Color(0.7f, 0.7f, 0f), new Color(0.7f, 0.0f, 0.7f),
            new Color(0.0f, 0.7f, 0.7f), new Color(0.4f, 0.4f, 0.4f), };
    public static final double MULTICOLOR_STROKE_ZOOM_FACTOR = 0.6;
    public static final Color SMALL_TEXT_COLOR = new Color(0.8f, 0.8f, 0.8f, 1f);
    public static final BasicStroke NULL_WIDTH_STROKE = new BasicStroke(0);

    /**
     * Edge color.
     * 
     * @return the color
     */
    public Color edgeColor()
    {
        return new Color(0.25f, 0.25f, 0.25f, 1f);
    }

    /**
     * Fam border color.
     * 
     * @return the color
     */
    public Color famBorderColor()
    {
        return Color.WHITE;
    }

    /**
     * Gets the scale.
     * 
     * @param context
     *            the context
     * @return the scale
     */
    public double getScale(final PPaintContext context)
    {
        return context.getScale();
    }

    /**
     * Grid color.
     * 
     * @return the color
     */
    public Color gridColor()
    {
        return new Color(0.75f, 0.75f, 0.75f, 1f);
    }

    /**
     * Grid stroke.
     * 
     * @return the stroke
     */
    public Stroke gridStroke()
    {
        return new BasicStroke(1);
    }
}
