/*
 * Copyright 2012,2017 Christian Pierre MOMON (christian.momon@devinsy.fr).
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Adaptations by TIP.
 * 
 */
package fr.inria.aviz.geneaquilt.gui.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;

import fr.devinsy.util.StringList;
import fr.inria.aviz.geneaquilt.model.util.GeneaQuiltUtils;

/**
 * The Class GenericFileFilter.
 * 
 * @author Christian Pierre MOMON
 * @author TIP
 */
public class GenericFileFilter extends FileFilter
{
    private String description;
    private String[] extensions;

    /**
     * Instantiates a new generic file filter.
     * 
     * @param description
     *            the description
     * @param availableExtensions
     *            the available extensions
     */
    public GenericFileFilter(final String description, final String... availableExtensions)
    {
        super();
        this.description = description;
        this.extensions = availableExtensions;
    }

    /**
     * Accepts all directories and all available files.
     * 
     * @param file
     *            the file
     * @return true, if successful
     */
    @Override
    public boolean accept(final File file)
    {
        boolean result;

        if (file.isDirectory())
        {
            result = true;
        }
        else if (this.extensions.length == 0)
        {
            result = true;
        }
        else
        {
            if (GeneaQuiltUtils.equalsAnyIgnoreCase(FilenameUtils.getExtension(file.getName()), this.extensions))
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }

        //
        return result;
    }

    /**
     * This method returns the description associated to this filter.
     * 
     * @return the description of this filter.
     */
    @Override
    public String getDescription()
    {
        String result;

        result = this.description;

        //
        return result;
    }

    /**
     * This method returns the extension filter in case of cardinality one of
     * the extension list.
     * 
     * @return the extension filter if there is only one, null otherwise.
     */
    public String getExtension()
    {
        String result;

        if (this.extensions.length == 1)
        {
            result = this.extensions[0];
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * This method returns the count of extension associated to this filter.
     * 
     * @return the extension count.
     */
    public int getExtensionCount()
    {
        int result;

        result = this.extensions.length;

        //
        return result;
    }

    /**
     * This method returns the extension list associated to this filter.
     * 
     * @return an extension list.
     */
    public StringList getExtensions()
    {
        StringList result;

        //
        result = new StringList();

        //
        for (String extension : this.extensions)
        {
            result.add(extension);
        }

        //
        return result;
    }
}
