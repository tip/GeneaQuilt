/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 *
 * Copyright 2008 Pierre Dragicevic <dragice@lri.fr>
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.util;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.RepaintManager;

/**
 * The Class PrintUtilities.
 */
public class PrintUtilities implements Printable
{
    private Component componentToBePrinted;

    /**
     * Instantiates a new prints the utilities.
     * 
     * @param componentToBePrinted
     *            the component to be printed
     */
    public PrintUtilities(final Component componentToBePrinted)
    {
        this.componentToBePrinted = componentToBePrinted;
    }

    /**
     * Prints the.
     */
    public void print()
    {
        PageFormat pageFormat = new PageFormat();
        Paper paper = new Paper();
        double size = 1;// in multiples of A0
        paper.setSize(size * 45.4 * 72, size * 32.7 * 75);
        paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
        pageFormat.setPaper(paper);
        pageFormat.setOrientation(PageFormat.LANDSCAPE);

        PrinterJob printJob = PrinterJob.getPrinterJob();
        printJob.setPrintable(this);
        if (printJob.printDialog())
        {
            try
            {
                printJob.setPrintable(this, pageFormat);
                printJob.print();
            }
            catch (PrinterException exception)
            {
                exception.printStackTrace();
                System.out.println("Error printing: " + exception);
            }
        }
    }

    /* (non-Javadoc)
     * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
     */
    @Override
    public int print(final Graphics g, final PageFormat pageFormat, final int pageIndex)
    {
        int result;

        System.err.println(pageFormat.getPaper().getWidth());

        if (pageIndex > 0)
        {
            result = NO_SUCH_PAGE;
        }
        else
        {
            Graphics2D graphic = (Graphics2D) g;
            graphic.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            double dl = pageFormat.getImageableWidth();
            double dh = pageFormat.getImageableHeight();
            double scale = Math.min(dl / this.componentToBePrinted.getWidth(), dh / this.componentToBePrinted.getHeight());
            graphic.scale(scale, scale);

            disableDoubleBuffering(this.componentToBePrinted);
            this.componentToBePrinted.paint(graphic);
            enableDoubleBuffering(this.componentToBePrinted);
            result = PAGE_EXISTS;
        }

        //
        return result;
    }

    /**
     * Disable double buffering.
     * 
     * @param component
     *            the component
     */
    public static void disableDoubleBuffering(final Component component)
    {
        RepaintManager currentManager = RepaintManager.currentManager(component);
        currentManager.setDoubleBufferingEnabled(false);
    }

    /**
     * Enable double buffering.
     * 
     * @param component
     *            the component
     */
    public static void enableDoubleBuffering(final Component component)
    {
        RepaintManager currentManager = RepaintManager.currentManager(component);
        currentManager.setDoubleBufferingEnabled(true);
    }

    /**
     * Prints the component.
     * 
     * @param component
     *            the component
     */
    public static void printComponent(final Component component)
    {
        new PrintUtilities(component).print();
    }
}