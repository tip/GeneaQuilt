/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;

/**
 * The Class MulticolorStroke allows to paint shapes using a stroke with cycling
 * colors.
 * 
 * @author dragicevic
 */
public class MulticolorStroke
{
    private final Color[] colors;
    private final Stroke[] strokes;
    private final float spacing;
    private final float width;

    /**
     * Creates a regular single-color stroke.
     * 
     * @param width
     *            the width
     * @param color
     *            the color
     */
    public MulticolorStroke(final float width, final Color color)
    {
        this(width, new Color[] { color }, 0);
    }

    /**
     * Creates a multicolor stroke.
     * 
     * @param width
     *            the width
     * @param colors
     *            the colors
     * @param spacing
     *            the spacing
     */
    public MulticolorStroke(final float width, final Color[] colors, final float spacing)
    {
        this.width = width;
        this.colors = colors.clone();
        this.spacing = spacing;
        this.strokes = new Stroke[colors.length];
        createStrokes();
    }

    /**
     * Creates the strokes.
     */
    private void createStrokes()
    {
        if (this.colors.length == 1)
        {
            this.strokes[0] = new BasicStroke(this.width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f);
        }
        else
        {
            float[] dash = new float[2];
            dash[0] = this.spacing;
            dash[1] = this.spacing * (this.colors.length - 1);
            for (int index = 0; index < this.colors.length; index++)
            {
                this.strokes[index] = new BasicStroke(this.width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, this.spacing * index);
            }
        }
    }

    /**
     * Paints the shape using a multicolor stroke and no fill.
     * 
     * @param graphic
     *            the graphics
     * @param shape
     *            the shape to draw
     */
    public void draw(final Graphics2D graphic, final Shape shape)
    {
        for (int index = 0; index < this.colors.length; index++)
        {
            graphic.setColor(this.colors[index]);
            graphic.setStroke(this.strokes[index]);
            graphic.draw(shape);
        }
    }
}
