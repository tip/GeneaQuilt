/*
 * This program is a part of the companion code for Core Java 8th ed.
 * (http://horstmann.com/corejava)
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.aviz.geneaquilt.gui.util;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

/**
 * The Class ImageTransferable is a wrapper for the data transfer of image
 * objects.
 * 
 * @author TIP
 */
public class ImageTransferable implements Transferable
{
    private Image theImage;

    /**
     * Constructs the selection.
     * 
     * @param image
     *            an image
     */
    public ImageTransferable(final Image image)
    {

        this.theImage = image;
    }

    /**
     * Gets the transfer data.
     * 
     * @param flavor
     *            the flavor
     * @return the transfer data
     * @throws UnsupportedFlavorException
     *             the unsupported flavor exception
     */
    @Override
    public Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException
    {
        Object result;

        if (flavor.equals(DataFlavor.imageFlavor))
        {
            result = this.theImage;
        }
        else
        {
            throw new UnsupportedFlavorException(flavor);
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.awt.datatransfer.Transferable#getTransferDataFlavors()
     */
    @Override
    public DataFlavor[] getTransferDataFlavors()
    {
        DataFlavor[] result;

        result = new DataFlavor[] { DataFlavor.imageFlavor };

        //
        return result;
    }

    /**
     * Checks if is data flavor supported.
     * 
     * @param flavor
     *            the flavor
     * @return true, if is data flavor supported
     */
    @Override
    public boolean isDataFlavorSupported(final DataFlavor flavor)
    {
        boolean result;

        result = flavor.equals(DataFlavor.imageFlavor);

        //
        return result;
    }
}
