/*
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon, Devinsy and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GUIToolBox.
 * 
 * @author TIP
 */
public class GUIToolBox
{
    private static final Logger logger = LoggerFactory.getLogger(GUIToolBox.class);

    /**
     * Available look and feels.
     * 
     * @return the list
     */
    public static List<String> availableLookAndFeels()
    {
        List<String> result;

        //
        result = new ArrayList<String>();

        //
        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
        {
            result.add(info.getName());
        }

        //
        return result;
    }

    /**
     * Copy to clipboard.
     * 
     * @param source
     *            the source
     */
    public static void copyToClipboard(final Image source)
    {
        //
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

        if (clipboard != null)
        {
            ImageTransferable selection = new ImageTransferable(source);
            clipboard.setContents(selection, null);
        }
    }

    /**
     * Crop.
     * 
     * @param source
     *            the source
     * @return the buffered image
     */
    public static BufferedImage crop(final BufferedImage source)
    {
        BufferedImage result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            //
            int firstLine;
            int firstColumn;
            int lastColumn;
            int lastLine;

            //
            firstLine = 0;
            while ((firstLine < source.getHeight()) && (isWhiteLine(source, firstLine)))
            {
                firstLine += 1;
            }

            //
            if (firstLine == source.getHeight())
            {
                firstLine = 0;
                firstColumn = 0;
                lastColumn = source.getWidth() - 1;
                lastLine = source.getHeight() - 1;
            }
            else
            {
                //
                lastLine = source.getHeight() - 1;
                while ((firstLine > 0) && (isWhiteLine(source, lastLine)))
                {
                    lastLine -= 1;
                }

                //
                firstColumn = 0;
                while ((firstColumn < source.getWidth()) && (isWhiteColumn(source, firstColumn)))
                {
                    firstColumn += 1;
                }

                //
                lastColumn = source.getWidth() - 1;
                while ((lastColumn > 0) && (isWhiteColumn(source, lastColumn)))
                {
                    lastColumn -= 1;
                }
            }

            //
            result = new BufferedImage((lastColumn - firstColumn + 1), (lastLine - firstLine + 1), BufferedImage.TYPE_INT_ARGB);
            result.getGraphics().drawImage(source, -firstColumn, -firstLine, null);
        }

        //
        return result;
    }

    /**
     * Checks if is white column.
     * 
     * @param source
     *            the source
     * @param column
     *            the column
     * @return true, if is white column
     */
    public static boolean isWhiteColumn(final BufferedImage source, final int column)
    {
        boolean result;

        int line = 0;
        boolean ended = false;
        result = false;
        while (!ended)
        {
            if (line < source.getHeight())
            {
                if (source.getRGB(column, line) == Color.white.getRGB())
                {
                    line += 1;
                }
                else
                {
                    ended = true;
                    result = false;
                }
            }
            else
            {
                ended = true;
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Checks if is white line.
     * 
     * @param source
     *            the source
     * @param line
     *            the line
     * @return true, if is white line
     */
    public static boolean isWhiteLine(final BufferedImage source, final int line)
    {
        boolean result;

        int column = 0;
        boolean ended = false;
        result = false;
        while (!ended)
        {
            if (column < source.getWidth())
            {
                if (source.getRGB(column, line) == Color.white.getRGB())
                {
                    column += 1;
                }
                else
                {
                    ended = true;
                    result = false;
                }
            }
            else
            {
                ended = true;
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Save screenshot.
     * 
     * @param source
     *            the source
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void saveScreenshot(final Component source, final File target) throws IOException
    {
        //
        BufferedImage targetImage = crop(takeScreenshot(source));

        // File f = File.createTempFile("myOutputFile.jpg");
        ImageIO.write(targetImage, "png", target);
    }

    /**
     * Take screenshot.
     * 
     * @param source
     *            the source
     * @return the buffered image
     */
    public static BufferedImage takeScreenshot(final Component source)
    {
        BufferedImage result;

        //
        result = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);

        // Now paint the component directly onto the image
        Graphics2D imageGraphics = result.createGraphics();
        source.paint(imageGraphics);

        //
        return result;
    }
}
