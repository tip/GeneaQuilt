/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * Copyright 2008 Pierre Dragicevic <dragice@lri.fr>
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.util;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.color.ColorSpace;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.LookupOp;
import java.awt.image.LookupTable;
import java.awt.image.MemoryImageSource;
import java.awt.image.RGBImageFilter;
import java.awt.image.ShortLookupTable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.Timer;

/**
 * Various useful GUI utilities that are missing in Swing (or so I think).
 * 
 * @author Pierre Dragicevic
 */
public class GUIUtils
{
    /**
	 * 
	 */
    public interface AdvancedKeyListener extends KeyListener
    {
        /**
         * Key pressed once.
         * 
         * @param event
         *            the event
         */
        void keyPressedOnce(KeyEvent event);

        /**
         * Key repeated.
         * 
         * @param event
         *            the event
         */
        void keyRepeated(KeyEvent event);
    }

    /**
     * The Class AdvancedKeyListenerFilter.
     */
    private static class AdvancedKeyListenerFilter implements KeyListener
    {
        private AdvancedKeyListener delegate;

        private boolean autorepeat;

        final Hashtable<Integer, KeyEvent> keysDown = new java.util.Hashtable<Integer, KeyEvent>();

        /** The key repeat timer. */
        private Timer keyRepeatTimer = new Timer(10, new ActionListener()
        {
            /**
			 * 
			 */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                repeatKeys(); // cobertura:ignore
            } // cobertura:ignore
        });

        /**
         * Instantiates a new advanced key listener filter.
         * 
         * @param destination
         *            the destination
         * @param autorepeat
         *            the autorepeat
         */
        AdvancedKeyListenerFilter(final AdvancedKeyListener destination, final boolean autorepeat)
        {
            this.autorepeat = autorepeat;
            this.delegate = destination;
        }

        /* (non-Javadoc)
         * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
         */
        @Override
        public void keyPressed(final KeyEvent event)
        {
            this.delegate.keyPressed(event);
            if (!this.keysDown.containsKey(event.getKeyCode()))
            {
                this.keysDown.put(event.getKeyCode(), event);

                //
                this.delegate.keyPressedOnce(event);

                //
                if (this.keysDown.size() == 1 && this.autorepeat)
                {
                    this.keyRepeatTimer.start();
                }
            }
        }

        /* (non-Javadoc)
         * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
         */
        @Override
        public void keyReleased(final KeyEvent event)
        {
            if (this.keysDown.containsKey(event.getKeyCode()))
            {
                this.keysDown.remove(event.getKeyCode());

                if (this.keysDown.size() == 0 && this.autorepeat)
                {
                    this.keyRepeatTimer.stop();
                }

                this.delegate.keyReleased(event);
            }
        }

        /* (non-Javadoc)
         * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
         */
        @Override
        public void keyTyped(final KeyEvent event)
        {
            // TODO Auto-generated method stub
            this.delegate.keyTyped(event);
        }

        /**
         * Repeat keys.
         * 
         * @return true, if successful
         */
        boolean repeatKeys()
        {
            for (Enumeration<Integer> e = this.keysDown.keys(); e.hasMoreElements();)
            {
                this.delegate.keyRepeated(this.keysDown.get(e.nextElement()));
            }

            //
            return this.keysDown.size() > 0;
        }
    }

    /**
     * The Class WaitMessageWindow.
     */
    private static class WaitMessageWindow extends JWindow
    {
        private static final long serialVersionUID = 7014533591996328351L;

        private static final String DEFAULT_MESSAGE = "Please wait…";
        private final JLabel label;

        /**
         * Instantiates a new wait message window.
         */
        public WaitMessageWindow()
        {
            super();
            this.label = new JLabel(DEFAULT_MESSAGE);
            this.label.setOpaque(true);
            this.label.setBackground(Color.white);
            this.label.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black), BorderFactory.createEmptyBorder(5, 10, 5, 10)));
            getContentPane().add(this.label, BorderLayout.CENTER);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            pack();
            setVisible(false);
            setAlwaysOnTop(true);
        }

        /**
         * Sets the message.
         * 
         * @param msg
         *            the new message
         */
        public void setMessage(final String msg)
        {
            this.label.setText(msg);
            pack();
        }

        /* (non-Javadoc)
         * @see java.awt.Window#setVisible(boolean)
         */
        @Override
        public void setVisible(final boolean visible)
        {
            if (!isVisible() && visible)
            {
                super.setVisible(true);
                GUIUtils.centerOnPrimaryScreen(this);
            }
            else if (isVisible() && !visible)
            {
                super.setVisible(false);
            }
        }
    }

    /**
     * This is a temporary fix for the videos not showing in fullscreen timeline
     * mode.
     */
    public static final boolean USE_TRUE_FULL_SCREEN = System.getProperty("os.name").toLowerCase().indexOf("mac") > -1;

    public static final Cursor NO_CURSOR = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(16, 16, new int[16 * 16], 0, 16)),
            new Point(0, 0), "invisibleCursor");

    /**
     * Manages a waiting cursor. Make sure you always pass the same window
     * argument.
     */
    private static int computationCount = 0;

    private static WaitMessageWindow computationMessageWindow = new WaitMessageWindow();

    private static Vector<AdvancedKeyListenerFilter> advancedListeners = new Vector<AdvancedKeyListenerFilter>();

    /**
     * Adds an advanced key listener to a component, or adds a global listener
     * if the component is null.
     * 
     * This method will remove the auto-repeat feature, which is most of the
     * time unwanted. There is a separate option to activate an autorepeat that
     * is faster than the system one and triggers a separate callback.
     * 
     * @param component
     *            the component
     * @param listener
     *            the listener
     * @param autorepeat
     *            the autorepeat
     */
    public static void addAdvancedKeyListener(final Component component, final AdvancedKeyListener listener, final boolean autorepeat)
    {
        final AdvancedKeyListenerFilter filterListener = new AdvancedKeyListenerFilter(listener, autorepeat);
        if (component != null)
        {
            // add a component listener
            component.addKeyListener(filterListener);
        }
        else
        {
            // add a global listener
            Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener()
            {
                /**
				 * 
				 */
                @Override
                public void eventDispatched(final AWTEvent event)
                {
                    if (event instanceof KeyEvent)
                    {
                        switch (event.getID())
                        {
                            case KeyEvent.KEY_PRESSED:
                                filterListener.keyPressed((KeyEvent) event);
                            break;
                            case KeyEvent.KEY_RELEASED:
                                filterListener.keyReleased((KeyEvent) event);
                            break;
                            case KeyEvent.KEY_TYPED:
                                filterListener.keyTyped((KeyEvent) event);
                            break;
                        }
                    }
                }
            }, AWTEvent.KEY_EVENT_MASK);
        }
        advancedListeners.add(filterListener);
    }

    /**
     * Adds the global key listener.
     * 
     * @param keycode
     *            the keycode
     * @param listener
     *            the listener
     * @param command
     *            the command
     */
    public static void addGlobalKeyListener(final int keycode, final ActionListener listener, final String command)
    {
        addGlobalKeyListener(keycode, 0, listener, command);
    }

    /**
     * Adds the global key listener.
     * 
     * @param keycode
     *            the keycode
     * @param modifiers
     *            the modifiers
     * @param listener
     *            the listener
     * @param command
     *            the command
     */
    public static void addGlobalKeyListener(final int keycode, final int modifiers, final ActionListener listener, final String command)
    {
        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener()
        {
            /**
			 * 
			 */
            @Override
            public void eventDispatched(final AWTEvent event)
            {
                if ((event instanceof KeyEvent && event.getID() == KeyEvent.KEY_PRESSED) && (((KeyEvent) event).getKeyCode() == keycode) && (((KeyEvent) event).getModifiers() == modifiers))
                {
                    listener.actionPerformed(new ActionEvent(new GUIUtils(), KeyEvent.KEY_PRESSED, command));
                }
            }
        }, AWTEvent.KEY_EVENT_MASK);
    }

    /**
     * Begin long computation.
     * 
     * @param window
     *            the window
     * @param message
     *            the message
     */
    public static void beginLongComputation(final Window window, final String message)
    {
        computationCount++;
        computationMessageWindow.setMessage(message);
        if (computationCount == 1)
        {
            window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            computationMessageWindow.setVisible(true);
        }
    }

    /**
     * Bound 01.
     * 
     * @param x
     *            the x
     * @return the float
     */
    private static float bound01(final float x)
    {
        float result;

        if (x < 0)
        {
            result = 0;
        }
        else if (x > 1)
        {
            result = 1;
        }
        else
        {
            result = x;
        }

        //
        return result;
    }

    /**
     * Centers a window on the primary display.
     * 
     * @param toplevel
     *            the toplevel
     */
    public static void centerOnPrimaryScreen(final Window toplevel)
    {
        Dimension screenRes = Toolkit.getDefaultToolkit().getScreenSize();
        toplevel.setLocation((screenRes.width - toplevel.getWidth()) / 2, (screenRes.height - toplevel.getHeight()) / 2);
    }

    /**
     * Centers a window on the primary display.
     * 
     * @param toplevel
     *            the toplevel
     * @param newwidth
     *            the newwidth
     * @param newheight
     *            the newheight
     */
    public static void centerOnPrimaryScreen(final Window toplevel, final int newwidth, final int newheight)
    {
        Dimension screenRes = Toolkit.getDefaultToolkit().getScreenSize();
        toplevel.setBounds((screenRes.width - newwidth) / 2, (screenRes.height - newheight) / 2, newwidth, newheight);
    }

    /**
     * This is a simpler version of toMonochrome.
     * 
     * @param source
     *            the source
     * @param c
     *            the c
     * @param amount
     *            the amount
     * @return the image
     */
    public static Image colorizeImage(final Image source, final Color c, final float amount)
    {
        Image result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            BufferedImage buffer = new BufferedImage(source.getWidth(null), source.getHeight(null), BufferedImage.TYPE_INT_ARGB);
            Graphics graphic = buffer.getGraphics();
            graphic.drawImage(source, 0, 0, null);
            graphic.setColor(multiplyAlpha(c, amount));
            graphic.fillRect(0, 0, buffer.getWidth(null), buffer.getHeight(null));
            result = buffer;
        }

        //
        return result;
    }

    /**
     * Clears an image (makes it totally transparent).
     * 
     * @param width
     *            the width
     * @param height
     *            the height
     * @return the image
     */
    public static Image createTransparentImage(final int width, final int height)
    {
        Image result;

        BufferedImage buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphic = buffer.createGraphics();
        graphic.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR, 0.0f));
        Rectangle2D.Double rect = new Rectangle2D.Double(0, 0, width, height);
        graphic.fill(rect);

        result = buffer;

        //
        return result;
    }

    /**
     * End long computation.
     * 
     * @param window
     *            the window
     */
    public static void endLongComputation(final Window window)
    {
        computationCount -= 1;
        if (computationCount == 0)
        {
            window.setCursor(Cursor.getDefaultCursor());
            window.toFront();
            computationMessageWindow.setVisible(false);
        }
    }

    /**
     * Makes a window full screen (method 2).
     * 
     * @param toplevel
     *            the toplevel
     */
    public static void fillPrimaryScreen(final Window toplevel)
    {
        Dimension screenRes = Toolkit.getDefaultToolkit().getScreenSize();
        toplevel.setBounds(0, 0, screenRes.width, screenRes.height);
    }

    /**
     * Grow.
     * 
     * @param r
     *            the r
     * @param amountx
     *            the amountx
     * @param amounty
     *            the amounty
     */
    public static void grow(final Rectangle2D r, final float amountx, final float amounty)
    {
        r.setRect(r.getX() - amountx, r.getY() - amounty, r.getWidth() + amountx * 2, r.getHeight() + amounty * 2);
    }

    /**
     * Mixes two colors.
     * 
     * @param c0
     *            the c 0
     * @param c1
     *            the c 1
     * @param amount
     *            the amount
     * @return the color
     */
    public static Color mix(final Color c0, final Color c1, final float amount)
    {
        Color result;

        float r0 = c0.getRed() / 255f;
        float g0 = c0.getGreen() / 255f;
        float b0 = c0.getBlue() / 255f;
        float a0 = c0.getAlpha() / 255f;
        float r1 = c1.getRed() / 255f;
        float g1 = c1.getGreen() / 255f;
        float b1 = c1.getBlue() / 255f;
        float a1 = c1.getAlpha() / 255f;

        result = new Color(bound01(r0 + (r1 - r0) * amount), bound01(g0 + (g1 - g0) * amount), bound01(b0 + (b1 - b0) * amount), bound01(a0 + (a1 - a0) * amount));

        //
        return result;
    }

    /**
     * Changes the opacity of a color.
     * 
     * @param c0
     *            the c 0
     * @param alpha
     *            the alpha
     * @return the color
     */
    public static Color multiplyAlpha(final Color c0, final float alpha)
    {
        Color result;

        if (alpha == 1)
        {
            result = c0;
        }
        else
        {
            float r0 = c0.getRed() / 255f;
            float g0 = c0.getGreen() / 255f;
            float b0 = c0.getBlue() / 255f;
            float a0 = c0.getAlpha() / 255f;

            result = new Color(r0, g0, b0, bound01(a0 * alpha));
        }

        //
        return result;
    }

    /**
     * Repeat keys.
     * 
     * @return true, if successful
     */
    public static boolean repeatKeys()
    {
        boolean result;

        int count = advancedListeners.size();
        result = false;
        for (int index = 0; index < count; index++)
        {
            result |= advancedListeners.elementAt(index).repeatKeys();
        }

        //
        return result;
    }

    /**
     * Round image.
     * 
     * @param source
     *            the source
     * @param radius
     *            the radius
     * @param alpha
     *            the alpha
     * @return the image
     */
    public static Image roundImage(final Image source, final int radius, final float alpha)
    {
        Image result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            int width = source.getWidth(null);
            int height = source.getHeight(null);
            result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphic = (Graphics2D) result.getGraphics();
            graphic.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR, 0.0f));
            Rectangle2D.Double rect = new Rectangle2D.Double(0, 0, width, height);
            graphic.fill(rect);
            graphic.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
            graphic.clip(new RoundRectangle2D.Float(0, 0, width - 1, height - 1, radius, radius));
            graphic.drawImage(source, 0, 0, null);
        }

        //
        return result;
    }

    // The following adds a separate & fast auto-repeat feature to the
    // keyboard event handling mechanism, and removes the default
    // auto-repeat.

    /**
     * Makes a window full screen (method 1). Returns false if not supported.
     * 
     * @param toplevel
     *            the toplevel
     * @param fullscreen
     *            the fullscreen
     * @return true, if successful
     */
    public static boolean setFullScreen(final Window toplevel, final boolean fullscreen)
    {
        boolean result;

        GraphicsDevice device;
        device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        if (USE_TRUE_FULL_SCREEN && device.isFullScreenSupported())
        {
            boolean currentlyfullscreen = device.getFullScreenWindow() == toplevel;
            if (fullscreen != currentlyfullscreen)
            {
                device.setFullScreenWindow(toplevel);
            }
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Converts an image to grayscale.
     * 
     * @param source
     *            the source
     * @return the image
     */
    public static Image toGrayscale(final Image source)
    {
        Image result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            BufferedImage src = new BufferedImage(source.getWidth(null), source.getHeight(null), BufferedImage.TYPE_INT_ARGB);
            src.getGraphics().drawImage(source, 0, 0, null);
            BufferedImageOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
            result = op.filter(src, null);
        }

        //
        return result;
    }

    /**
     * For testing lookup tables.
     * 
     * @param source
     *            the source
     * @return the image
     */
    public static Image toInverseVideo(final Image source)
    {
        Image result;

        //
        BufferedImage src = new BufferedImage(source.getWidth(null), source.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics graphic = src.getGraphics();
        graphic.drawImage(source, 0, 0, null);
        //
        short[][] lookup = new short[4][256];
        for (int c = 0; c < 4; c++)
        {
            for (short b = 0; b < 256; b++)
            {
                if (c == 3)
                {
                    lookup[c][b] = b;
                }
                else
                {
                    lookup[c][b] = (short) (255 - b);
                }
            }
        }
        LookupTable table = new ShortLookupTable(0, lookup);
        LookupOp op = new LookupOp(table, null);

        result = op.filter(src, null);

        //
        return result;
    }

    /**
     * Creates a monochrome image.
     * 
     * @param source
     *            the source
     * @param color
     *            the color
     * @param opacity
     *            the opacity
     * @return the image
     */
    public static Image toMonochrome(final Image source, final Color color, final float opacity)
    {
        Image result;

        // If we don't do this, createImage will not compute the image right
        // away
        BufferedImage src = new BufferedImage(source.getWidth(null), source.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics graphic = src.getGraphics();
        graphic.drawImage(source, 0, 0, null);
        //
        final int r0 = color.getRed();
        final int g0 = color.getGreen();
        final int b0 = color.getBlue();
        final int a0 = (int) (opacity * 255);
        //
        RGBImageFilter filter = new RGBImageFilter()
        {
            {
                // The filter's operation does not depend on the
                // pixel's location, so IndexColorModels can be
                // filtered directly.
                this.canFilterIndexColorModel = true;
            }

            /**
			 * 
			 */
            @Override
            public int filterRGB(final int x, final int y, final int rgba)
            {
                int result;

                int r = (rgba >> 16) & 0xff;
                int g = (rgba >> 8) & 0xff;
                int b = rgba & 0xff;
                // int a = (rgba >> 24) & 0xff ;

                float br = (r * 0.8f + g * 0.65f + b) / 625f;
                if (br < 0.5f)
                {
                    r = (int) (r0 * br * 2);
                    g = (int) (g0 * br * 2);
                    b = (int) (b0 * br * 2);
                }
                else
                {
                    r = Math.min(255, (int) (r0 + (br - 0.5f) * 255 * 2f));
                    g = Math.min(255, (int) (g0 + (br - 0.5f) * 255 * 2f));
                    b = Math.min(255, (int) (b0 + (br - 0.5f) * 255 * 2f));
                }

                result = ((a0 << 24) | (r << 16) | (g << 8) | b);

                //
                return result;
            }
        };

        //
        ImageProducer producer = new FilteredImageSource(src.getSource(), filter);
        result = Toolkit.getDefaultToolkit().createImage(producer);

        //
        return result;
    }

    /**
     * Update computation message.
     * 
     * @param message
     *            the message
     */
    public static void updateComputationMessage(final String message)
    {
        computationMessageWindow.setMessage(message);
    }
}
