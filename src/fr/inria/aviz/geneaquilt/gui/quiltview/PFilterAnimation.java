/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import edu.umd.cs.piccolo.activities.PActivity;
import edu.umd.cs.piccolo.activities.PInterpolatingActivity;

/**
 * The Class <b>PFilterAnimation</b> manages the animation of a filtered
 * animation
 * 
 * @author Pierre Dragicevic
 */
public class PFilterAnimation extends PInterpolatingActivity
{
    /**
     * <b>Target</b> Objects that want their color to be set by the color
     * activity must implement this interface.
     */
    public interface Target
    {
        /**
         * End filtering.
         */
        void endFiltering();

        /**
         * Gets the filtering parameter.
         * 
         * @return the filtering parameter
         */
        float getFilteringParameter();

        /**
         * Sets the filtering parameter.
         * 
         * @param filter
         *            the new filtering parameter
         */
        void setFilteringParameter(float filter);

        /**
         * Start filtering.
         * 
         * @param destFilter
         *            the dest filter
         */
        void startFiltering(float destFilter);
    }

    private float source;
    private float destination;
    private Target target;

    private static PFilterAnimation currentAnimation = null;

    /**
     * Create a new PDOIAnimationActivity.
     * <P>
     * 
     * @param duration
     *            the length of one loop of the activity
     * @param stepRate
     *            the amount of time between steps of the activity
     * @param aTarget
     *            the object that the activity will be applied to and where the
     *            source state will be taken from.
     * @param aDestination
     *            the destination filtering state. 0 is unfiltered, 1 is
     *            filtered.
     */
    public PFilterAnimation(final long duration, final long stepRate, final Target aTarget, final float aDestination)
    {
        super(duration, stepRate);
        this.target = aTarget;
        this.destination = aDestination;
        if (currentAnimation != null && currentAnimation != this && currentAnimation.isStepping())
        {
            currentAnimation.terminate(PActivity.TERMINATE_WITHOUT_FINISHING);
        }
        setSlowInSlowOut(false);
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.activities.PInterpolatingActivity#activityFinished()
     */
    @Override
    protected void activityFinished()
    {
        this.source = this.target.getFilteringParameter();
        this.target.endFiltering();
        super.activityFinished();
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.activities.PInterpolatingActivity#activityStarted()
     */
    @Override
    protected void activityStarted()
    {
        this.source = this.target.getFilteringParameter();
        this.target.startFiltering(this.destination);
        super.activityStarted();
        currentAnimation = this;
    }

    /**
     * Gets the destination filtering.
     * 
     * @return the destination filtering
     */
    public float getDestinationFiltering()
    {
        return this.destination;
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.activities.PActivity#isAnimation()
     */
    @Override
    protected boolean isAnimation()
    {
        return true;
    }

    /**
     * Sets the destination filtering.
     * 
     * @param newDestination
     *            the new value
     */
    public void setDestinationFiltering(final float newDestination)
    {
        this.destination = newDestination;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRelativeTargetValue(final float zeroToOne)
    {
        super.setRelativeTargetValue(zeroToOne);
        float filteringParameter = this.source + (this.destination - this.source) * zeroToOne;
        this.target.setFilteringParameter(filteringParameter);
    }
}
