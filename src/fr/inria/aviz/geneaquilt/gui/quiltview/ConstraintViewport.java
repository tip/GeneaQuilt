/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;

/**
 * The Class ConstraintViewport.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class ConstraintViewport implements PropertyChangeListener
{
    private PCanvas canvas;
    private PNode root;
    private List<PNode> list = new ArrayList<PNode>();
    private boolean enabled = true;

    /**
     * Creates a constraint viewport.
     */
    public ConstraintViewport()
    {
    }

    /**
     * Connects to the canvas, looking at the specified root node.
     * 
     * @param canvas
     *            the canvas holding the camera
     * @param root
     *            the root node
     */
    public void connect(final PCanvas canvas, final PNode root)
    {
        this.canvas = canvas;
        this.root = root;
        canvas.getCamera().addPropertyChangeListener(this);
    }

    /**
     * Disconnects.
     */
    public void disconnect()
    {
        if (this.canvas != null)
        {
            this.canvas.getCamera().removePropertyChangeListener(this);
            this.canvas = null;
            this.root = null;
        }
    }

    /**
     * Maybe move view.
     */
    protected void maybeMoveView()
    {
        if (this.enabled)
        {
            PBounds bounds = this.canvas.getCamera().getViewBounds();
            this.list.clear();
            this.root.findIntersectingNodes(bounds, (ArrayList<PNode>) this.list);
            if (this.list.size() < 2)
            {
                PBounds fb = this.root.getFullBounds();
                fb.x = bounds.x;
                fb.width = bounds.width;
                this.list.clear();
                this.root.findIntersectingNodes(fb, (ArrayList<PNode>) this.list);

                if (this.list.size() >= 2)
                {
                    PBounds stripBounds = new PBounds();
                    for (PNode node : this.list)
                    {
                        if (!(node instanceof QuiltManager))
                        {
                            stripBounds.add(node.getBoundsReference());
                        }
                    }
                    if (bounds.getMaxY() < stripBounds.getMinY())
                    {
                        // the view is below the visible portion
                        bounds.y = stripBounds.getMinY();
                        this.canvas.getCamera().animateViewToPanToBounds(bounds, 200);
                    }
                    else if (bounds.getMinY() > stripBounds.getMaxY())
                    {
                        bounds.y = stripBounds.getMaxY() - bounds.height;
                        this.canvas.getCamera().animateViewToPanToBounds(bounds, 200);
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propertyChange(final PropertyChangeEvent event)
    {
        maybeMoveView();
    }

    /**
     * Enables/disables the constraint on the viewport.
     * 
     * @param enabled
     *            the state to set
     */
    public void setEnabled(final boolean enabled)
    {
        this.enabled = enabled;
    }
}
