/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.freehep.graphicsbase.util.export.ExportDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PLayer;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import fr.devinsy.util.StringList;
import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.nodes.PVertex;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.gui.nodes.TextOutlineManager;
import fr.inria.aviz.geneaquilt.gui.nodes.TimeLine;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.DOIManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.SelectionManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.SlidingController;
import fr.inria.aviz.geneaquilt.gui.util.GUIToolBox;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;
import fr.inria.aviz.geneaquilt.gui.util.PrintUtilities;
import fr.inria.aviz.geneaquilt.model.DateRange;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;
import fr.inria.aviz.geneaquilt.model.VertexComparator.Sorting;

/**
 * The Class GeneaQuilt.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class GeneaQuiltPanel extends JPanel implements PFilterAnimation.Target
{
    private static final long serialVersionUID = -5286050885229087472L;

    private Logger logger = LoggerFactory.getLogger(GeneaQuiltPanel.class);

    private int searchFlags = Pattern.LITERAL | Pattern.CASE_INSENSITIVE;
    private Network network;
    private QuiltManager quiltManager;
    private TextOutlineManager outlineManager;

    private BirdsEyeView brdsvwBev;
    private JSplitPane jspltpnViewControlSplit;
    private JSplitPane jspltpnTimelineSplit;
    private DetailsTable dtlstblDetailsTable;
    private JLabel jlblSearchLabel;
    private JTextField jtxtfldSearchField;
    private JCheckBox jchckbxLiteralFlag;
    private JCheckBox jchckbxCaseSensitiveFlag;
    private JComboBox jcmbxFieldList;
    private PCanvas pcnvsCanvas;
    private PCanvas pcnvsTimeLineCanvas;
    private JToggleButton jtglbtnFilterBox;

    private TimeLine timeLine;
    private DOIManager doiManager;
    // private PLayer bgLayer;
    private PLayer mainLayer;
    // private PLayer fgLayer;

    /** Set this to false to turn filtering animation off. */
    private boolean filteringAnimationEnabled = true;
    private float filteringParameter = 0;

    private PNode centerNode;
    private Point2D centerNodePreviousPosition;

    /**
     * Creates a GeneaQuilt with a specified PCanvas to draw to.
     * 
     * @param parent
     *            the parent
     * @param source
     *            the source
     * @wbp.parser.constructor
     */
    public GeneaQuiltPanel(final JComponent parent, final Network source)
    {
        this(parent, source, Sorting.NONE);
    }

    /**
     * Creates a GeneaQuilt with a specified PCanvas to draw to.
     * 
     * @param parent
     *            the parent
     * @param source
     *            the source
     * @param vertexSorting
     *            the vertex sorting
     */
    public GeneaQuiltPanel(final JComponent parent, final Network source, final Sorting vertexSorting)
    {
        if (source == null)
        {
            return;
        }

        this.network = source;
        // /////////////////////////////

        //
        this.pcnvsCanvas = new PCanvas()
        {
            private static final long serialVersionUID = -4065477775754975129L;

            /**
             * {@inheritDoc}
             */
            @Override
            public void print(final Graphics graphic)
            {
                try
                {
                    Printer.setPrinting(true);
                    super.print(graphic);
                }
                finally
                {
                    Printer.setPrinting(false);
                    graphic.dispose();
                }
            }
        };
        this.pcnvsCanvas.requestFocus();

        this.mainLayer = this.pcnvsCanvas.getLayer();

        // Quilt
        this.quiltManager = new QuiltManager(this.network, vertexSorting);
        this.mainLayer.addChild(this.quiltManager);

        // Selections
        PNode selectionLayer = new PNode();
        this.mainLayer.addChild(selectionLayer);
        SelectionManager selectionManager = new SelectionManager(this.quiltManager, selectionLayer);
        this.quiltManager.setSelectionManager(selectionManager);

        // Text outlines
        this.outlineManager = new TextOutlineManager(this.quiltManager);
        this.mainLayer.addChild(this.outlineManager);

        ConstraintViewport cvp = new ConstraintViewport();
        cvp.setEnabled(false);
        cvp.connect(this.pcnvsCanvas, this.quiltManager);

        this.pcnvsCanvas.setZoomEventHandler(null);
        this.pcnvsCanvas.addInputEventListener(new MouseWheelZoomController());
        DisabablePanEventHandler panHandler = new DisabablePanEventHandler();
        // canvas.addInputEventListener(new MouseSelectionController(manager,
        // panHandler));
        // canvas.addInputEventListener(new LinkSlidingController(manager,
        // panHandler));
        this.pcnvsCanvas.addInputEventListener(new SlidingController(this.quiltManager, panHandler));
        this.pcnvsCanvas.setPanEventHandler(panHandler);

        // //////////////// BEV Panel \\\\\\\\\\\\\\\\\\\\\\
        {
            this.brdsvwBev = new BirdsEyeView(this.quiltManager.getHull());
            PLayer[] bevLayers = new PLayer[] { this.mainLayer }; // , bgLayer,
            // fgLayer};
            this.brdsvwBev.connect(this.pcnvsCanvas, bevLayers);
            this.brdsvwBev.setPreferredSize(new Dimension(150, 150));
        }

        // //////////////// CONTROL Panel \\\\\\\\\\\\\\\\\\\\\\
        Box controlsBox = Box.createVerticalBox();

        //
        controlsBox.add(Box.createVerticalStrut(3));

        //
        Box buttonsBox = new Box(BoxLayout.X_AXIS);
        controlsBox.add(buttonsBox);
        {
            //
            JButton btnViewAll = new JButton("View All");
            btnViewAll.setMaximumSize(new Dimension(Short.MAX_VALUE, 100));
            btnViewAll.addActionListener(new ActionListener()
            {
                /**
                 * 
                 */
                @Override
                public void actionPerformed(final ActionEvent event)
                {
                    viewAll();
                }
            });

            buttonsBox.add(btnViewAll);

            //
            JButton btnExportGraphic = new JButton("Export…");
            btnExportGraphic.setMaximumSize(new Dimension(Short.MAX_VALUE, 100));
            btnExportGraphic.addActionListener(new ActionListener()
            {
                /**
                 * 
                 */
                @Override
                public void actionPerformed(final ActionEvent event)
                {
                    if (GeneaQuiltPanel.this.pcnvsCanvas != null)
                    {
                        ExportDialog export = new ExportDialog();
                        export.showExportDialog(GeneaQuiltPanel.this, "Export view as…", GeneaQuiltPanel.this.pcnvsCanvas, GeneaQuiltPanel.this.network.getName());
                    }
                }
            });

            buttonsBox.add(btnExportGraphic);
        }

        //
        controlsBox.add(Box.createVerticalStrut(3));

        //
        buttonsBox = new Box(BoxLayout.X_AXIS);
        controlsBox.add(buttonsBox);
        {
            //
            JButton btnViewAll = new JButton("Copy panels");
            btnViewAll.setToolTipText("Copy panels to clipboard");
            btnViewAll.setMaximumSize(new Dimension(Short.MAX_VALUE, 100));
            btnViewAll.addActionListener(new ActionListener()
            {
                /**
                 * 
                 */
                @Override
                public void actionPerformed(final ActionEvent event)
                {
                    BufferedImage image = GUIToolBox.takeScreenshot(GeneaQuiltPanel.this);

                    image = GUIToolBox.crop(image);

                    GUIToolBox.copyToClipboard(image);

                    if (GeneaQuiltPanel.this.quiltManager.sorting == Sorting.DATERANGE)
                    {
                        GeneaQuiltPanel.this.quiltManager.sorting = Sorting.COMPONENT_DATERANGE;
                    }
                    else
                    {
                        GeneaQuiltPanel.this.quiltManager.sorting = Sorting.DATERANGE;
                    }
                }
            });

            buttonsBox.add(btnViewAll);

            //
            JButton btnExportGraphic = new JButton("Copy canvas");
            btnExportGraphic.setToolTipText("Copy canvas to clipboard");
            btnExportGraphic.setMaximumSize(new Dimension(Short.MAX_VALUE, 100));
            btnExportGraphic.addActionListener(new ActionListener()
            {
                /**
                 * 
                 */
                @Override
                public void actionPerformed(final ActionEvent event)
                {
                    BufferedImage image = GUIToolBox.takeScreenshot(getCanvas());

                    image = GUIToolBox.crop(image);

                    GUIToolBox.copyToClipboard(image);
                }
            });

            buttonsBox.add(btnExportGraphic);
        }

        //
        controlsBox.add(Box.createVerticalStrut(2));

        //
        Box labelSettingBox = new Box(BoxLayout.X_AXIS)
        {
            private static final long serialVersionUID = -974480947341092661L;

            /**
             * 
             */
            @Override
            public Dimension getMaximumSize()
            {
                Dimension result;

                result = getPreferredSize();
                result.width = Short.MAX_VALUE;

                //
                return result;
            }
        };
        {
            //
            JLabel label = new JLabel("Label: ");
            labelSettingBox.add(label);

            //
            JComboBox comboBox = new JComboBox();
            comboBox.setModel(new DefaultComboBoxModel(new String[] { "Label by Name", "Label by Surname", "Label by Given Name" }));
            comboBox.setSelectedIndex(0);
            comboBox.addActionListener(new ActionListener()
            {
                /**
                 * 
                 */
                @Override
                public void actionPerformed(final ActionEvent event)
                {
                    JComboBox comboBox = (JComboBox) event.getSource();
                    switch (comboBox.getSelectedIndex())
                    {
                        case 0:
                            setLabelBy("NAME");
                        break;

                        case 1:
                            setLabelBy("NAME.SURN");
                        break;

                        case 2:
                            setLabelBy("NAME.GIVN");
                        break;

                        default:
                    }

                }
            });
            labelSettingBox.add(comboBox);
        }
        controlsBox.add(labelSettingBox);

        //
        controlsBox.add(Box.createVerticalStrut(2));

        //
        Box searchBox = new Box(BoxLayout.X_AXIS)
        {
            private static final long serialVersionUID = -3514781558270743186L;

            /**
             * 
             */
            @Override
            public Dimension getMaximumSize()
            {
                Dimension result;

                result = getPreferredSize();
                result.width = Short.MAX_VALUE;

                //
                return result;
            }
        };

        //
        this.jlblSearchLabel = new JLabel("Search");
        this.jlblSearchLabel.setPreferredSize(new Dimension(90, (int) this.jlblSearchLabel.getPreferredSize().getHeight()));

        searchBox.add(this.jlblSearchLabel);

        //
        this.jtxtfldSearchField = new JTextField(20);
        this.jlblSearchLabel.setLabelFor(this.jtxtfldSearchField);
        this.jtxtfldSearchField.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                selectSearch(GeneaQuiltPanel.this.jtxtfldSearchField.getText(), (String) GeneaQuiltPanel.this.jcmbxFieldList.getSelectedItem(), GeneaQuiltPanel.this.searchFlags);
                GeneaQuiltPanel.this.jtxtfldSearchField.setText("");
            }
        });
        this.jtxtfldSearchField.getDocument().addDocumentListener(new DocumentListener()
        {
            /**
             * 
             */
            @Override
            public void changedUpdate(final DocumentEvent event)
            {
                search();
            }

            /*
             * (non-Javadoc)
             * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
             */
            @Override
            public void insertUpdate(final DocumentEvent event)
            {
                search();
            }

            /**
             * 
             */
            @Override
            public void removeUpdate(final DocumentEvent event)
            {
                search();
            }
        });

        searchBox.add(this.jtxtfldSearchField);

        //
        this.jchckbxLiteralFlag = new JCheckBox("Literal");
        this.jchckbxLiteralFlag.setSelected((this.searchFlags & Pattern.LITERAL) != 0);
        this.jchckbxLiteralFlag.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                if (GeneaQuiltPanel.this.jchckbxLiteralFlag.isSelected())
                {
                    GeneaQuiltPanel.this.searchFlags |= Pattern.LITERAL;
                }
                else
                {
                    GeneaQuiltPanel.this.searchFlags &= (~Pattern.LITERAL);
                }

                search();
            }
        });

        searchBox.add(this.jchckbxLiteralFlag);

        //
        this.jchckbxCaseSensitiveFlag = new JCheckBox("NoCase");
        this.jchckbxCaseSensitiveFlag.setSelected((this.searchFlags & Pattern.CASE_INSENSITIVE) != 0);
        this.jchckbxCaseSensitiveFlag.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                if (GeneaQuiltPanel.this.jchckbxCaseSensitiveFlag.isSelected())
                {
                    GeneaQuiltPanel.this.searchFlags |= Pattern.CASE_INSENSITIVE;
                }
                else
                {
                    GeneaQuiltPanel.this.searchFlags &= (~Pattern.CASE_INSENSITIVE);
                }

                search();
            }
        });

        searchBox.add(this.jchckbxCaseSensitiveFlag);

        //
        controlsBox.add(searchBox);

        //
        Set<String> propertySet = getProperties();
        propertySet.add("*");
        this.jcmbxFieldList = new JComboBox(propertySet.toArray())
        {
            private static final long serialVersionUID = 120848256709927611L;

            /**
             * 
             */
            @Override
            public Dimension getMaximumSize()
            {
                Dimension size = getPreferredSize();
                size.width = Short.MAX_VALUE;
                return size;
            }
        };

        controlsBox.add(this.jcmbxFieldList);

        //
        // details.getContentPane().add(searchBox, BorderLayout.NORTH);
        this.dtlstblDetailsTable = new DetailsTable(getSelectionManager());
        this.dtlstblDetailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.dtlstblDetailsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
        {
            /**
             * 
             */
            @Override
            public void valueChanged(final ListSelectionEvent event)
            {
                if (!event.getValueIsAdjusting())
                {
                    int row = GeneaQuiltPanel.this.dtlstblDetailsTable.getSelectedRow();
                    if (row >= 0)
                    {
                        String field = (String) GeneaQuiltPanel.this.dtlstblDetailsTable.getValueAt(row, 0);
                        if ("ID".equals(field))
                        {
                            String id = (String) GeneaQuiltPanel.this.dtlstblDetailsTable.getValueAt(row, 1);
                            showNodeId(id);
                        }
                    }
                }
            }
        });
        controlsBox.add(this.dtlstblDetailsTable.getScrollPane());

        //
        this.jtglbtnFilterBox = new JToggleButton("Filter")
        {
            private static final long serialVersionUID = -6206357399345303550L;

            /**
             * 
             */
            @Override
            public Dimension getMaximumSize()
            {
                Dimension result;
                result = getPreferredSize();
                result.width = Short.MAX_VALUE;

                //
                return result;
            }
        };

        this.jtglbtnFilterBox.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                filterItems(event.getStateChange() == ItemEvent.SELECTED);
            }
        });

        controlsBox.add(this.jtglbtnFilterBox);
        // Control Panel over.

        // //////////////// OverviewSplit Panel \\\\\\\\\\\\\\\\\\\\\\
        JSplitPane overviewSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.brdsvwBev, controlsBox);
        overviewSplit.setMinimumSize(new Dimension(200, 0));
        overviewSplit.setPreferredSize(new Dimension(200, 0));
        overviewSplit.setResizeWeight(.25);
        overviewSplit.setDividerSize(6);

        // //////////////// ViewControlSplit Panel \\\\\\\\\\\\\\\\\\\\\\
        this.pcnvsCanvas.setPreferredSize(new Dimension(0, 0));
        this.jspltpnViewControlSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.pcnvsCanvas, overviewSplit);
        this.jspltpnViewControlSplit.setPreferredSize(new Dimension(500, 0));
        this.jspltpnViewControlSplit.setResizeWeight(.95);
        this.jspltpnViewControlSplit.setDividerSize(6);

        // //////////////// TimeLine Panel \\\\\\\\\\\\\\\\\\\\\\
        this.timeLine = new TimeLine(this.quiltManager);
        DateRange fullRange = this.timeLine.getFullRange();
        if (fullRange == null || !fullRange.isValid())
        {
            this.timeLine = null;
        }

        this.jspltpnTimelineSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        this.jspltpnTimelineSplit.setRightComponent(this.jspltpnViewControlSplit);

        if (this.timeLine == null)
        {
            this.pcnvsTimeLineCanvas = null;

            this.jspltpnTimelineSplit.setDividerLocation(0);
            this.jspltpnTimelineSplit.setDividerSize(0);
        }
        else
        {
            this.pcnvsTimeLineCanvas = new PCanvas();
            this.pcnvsTimeLineCanvas.setZoomEventHandler(null);
            this.pcnvsTimeLineCanvas.setPanEventHandler(null);
            this.pcnvsTimeLineCanvas.setPreferredSize(new Dimension(Integer.MAX_VALUE, 50));
            this.timeLine.connect(this.pcnvsCanvas, this.pcnvsTimeLineCanvas);
            this.quiltManager.getSelectionManager().addChangeListener(this.timeLine);
            this.pcnvsTimeLineCanvas.getLayer().addChild(this.timeLine);

            this.jspltpnTimelineSplit.setLeftComponent(this.pcnvsTimeLineCanvas);

            this.jspltpnTimelineSplit.setDividerLocation((int) this.pcnvsTimeLineCanvas.getPreferredSize().getHeight());
            this.jspltpnTimelineSplit.setDividerSize(6);
        }

        //
        // setBorder(new EmptyBorder(5, 5, 5, 5));
        setLayout(new BorderLayout(0, 0));
        add(this.jspltpnTimelineSplit, BorderLayout.CENTER);

        validate();

        // FIXME
        // SwingUtilities.invokeLater(new Runnable()
        // {
        // /**
        // *
        // */
        // @Override
        // public void run()
        // {
        // GeneaQuiltPanel.this.brdsvwBev.autoScale();
        // GeneaQuiltPanel.this.brdsvwBev.updateFromViewed();
        // }
        // });
    }

    /**
     * Clear.
     */
    public void clear()
    {
        if (this.quiltManager != null)
        {
            getSelectionManager().clearSelections();
            this.quiltManager.removeAllChildren();
            // bgLayer.removeAllChildren();
            this.mainLayer.removeAllChildren();
            // fgLayer.removeAllChildren();
        }

        if (this.jspltpnViewControlSplit != null)
        {
            remove(this.jspltpnViewControlSplit);
        }

        if (this.jspltpnTimelineSplit != null)
        {
            remove(this.jspltpnTimelineSplit);
        }

        if (this.pcnvsTimeLineCanvas != null)
        {
            this.timeLine = null;
            remove(this.pcnvsTimeLineCanvas);
            this.pcnvsTimeLineCanvas = null;
        }
        // bird.removeAll();
        // bird.setVisible(false);
        // bird.dispose();
        System.gc();
    }

    /**
     * Called by PFilterAnimation. Don't call this yourself.
     */
    @Override
    public void endFiltering()
    {
        // bev.autoScale();
        // if (centerNode != null)
        // canvas.getCamera().animateViewToCenterBounds(centerNode.getFullBoundsReference(),
        // false, 0);
    }

    /**
     * Turns filtering on or off.
     * 
     * @param filter
     *            the filter
     */
    public void filterItems(final boolean filter)
    {

        if (getSelectionManager().isEmpty())
        {
            this.jtglbtnFilterBox.setSelected(false);
            // return;
        }

        float destFilteringParameter;
        if (filter)
        {
            destFilteringParameter = 1;
        }
        else
        {
            destFilteringParameter = 0;
        }

        if (this.filteringParameter != destFilteringParameter)
        {
            if (this.doiManager == null)
            {
                this.doiManager = new DOIManager(getManager());
            }

            if (this.filteringAnimationEnabled)
            {
                PFilterAnimation activity = new PFilterAnimation(500, 50, this, destFilteringParameter);
                this.pcnvsCanvas.getRoot().addActivity(activity);
            }
            else
            {
                startFiltering(destFilteringParameter);
                setFilteringParameter(destFilteringParameter);
                endFiltering();
            }
        }
    }

    /**
     * Gets the bev.
     * 
     * @return the BirdsEyeView
     */
    public BirdsEyeView getBev()
    {
        return this.brdsvwBev;
    }

    /**
     * Gets the canvas.
     * 
     * @return the PCanvas
     */
    public PCanvas getCanvas()
    {
        return this.pcnvsCanvas;
    }

    /**
     * Gets the center node.
     * 
     * @return the center node
     */
    private PNode getCenterNode()
    {
        PNode result;

        ArrayList<PNode> list = new ArrayList<PNode>();
        PBounds viewBounds = this.pcnvsCanvas.getCamera().getViewBounds();
        this.quiltManager.findIntersectingNodes(viewBounds, list);
        result = null;
        double distance = Double.MAX_VALUE;
        for (PNode node : list)
        {
            if ((node instanceof PVertex) || (node instanceof PEdge))
            {
                PBounds bounds = node.getFullBoundsReference();
                double currentDistance = Math.hypot(viewBounds.getCenterX() - bounds.getCenterX(), viewBounds.getCenterY() - bounds.getCenterY());
                if (currentDistance < distance)
                {
                    distance = currentDistance;
                    result = node;
                }
            }
        }

        //
        return result;
    }

    /**
     * Called by PFilterAnimation. Don't call this yourself.
     * 
     * @return the filtering parameter
     */
    @Override
    public float getFilteringParameter()
    {
        return this.filteringParameter;
    }

    /**
     * Gets the manager.
     * 
     * @return the QuiltManager
     */
    public QuiltManager getManager()
    {
        return this.quiltManager;
    }

    /**
     * Gets the network.
     * 
     * @return the network
     */
    public Network getNetwork()
    {
        return this.network;
    }

    /**
     * Gets the properties.
     * 
     * @return the properties
     */
    private Set<String> getProperties()
    {
        Set<String> result;

        result = new TreeSet<String>();
        for (Vertex vertex : this.network.getVertices())
        {
            if (vertex instanceof Indi)
            {
                Indi indi = (Indi) vertex;
                for (String key : indi.getProps().keySet())
                {
                    result.add(key);
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the quilt manager.
     * 
     * @return the quilt manager
     */
    public QuiltManager getQuiltManager()
    {
        return this.quiltManager;
    }

    /**
     * Gets the selection manager.
     * 
     * @return the SelectionManager
     */
    public SelectionManager getSelectionManager()
    {
        return getManager().getSelectionManager();
    }

    /**
     * Computes statistics about the genealogy.
     * 
     * @return the stats.
     */
    public String getStats()
    {
        String result;

        if (this.network == null)
        {
            result = "No network. No statistics. Please, open a network.";
        }
        else
        {
            StringList buffer = new StringList();
            buffer.append("Stats for ").append(this.network.getName()).appendln(":");
            buffer.appendln();

            int individuals = 0;
            int families = 0;
            for (Vertex vertex : this.network.getVertices())
            {
                PNode node = vertex.getNode();
                if (node instanceof PIndi)
                {
                    individuals += 1;
                }
                else if (node instanceof PFam)
                {
                    families += 1;
                }
            }
            int edges = this.network.getEdgeCount();
            int generations = this.getQuiltManager().getIndiGenerations().length;

            buffer.append(generations).appendln(" generations");
            buffer.append(families).appendln(" families");
            buffer.append(individuals).appendln(" individuals");
            buffer.append(edges).appendln(" edges");

            result = buffer.toString();
        }

        //
        return result;
    }

    /**
     * Restore center.
     */
    private void restoreCenter()
    {
        // Force layout
        this.quiltManager.getFullBoundsReference();
        // Update highlights
        getSelectionManager().getHighlightManager().updateHighlightShapes();

        // Re-center camera
        if (this.centerNode != null)
        {
            PBounds cb = this.centerNode.getFullBoundsReference();
            double dx = cb.getCenterX() - this.centerNodePreviousPosition.getX();
            double dy = cb.getCenterY() - this.centerNodePreviousPosition.getY();
            PBounds viewBounds = this.pcnvsCanvas.getCamera().getViewBounds();
            viewBounds.setFrame(viewBounds.getX() + dx, viewBounds.getY() + dy, viewBounds.getWidth(), viewBounds.getHeight());
            this.pcnvsCanvas.getCamera().setViewBounds(viewBounds);
            this.centerNodePreviousPosition.setLocation(cb.getCenterX(), cb.getCenterY());
        }
    }

    /**
     * Save center.
     */
    private void saveCenter()
    {
        this.centerNode = getCenterNode();
        if (this.centerNode != null)
        {
            PBounds bounds = this.centerNode.getFullBoundsReference();
            this.centerNodePreviousPosition = new Point2D.Double(bounds.getCenterX(), bounds.getCenterY());
        }
    }

    /**
     * Search and higligh nodes containing the specified text in the text field.
     **/
    public void search()
    {
        search(this.jtxtfldSearchField.getText(), (String) this.jcmbxFieldList.getSelectedItem(), this.searchFlags);
    }

    /**
     * Search and higligh nodes containing the specified text in the specified
     * field.
     * 
     * @param text
     *            the text to search, empty means reset
     * @param field
     *            the field to search in or null or "*" to mean all.
     * @param flags
     *            Pattern compilation flags
     * @return a collection of matching vertices
     */
    public Collection<Vertex> search(final String text, final String field, final int flags)
    {
        SelectionManager selManager = getSelectionManager();
        String validField;
        if ((field != null) && (field.equals("*")))
        {
            validField = null;
        }
        else
        {
            validField = field;
        }
        int found = 0;
        ArrayList<Vertex> selection = new ArrayList<Vertex>();
        Pattern pattern;
        if (text == null || text.length() == 0)
        {
            pattern = null;
        }
        else
        {
            pattern = Pattern.compile(text, flags);
        }

        Color selColor = GUIUtils.multiplyAlpha(selManager.getNextSelectionColor(), 0.7f);

        for (Vertex vertex : this.network.getVertices())
        {
            if (vertex instanceof Indi)
            {
                Indi indi = (Indi) vertex;
                // if (indi.search(text, field)) {
                if (indi.matches(pattern, validField))
                {
                    found++;
                    PNode pindi = indi.getNode();
                    pindi.setPaint(selColor);
                    selection.add(indi);
                }
                else
                {
                    PNode pindi = indi.getNode();
                    pindi.setPaint(null);
                }
            }
        }
        if (found == 0)
        {
            this.jlblSearchLabel.setText("Search");
            // if (savedSearchBounds != null) {
            // canvas.getCamera().animateViewToPanToBounds(
            // savedSearchBounds,
            // 500);
            // savedSearchBounds = null;
            // }
        }
        else
        {
            this.jlblSearchLabel.setText("Search (" + found + ")");
            // if (text.length()==1 && savedSearchBounds == null) {
            // savedSearchBounds = canvas.getCamera().getViewBounds();
            // }
            Vertex first = selection.get(0);
            this.pcnvsCanvas.getCamera().animateViewToPanToBounds(first.getNode().getFullBounds(), 200);
        }
        return selection;
    }

    /**
     * Search and select the specified string.
     * 
     * @param text
     *            the string to search
     * @param field
     *            the field to search into or null or "*" for all
     * @param flags
     *            the Pattern.compile flags.
     */
    public void selectSearch(final String text, final String field, final int flags)
    {
        this.quiltManager.select(search(text, field, flags));
    }

    /**
     * Called by PFilterAnimation. Don't call this yourself.
     * 
     * @param filter
     *            if = 0, don't filter. If = 1, filter.
     */
    @Override
    public void setFilteringParameter(final float filter)
    {
        if (filter != this.filteringParameter)
        {
            this.filteringParameter = filter;

            final double TINY_SCALE = .01;
            final double SMALL_SCALE = .5;
            final double MEDIUM_SCALE = .8;
            final double FULL_SCALE = 1;
            for (Vertex vertex : this.network.getVertices())
            {
                double doi = vertex.getDOI();
                if (doi > 5)
                {
                    setScale(vertex.getNode(), TINY_SCALE + (1 - TINY_SCALE) * (1 - this.filteringParameter));
                }
                else if (doi > 3)
                {
                    setScale(vertex.getNode(), SMALL_SCALE + (1 - SMALL_SCALE) * (1 - this.filteringParameter));
                }
                else if (doi > 1)
                {
                    setScale(vertex.getNode(), MEDIUM_SCALE + (1 - MEDIUM_SCALE) * (1 - this.filteringParameter));
                }
                else
                {
                    setScale(vertex.getNode(), FULL_SCALE + (1 - FULL_SCALE) * (1 - this.filteringParameter));
                }
            }

            restoreCenter();
        }
    }

    /**
     * Sets the label by.
     * 
     * @param attribute
     *            the new label by
     */
    void setLabelBy(final String attribute)
    {
        try
        {
            saveCenter();

            for (Vertex vertex : this.network.getVertices())
            {
                if (vertex instanceof Indi)
                {
                    Indi indi = (Indi) vertex;
                    indi.setLabelBy(attribute);
                }
            }
        }
        finally
        {
            restoreCenter();
        }
    }

    /**
     * Sets the network.
     * 
     * @param network
     *            the new network
     */
    public void setNetwork(final Network network)
    {
        // TODO
    }

    /**
     * Show node id.
     * 
     * @param id
     *            the id
     */
    void showNodeId(final String id)
    {
        Vertex vertex = this.network.getVertex(id);
        if (vertex != null)
        {
            PNode node = vertex.getNode();
            this.pcnvsCanvas.getCamera().animateViewToCenterBounds(node.getFullBounds(), false, 200);
        }
    }

    /**
     * Prints.
     */
    public void simplePrint()
    {
        GraphicsConstants.instance = new PrintConstants();
        this.quiltManager.rebuild();
        PrintUtilities.printComponent(this.pcnvsCanvas);
        GraphicsConstants.instance = new GraphicsConstants();
        this.quiltManager.rebuild();
    }

    // Note: these fields have been added to allow for benchmarking

    /**
     * Called by PFilterAnimation. Don't call this yourself.
     * 
     * @param destFilterParameter
     *            the dest filter parameter
     */
    @Override
    public void startFiltering(final float destFilterParameter)
    {

        if (destFilterParameter == 1)
        {
            this.doiManager.computeDOI();
        }

        saveCenter();
    }

    /**
     * View all.
     */
    void viewAll()
    {
        this.pcnvsCanvas.getCamera().setViewBounds(this.quiltManager.getFullBoundsReference());
    }

    /**
     * Sets the scale.
     * 
     * @param node
     *            the node
     * @param scale
     *            the scale
     */
    private static void setScale(final PNode node, final double scale)
    {
        if (scale == 0)
        {
            node.setVisible(false);
        }
        else
        {
            if (!node.getVisible())
            {
                node.setVisible(true);
            }

            if ((node instanceof PIndi) || (node instanceof PFam) || (node instanceof PEdge))
            {
                node.setScale(scale);
                // PBounds b = node.getFullBoundsReference();
                // node.setBounds(b.x, b.y, b.width*scale, b.height*scale);
            }
        }
    }
}
