/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.awt.geom.AffineTransform;

import edu.umd.cs.piccolo.PCamera;
import edu.umd.cs.piccolo.activities.PTransformActivity;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PPanEventHandler;
import edu.umd.cs.piccolo.util.PBounds;

/**
 * The Class <b>DisabablePanEventHandler</b> is a pan event that can be
 * disabled.
 * 
 * @author Pierre Dragicevic
 */
public class DisabablePanEventHandler extends PPanEventHandler
{
    private boolean enabled = true;

    /**
     * Checks if is enabled.
     * 
     * @return if the handler is enabled
     */
    public boolean isEnabled()
    {
        return this.enabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final PInputEvent event)
    {
        if (this.enabled && (event.getClickCount() == 2))
        {
            PCamera camera = event.getCamera();
            PBounds cb = camera.getBoundsReference();
            AffineTransform t2 = AffineTransform.getTranslateInstance(-event.getPosition().getX() + cb.getWidth() / 2, -event.getPosition().getY() + cb.getHeight() / 2);
            PTransformActivity activity = camera.animateViewToTransform(t2, 250);
            activity.setSlowInSlowOut(false);
        }
        else
        {
            super.mouseClicked(event);
        }
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.event.PPanEventHandler#pan(edu.umd.cs.piccolo.event.PInputEvent)
     */
    @Override
    protected void pan(final PInputEvent event)
    {
        if (this.enabled)
        {
            super.pan(event);
        }
    }

    /**
     * Sets the enabled state.
     * 
     * @param enabled
     *            new state
     */
    public void setEnabled(final boolean enabled)
    {
        this.enabled = enabled;
    }
}
