/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JMenuItem;

import org.freehep.graphicsbase.util.export.ExportDialog;
import org.freehep.graphicsio.pdf.PDFGraphics2D;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;

/**
 * The Class Printer.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class Printer
{
    private static boolean printing = false;

    /**
     * Creates an export menu item.
     * 
     * @return a JMenuItem
     */
    public static JMenuItem createExportMenu()
    {
        JMenuItem result = new JMenuItem("Export Graphics to ...");
        result.addActionListener(new ActionListener()
        {
            /**
			 * 
			 */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                PCanvas panel = GeneaQuiltOldWindow.getQuilt().getCanvas();
                if (panel == null)
                {
                    return;
                }
                ExportDialog export = new ExportDialog();
                // Properties prop = new Properties();
                // Should look into the sources to have this magic name
                // prop.setProperty(
                //                        export.getClass().getName()+".SaveAsFile",  //$NON-NLS-1$
                // filename);
                // export.setUserProperties(prop);
                export.showExportDialog(panel, "Export view as ...", panel, (new File(GeneaQuiltOldWindow.getQuilt().getFile())).getName());
                // ExportFileType t = new PDFExportFileType();
                // if (fileChooser.showDialog(null, "OK")
                // == JFileChooser.APPROVE_OPTION) {
                // File f = t.adjustFilename(
                // fileChooser.getSelectedFile(),
                // null);
                // if (f.exists()) {
                // int ok =
                // JOptionPane.showConfirmDialog(null,"Replace existing file?");
                // if (ok != JOptionPane.OK_OPTION) return;
                // }
                //
                // printPDF(f, t, panel, panel);
                // }
            }
        });

        //
        return result;
    }

    /**
     * Checks if is printing.
     * 
     * @return the printing
     */
    public static boolean isPrinting()
    {
        return printing;
    }

    // private static void printPDF(
    // File file,
    // ExportFileType t,
    // Component comp,
    // PCanvas canvas) {
    // try {
    // PDFGraphics2D pdfGraphics = new PDFGraphics2D(file, comp);
    // PCamera camera = canvas.getCamera();
    // final PBounds originalCameraBounds = camera.getBounds();
    // final AffineTransform originalTransform =
    // canvas.getLayer().getChild(0).getTransform();
    // final PBounds layerBounds = camera.getUnionOfLayerFullBounds();
    // camera.setBounds(layerBounds);
    // Dimension size = PageConstants.getSize(
    // pdfGraphics.getProperty(PDFGraphics2D.PAGE_SIZE),
    // pdfGraphics.getProperty(PDFGraphics2D.ORIENTATION));
    //
    // double vscale = size.height / layerBounds.getHeight();
    // double hscale = size.width / layerBounds.getWidth();
    // double scale;
    // if (hscale < vscale) {
    // scale = hscale;
    // }
    // else {
    // scale = vscale;
    // }
    // //scale = 0.5;
    // // double r = scale * layerBounds.getWidth() / size.width;
    // // double c = scale * layerBounds.getHeight() / size.height;
    // // int rows = (int)Math.ceil(r);
    // // int cols = (int)Math.ceil(c);
    // //
    // //// if (rows > 1 || cols > 1) {
    // // pdfGraphics.setMultiPage(true);
    // //// }
    //
    // // ArrayList<PNode> list = new ArrayList<PNode>();
    // // int rows = 1;
    // // int cols = 1;
    // pdfGraphics.startExport();
    // for (int row = 0; row < rows; row++) {
    // for (int col = 0; col < cols; col++) {
    // PBounds bounds = new PBounds(
    // row*size.width/scale,
    // col*size.height/scale,
    // size.width/scale, size.height/scale);
    // list.clear();
    // canvas.getLayer().findIntersectingNodes(bounds, list);
    // if (list.size() < 2)
    // continue;
    // pdfGraphics.openPage(size, "Genealogy"); //"Page "+(col+1)+"x"+(row+1));
    // PDFGraphics2D g = (PDFGraphics2D)pdfGraphics.create();
    // g.translate(-row*size.width, -col*size.height);
    // g.scale(scale, scale);
    // final PPaintContext pc = new PPaintContext(g);
    // final PPaintContext pc = new PPrintContext(g);
    //
    // pc.setRenderQuality(PPaintContext.HIGH_QUALITY_RENDERING);
    // camera.setBounds(layerBounds);
    // setPrinting(true);
    // canvas.getCamera().fullPaint(pc);
    // g.dispose();
    // pdfGraphics.closePage();
    // }
    // }
    // pdfGraphics.endExport();
    // pdfGraphics.closeStream();
    // pdfGraphics.dispose();
    // canvas.getLayer().getChild(1).setTransform(originalTransform);
    // camera.setBounds(originalCameraBounds);
    // }
    // catch(Exception exception) {
    // JOptionPane.showMessageDialog(comp, e.getMessage(),
    // "Error writing file "+file, JOptionPane.ERROR_MESSAGE);
    // }
    // finally {
    // setPrinting(false);
    // }
    // }
    /**
     * Prints the all.
     * 
     * @param canvas
     *            the canvas
     * @param graphic
     *            the graphic
     */
    //
    private static void printAll(final PCanvas canvas, final PDFGraphics2D graphic)
    {
        final PBounds clippingRect = new PBounds(graphic.getClipBounds());
        clippingRect.expandNearestIntegerDimensions();

        final PBounds originalCameraBounds = canvas.getCamera().getBounds();
        final PBounds layerBounds = canvas.getCamera().getUnionOfLayerFullBounds();
        canvas.getCamera().setBounds(layerBounds);

        final double clipRatio = clippingRect.getWidth() / clippingRect.getHeight();
        final double nodeRatio = ((double) canvas.getWidth()) / ((double) canvas.getHeight());
        final double scale;
        if (nodeRatio <= clipRatio)
        {
            scale = clippingRect.getHeight() / canvas.getCamera().getHeight();
        }
        else
        {
            scale = clippingRect.getWidth() / canvas.getCamera().getWidth();
        }
        graphic.scale(scale, scale);
        graphic.translate(-clippingRect.x, -clippingRect.y);

        final PPaintContext pc = new PPaintContext(graphic);
        pc.setRenderQuality(PPaintContext.HIGH_QUALITY_RENDERING);
        canvas.getCamera().fullPaint(pc);

        canvas.getCamera().setBounds(originalCameraBounds);
    }

    /**
     * Sets the printing.
     * 
     * @param printing
     *            the printing to set
     */
    public static void setPrinting(final boolean printing)
    {
        Printer.printing = printing;
    }
}
