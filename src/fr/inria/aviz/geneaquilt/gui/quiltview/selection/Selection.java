/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight.HighlightManager;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class Selection. A node representing a selection.
 * 
 * @author dragice
 */
public class Selection
{
    /** The Highlight mode. */
    public enum HighlightMode
    {
        /** predecessors and successors are highlighted */
        HIGHLIGHT_ALL,
        /** none of the predecessors and successors are highlighted */
        HIGHLIGHT_NONE,
        /** only the successors are highlighted */
        HIGHLIGHT_SUCCESSORS,
        /** only the predecessors are highlighted */
        HIGHLIGHT_PREDECESSORS
    }

    private final SelectionManager selectionManager;
    private final HighlightManager highlightManager;
    private Network network;
    private final Color selectionColor_light;
    private final Color selectionColor_strong;
    private final Color selectionColor_opaque;

    private PNode selectedObject;

    /** For optimization */
    private final List<PNode> highlighted_successors_tmp = new ArrayList<PNode>();

    /** For optimization */
    private final List<PNode> highlighted_predecessors_tmp = new ArrayList<PNode>();
    private HighlightMode mode;

    /**
     * Creates a selection with a manager, a selected object and a color.
     * 
     * @param selectionManager
     *            the manager
     * @param selectedObject
     *            the object
     * @param selectionColor
     *            the color
     */
    public Selection(final SelectionManager selectionManager, final PNode selectedObject, final Color selectionColor)
    {
        this.selectionManager = selectionManager;
        this.network = selectionManager.getNetwork();
        this.selectionColor_light = GUIUtils.multiplyAlpha(selectionColor, 0.3f);
        this.selectionColor_strong = GUIUtils.multiplyAlpha(selectionColor, 0.5f);
        this.selectionColor_opaque = selectionColor;
        this.selectedObject = selectedObject;
        this.highlightManager = selectionManager.getHighlightManager();
        setHighlightMode(HighlightMode.HIGHLIGHT_ALL);
    }

    /**
     * Clear highlights.
     */
    protected void clearHighlights()
    {
        this.highlightManager.clearHighlights(this);
    }

    /**
     * revised: ancestor: out.
     * 
     * @param vertex
     *            the vertex
     * @return the p node
     */
    protected PNode findNextPredecessor(final Vertex vertex)
    {
        PNode result;

        // highlightFound(v.getNode());
        Collection<Edge> outEdges = this.network.getOutEdges(vertex);

        Edge highlightUnique = null;
        if (vertex.getNode() instanceof PFam)
        {
            highlightUnique = getTopmostEdge(outEdges);
            if (highlightUnique != null)
            {
                result = highlightUnique.getNode();
            }
            else
            {
                result = null;
            }
        }
        else if (vertex.getNode() instanceof PIndi)
        {
            highlightUnique = getLeftmostEdge(outEdges);
            if (highlightUnique != null)
            {
                result = highlightUnique.getNode();
            }
            else
            {
                result = null;
            }
        }
        else
        {
            result = null;
        }

        // highlightFound(nextEdge);

        //
        return result;
    }

    /**
     * added: descendant: in.
     * 
     * @param vertex
     *            the vertex
     * @return the p node
     */
    protected PNode findNextSuccessor(final Vertex vertex)
    {
        PNode result;

        // highlightFound(v.getNode());
        Collection<Edge> inEdges = this.network.getInEdges(vertex);

        Edge highlightUnique = null;
        // boolean showFrom = true, showTo = true;
        if (vertex.getNode() instanceof PFam)
        {
            highlightUnique = getBottommostEdge(inEdges);
            // showTo = false;
            if (highlightUnique != null)
            {
                result = highlightUnique.getNode();
            }
            else
            {
                result = null;
            }
        }
        else if (vertex.getNode() instanceof PIndi)
        {
            highlightUnique = getRightmostEdge(inEdges);
            // showTo = false;
            if (highlightUnique != null)
            {
                result = highlightUnique.getNode();
            }
            else
            {
                result = null;
            }
        }
        else
        {
            result = null;
        }

        // highlightFound(nextEdge);

        //
        return result;
    }

    /**
     * Gets the highlighted objects.
     * 
     * @return the highlighted objects
     */
    public Set<PNode> getHighlightedObjects()
    {
        return this.highlightManager.getHighlightedObjects(this);
    }

    /**
     * Gets the highlight mode.
     * 
     * @return the highlight mode
     */
    public HighlightMode getHighlightMode()
    {
        return this.mode;
    }

    /**
     * Gets the light color.
     * 
     * @return the light color
     */
    public Color getLightColor()
    {
        return this.selectionColor_light;
    }

    /**
     * Gets the opaque color.
     * 
     * @return Returns the selection color
     */
    public Color getOpaqueColor()
    {
        return this.selectionColor_opaque;
    }

    /**
     * Gets the selected object.
     * 
     * @return the selected object
     */
    public PNode getSelectedObject()
    {
        return this.selectedObject;
    }

    /**
     * Gets the strong color.
     * 
     * @return the strong color
     */
    public Color getStrongColor()
    {
        return this.selectionColor_strong;
    }

    /**
     * Highlight.
     * 
     * @param node
     *            the node
     */
    protected void highlight(final PNode node)
    {
        highlight(node, true, true);
    }

    /**
     * Highlight.
     * 
     * @param node
     *            the node
     * @param showFromConnector
     *            the show from connector
     * @param showToConnector
     *            the show to connector
     */
    protected void highlight(final PNode node, final boolean showFromConnector, final boolean showToConnector)
    {

        if (node instanceof PFam)
        {
            this.highlightManager.setPathHighlighted(node, this, true);
        }

        if ((node instanceof PEdge) && (showFromConnector || showToConnector))
        {
            PEdge edge = (PEdge) node;
            PNode from = edge.getEdge().getFromVertex().getNode();
            PNode to = edge.getEdge().getToVertex().getNode();

            if (showFromConnector)
            {
                this.highlightManager.setPathHighlighted(from, node, this, true);
            }

            if (showToConnector)
            {
                this.highlightManager.setPathHighlighted(node, to, this, true);
            }
        }

        if (node instanceof PEdge)
        {
            this.highlightManager.setPathHighlighted(node, this, true);
        }
    }

    /**
     * Highlights the ascendants/predecessors.
     */
    protected void highlightPredecessors()
    {
        this.highlighted_predecessors_tmp.clear();
        if (this.selectedObject instanceof PEdge)
        {
            PEdge pedge = (PEdge) this.selectedObject;
            highlightPredecessors(pedge.getEdge().getFromVertex());
        }
        else if (this.selectedObject instanceof PIndi)
        {
            PIndi pindi = (PIndi) this.selectedObject;
            highlightPredecessors(pindi.getIndi());
        }
        else if (this.selectedObject instanceof PFam)
        {
            PFam pfam = (PFam) this.selectedObject;
            highlightPredecessors(pfam.getFam());
        }
        this.highlighted_predecessors_tmp.clear();
    }

    /**
     * Highlight predecessors.
     * 
     * @param vertex
     *            the vertex
     */
    protected void highlightPredecessors(final Vertex vertex)
    {
        highlight(vertex.getNode());

        Collection<Edge> inEdges = this.network.getInEdges(vertex);

        // -- Avoid overlapping
        Edge highlightUnique = null;
        boolean showFrom = true;
        boolean showTo = true;
        if (vertex.getNode() instanceof PFam)
        {
            highlightUnique = getBottommostEdge(inEdges);
            showTo = false;
        }
        else if (vertex.getNode() instanceof PIndi)
        {
            highlightUnique = getRightmostEdge(inEdges);
            showTo = false;
        }

        for (Edge edge : inEdges)
        {
            if (highlightUnique == null || edge == highlightUnique)
            {
                highlight(edge.getNode());
            }
            else
            {
                highlight(edge.getNode(), showFrom, showTo);
            }
            if (!this.highlighted_predecessors_tmp.contains(this.network.getSource(edge).getNode()))
            {
                this.highlighted_predecessors_tmp.add(this.network.getSource(edge).getNode());
                highlightPredecessors(this.network.getSource(edge));
            }
        }
    }

    /**
     * Highlights the selection.
     */
    protected void highlightSelection()
    {

        this.highlightManager.setSelectionHighlighted(this.selectedObject, this, true);

    }

    /**
     * Hightlight the descendants/successors.
     */
    protected void highlightSuccessors()
    {
        this.highlighted_successors_tmp.clear();
        if (this.selectedObject instanceof PEdge)
        {
            PEdge pedge = (PEdge) this.selectedObject;
            highlightSuccessors(pedge.getEdge().getFromVertex());
        }
        else if (this.selectedObject instanceof PIndi)
        {
            PIndi pindi = (PIndi) this.selectedObject;
            highlightSuccessors(pindi.getIndi());
        }
        else if (this.selectedObject instanceof PFam)
        {
            PFam pfam = (PFam) this.selectedObject;
            highlightSuccessors(pfam.getFam());
        }
        this.highlighted_successors_tmp.clear();
    }

    /**
     * Highlight successors.
     * 
     * @param vertex
     *            the vertex
     */
    protected void highlightSuccessors(final Vertex vertex)
    {
        highlight(vertex.getNode());

        Collection<Edge> outEdges = this.network.getOutEdges(vertex);

        // -- Avoid overlapping
        Edge highlightUnique = null;
        boolean showFrom = true, showTo = true;
        if (vertex.getNode() instanceof PFam)
        {
            highlightUnique = getTopmostEdge(outEdges);
            showFrom = false;
        }
        else if (vertex.getNode() instanceof PIndi)
        {
            highlightUnique = getLeftmostEdge(outEdges);
            showFrom = false;
        }

        for (Edge edge : outEdges)
        {
            if (highlightUnique == null || edge == highlightUnique)
            {
                highlight(edge.getNode());
            }
            else
            {
                highlight(edge.getNode(), showFrom, showTo);
            }
            if (!this.highlighted_successors_tmp.contains(this.network.getDest(edge).getNode()))
            {
                this.highlighted_successors_tmp.add(this.network.getDest(edge).getNode());
                highlightSuccessors(this.network.getDest(edge));
            }
        }
    }

    /**
     * Sets the highlight mode.
     * 
     * @param mode
     *            the new mode
     */
    public void setHighlightMode(final HighlightMode mode)
    {
        this.mode = mode;
        updateHighlights();
    }

    /**
     * Sets the selected object.
     * 
     * @param selectedObject
     *            the next selected object
     */
    public void setSelectedObject(final PNode selectedObject)
    {
        this.selectedObject = selectedObject;
        updateHighlights();
        this.selectionManager.fireChangeListeners();
    }

    /**
     * Toggles the highlight mode.
     */
    public void toggleHighlightMode()
    {
        switch (this.mode)
        {
            case HIGHLIGHT_ALL:
                this.mode = HighlightMode.HIGHLIGHT_NONE;
            break;
            case HIGHLIGHT_NONE:
                this.mode = HighlightMode.HIGHLIGHT_PREDECESSORS;
            break;
            case HIGHLIGHT_PREDECESSORS:
                this.mode = HighlightMode.HIGHLIGHT_SUCCESSORS;
            break;
            case HIGHLIGHT_SUCCESSORS:
                this.mode = HighlightMode.HIGHLIGHT_ALL;
            break;
        }

        updateHighlights();
    }

    /**
     * Recomputes the highlights.
     */
    public void updateHighlights()
    {
        this.highlightManager.repaint();
        clearHighlights();
        if (this.mode == HighlightMode.HIGHLIGHT_ALL || this.mode == HighlightMode.HIGHLIGHT_PREDECESSORS)
        {
            highlightPredecessors();
        }

        if (this.mode == HighlightMode.HIGHLIGHT_ALL || this.mode == HighlightMode.HIGHLIGHT_SUCCESSORS)
        {
            highlightSuccessors();
        }

        highlightSelection();
        this.highlightManager.selectionModeChanged(this);
        this.highlightManager.repaint();
    }

    /**
     * Gets the bottommost edge.
     * 
     * @param edges
     *            the edges
     * @return the bottommost edge
     */
    private static Edge getBottommostEdge(final Collection<Edge> edges)
    {
        Edge result;

        float maxy = Float.MIN_VALUE;
        result = null;
        for (Edge edge : edges)
        {
            float y = (float) edge.getNode().getFullBoundsReference().getY();
            if (y > maxy)
            {
                maxy = y;
                result = edge;
            }
        }

        //
        return result;
    }

    /**
     * Gets the leftmost edge.
     * 
     * @param edges
     *            the edges
     * @return the leftmost edge
     */
    private static Edge getLeftmostEdge(final Collection<Edge> edges)
    {
        Edge result;

        float minx = Float.MAX_VALUE;
        result = null;
        for (Edge edge : edges)
        {
            float x = (float) edge.getNode().getFullBoundsReference().getX();
            if (x < minx)
            {
                minx = x;
                result = edge;
            }
        }

        //
        return result;
    }

    /**
     * Gets the rightmost edge.
     * 
     * @param edges
     *            the edges
     * @return the rightmost edge
     */
    private static Edge getRightmostEdge(final Collection<Edge> edges)
    {
        Edge result;

        float maxx = Float.MIN_VALUE;
        result = null;
        for (Edge edge : edges)
        {
            float x = (float) edge.getNode().getFullBoundsReference().getX();
            if (x > maxx)
            {
                maxx = x;
                result = edge;
            }
        }

        //
        return result;
    }

    /**
     * Gets the topmost edge.
     * 
     * @param edges
     *            the edges
     * @return the topmost edge
     */
    private static Edge getTopmostEdge(final Collection<Edge> edges)
    {
        Edge result;

        float miny = Float.MAX_VALUE;
        result = null;
        for (Edge edge : edges)
        {
            float y = (float) edge.getNode().getFullBoundsReference().getY();
            if (y < miny)
            {
                miny = y;
                result = edge;
            }
        }

        //
        return result;
    }
}
