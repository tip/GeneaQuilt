/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight;

import java.awt.Color;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.gui.util.MulticolorStroke;

/**
 * The Class SelectionCombination computes and stores graphic attributes
 * associated to a specific set of selections, such as their average color. It
 * ensures that every combination of selections is represented by a unique
 * object.
 * 
 * @author dragice
 */
public class SelectionCombination
{
    private static final Set<SelectionCombination> allCombinations = new HashSet<SelectionCombination>();
    private static final SelectionCombination EMPTY_SELECTION = new SelectionCombination(new HashSet<Selection>());
    private final Set<Selection> selections;

    private final Color combinedColor;
    private final Color combinedColorTranslucent;
    private final Color combinedColorLight;
    private final Hashtable<Float, MulticolorStroke> multicolorStrokes = new Hashtable<Float, MulticolorStroke>();

    /**
     * Instantiates a new selection combination.
     * 
     * @param selections
     *            the selections
     */
    protected SelectionCombination(final Set<Selection> selections)
    {
        this.selections = selections;
        this.combinedColor = getAverageColor(selections);

        float alpha;
        if (selections.size() > 1)
        {
            alpha = 0.5f;
        }
        else
        {
            alpha = 0.3f;
        }
        this.combinedColorTranslucent = GUIUtils.multiplyAlpha(this.combinedColor, alpha);

        float amount;
        if (selections.size() > 1)
        {
            amount = 0f;
        }
        else
        {
            amount = 0.5f;
        }
        this.combinedColorLight = GUIUtils.mix(this.combinedColor, Color.white, amount);
    }

    /**
     * Contains.
     * 
     * @param selection
     *            the selection
     * @return true, if successful
     */
    public boolean contains(final Selection selection)
    {
        return this.selections.contains(selection);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object object)
    {
        boolean result;

        if (object instanceof SelectionCombination)
        {
            result = this.selections.equals(((SelectionCombination) object).selections);
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Gets the instance without selection.
     * 
     * @param selection
     *            the selection
     * @return the instance without selection
     */
    public SelectionCombination getInstanceWithoutSelection(final Selection selection)
    {
        Set<Selection> sels = new HashSet<Selection>(this.selections);
        sels.remove(selection);
        return getInstance(sels);
    }

    /**
     * Gets the instance with selection.
     * 
     * @param newSelection
     *            the new selection
     * @return the instance with selection
     */
    public SelectionCombination getInstanceWithSelection(final Selection newSelection)
    {
        Set<Selection> sels = new HashSet<Selection>(this.selections);
        sels.add(newSelection);
        return getInstance(sels);
    }

    /**
     * Gets the light combined color.
     * 
     * @return the light combined color
     */
    public Color getLightCombinedColor()
    {
        return this.combinedColorLight;
    }

    /**
     * Gets the multicolor stroke.
     * 
     * @param width
     *            the width
     * @param light
     *            the light
     * @return the multicolor stroke
     */
    public MulticolorStroke getMulticolorStroke(final float width, final boolean light)
    {
        MulticolorStroke result;

        result = this.multicolorStrokes.get(width);
        if (result == null)
        {
            result = createMulticolorStroke(this.selections, width, light);
            this.multicolorStrokes.put(width, result);
        }

        //
        return result;
    }

    /**
     * Gets the opaque combined color.
     * 
     * @return Returns the color obtained by blending all selection colors.
     */
    public Color getOpaqueCombinedColor()
    {
        return this.combinedColor;
    }

    /**
     * Gets the selection count.
     * 
     * @return the selection count
     */
    public int getSelectionCount()
    {
        return this.selections.size();
    }

    /**
     * Gets the selections.
     * 
     * @return the selections
     */
    public Set<Selection> getSelections()
    {
        return this.selections;
    }

    /**
     * Gets the translucent combined color.
     * 
     * @return the translucent combined color
     */
    public Color getTranslucentCombinedColor()
    {
        return this.combinedColorTranslucent;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int result;

        result = super.hashCode();

        //
        return result;
    }

    /**
     * Checks if is empty.
     * 
     * @return true, if is empty
     */
    public boolean isEmpty()
    {
        return this.selections.isEmpty();
    }

    /**
     * Cleanup deleted selection.
     * 
     * @param selection
     *            the selection
     */
    private static void cleanupDeletedSelection(final Selection selection)
    {
        Set<SelectionCombination> combinationsTmp = new HashSet<SelectionCombination>(allCombinations);
        for (SelectionCombination sc : combinationsTmp)
        {
            if (sc.contains(selection))
            {
                allCombinations.remove(sc);
            }
        }
    }

    /**
     * Clear.
     */
    public static void clear()
    {
        allCombinations.clear();
    }

    /**
     * Creates the multicolor stroke.
     * 
     * @param selections
     *            the selections
     * @param width
     *            the width
     * @param light
     *            the light
     * @return the multicolor stroke
     */
    protected static MulticolorStroke createMulticolorStroke(final Set<Selection> selections, final float width, final boolean light)
    {
        MulticolorStroke result;

        Color[] colors = new Color[selections.size()];
        int colorIndex = 0;
        for (Selection selection : selections)
        {
            if (light)
            {
                colors[colorIndex] = selection.getLightColor();
            }
            else
            {
                colors[colorIndex] = selection.getStrongColor();
            }
            colorIndex += 1;
        }

        result = new MulticolorStroke(width, colors, GraphicsConstants.PATH_HIGHLIGHT_MULTICOLOR_SPACING);

        //
        return result;
    }

    /**
     * Gets the average color.
     * 
     * @param selections
     *            the selections
     * @return the average color
     */
    protected static Color getAverageColor(final Set<Selection> selections)
    {
        Color result;

        if (selections.size() == 0)
        {
            result = Color.white;
        }
        else
        {
            int r = 0, g = 0, b = 0, a = 0;
            int count = 0;
            for (Selection s : selections)
            {
                Color c = s.getOpaqueColor();
                r += c.getRed();
                g += c.getGreen();
                b += c.getBlue();
                a += c.getAlpha();
                count++;
            }

            result = new Color(r / count, g / count, b / count, a / count);
        }

        //
        return result;
    }

    /**
     * Gets the empty instance.
     * 
     * @return the empty instance
     */
    public static SelectionCombination getEmptyInstance()
    {
        SelectionCombination result;

        if (!allCombinations.contains(EMPTY_SELECTION))
        {
            allCombinations.add(EMPTY_SELECTION);
        }

        result = EMPTY_SELECTION;

        //
        return result;
    }

    /**
     * Gets the single instance of SelectionCombination.
     * 
     * @param selection
     *            the selection
     * @return single instance of SelectionCombination
     */
    public static SelectionCombination getInstance(final Selection selection)
    {
        Set<Selection> selections = new HashSet<Selection>();
        selections.add(selection);
        return getInstance(selections);
    }

    /**
     * Gets the single instance of SelectionCombination.
     * 
     * @param selections
     *            the selections
     * @return single instance of SelectionCombination
     */
    public static SelectionCombination getInstance(final Set<Selection> selections)
    {
        for (SelectionCombination sc : allCombinations)
        {
            if (sc.selections.equals(selections))
            {
                return sc;
            }
        }
        SelectionCombination sc = new SelectionCombination(selections);
        allCombinations.add(sc);
        return sc;
    }
}
