/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection.HighlightMode;
import fr.inria.aviz.geneaquilt.gui.util.MulticolorStroke;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;

/**
 * The Class SelectionHighlight. The graphical object that represents a
 * selection highlight.
 * 
 * @author dragice
 */
public class SelectionHighlight extends Highlight
{
    private static final long serialVersionUID = 8508754626920411865L;
    private List<Selection> orderedSelections = new ArrayList<Selection>();
    private SelectionCombination predecessorSelections = SelectionCombination.getEmptyInstance();
    private SelectionCombination successorSelections = SelectionCombination.getEmptyInstance();
    private Shape predecessorModeFeedback = null, successorModeFeedback = null;

    private static float minimumScreenWidth = 3;
    private static float minimumScreenHeight = 3;

    /**
     * Creates a selection highlight on a node, with no associated selection.
     * 
     * @param node
     *            the node
     */
    public SelectionHighlight(final PNode node)
    {
        super(node);
    }

    /* (non-Javadoc)
     * @see fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight.Highlight#addSelection(fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection)
     */
    @Override
    public void addSelection(final Selection s)
    {
        super.addSelection(s);
        this.orderedSelections.add(s);
        selectionModeChanged();
    }

    /**
     * Creates the mode feedback.
     * 
     * @param r
     *            the r
     * @param x
     *            the x
     * @param y
     *            the y
     * @return the shape
     */
    private Shape createModeFeedback(final PBounds r, final int x, final int y)
    {
        Shape result;

        final float d = 4;
        if (x == -1)
        {
            result = new Line2D.Float((float) r.getX() - d, (float) r.getY(), (float) r.getX() - d, (float) r.getY() + (float) r.getHeight());
        }
        else if (x == 1)
        {
            result = new Line2D.Float((float) r.getX() + (float) r.getWidth() + d, (float) r.getY(), (float) r.getX() + (float) r.getWidth() + d, (float) r.getY() + (float) r.getHeight());
        }
        else if (y == -1)
        {
            result = new Line2D.Float((float) r.getX(), (float) r.getY() - d, (float) r.getX() + (float) r.getWidth(), (float) r.getY() - d);
        }
        else if (y == 1)
        {
            result = new Line2D.Float((float) r.getX(), (float) r.getY() + (float) r.getHeight() + d, (float) r.getX() + (float) r.getWidth(), (float) r.getY() + (float) r.getHeight() + d);
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the last selection.
     * 
     * @return the last selection
     */
    public Selection getLastSelection()
    {
        Selection result;

        if (isEmpty())
        {
            result = null;
        }
        else
        {
            result = this.orderedSelections.get(this.orderedSelections.size() - 1);
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        if (this.shape != null && !isEmpty())
        {
            final Graphics2D graphic = paintContext.getGraphics();
            double scale = PrintConstants.instance.getScale(paintContext);

            Rectangle2D bounds = this.shape.getBounds2D();
            float h = (float) (bounds.getHeight() * scale);
            float w = (float) (bounds.getWidth() * scale);
            if (h <= minimumScreenHeight || w < minimumScreenWidth)
            {
                graphic.setColor(this.selections.getTranslucentCombinedColor());
                graphic.setStroke(GraphicsConstants.SELECTION_STROKE);
                // Grow the rectangle
                float h2 = (float) Math.max(bounds.getHeight(), minimumScreenHeight / scale);
                float w2 = (float) Math.max(bounds.getWidth(), minimumScreenWidth / scale);
                graphic.fillRect((int) (bounds.getCenterX() - w2 / 2), (int) (bounds.getCenterY() - h2 / 2), (int) (w2 + 0.5f), (int) (h2 + 0.5f));
            }
            else if (scale < GraphicsConstants.MULTICOLOR_STROKE_ZOOM_FACTOR)
            {
                graphic.setColor(this.selections.getTranslucentCombinedColor());
                graphic.setStroke(GraphicsConstants.SELECTION_STROKE);
                graphic.draw(this.shape);
                if (this.predecessorModeFeedback != null)
                {
                    graphic.setColor(this.predecessorSelections.getTranslucentCombinedColor());
                    graphic.setStroke(GraphicsConstants.SELECTION_STROKE);
                    graphic.draw(this.predecessorModeFeedback);
                }
                if (this.successorModeFeedback != null)
                {
                    graphic.setColor(this.successorSelections.getTranslucentCombinedColor());
                    graphic.setStroke(GraphicsConstants.SELECTION_STROKE);
                    graphic.draw(this.successorModeFeedback);
                }
            }
            else
            {
                MulticolorStroke stroke = this.selections.getMulticolorStroke(GraphicsConstants.SELECTION_HIGHLIGHT_WIDTH, false);
                stroke.draw(graphic, this.shape);
                if (this.predecessorModeFeedback != null)
                {
                    stroke = this.predecessorSelections.getMulticolorStroke(GraphicsConstants.SELECTION_HIGHLIGHT_WIDTH, false);
                    stroke.draw(graphic, this.predecessorModeFeedback);
                }
                if (this.successorModeFeedback != null)
                {
                    stroke = this.successorSelections.getMulticolorStroke(GraphicsConstants.SELECTION_HIGHLIGHT_WIDTH, false);
                    stroke.draw(graphic, this.successorModeFeedback);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight.Highlight#removeSelection(fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection)
     */
    @Override
    public void removeSelection(final Selection selection)
    {
        super.removeSelection(selection);
        this.orderedSelections.remove(selection);
        selectionModeChanged();
    }

    /**
     * Called when the highlighting mode has changed in one of the selections.
     */
    public void selectionModeChanged()
    {
        this.predecessorSelections = SelectionCombination.getEmptyInstance();
        this.successorSelections = SelectionCombination.getEmptyInstance();
        for (Selection selection : this.selections.getSelections())
        {
            if (selection.getHighlightMode() == HighlightMode.HIGHLIGHT_ALL || selection.getHighlightMode() == HighlightMode.HIGHLIGHT_SUCCESSORS)
            {
                this.successorSelections = this.successorSelections.getInstanceWithSelection(selection);
            }
            if (selection.getHighlightMode() == HighlightMode.HIGHLIGHT_ALL || selection.getHighlightMode() == HighlightMode.HIGHLIGHT_PREDECESSORS)
            {
                this.predecessorSelections = this.predecessorSelections.getInstanceWithSelection(selection);
            }
        }
        updateShape();
    }

    /**
     * Computes the shape and bounds of the highlight according to the position
     * of the object to be highlighted.
     */
    @Override
    public void updateShape()
    {
        PBounds bounds = this.from.getFullBoundsReference();

        if (!(this.shape instanceof Rectangle2D))
        {
            this.shape = new Rectangle2D.Double();
        }
        float growx = 0;
        float growy = -1;
        this.shape = new Rectangle2D.Double(bounds.getX() - growx, bounds.getY() - growy, bounds.getWidth() + growx * 2, bounds.getHeight() + growy * 2);
        // grow rectangle
        // float m = -2;//GraphicsConstants.SELECTION_HIGHLIGHT_WIDTH / 2 + 1;
        // finalbounds.setFrame(finalbounds.getX() - m, finalbounds.getY() - m,
        // finalbounds.getWidth() + m*2, finalbounds.getHeight() + m*2);

        // Add highlighting mode feedback

        PNode selectedObject = this.from;
        Rectangle2D finalbounds = new Rectangle2D.Double();
        finalbounds.setFrame(this.shape.getBounds2D());

        this.predecessorModeFeedback = null;
        if (this.predecessorSelections != null && !this.predecessorSelections.isEmpty())
        {
            if (selectedObject instanceof PIndi)
            {
                this.predecessorModeFeedback = createModeFeedback(bounds, 1, 0);
            }
            else if (selectedObject instanceof PFam)
            {
                this.predecessorModeFeedback = createModeFeedback(bounds, 0, 1);
            }
            if (this.predecessorModeFeedback != null)
            {
                finalbounds = finalbounds.createUnion(this.predecessorModeFeedback.getBounds2D());
            }
        }
        this.successorModeFeedback = null;
        if (this.successorSelections != null && !this.successorSelections.isEmpty())
        {
            if (selectedObject instanceof PIndi)
            {
                this.successorModeFeedback = createModeFeedback(bounds, -1, 0);
            }
            else if (selectedObject instanceof PFam)
            {
                this.successorModeFeedback = createModeFeedback(bounds, 0, -1);
            }
            if (this.successorModeFeedback != null)
            {
                finalbounds = finalbounds.createUnion(this.successorModeFeedback.getBounds2D());
            }
        }

        setBounds(finalbounds);// .getX() - m, finalbounds.getY() - m,
                               // finalbounds.getWidth() + m*2,
                               // finalbounds.getHeight() + m*2);
    }
}
