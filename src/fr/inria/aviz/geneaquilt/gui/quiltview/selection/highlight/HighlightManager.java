/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;

/**
 * The Class HighlightManager.
 */
public class HighlightManager extends PNode
{
    /**
	 * 
	 */
    private class HighlightKey
    {
        private PNode from, to;

        /**
         * Instantiates a new highlight key.
         * 
         * @param h
         *            the h
         */
        public HighlightKey(final Highlight h)
        {
            this(h.from, h.to);
        }

        /**
         * Instantiates a new highlight key.
         * 
         * @param from
         *            the from
         * @param to
         *            the to
         */
        public HighlightKey(final PNode from, final PNode to)
        {
            this.from = from;
            this.to = to;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(final Object o)
        {
            boolean result;

            if (!(o instanceof HighlightKey))
            {
                result = false;
            }
            else
            {
                result = (((HighlightKey) o).from == this.from && ((HighlightKey) o).to == this.to) || (((HighlightKey) o).from == this.to && ((HighlightKey) o).to == this.from);
            }

            //
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode()
        {
            int result;

            result = this.from.hashCode() + (this.to == null ? 0 : this.to.hashCode());

            //
            return result;
        }
    }

    private static final long serialVersionUID = 3026965457601422741L;

    private Hashtable<HighlightKey, PathHighlight> pathHighlights = new Hashtable<HighlightKey, PathHighlight>();
    private Hashtable<PNode, Set<PathHighlight>> pathHighlightsPerNode = new Hashtable<PNode, Set<PathHighlight>>();
    private Hashtable<PNode, SelectionHighlight> selectionHighlights = new Hashtable<PNode, SelectionHighlight>();
    private boolean testOverlap = false;
    private PBounds fullBounds;

    /**
     * Instantiates a new highlight manager.
     * 
     * @param fullBoundsReference
     *            the full bounds reference
     */
    public HighlightManager(final PBounds fullBoundsReference)
    {
        super();
        this.fullBounds = fullBoundsReference;
    }

    /**
     * Adds the.
     * 
     * @param source
     *            the source
     */
    private void add(final Highlight source)
    {
        // if (h.getSelectionCount() < 2)
        // addChild(0, h);
        // else
        addChild(source);

        if (source instanceof PathHighlight)
        {
            PathHighlight ph = (PathHighlight) source;
            this.pathHighlights.put(new HighlightKey(ph), ph);
            Set<PathHighlight> sh = this.pathHighlightsPerNode.get(ph.from);
            if (sh == null)
            {
                sh = new HashSet<PathHighlight>();
                this.pathHighlightsPerNode.put(ph.from, sh);
            }
            sh.add(ph);
            if (ph.to != null)
            {
                sh = this.pathHighlightsPerNode.get(ph.to);
                if (sh == null)
                {
                    sh = new HashSet<PathHighlight>();
                    this.pathHighlightsPerNode.put(ph.to, sh);
                }
                sh.add(ph);
            }
        }
        else if (source instanceof SelectionHighlight)
        {
            SelectionHighlight sh = (SelectionHighlight) source;
            this.selectionHighlights.put(sh.from, sh);
        }
    }

    /**
     * Break highlights.
     * 
     * @param from
     *            the from
     * @param to
     *            the to
     * @param sh
     *            the sh
     */
    private void breakHighlights(final PNode from, final PNode to, final Set<PathHighlight> sh)
    {
        if (sh == null || sh.size() < 2)
        {
            return;
        }

        Set<PathHighlight> sh2 = new HashSet<PathHighlight>(sh);

        final boolean vertical = (from.getFullBoundsReference().getCenterX() == to.getFullBoundsReference().getCenterX());

        ArrayList<PNode> sortedNodes = new ArrayList<PNode>();
        for (Highlight h : sh)
        {
            if (!sortedNodes.contains(h.from))
            {
                sortedNodes.add(h.from);
            }
            if (h.to != null && !sortedNodes.contains(h.to))
            {
                sortedNodes.add(h.to);
            }
        }

        int count = sortedNodes.size();
        if (count < 2)
        {
            return;
        }

        Collections.sort(sortedNodes, new Comparator<PNode>()
        {
            @Override
            public int compare(final PNode o1, final PNode o2)
            {
                if (vertical)
                {
                    return Double.compare(o1.getGlobalBounds().getCenterY(), o2.getGlobalBounds().getCenterY());
                }
                else
                {
                    return Double.compare(o1.getGlobalBounds().getCenterX(), o2.getGlobalBounds().getCenterX());
                }
            }
        });

        ArrayList<Highlight> shortHighlights = new ArrayList<Highlight>();

        for (int i = 0; i < count - 1; i++)
        {
            PathHighlight h = getPathHighlight(sortedNodes.get(i), sortedNodes.get(i + 1));
            if (h == null)
            {
                h = new PathHighlight(sortedNodes.get(i), sortedNodes.get(i + 1));
                add(h);
            }
            shortHighlights.add(h);
            for (Highlight h2 : sh)
            {
                if (h2 != h && overlap(h.from, h.to, h2.from, h2.to))
                {
                    h.addSelections(h2.getSelections());
                }
            }
            if (h.getSelectionCount() > 1)
            {
                moveToFront(h);
            }
        }

        for (Highlight h : sh2)
        {
            if (!shortHighlights.contains(h) && h.to != null)
            {
                remove(h);
            }
        }
    }

    /**
     * Clear.
     */
    public void clear()
    {
        removeAllChildren();
        this.pathHighlights.clear();
        this.pathHighlightsPerNode.clear();
        this.selectionHighlights.clear();
    }

    /**
     * Clear highlights.
     * 
     * @param selection
     *            the selection
     */
    public void clearHighlights(final Selection selection)
    {
        List children_copy = new ArrayList(getChildrenReference());
        for (Object object : children_copy)
        {
            if (object instanceof Highlight)
            {
                Highlight highlight = (Highlight) object;
                if (highlight.containsSelection(selection))
                {
                    highlight.removeSelection(selection);
                    if (highlight.isEmpty())
                    {
                        remove(highlight);
                    }
                    else
                    {
                        if (highlight instanceof PathHighlight && highlight.getSelectionCount() == 1)
                        {
                            moveToBack(highlight);
                        }
                    }
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.PNode#getFullBoundsReference()
     */
    @Override
    public PBounds getFullBoundsReference()
    {
        return this.fullBounds;
    }

    /**
     * Gets the highlighted objects.
     * 
     * @param selection
     *            the selection
     * @return the highlighted objects
     */
    public Set<PNode> getHighlightedObjects(final Selection selection)
    {
        Set<PNode> result;

        result = new HashSet<PNode>();
        for (Object object : getChildrenReference())
        {
            if (object instanceof Highlight)
            {
                Highlight h = (Highlight) object;
                if (h.containsSelection(selection))
                {
                    result.add(h.from);
                    if (h.to != null)
                    {
                        result.add(h.to);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the path highlight.
     * 
     * @param node
     *            the node
     * @return the path highlight
     */
    public PathHighlight getPathHighlight(final PNode node)
    {
        return getPathHighlight(node, null);
    }

    /**
     * Gets the path highlight.
     * 
     * @param from
     *            the from
     * @param to
     *            the to
     * @return the path highlight
     */
    public PathHighlight getPathHighlight(final PNode from, final PNode to)
    {
        return this.pathHighlights.get(new HighlightKey(from, to));
    }

    /**
     * Gets the selection highlight.
     * 
     * @param node
     *            the node
     * @return the selection highlight
     */
    public SelectionHighlight getSelectionHighlight(final PNode node)
    {
        return this.selectionHighlights.get(node);
    }

    /**
     * Faster than PNode.moveToFront().
     * 
     * @param node
     *            the node
     */
    private void moveToBack(final PNode node)
    {
        List list = getChildrenReference();
        list.remove(list.indexOf(node));
        list.add(0, node);
    }

    /**
     * Faster than PNode.moveToFront().
     * 
     * @param node
     *            the node
     */
    private void moveToFront(final PNode node)
    {
        List liste = getChildrenReference();
        liste.remove(liste.indexOf(node));
        liste.add(node);
    }

    /**
     * Overlap.
     * 
     * @param from
     *            the from
     * @param to
     *            the to
     * @param from2
     *            the from 2
     * @param to2
     *            the to 2
     * @return true, if successful
     */
    private boolean overlap(final PNode from, final PNode to, final PNode from2, final PNode to2)
    {
        if (from == null || to == null || from2 == null || to2 == null)
        {
            return false;
        }

        PBounds bfrom = from.getFullBoundsReference();
        PBounds bto = to.getFullBoundsReference();
        PBounds bfrom2 = from2.getFullBoundsReference();
        PBounds bto2 = to2.getFullBoundsReference();
        // vertical case
        if (bfrom.getCenterX() == bto.getCenterX())
        {
            if (Math.min(bfrom.getCenterY(), bto.getCenterY()) >= Math.max(bfrom2.getCenterY(), bto2.getCenterY()))
            {
                return false;
            }
            if (Math.max(bfrom.getCenterY(), bto.getCenterY()) <= Math.min(bfrom2.getCenterY(), bto2.getCenterY()))
            {
                return false;
            }
            return true;
        }
        // horizontal case
        if (bfrom.getCenterY() == bto.getCenterY())
        {
            if (Math.min(bfrom.getCenterX(), bto.getCenterX()) >= Math.max(bfrom2.getCenterX(), bto2.getCenterX()))
            {
                return false;
            }
            if (Math.max(bfrom.getCenterX(), bto.getCenterX()) <= Math.min(bfrom2.getCenterX(), bto2.getCenterX()))
            {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Removes the.
     * 
     * @param highlight
     *            the highlight
     */
    private void remove(final Highlight highlight)
    {
        removeChild(highlight);

        if (highlight instanceof PathHighlight)
        {
            this.pathHighlights.remove(new HighlightKey(highlight));
            Set<PathHighlight> sh = this.pathHighlightsPerNode.get(highlight.from);
            if (sh != null)
            {
                sh.remove(highlight);
                if (sh.isEmpty())
                {
                    this.pathHighlightsPerNode.remove(highlight.from);
                }
            }
            if (highlight.to != null)
            {
                sh = this.pathHighlightsPerNode.get(highlight.to);
                if (sh != null)
                {
                    sh.remove(highlight);
                    if (sh.isEmpty())
                    {
                        this.pathHighlightsPerNode.remove(highlight.from);
                    }
                }
            }
        }
        else if (highlight instanceof SelectionHighlight)
        {
            this.selectionHighlights.remove(highlight.from);
        }
    }

    /**
     * Selection mode changed.
     * 
     * @param selection
     *            the selection
     */
    public void selectionModeChanged(final Selection selection)
    {
        PNode n = selection.getSelectedObject();
        SelectionHighlight h = getSelectionHighlight(n);
        if (h != null)
        {
            h.selectionModeChanged();
        }
    }

    /**
     * Sets the path highlighted.
     * 
     * @param from
     *            the from
     * @param to
     *            the to
     * @param selection
     *            the selection
     * @param highlightOn
     *            the highlight on
     */
    public void setPathHighlighted(final PNode from, final PNode to, final Selection selection, final boolean highlightOn)
    {
        PathHighlight path = getPathHighlight(from, to);

        if (highlightOn)
        {
            if (path == null)
            {
                path = new PathHighlight(from, to);
                boolean overlapTo = this.testOverlap && to != null && !(to instanceof PEdge) && willOverlap(from, to, this.pathHighlightsPerNode.get(to));
                boolean overlapFrom = this.testOverlap && to != null && !(from instanceof PEdge) && willOverlap(from, to, this.pathHighlightsPerNode.get(from));
                path.addSelection(selection);
                add(path);

                if (overlapTo)
                {
                    breakHighlights(from, to, this.pathHighlightsPerNode.get(to));
                }

                if (overlapFrom)
                {
                    breakHighlights(from, to, this.pathHighlightsPerNode.get(from));
                }
            }
            else
            {
                path.addSelection(selection);
                if (path.getSelectionCount() > 1)
                {
                    moveToFront(path);
                }
            }
        }
        else
        {
            if (path != null)
            {
                path.removeSelection(selection);
                if (path.isEmpty())
                {
                    remove(path);
                }
                else
                {
                    if (path.getSelectionCount() == 0)
                    {
                        moveToBack(path);
                    }
                }
            }
        }
    }

    /**
     * Sets the path highlighted.
     * 
     * @param node
     *            the node
     * @param selection
     *            the selection
     * @param highlight
     *            the highlight
     */
    public void setPathHighlighted(final PNode node, final Selection selection, final boolean highlight)
    {
        setPathHighlighted(node, null, selection, highlight);
    }

    /**
     * Sets the selection highlighted.
     * 
     * @param node
     *            the node
     * @param selection
     *            the selection
     * @param turnOn
     *            the turn on
     */
    public void setSelectionHighlighted(final PNode node, final Selection selection, final boolean turnOn)
    {

        SelectionHighlight highlight = getSelectionHighlight(node);

        if (turnOn)
        {
            if (highlight == null)
            {
                highlight = new SelectionHighlight(node);
                highlight.addSelection(selection);
                add(highlight);
            }
            else
            {
                highlight.addSelection(selection);
                moveToFront(highlight);
            }
        }
        else
        {
            if (highlight != null)
            {
                highlight.removeSelection(selection);
                if (highlight.isEmpty())
                {
                    remove(highlight);
                }
            }
        }

    }

    /**
     * Sets the test overlap.
     * 
     * @param test
     *            the new test overlap
     */
    public void setTestOverlap(final boolean test)
    {
        this.testOverlap = true;
    }

    /**
     * Update highlight shapes.
     */
    public void updateHighlightShapes()
    {
        for (Object object : getChildrenReference())
        {
            if (object instanceof Highlight)
            {
                ((Highlight) object).updateShape();
            }
        }
    }

    /* (non-Javadoc)
     * @see edu.umd.cs.piccolo.PNode#validateFullBounds()
     */
    @Override
    protected boolean validateFullBounds()
    {
        return false;
    }

    /**
     * Will overlap.
     * 
     * @param from
     *            the from
     * @param to
     *            the to
     * @param pathHiglights
     *            the path higlights
     * @return true, if successful
     */
    private boolean willOverlap(final PNode from, final PNode to, final Set<PathHighlight> pathHiglights)
    {
        if (from == null || to == null || pathHiglights == null || pathHiglights.size() < 1)
        {
            return false;
        }

        for (PathHighlight pathHighlight : pathHiglights)
        {
            if (overlap(from, to, pathHighlight.from, pathHighlight.to))
            {
                return true;
            }
        }
        return false;
    }
}
