/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight;

import java.awt.Color;
import java.awt.Shape;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Set;

import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;

/**
 * The Class <b>Highlight</b> manages a highlight.
 * 
 * @author Pierre Dragicevic
 * @version $Revision$
 */
public abstract class Highlight extends PNode
{
    private static final long serialVersionUID = -6951831210502330795L;

    static Hashtable<String, Color> mixedColors = new Hashtable<String, Color>();

    protected PNode from;
    protected PNode to;
    protected SelectionCombination selections = SelectionCombination.getEmptyInstance();
    protected Shape shape = null;

    /**
     * Creates a highlight on one node, with no associated selection.
     * 
     * @param node
     *            the node
     */
    public Highlight(final PNode node)
    {
        this(node, null);
    }

    /**
     * Creates a highlight between two nodes, with no associated selection.
     * 
     * @param from
     *            the starting node
     * @param to
     *            the ending node
     */
    public Highlight(final PNode from, final PNode to)
    {
        this.from = from;
        this.to = to;
        setPickable(false);
        updateShape();
    }

    /**
     * Assign a selection to this highlight. Several selections can share the
     * same highlight.
     * 
     * @param s
     *            the s
     */
    public void addSelection(final Selection s)
    {
        this.selections = this.selections.getInstanceWithSelection(s);
        // repaint();
    }

    /**
     * Adds a collection of selections in the current hightlighted selection.
     * 
     * @param selections
     *            the collection
     */
    public void addSelections(final Collection<Selection> selections)
    {
        for (Selection selection : selections)
        {
            addSelection(selection);
        }
    }

    /**
     * Tests if a specified selection is contained in this selection.
     * 
     * @param selection
     *            the selection
     * @return true/false
     */
    public boolean containsSelection(final Selection selection)
    {
        return this.selections.contains(selection);
    }

    /**
     * Gets the selection count.
     * 
     * @return the number of selections
     */
    public int getSelectionCount()
    {
        return this.selections.getSelectionCount();
    }

    /**
     * Gets the selections.
     * 
     * @return the selection set
     */
    public Set<Selection> getSelections()
    {
        return this.selections.getSelections();
    }

    /**
     * Checks if is empty.
     * 
     * @return true if the selection is empty
     */
    public boolean isEmpty()
    {
        return this.selections.isEmpty();
    }

    /**
     * Remove a selection from this highlight.
     * 
     * @param selection
     *            the selection
     */
    public void removeSelection(final Selection selection)
    {
        this.selections = this.selections.getInstanceWithoutSelection(selection);
        // repaint();
    }

    /**
     * Computes the shape and bounds of the highlight according to the position
     * of the object(s) to be highlighted. Call this method whenever the bounds
     * of the "from" or "to" node you passed to the constructor change.
     */
    public abstract void updateShape();
}
