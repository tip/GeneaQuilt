/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.nodes.PIsoShape;
import fr.inria.aviz.geneaquilt.gui.util.MulticolorStroke;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;

/**
 * The Class PathHighlight. The graphical object that represents a path
 * highlight.
 * 
 * @author dragice
 */
public class PathHighlight extends Highlight
{
    private static final long serialVersionUID = -2725616075873682953L;

    float pathWidth;

    /**
     * Creates a highlight on one node, with no associated selection.
     * 
     * @param node
     *            the node
     */
    public PathHighlight(final PNode node)
    {
        super(node);
    }

    /**
     * Creates a highlight between two nodes, with no associated selection.
     * 
     * @param from
     *            the from
     * @param to
     *            the to
     */
    public PathHighlight(final PNode from, final PNode to)
    {
        super(from, to);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        if (!isEmpty())
        {
            final Graphics2D graphic = paintContext.getGraphics();
            if (this.to == null && this.from instanceof PIsoShape)
            {
                // Special case: edges.
                graphic.setColor(this.selections.getOpaqueCombinedColor());

                // Temporary fix.
                PIsoShape.paintIsoShape(paintContext, this.shape, getFullBoundsReference());
            }
            else if (this.to == null)
            {
                // Highlight a unique object.
                graphic.setColor(this.selections.getTranslucentCombinedColor());
                graphic.fill(this.shape);
            }
            else
            {
                // Highlight the line connecting two objects.
                double scale = PrintConstants.instance.getScale(paintContext);
                if (scale < 1 / GraphicsConstants.PATH_HIGHLIGHT_WIDTH && paintContext.getRenderQuality() == PPaintContext.LOW_QUALITY_RENDERING)
                {
                    graphic.setColor(this.selections.getLightCombinedColor());
                    graphic.setStroke(GraphicsConstants.NULL_WIDTH_STROKE);
                    graphic.draw(this.shape);
                }
                else
                {
                    if (scale < GraphicsConstants.MULTICOLOR_STROKE_ZOOM_FACTOR)
                    {
                        graphic.setColor(this.selections.getTranslucentCombinedColor());
                        graphic.setStroke(GraphicsConstants.PATH_HIGHLIGHT_STROKE);
                        graphic.draw(this.shape);
                    }
                    else
                    {
                        MulticolorStroke mstroke = this.selections.getMulticolorStroke(this.pathWidth, this.selections.getSelectionCount() == 1);
                        mstroke.draw(graphic, this.shape);
                    }
                }
            }
        }
    }

    /**
     * Computes the shape and bounds of the highlight according to the position
     * of the object(s) to be highlighted. Call this method whenever the bounds
     * of the "from" or "to" node you passed to the constructor change.
     */
    @Override
    public void updateShape()
    {
        if (this.to == null)
        {

            PBounds bounds = this.from.getFullBoundsReference();
            if (this.from instanceof PIsoShape)
            {
                // Special case: edges
                this.shape = ((PIsoShape) this.from).getShape();
            }
            else
            {
                if (this.shape instanceof Rectangle2D)
                {
                    ((Rectangle2D) this.shape).setRect(bounds);
                }
                else
                {
                    this.shape = new Rectangle2D.Double(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
                }
            }
            setBounds(bounds);
        }
        else
        {
            PBounds b1 = this.from.getFullBoundsReference();
            PBounds b2 = this.to.getFullBoundsReference();
            if (this.shape instanceof Line2D)
            {
                ((Line2D) this.shape).setLine(b1.getCenterX(), b1.getCenterY(), b2.getCenterX(), b2.getCenterY());
            }
            else
            {
                this.shape = new Line2D.Double(b1.getCenterX(), b1.getCenterY(), b2.getCenterX(), b2.getCenterY());
            }
            double x0 = Math.min(b1.getCenterX(), b2.getCenterX());
            double y0 = Math.min(b1.getCenterY(), b2.getCenterY());
            double x1 = Math.max(b1.getCenterX(), b2.getCenterX());
            double y1 = Math.max(b1.getCenterY(), b2.getCenterY());

            if (Math.abs(b1.getCenterX() - b2.getCenterX()) < Math.abs(b1.getCenterY() - b2.getCenterY()))
            {
                this.pathWidth = (float) Math.min(b1.getWidth(), b2.getWidth());
                this.pathWidth = Math.max(3, this.pathWidth - 5);
            }
            else
            {
                this.pathWidth = (float) Math.min(b1.getHeight(), b2.getHeight());
                this.pathWidth = Math.max(3, this.pathWidth - 8);
            }

            setBounds(x0 - this.pathWidth / 2 - 1, y0 - this.pathWidth / 2 - 1, x1 - x0 + this.pathWidth + 2, y1 - y0 + this.pathWidth + 2);
        }
    }
}
