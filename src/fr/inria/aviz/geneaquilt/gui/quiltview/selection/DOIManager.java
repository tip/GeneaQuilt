/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import edu.uci.ics.jung.algorithms.util.MapBinaryHeap;
import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.nodes.PVertex;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class <b>DOIManager</b> manages the degree of interest of nodes.
 * 
 * @author Jean-Daniel Fekete
 */
public class DOIManager
{
    private SelectionManager selectionManager;
    private QuiltManager quiltManager;
    private Map<PNode, Double> distance = new HashMap<PNode, Double>();
    private MapBinaryHeap<PNode> heap;
    private double highlightDistance = 0;
    private double maxDistance = 10;

    /** The node comparator. */
    private final Comparator<PNode> nodeComparator = new Comparator<PNode>()
    {
        @Override
        public int compare(final PNode alpha, final PNode bravo)
        {
            int result;

            Double alphaValue = DOIManager.this.distance.get(alpha);
            Double bravoValue = DOIManager.this.distance.get(bravo);

            result = (int) Math.signum(alphaValue.doubleValue() - bravoValue.doubleValue());

            //
            return result;
        }
    };

    /**
     * Creates a DOIManager for a specified quiltManager and selectionManager.
     * 
     * @param quiltManager
     *            the QuiltManager
     */
    public DOIManager(final QuiltManager quiltManager)
    {
        this.quiltManager = quiltManager;
        this.selectionManager = quiltManager.getSelectionManager();
    }

    /**
     * Computes the DOI in the network.
     */
    public void computeDOI()
    {
        this.distance.clear();
        this.heap = new MapBinaryHeap<PNode>(this.nodeComparator);
        // Set<Set<Vertex>> components = new HashSet<Set<Vertex>>();

        for (Selection selection : this.selectionManager.getSelections())
        {
            PNode node = selection.getSelectedObject();
            updateDistance(node, 0);
            // if (n instanceof PVertex) {
            // PVertex pv = (PVertex) n;
            // components.add(
            // getNetwork().getComponentSet(pv.getVertex()));
            // }
            // else {
            // PEdge pe = (PEdge)n;
            // Vertex v = getNetwork().getSource(pe.getEdge());
            // components.add(
            // getNetwork().getComponentSet(v));
            // }

            for (PNode highlightedNode : selection.getHighlightedObjects())
            {
                updateDistance(highlightedNode, this.highlightDistance);
            }
        }

        while (!this.heap.isEmpty())
        {
            PNode node = this.heap.remove();
            double newDistance = nextDistFrom(node);
            // if (newD > maxDistance)
            // continue;
            if (node instanceof PVertex)
            {
                PVertex pVertex = (PVertex) node;
                Vertex vertex = pVertex.getVertex();

                for (Edge edge : getNetwork().getOutEdges(vertex))
                {
                    updateDistance(edge.getNode(), newDistance);
                }

                for (Edge edge : getNetwork().getInEdges(vertex))
                {
                    updateDistance(edge.getNode(), newDistance);
                }
            }
            else if (node instanceof PEdge)
            {
                PEdge pedge = (PEdge) node;
                Edge edge = pedge.getEdge();
                updateDistance(getNetwork().getSource(edge).getNode(), newDistance);
                updateDistance(getNetwork().getDest(edge).getNode(), newDistance);
            }
            else
            {
                System.err.println("Unexpected node " + node);
            }
        }

        for (Vertex vertex : getNetwork().getVertices())
        {
            Double nodeDistance = this.distance.get(vertex.getNode());
            if (nodeDistance == null)
            {
                // assert(!components.contains(getNetwork().getComponentSet(v)));
                vertex.setDOI(this.maxDistance);
            }
            else
            {
                vertex.setDOI(nodeDistance.doubleValue());
            }
        }
    }

    /**
     * Gets the network.
     * 
     * @return the network
     */
    public Network getNetwork()
    {
        return this.quiltManager.getNetwork();
    }

    /**
     * Gets the quilt manager.
     * 
     * @return the quiltManager
     */
    public QuiltManager getQuiltManager()
    {
        return this.quiltManager;
    }

    /**
     * Gets the selection manager.
     * 
     * @return the selectionManager
     */
    public SelectionManager getSelectionManager()
    {
        return this.selectionManager;
    }

    /**
     * Next dist from.
     * 
     * @param node
     *            the node
     * @return the double
     */
    protected double nextDistFrom(final PNode node)
    {
        double result;

        Double nodeDistance = this.distance.get(node);
        result = nodeDistance.doubleValue() + 1;
        // Can be log.

        //
        return result;
    }

    /**
     * Update distance.
     * 
     * @param node
     *            the node
     * @param newDistance
     *            the new distance
     * @return true, if successful
     */
    private boolean updateDistance(final PNode node, final double newDistance)
    {
        boolean result;

        Double nodeDistance = this.distance.get(node);
        if (nodeDistance == null)
        {
            this.distance.put(node, new Double(newDistance));
            this.heap.add(node);
            result = true;
        }
        else if (nodeDistance.doubleValue() > newDistance)
        {
            this.distance.put(node, new Double(newDistance));
            this.heap.update(node);
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }
}
