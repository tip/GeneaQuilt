/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.umd.cs.piccolo.PCamera;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.activities.PActivity;
import edu.umd.cs.piccolo.activities.PTransformActivity;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.nodes.PText;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PDimension;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.nodes.PIsoShape;
import fr.inria.aviz.geneaquilt.gui.nodes.PVertex;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.DisabablePanEventHandler;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class <b>SlidingController</b> implements the Bring&Go + Link Sliding
 * navigation.
 * 
 * @author Jean-Daniel Fekete TODO maybe show the spouses TODO slide by zooming
 *         out the zooming in instead of panning
 */
public class SlidingController extends MouseSelectionController
{
    /** 5 pixels for sliding on node */
    private static final int NODE_SLIDING_OFFSET = 50;
    private static final int SLIDING_DISTANCE = 50;
    private static final int SLIDING_INNER = 40;
    private Network network;
    private PCamera camera;
    private PNode slider;
    private PNode sliderBackground = new PNode();
    private List<PVertex> neighbors = new ArrayList<PVertex>();
    private List<Line2D> lines = new ArrayList<Line2D>();
    private PIndi focusNode;
    private PIndi destinationNode;
    private Point2D focusOffset;
    // private double focusScale;
    private PBounds tmpBounds = new PBounds();
    private Arc2D.Double tmpArc = new Arc2D.Double();
    private PNode cursor;
    private static final int CURSOR_SIZE = 9;
    private int state;
    private static final int STATE_NONE = 0;

    /**
     * The difference with STATE_NONE is that sliding has been already triggered
     * and we want to prevent the user from changing the selection
     */
    private static final int STATE_IDLE = 3;
    private static final int STATE_SLIDING_LEFT = 1;
    private static final int STATE_SLIDING_RIGHT = 2;
    PTransformActivity currentCameraAnimation = null;

    /** change this to enable/disable smooth panning */
    private boolean smoothPanning = true;

    // Graphical attributes.
    private static final Color TEXT_COLOR = new Color(0.2f, 0.2f, 0.8f, 1f);
    private static final Color CURSOR_COLOR = new Color(0.3f, 0.3f, 0.9f, 1f);
    private static final Color PATH_COLOR = new Color(0.4f, 0.4f, 1f, 1f);
    private static final float PATH_STROKED_WIDTH = 1;
    private static final Color BACKGROUND_COLOR = new Color(1f, 1f, 1f, 0.45f);
    private static final int BACKGROUND_RADIUS = 9;

    /**
     * Creates a sliding controller.
     * 
     * @param manager
     *            the QuiltManager
     * @param panEventHandler
     *            the underlying PanEventHandler
     */
    public SlidingController(final QuiltManager manager, final DisabablePanEventHandler panEventHandler)
    {
        super(manager, panEventHandler);
        this.network = manager.getNetwork();
        this.slider = new PNode();
        this.cursor = new PNode();
        this.cursor.setBounds(0, 0, CURSOR_SIZE, CURSOR_SIZE);
    }

    /**
     * Gets the camera bounds.
     * 
     * @param node
     *            the node
     * @return the camera bounds
     */
    private PBounds getCameraBounds(final PNode node)
    {
        PBounds result;

        result = node.getFullBounds();

        this.camera.viewToLocal(result);
        this.camera.globalToLocal(result);

        //
        return result;
    }

    /**
     * In left zone.
     * 
     * @param node
     *            the node
     * @param event
     *            the event
     * @return true, if successful
     */
    private boolean inLeftZone(final PNode node, final PInputEvent event)
    {
        boolean result;

        PBounds bounds = node.getFullBoundsReference();
        /*   if (state == STATE_NONE) {
                tmpBounds.setFrame(b);
                tmpBounds.x -= NODE_SLIDING_OFFSET*4/5;
                tmpBounds.width =  NODE_SLIDING_OFFSET;
                return tmpBounds.contains(event.getPosition());
            } else {*/
        // }

        if (event.getPosition().getX() < bounds.getX())
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * In right zone.
     * 
     * @param node
     *            the node
     * @param event
     *            the event
     * @return true, if successful
     */
    private boolean inRightZone(final PNode node, final PInputEvent event)
    {
        boolean result;

        PBounds bounds = node.getFullBoundsReference();
        /*	if (state == STATE_NONE) {
                tmpBounds.setFrame(b);
                tmpBounds.x += tmpBounds.width-NODE_SLIDING_OFFSET/5;
                tmpBounds.width =  NODE_SLIDING_OFFSET;
                return tmpBounds.contains(event.getPosition()); 
        	} else {*/
        // }

        if (event.getPosition().getX() > bounds.getMaxX())
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is animating pan.
     * 
     * @return true, if is animating pan
     */
    protected boolean isAnimatingPan()
    {
        boolean result;

        if ((this.currentCameraAnimation != null) && (this.currentCameraAnimation.isStepping()))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final PInputEvent event)
    {
        if (this.currentSelection != null)
        {
            PNode selNode = this.currentSelection.getSelectedObject();
            if ((this.state == STATE_NONE) || (this.state == STATE_IDLE))
            {
                if ((selNode instanceof PIndi) && ((!isAnimatingPan())))
                {
                    PIndi pindi = (PIndi) selNode;
                    if (inLeftZone(selNode, event))
                    {
                        setState(pindi, STATE_SLIDING_LEFT);
                    }
                    else if (inRightZone(selNode, event))
                    {
                        setState(pindi, STATE_SLIDING_RIGHT);
                    }
                }
            }
            else if (this.state == STATE_SLIDING_LEFT)
            {
                PIndi pindi = (PIndi) selNode;
                if (!inLeftZone(pindi, event) && pindi.getFullBoundsReference().contains(event.getPosition()))
                {
                    if (!isAnimatingPan())
                    {
                        setState(pindi, STATE_IDLE);
                    }
                }
                else
                {
                    double u = slide(event.getCanvasPosition());
                    if (u == 1)
                    {
                        setState(pindi, STATE_IDLE);
                        setSelectedObject(this.destinationNode);
                    }
                }
            }
            else if (this.state == STATE_SLIDING_RIGHT)
            {
                PIndi pindi = (PIndi) selNode;
                if (!inRightZone(pindi, event) && pindi.getFullBoundsReference().contains(event.getPosition()))
                {
                    if (!isAnimatingPan())
                    {
                        setState(pindi, STATE_IDLE);
                    }
                }
                else
                {
                    double u = slide(event.getCanvasPosition());
                    if (u == 1)
                    {
                        setState(pindi, STATE_IDLE);
                        setSelectedObject(this.destinationNode);
                    }
                }
            }
            else
            {
                PBounds bounds = selNode.getFullBoundsReference();
                if (selNode instanceof PIndi && bounds.contains(event.getPosition()))
                {
                    setState((PIndi) selNode, STATE_IDLE);
                }
            }
        }

        if (this.state == STATE_NONE)
        {
            super.mouseDragged(event);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final PInputEvent event)
    {
        setState(null, STATE_NONE);
        super.mouseReleased(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processEvent(final PInputEvent event, final int type)
    {
        this.camera = event.getCamera();
        super.processEvent(event, type);
    }

    /**
     * Sets the selected object.
     * 
     * @param node
     *            the new selected object
     */
    protected void setSelectedObject(final PNode node)
    {
        if (this.currentSelection != null)
        {
            if (!isAnimatingPan())
            {
                this.currentSelection.setSelectedObject(node);
            }
            else
            {
                this.currentCameraAnimation.setDelegate(new PActivity.PActivityDelegate()
                {
                    /**
                     * 
                     */
                    @Override
                    public void activityFinished(final PActivity arg0)
                    {
                        SlidingController.this.currentSelection.setSelectedObject(node);
                    }

                    /**
                     * 
                     */
                    @Override
                    public void activityStarted(final PActivity arg0)
                    {
                    }

                    /**
                     * 
                     */
                    @Override
                    public void activityStepped(final PActivity arg0)
                    {
                    }
                });
            }
        }
    }

    /**
     * Sets the state.
     * 
     * @param pindi
     *            the pindi
     * @param newState
     *            the new state
     */
    private void setState(final PIndi pindi, final int newState)
    {
        if (this.state != newState || pindi != this.focusNode)
        {
            if (pindi == null)
            {
                assert (newState == STATE_NONE || newState == STATE_IDLE);
            }
            if (newState == STATE_NONE || newState == STATE_IDLE)
            {
                /*if (currentCameraAnimation != null && currentCameraAnimation.isStepping()) {
                	currentCameraAnimation.terminate(PActivity.TERMINATE_AND_FINISH);
                }*/
                this.slider.removeAllChildren();
                this.sliderBackground.removeAllChildren();
                this.cursor.removeAllChildren();
                this.neighbors.clear();
                this.lines.clear();
            }
            else if (newState == STATE_SLIDING_LEFT)
            {
                if (this.state != STATE_IDLE)
                {
                    setState(pindi, STATE_IDLE); // force to state
                }
                List<Indi> parents = this.network.getParents(pindi.getIndi());
                if (parents.isEmpty())
                {
                    setState(pindi, STATE_IDLE); // nothing to do
                    return;
                }
                Collections.sort(parents, QuiltManager.COMPARATOR);

                PBounds b = getCameraBounds(pindi);
                PBounds fullb = pindi.getFullBoundsReference();
                Point2D cameraOrigin = this.camera.getViewBounds().getOrigin();
                // LOG.debug("Focus offset: "+focusOffset);
                this.focusOffset = new Point2D.Double(cameraOrigin.getX() - fullb.getX(), cameraOrigin.getY() - fullb.getCenterY());
                // focusScale = camera.getScale();

                PIndi me = new PIndi(pindi.getIndi());
                me.setBounds(b);
                this.slider.addChild(this.sliderBackground);
                addNodeWithBackground(this.slider, this.sliderBackground, me);

                int i = 1;
                this.tmpArc.setArcByCenter(b.getX(), b.getCenterY(), SLIDING_INNER, 90, 180, Arc2D.OPEN);

                addPathWithBackground(this.slider, this.sliderBackground, this.tmpArc);
                double angle = Math.PI / (parents.size() + 1);

                for (Indi p : parents)
                {
                    PIndi pp = new PIndi(p);
                    // PBounds pb = p.getNode().getFullBoundsReference();
                    // if (camera.getViewBounds().contains(pb)) {
                    // pp.setBounds(getCameraBounds(p.getNode()));
                    // }
                    // else {
                    pp.setX(b.x - Math.sin(angle * i) * SLIDING_DISTANCE - pp.getWidth());
                    pp.setY(b.y - Math.cos(angle * i) * SLIDING_DISTANCE);
                    // }
                    pp.setPaint(new Color(1, 1, 1, 1));
                    this.neighbors.add(pp);
                    addNodeWithBackground(this.slider, this.sliderBackground, pp);
                    Line2D.Double line = new Line2D.Double(b.getX(), b.getCenterY(), pp.getFullBoundsReference().getMaxX(), pp.getFullBoundsReference().getCenterY());
                    this.lines.add(line);

                    addPathWithBackground(this.slider, this.sliderBackground, line);

                    i++;
                }

                PIsoShape cursorshape = new PIsoShape(PIsoShape.CIRCLE);
                cursorshape.setBounds(0, 0, CURSOR_SIZE, CURSOR_SIZE);
                addNodeWithBackground(this.cursor, this.cursor, cursorshape);
                this.cursor.setTransform(AffineTransform.getTranslateInstance(b.getX() - CURSOR_SIZE / 2, b.getCenterY() - CURSOR_SIZE / 2));
                this.slider.addChild(this.cursor);

                this.camera.addChild(this.slider);
            }
            else if (newState == STATE_SLIDING_RIGHT)
            {
                if (this.state != STATE_IDLE)
                {
                    setState(pindi, STATE_IDLE); // force to state
                }
                ArrayList<Vertex> fams = new ArrayList<Vertex>(this.network.getDescendants(pindi.getIndi()));
                if (fams.isEmpty())
                {
                    setState(pindi, STATE_IDLE); // nothing to do
                    return;
                }
                Collections.sort(fams, QuiltManager.COMPARATOR);

                ArrayList<Vertex> children = new ArrayList<Vertex>();
                for (Vertex v : fams)
                {
                    children.addAll(this.network.getDescendants(v));
                }
                Collections.sort(children, QuiltManager.COMPARATOR);
                // TODO change the sliding radius if there are too many children
                PBounds b = getCameraBounds(pindi);
                PBounds fullb = pindi.getFullBoundsReference();
                Point2D cameraOrigin = this.camera.getViewBounds().getOrigin();
                this.focusOffset = new Point2D.Double(cameraOrigin.getX() - fullb.getMaxX(), cameraOrigin.getY() - fullb.getCenterY());
                // focusScale = camera.getScale();

                PIndi me = new PIndi(pindi.getIndi());
                me.setBounds(b);
                this.slider.addChild(this.sliderBackground);
                addNodeWithBackground(this.slider, this.sliderBackground, me);
                double childAngle = -180.0 / (children.size() + 1);
                double initialAngle = 90;
                int i = 1;
                for (Vertex f : fams)
                {
                    double startAngle = initialAngle + i * childAngle - childAngle / 4;
                    i += this.network.getDescendantCount(f);
                    double endAngle = initialAngle + (i - 1) * childAngle + childAngle / 4;
                    if (endAngle < startAngle)
                    {
                        this.tmpArc.setArcByCenter(b.getMaxX(), b.getCenterY(), SLIDING_INNER, startAngle, endAngle - startAngle, Arc2D.OPEN);

                        addPathWithBackground(this.slider, this.sliderBackground, this.tmpArc);
                    }
                }
                double angle = Math.PI / (children.size() + 1);
                double dist = 2 * me.getHeight() / angle;
                dist = Math.max(dist, SLIDING_DISTANCE);

                i = 1;
                for (Vertex p : children)
                {
                    PIndi pp = new PIndi((Indi) p);
                    // PBounds pb = p.getNode().getFullBoundsReference();
                    // if (camera.getViewBounds().contains(pb)) {
                    // pp.setBounds(getCameraBounds(p.getNode()));
                    // }
                    // else {
                    pp.setX(b.getMaxX() + Math.sin(angle * i) * dist);
                    pp.setY(b.y - Math.cos(angle * i) * dist);
                    // }
                    this.neighbors.add(pp);
                    addNodeWithBackground(this.slider, this.sliderBackground, pp);
                    Line2D.Double line = new Line2D.Double(b.getMaxX(), b.getCenterY(), pp.getFullBoundsReference().getMinX(), pp.getFullBoundsReference().getCenterY());
                    this.lines.add(line);

                    addPathWithBackground(this.slider, this.sliderBackground, line);
                    i++;
                }

                PIsoShape cursorshape = new PIsoShape(PIsoShape.CIRCLE);
                cursorshape.setBounds(0, 0, CURSOR_SIZE, CURSOR_SIZE);
                addNodeWithBackground(this.cursor, this.cursor, cursorshape);
                this.cursor.setTransform(AffineTransform.getTranslateInstance(b.getMaxX() - CURSOR_SIZE / 2, b.getCenterY() - CURSOR_SIZE / 2));
                this.slider.addChild(this.cursor);

                this.camera.addChild(this.slider);
            }
            this.state = newState;
            this.focusNode = pindi;
        }
    }

    /**
     * Changes the view bounds of a camera with a smooth animation.
     * 
     * @param camera
     *            the camera
     * @param bounds
     *            the bounds
     */
    protected void setViewBounds(final PCamera camera, final PBounds bounds)
    {
        if (this.smoothPanning)
        {
            // The following allows a camera animation to start while another
            // animation
            // is already running.
            if (isAnimatingPan())
            {
                this.currentCameraAnimation.terminate(PActivity.TERMINATE_WITHOUT_FINISHING);
            }
            this.currentCameraAnimation = camera.animateViewToCenterBounds(bounds, true, 150);
            this.currentCameraAnimation.setSlowInSlowOut(false);
            // Play one step right now, otherwise nothing will happen while
            // dragging the mouse since animations
            // won't have time to play.
            this.currentCameraAnimation.setStartTime(System.currentTimeMillis() - this.currentCameraAnimation.getStepRate());
            this.currentCameraAnimation.processStep(System.currentTimeMillis());
        }
        else
        {
            camera.setViewBounds(bounds);
        }
    }

    /**
     * Slide.
     * 
     * @param position
     *            the position
     * @return the double
     */
    private double slide(final Point2D position)
    {
        double minDistSq = Double.MAX_VALUE;
        Line2D closest = null;
        int closestIdx = -1;
        int i = 0;
        for (Line2D line : this.lines)
        {
            double distance = line.ptSegDistSq(position);
            if (distance < minDistSq)
            {
                minDistSq = distance;
                closest = line;
                closestIdx = i;
            }
            i++;
        }
        if (closest == null)
        {
            return 0;
        }
        double u = closestParamToSegment(closest, position);
        if (u < 0)
        {
            u = 0;
        }
        else if (u > 1)
        {
            u = 1;
        }
        this.cursor.setTransform(AffineTransform.getTranslateInstance((1 - u) * closest.getX1() + u * closest.getX2() - CURSOR_SIZE / 2, (1 - u) * closest.getY1() + u * closest.getY2() - CURSOR_SIZE
                / 2));
        PBounds fnb = this.focusNode.getFullBoundsReference();
        Point2D p1;
        if (this.state == STATE_SLIDING_LEFT)
        {
            p1 = new Point2D.Double(fnb.getX(), fnb.getCenterY());
        }
        else
        {
            p1 = new Point2D.Double(fnb.getMaxX(), fnb.getCenterY());
        }

        // LOG.debug("Starting node: "+p1);
        this.destinationNode = (PIndi) this.neighbors.get(closestIdx).getVertex().getNode();
        PBounds p2b = this.destinationNode.getFullBoundsReference();
        PDimension d = new PDimension(closest.getP1(), closest.getP2());
        this.camera.getViewTransformReference().inverseTransform(d, d);

        Point2D p2;
        if (this.state == STATE_SLIDING_LEFT)
        {
            p2 = new Point2D.Double(p2b.getCenterX() - d.getWidth(), p2b.getCenterY() - d.getHeight());
        }
        else
        {
            p2 = new Point2D.Double(p2b.getCenterX() - d.getWidth(), p2b.getCenterY() - d.getHeight());
        }
        PBounds b = this.camera.getViewBounds();
        if (Math.abs(p2.getX() - p1.getX()) < b.getWidth() && Math.abs(p2.getY() - p1.getY()) < b.getHeight())
        {
            b.x = u * p2.getX() + (1 - u) * p1.getX() + this.focusOffset.getX();
            b.y = u * p2.getY() + (1 - u) * p1.getY() + this.focusOffset.getY();
        }
        else
        {
            // zoom out first, then zoom in
            double phase = u * 2;
            if (phase < 1)
            {
                // zoom out
            }
            else if (phase < 2)
            {
                // zoom in
            }
            b.x = u * p2.getX() + (1 - u) * p1.getX() + this.focusOffset.getX();
            b.y = u * p2.getY() + (1 - u) * p1.getY() + this.focusOffset.getY();
            // camera.setScale(focusScale);
        }
        setViewBounds(this.camera, b);
        return u;
    }

    /**
     * Adds a blurred white background to a node.
     * 
     * @param parent
     *            the parent
     * @param backgroundLayer
     *            the background layer
     * @param child
     *            the child
     */
    protected static void addNodeWithBackground(final PNode parent, final PNode backgroundLayer, final PNode child)
    {

        if (child instanceof PText)
        {
            ((PText) child).setTextPaint(TEXT_COLOR);
        }
        else
        {
            child.setPaint(CURSOR_COLOR);
        }
        PBounds bounds = child.getBoundsReference();

        for (float width = BACKGROUND_RADIUS; width >= -3; width -= 3)
        {
            PPath bgrect = PPath.createRectangle((float) bounds.x - width / 2, (float) bounds.y - width / 2, (float) bounds.width + width, (float) bounds.height + width);
            bgrect.setStroke(null);
            bgrect.setStrokePaint(null);
            bgrect.setPaint(BACKGROUND_COLOR);
            backgroundLayer.addChild(bgrect);
        }

        parent.addChild(child);
    }

    /**
     * Creates a ppath with a blurred white background and adds it to the node.
     * 
     * @param parent
     *            the parent
     * @param backgroundLayer
     *            the background layer
     * @param shape
     *            the shape
     */
    protected static void addPathWithBackground(final PNode parent, final PNode backgroundLayer, final Shape shape)
    {
        for (float width = BACKGROUND_RADIUS + PATH_STROKED_WIDTH; width > PATH_STROKED_WIDTH; width -= 3)
        {
            PPath bgpath = new PPath(shape);
            bgpath.setPaint(null);
            bgpath.setStroke(new BasicStroke(width));
            bgpath.setStrokePaint(BACKGROUND_COLOR);
            backgroundLayer.addChild(bgpath);
        }

        PPath fgpath = new PPath(shape);
        fgpath.setPaint(null);
        fgpath.setStroke(new BasicStroke(PATH_STROKED_WIDTH));
        fgpath.setStrokePaint(PATH_COLOR);
        parent.addChild(fgpath);
    }

    /**
     * Closest param to segment.
     * 
     * @param line
     *            the line
     * @param point
     *            the point
     * @return the double
     */
    private static double closestParamToSegment(final Line2D line, final Point2D point)
    {
        double result;

        if (line == null || point == null)
        {
            // Defensive.
            result = 0;
        }
        else
        {
            final double dx = line.getX2() - line.getX1();
            final double dy = line.getY2() - line.getY1();

            if ((dx == 0) && (dy == 0))
            {
                throw new IllegalArgumentException("p1 and p2 cannot be the same point");
            }
            else
            {
                result = ((point.getX() - line.getX1()) * dx + (point.getY() - line.getY1()) * dy) / (dx * dx + dy * dy);
            }
        }

        //
        return result;
    }
}
