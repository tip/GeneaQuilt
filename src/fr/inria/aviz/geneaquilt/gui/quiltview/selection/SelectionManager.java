/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JMenuItem;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.nodes.PVertex;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight.HighlightManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight.SelectionCombination;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.highlight.SelectionHighlight;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class <b>SelectionManager</b> manages selections.
 * 
 * @author Pierre Dragicevic
 */
public class SelectionManager
{
    private QuiltManager quiltManager;
    private HighlightManager highlightManager;
    private List<Selection> selections;
    private int nextSelectionColorIndex = 0;
    private List<ChangeListener> changeListeners = new ArrayList<ChangeListener>();
    private int inhibitNotify = 0;

    /** To disable and unable selection */
    private JMenuItem exportSelectionItem = null;

    // export

    /**
     * Creates a selection manager managing a quilt manager.
     * 
     * @param quiltManager
     *            the quilt manager
     * @param selectionLayer
     *            the selection layer
     */
    public SelectionManager(final QuiltManager quiltManager, final PNode selectionLayer)
    {
        this.quiltManager = quiltManager;
        this.highlightManager = new HighlightManager(quiltManager.getFullBoundsReference());
        selectionLayer.addChild(this.highlightManager);
        this.selections = new ArrayList<Selection>();
    }

    /**
     * Register a new change listener.
     * 
     * @param listener
     *            the listener
     */
    public void addChangeListener(final ChangeListener listener)
    {
        this.changeListeners.add(listener);
    }

    /**
     * Removes all selections.
     */
    public void clearSelections()
    {
        this.highlightManager.clear();
        SelectionCombination.clear();
        this.selections.clear();
        this.nextSelectionColorIndex = 0;
        fireChangeListeners();
        this.highlightManager.setTestOverlap(false);
    }

    /**
     * Fires a change.
     */
    public void fireChangeListeners()
    {
        if (this.inhibitNotify != 0)
        {
            return;
        }
        if (this.changeListeners.isEmpty())
        {
            return;
        }
        ChangeEvent event = new ChangeEvent(this);
        for (ChangeListener listener : this.changeListeners)
        {
            listener.stateChanged(event);
        }
        if (this.exportSelectionItem != null)
        {
            this.exportSelectionItem.setEnabled(this.selections.size() > 0);
        }
    }

    /**
     * Gets the highlight manager.
     * 
     * @return the foreground layer
     */
    public HighlightManager getHighlightManager()
    {
        return this.highlightManager;
    }

    /**
     * Returns the selection associated with the node.
     * 
     * @param node
     *            the node
     * @return the selection or null
     */
    public Selection getLastSelection(final PNode node)
    {
        Selection result;

        // for (Selection s : selections)
        // if (s.getSelectedObject() == node)
        // return s;
        SelectionHighlight selection = this.highlightManager.getSelectionHighlight(node);
        if (selection == null)
        {
            result = null;
        }
        else
        {
            result = selection.getLastSelection();
        }

        //
        return result;
    }

    /**
     * Gets the network.
     * 
     * @return the network
     */
    public Network getNetwork()
    {
        return this.quiltManager.getNetwork();
    }

    /**
     * Gets the next selection color.
     * 
     * @return the selection color for the next selection
     */
    public Color getNextSelectionColor()
    {
        return GraphicsConstants.SELECTION_COLORS[this.nextSelectionColorIndex % GraphicsConstants.SELECTION_COLORS.length];
    }

    /**
     * Gets the next selection color index.
     * 
     * @return the selection color index for the next selection
     */
    public int getNextSelectionColorIndex()
    {
        return this.nextSelectionColorIndex;
    }

    /**
     * Gets the selected network.
     * 
     * @return the selected network
     */
    public Network getSelectedNetwork()
    {
        Network result;

        // Built the list of selected vertices and edges
        Set<Vertex> vertices = new HashSet<Vertex>();
        Set<Edge> edges = new HashSet<Edge>();
        for (Selection selection : this.selections)
        {
            Set<PNode> nodes = selection.getHighlightedObjects();
            for (PNode node : nodes)
            {
                if (node instanceof PVertex)
                {
                    vertices.add(((PVertex) node).getVertex());
                }
                else if (node instanceof PEdge)
                {
                    edges.add(((PEdge) node).getEdge());
                }
            }
        }

        // Build the network
        result = new Network();
        for (Vertex vertex : vertices)
        {
            result.addVertex(vertex);
        }
        for (Edge edge : edges)
        {
            result.addEdge(edge, edge.getFromVertex(), edge.getToVertex());
        }

        //
        return result;
    }

    /**
     * Gets the selections.
     * 
     * @return the selections
     */
    public Collection<Selection> getSelections()
    {
        return this.selections;
    }

    /**
     * Checks if is empty.
     * 
     * @return if the selection is empty
     */
    public boolean isEmpty()
    {
        return this.selections.isEmpty();
    }

    /**
     * Returns true if the specified node is selectable.
     * 
     * @param node
     *            the node
     * @return true if it can be selected
     */
    public boolean isSelectable(final PNode node)
    {
        boolean result;

        if (node == null)
        {
            result = false;
        }
        else
        {
            // if (isSelected(node))
            // return false;
            result = node instanceof PEdge || node instanceof PFam || node instanceof PIndi;
        }

        //
        return result;
    }

    /**
     * Returns true if the specified node is selected.
     * 
     * @param node
     *            the node
     * @return true/false
     */
    public boolean isSelected(final PNode node)
    {
        for (Selection selection : this.selections)
        {
            if (selection.getSelectedObject() == node)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes all the listeners.
     */
    public void removeAllChangerListeners()
    {
        this.changeListeners.clear();
    }

    /**
     * Removes the specified change listener.
     * 
     * @param listener
     *            the listener
     */
    public void removeChangeListener(final ChangeListener listener)
    {
        this.changeListeners.remove(listener);
    }

    /**
     * Selects the specified node.
     * 
     * @param node
     *            the node
     * @return the new selection object or null
     */
    public Selection select(final PNode node)
    {
        Selection result;

        if (isSelectable(node))
        {
            this.highlightManager.setTestOverlap(this.selections.size() > 0);
            result = new Selection(this, node, getNextSelectionColor());
            this.selections.add(result);
            this.nextSelectionColorIndex += 1;
            fireChangeListeners();
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Sets the export selection item.
     * 
     * @param exportSelectionItem
     *            the new export selection item
     */
    public void setExportSelectionItem(final JMenuItem exportSelectionItem)
    {
        this.exportSelectionItem = exportSelectionItem;
    }

    /**
     * Sets the selection color index for the next selection.
     * 
     * @param index
     *            the new index
     */
    public void setNextSelectionColorIndex(final int index)
    {
        this.nextSelectionColorIndex = index;
    }
}
