/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection;

import java.awt.Color;
import java.awt.geom.Point2D;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PInputEvent;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.DisabablePanEventHandler;
import fr.inria.aviz.geneaquilt.gui.quiltview.GeneaQuiltOldWindow;

/**
 * The Class <b>LinkSlidingController</b> implements the link sliding.
 * 
 * @author Juhee Bae
 */
public class LinkSlidingController extends MouseSelectionController
{
    // private final QuiltManager manager;
    // private final DisabablePanEventHandler panEventHandler;
    // private Selection currentSelection = null;

    private double startX = 0;
    private double startY = 0;
    private double endX = 0;
    private double endY = 0;
    private int ancestor = 2; // # of direction of ancestor: up and left
    private int descendant = 2; // # of direction of descendant: right and down
    private int chosenItem = 0; // edge, indi, fam

    private String direction = "";
    private String suffix = "family";
    private String savedDirection = "ancestor";

    private PNode to;
    private PNode from;
    private boolean draggedMouse;
    private boolean clickedMouse;

    /**
     * Instantiates a new link sliding controller.
     * 
     * @param manager
     *            the manager
     * @param panEventHandler
     *            the pan event handler
     */
    public LinkSlidingController(final QuiltManager manager, final DisabablePanEventHandler panEventHandler)
    {
        super(manager, panEventHandler);
        // this.manager = manager;
        // this.panEventHandler = panEventHandler;
    }

    /**
     * Decide direction.
     * 
     * @param event
     *            the event
     */
    private void decideDirection(final PInputEvent event)
    {
        // TODO Auto-generated method stub
        GeneaQuiltOldWindow quilt = GeneaQuiltOldWindow.getQuilt();
        PCanvas canvas = quilt.getCanvas();
        if (this.draggedMouse)
        {
            // decide direction of mouse movement and move camera to ancestor or
            // descendant
            Point2D p = event.getPosition();

            // grab position when mouse is released
            this.endX = p.getX();
            this.endY = p.getY();

            // inclination of mouse movement
            double vx = (this.endX - this.startX);
            double vy = (this.endY - this.startY);
            double incline = vy / vx;

            double anc_x = -1;
            double anc_y = -1;
            double des_x = 1;
            double des_y = 1;
            double dotproduct = (anc_x * vx + anc_y * vy);

            // dot product > 0 if ancestor, else descendant
            if (dotproduct >= 0)
            {
                // System.out.println("ancestor");
                this.suffix = "ancestor";
            }
            else
            {
                // System.out.println("descendant");
                this.suffix = "descendant";
            }
            // index of ancestor or descendant? 0 OR 1
            int index = 0;

            // <---A <---B---> A ---> (A: left, B: up) or (A: right, B: down)
            // ----------------------
            // -1 1
            do
            {
                if (index == 0 && (incline < -1 || incline > 1))
                {
                    // smaller than -1, greater than 1
                    this.direction = index + this.suffix; // print this to check
                                                          // direction
                    break;
                }
                else if (index == 1 && -1 <= incline && incline <= 1)
                {
                    // in between
                    this.direction = index + this.suffix;
                    break;
                }
                else
                { // exceptional direction

                }
                index++; // status 0 or 1
            } while (index < this.ancestor);

            // get next edge, indi, or fam
            if (this.suffix.equalsIgnoreCase("descendant"))
            {
                this.to = new PNode();
                if (this.chosenItem == 0)
                {
                    PEdge pedge = (PEdge) event.getPickedNode();
                    this.to = pedge.getEdge().getFromVertex().getNode();
                }
                else if (this.chosenItem == 1)
                {
                    PIndi pindi = (PIndi) event.getPickedNode();
                    this.to = this.currentSelection.findNextSuccessor(pindi.getIndi());
                }
                else if (this.chosenItem == 2)
                {
                    PFam pfam = (PFam) event.getPickedNode();
                    this.to = this.currentSelection.findNextSuccessor(pfam.getFam());
                }
                else
                {
                }
                if (this.to != null)
                {
                    canvas.getCamera().animateViewToCenterBounds(this.to.getFullBounds(), false, 500);
                    this.to.setPaint(Color.yellow);
                }
            }// get previous edge, indi, or fam
            else if (this.suffix.equalsIgnoreCase("ancestor"))
            {
                this.from = new PNode();
                if (this.chosenItem == 0)
                {
                    PEdge pedge = (PEdge) event.getPickedNode();
                    this.from = pedge.getEdge().getToVertex().getNode();
                }
                else if (this.chosenItem == 1)
                {
                    PIndi pindi = (PIndi) event.getPickedNode();
                    this.from = this.currentSelection.findNextPredecessor(pindi.getIndi());
                }
                else if (this.chosenItem == 2)
                {
                    PFam pfam = (PFam) event.getPickedNode();
                    this.from = this.currentSelection.findNextPredecessor(pfam.getFam());
                }
                else
                {
                }
                if (this.from != null)
                {
                    canvas.getCamera().animateViewToCenterBounds(this.from.getFullBounds(), false, 500);
                    this.from.setPaint(Color.green);
                }
            }

            // manager.saveDirection(suffix);
        }
        else
        {
            // if the mouse is not dragged, then get the direction saved

            // if(manager.getSavedDirection()!=null) savedDirection =
            // manager.getSavedDirection();

            // get next edge, indi, or fam
            if (this.savedDirection.equalsIgnoreCase("descendant"))
            {
                this.to = new PNode();
                if (this.chosenItem == 0)
                {
                    PEdge pedge = (PEdge) event.getPickedNode();
                    this.to = pedge.getEdge().getFromVertex().getNode();
                }
                else if (this.chosenItem == 1)
                {
                    PIndi pindi = (PIndi) event.getPickedNode();
                    this.to = this.currentSelection.findNextSuccessor(pindi.getIndi());
                }
                else if (this.chosenItem == 2)
                {
                    PFam pfam = (PFam) event.getPickedNode();
                    this.to = this.currentSelection.findNextSuccessor(pfam.getFam());
                }
                else
                {
                }
                if (this.to != null)
                {
                    canvas.getCamera().animateViewToCenterBounds(this.to.getFullBounds(), false, 500);
                    this.to.setPaint(Color.yellow);
                }
            }// get previous edge, indi, or fam
            else if (this.savedDirection.equalsIgnoreCase("ancestor"))
            {
                this.from = new PNode();
                if (this.chosenItem == 0)
                {
                    PEdge pedge = (PEdge) event.getPickedNode();
                    this.from = pedge.getEdge().getToVertex().getNode();
                }
                else if (this.chosenItem == 1)
                {
                    PIndi pindi = (PIndi) event.getPickedNode();
                    this.from = this.currentSelection.findNextPredecessor(pindi.getIndi());
                }
                else if (this.chosenItem == 2)
                {
                    PFam pfam = (PFam) event.getPickedNode();
                    this.from = this.currentSelection.findNextPredecessor(pfam.getFam());
                }
                else
                {
                }
                if (this.from != null)
                {
                    canvas.getCamera().animateViewToCenterBounds(this.from.getFullBounds(), false, 500);
                    this.from.setPaint(Color.green);
                }
            }
        }

    }

    /* (non-Javadoc)
     * @see fr.inria.aviz.geneaquilt.gui.quiltview.selection.MouseSelectionController#mouseDragged(edu.umd.cs.piccolo.event.PInputEvent)
     */
    @Override
    public void mouseDragged(final PInputEvent event)
    {

        super.mouseDragged(event);

        // drag and change color of the node - true
        this.draggedMouse = true;
        // if (currentSelection != null) {
        // updateSelection(event);
        // } else{
        // super.mouseDragged(event);
        // }
    }

    /*private void updateSelection(PInputEvent event) {
    	// TODO Auto-generated method stub

    	if (event.isLeftMouseButton()) {

    		PNode node = getPickedNode(event);
    				
    		SelectionManager selections = manager.getSelectionManager();
    		
    		// Drag an existing selection?
    		if (currentSelection == null && selections.isSelected(node)) {
    			currentSelection = selections.getLastSelection(node);
    		}
    		
    		if (selections.isSelectable(node)) {
    			if (currentSelection == null) {
    				if (!event.isShiftDown())
    					selections.clearSelections();
    				currentSelection = selections.select(node);
    			} else {
    				currentSelection.setSelectedObject(node);
    			}					

    			currentSelection.setHighlightMode(HighlightMode.HIGHLIGHT_ALL);
    		}
    	}
    }*/

    // Like event.getPickedNode() but does not grab objects during drags
    /*protected PNode getPickedNode(PInputEvent event) {
    	PPickPath picked = new PPickPath(event.getCamera(), new PBounds(event.getPosition().getX(), event.getPosition().getY(), 1, 1));
    	manager.fullPick(picked);
    	return picked.getPickedNode();
    }*/

    /* (non-Javadoc)
     * @see fr.inria.aviz.geneaquilt.gui.quiltview.selection.MouseSelectionController#mousePressed(edu.umd.cs.piccolo.event.PInputEvent)
     */
    @Override
    public void mousePressed(final PInputEvent event)
    {

        super.mousePressed(event);

        // currentSelection = null;
        // updateSelection(event);
        this.clickedMouse = true;

        // remove the colors of previously focused nodes
        if (this.to != null)
        {
            this.to.setPaint(Color.white);
        }
        if (this.from != null)
        {
            this.from.setPaint(Color.white);
        }

        Point2D p = event.getPosition();
        // save the current position
        this.startX = p.getX();
        this.startY = p.getY();

        PNode pnode = event.getPickedNode();
        if (pnode instanceof PEdge)
        {
            this.chosenItem = 0;
        }
        else if (pnode instanceof PIndi)
        {
            this.chosenItem = 1;
        }
        else if (pnode instanceof PFam)
        {
            this.chosenItem = 2;
        }
        else
        {
            this.chosenItem = 3;
        }

        this.draggedMouse = false;

        // if (currentSelection != null) {
        // panEventHandler.setEnabled(false);
        // } else{
        // super.mousePressed(event);
        // }
    }

    /*public void mouseClicked(PInputEvent event) {
    	if (currentSelection == null && event.isLeftMouseButton() && !event.isShiftDown()) {
    		SelectionManager selections = manager.getSelectionManager();
    		selections.clearSelections();
    	} else if(event.isRightMouseButton()){
    		SelectionManager selections = manager.getSelectionManager();
    		selections.clearSelections();
    	} else { 
    		super.mouseClicked(event);
    	}
    }*/

    /* (non-Javadoc)
     * @see fr.inria.aviz.geneaquilt.gui.quiltview.selection.MouseSelectionController#mouseReleased(edu.umd.cs.piccolo.event.PInputEvent)
     */
    @Override
    public void mouseReleased(final PInputEvent event)
    {
        super.mouseReleased(event);

        if (this.currentSelection != null)
        {
            // move camera when mouse released
            decideDirection(event);
            this.panEventHandler.setEnabled(true);
        }
    }
}
