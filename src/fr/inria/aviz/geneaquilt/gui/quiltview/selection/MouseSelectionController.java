/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.selection;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPickPath;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.DisabablePanEventHandler;

/**
 * The Class <b>MouseSelectionController</b> implements the selection and Pan.
 * 
 * @author Pierre Dragicevic
 */
public class MouseSelectionController extends PBasicInputEventHandler
{
    protected final QuiltManager manager;
    protected final DisabablePanEventHandler panEventHandler;
    protected Selection currentSelection = null;
    protected boolean selectionCreated = false;
    protected boolean selectionEnabled = true;

    /**
     * Creates a controller on a quilt manager and a pan event handler.
     * 
     * @param manager
     *            the quilt manager
     * @param panEventHandler
     *            the pan event handler
     */
    public MouseSelectionController(final QuiltManager manager, final DisabablePanEventHandler panEventHandler)
    {
        this.manager = manager;
        this.panEventHandler = panEventHandler;
    }

    /**
     * Like event.getPickedNode() but does not grab objects during drags.
     * 
     * @param event
     *            the event
     * @return the picked node
     */
    protected PNode getPickedNode(final PInputEvent event)
    {
        PPickPath picked = new PPickPath(event.getCamera(), new PBounds(event.getPosition().getX(), event.getPosition().getY(), 1, 1));
        this.manager.fullPick(picked);
        return picked.getPickedNode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final PInputEvent event)
    {
        if (this.currentSelection == null && event.isLeftMouseButton() && !event.isShiftDown())
        {
            SelectionManager selections = this.manager.getSelectionManager();
            selections.clearSelections();
        }
        else if (this.currentSelection != null && !this.selectionCreated && !event.isShiftDown())
        {
            this.currentSelection.toggleHighlightMode();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final PInputEvent event)
    {
        if (this.currentSelection != null)
        {
            updateSelection(event);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final PInputEvent event)
    {
        this.currentSelection = null;
        this.selectionCreated = false;
        updateSelection(event);

        if (this.currentSelection != null)
        {
            this.panEventHandler.setEnabled(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final PInputEvent event)
    {
        if (this.currentSelection != null)
        {
            this.panEventHandler.setEnabled(true);
        }
    }

    /**
     * Update selection.
     * 
     * @param event
     *            the event
     */
    protected void updateSelection(final PInputEvent event)
    {
        if (event.isLeftMouseButton())
        {
            PNode node = getPickedNode(event);

            SelectionManager selections = this.manager.getSelectionManager();

            // Drag an existing selection?
            if (this.currentSelection == null && selections.isSelected(node))
            {
                this.currentSelection = selections.getLastSelection(node);
            }

            if (selections.isSelectable(node))
            {
                if (this.currentSelection == null)
                {
                    if (!event.isShiftDown())
                    {
                        selections.clearSelections();
                    }
                    this.currentSelection = selections.select(node);
                    this.selectionCreated = true;
                }
                else
                {
                    this.currentSelection.setSelectedObject(node);
                }
            }
        }
    }
}
