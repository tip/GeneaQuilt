/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015,2016 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.freehep.graphicsbase.util.export.ExportDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PDebug;
import fr.devinsy.util.StringList;
import fr.inria.aviz.geneaquilt.gui.nodes.GraphicsConstants;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.gui.util.PrintConstants;
import fr.inria.aviz.geneaquilt.gui.util.PrintUtilities;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;
import fr.inria.aviz.geneaquilt.model.io.DOTLayersReader;
import fr.inria.aviz.geneaquilt.model.io.DOTWriter;
import fr.inria.aviz.geneaquilt.model.io.GEDReader;
import fr.inria.aviz.geneaquilt.model.io.JSONWriter;
import fr.inria.aviz.geneaquilt.model.io.LayerWriter;
import fr.inria.aviz.geneaquilt.model.io.LayersReader;
import fr.inria.aviz.geneaquilt.model.io.PEDReader;
import fr.inria.aviz.geneaquilt.model.io.TIPReader;

/**
 * Class GeneaQuilt
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class GeneaQuiltOldWindow
{
    private Logger logger = LoggerFactory.getLogger(GeneaQuiltOldWindow.class);

    private static final String DEFAULT_TITLE = "Genealogy Quilt";

    private File currentFile;
    private Network network;
    private JFrame frame;
    private GeneaQuiltPanel quiltPanel = null;

    /**
     * Creates a GeneaQuilt with a specified PCanvas to draw to
     * 
     * @param source
     *            the file name
     */
    public GeneaQuiltOldWindow(final File source)
    {
        this.currentFile = source;
        // ///////////////////////////////////////////

        this.frame = new JFrame(DEFAULT_TITLE);
        this.frame.getContentPane().setLayout(new BorderLayout());
        JMenuBar mb = new JMenuBar();
        this.frame.setJMenuBar(mb);
        JMenu fileMenu = new JMenu("File");
        mb.add(fileMenu);
        JMenuItem openMenu = new JMenuItem("Open...");
        openMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                // Menu Open.
                File selectedFile = showFileChooser();

                if (selectedFile != null)
                {
                    GUIUtils.beginLongComputation(GeneaQuiltOldWindow.this.frame, "Loading file…");
                    GeneaQuiltOldWindow.this.currentFile = selectedFile;
                    Network loadedNetwork = load(selectedFile);
                    GUIUtils.endLongComputation(GeneaQuiltOldWindow.this.frame);

                    if (loadedNetwork == null)
                    {
                        GeneaQuiltOldWindow.this.logger.error("Couldn't read the network");
                        JOptionPane.showMessageDialog(GeneaQuiltOldWindow.this.frame, "Could not read the file. Make sure its format is correct.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else
                    {
                        GeneaQuiltOldWindow.this.frame.remove(GeneaQuiltOldWindow.this.quiltPanel);

                        GeneaQuiltOldWindow.this.network = loadedNetwork;
                        GeneaQuiltOldWindow.this.frame.setTitle(DEFAULT_TITLE + ": " + selectedFile);

                        GUIUtils.beginLongComputation(GeneaQuiltOldWindow.this.frame, "Reading layers…");
                        final LayersReader layersReader = new LayersReader();
                        // GUIUtils.updateComputationMessage("Loading layers...");

                        if (!layersReader.load(selectedFile.getAbsolutePath(), loadedNetwork))
                        {
                            GUIUtils.endLongComputation(GeneaQuiltOldWindow.this.frame);
                            GUIUtils.beginLongComputation(GeneaQuiltOldWindow.this.frame, "Building layers…");
                            DOTLayersReader reader = new DOTLayersReader();
                            reader.load(loadedNetwork);
                        }
                        GUIUtils.endLongComputation(GeneaQuiltOldWindow.this.frame);

                        GeneaQuiltOldWindow.this.quiltPanel = new GeneaQuiltPanel(GeneaQuiltOldWindow.this.frame, GeneaQuiltOldWindow.this.network);
                        GeneaQuiltOldWindow.this.frame.getContentPane().add(GeneaQuiltOldWindow.this.quiltPanel, BorderLayout.CENTER);

                        GeneaQuiltOldWindow.this.frame.validate();

                        if (!layersReader.layerFileExists(GeneaQuiltOldWindow.this.currentFile.getAbsolutePath()))
                        {
                            GUIUtils.beginLongComputation(GeneaQuiltOldWindow.this.frame, "Saving layers…");
                            saveLayer();
                            GUIUtils.endLongComputation(GeneaQuiltOldWindow.this.frame);
                        }
                    }
                }
            }
        });
        fileMenu.add(openMenu);
        fileMenu.addSeparator();

        JMenuItem exportMenu = new JMenuItem("Export to JSON...");
        exportMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                saveAsJSON();
            }
        });
        fileMenu.add(exportMenu);

        exportMenu = new JMenuItem("Export to DOT...");
        exportMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                saveAsDOT();
            }
        });
        fileMenu.add(exportMenu);

        //
        JMenuItem exportSelectionMenu = new JMenuItem("Export selection to DOT...");
        exportSelectionMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                saveSelectionAsDOT();
            }
        });
        fileMenu.add(exportSelectionMenu);

        //
        JMenuItem exportGraphicMenu = new JMenuItem("Export Graphics to…");
        exportGraphicMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                PCanvas panel = GeneaQuiltOldWindow.this.quiltPanel.getCanvas();

                if (panel != null)
                {
                    ExportDialog export = new ExportDialog();
                    export.showExportDialog(panel, "Export view as…", panel, (GeneaQuiltOldWindow.this.currentFile).getName());
                }
            }
        });
        fileMenu.add(exportGraphicMenu);

        //
        JMenuItem statsMenu = new JMenuItem("Stats...");
        statsMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                JOptionPane.showMessageDialog(GeneaQuiltOldWindow.this.frame, getStats(), "Stats", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        fileMenu.add(statsMenu);

        fileMenu.addSeparator();
        JMenuItem quitMenu = new JMenuItem("Quit");
        quitMenu.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                if (JOptionPane.showConfirmDialog(GeneaQuiltOldWindow.this.frame, "Confirm quit?", "Quit", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                {
                    System.exit(0);
                }
            }
        });
        fileMenu.add(quitMenu);

        JMenu viewMenu = new JMenu("View");
        mb.add(viewMenu);
        JMenuItem viewAll = new JMenuItem("View All");
        viewAll.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                GeneaQuiltOldWindow.this.quiltPanel.viewAll();
            }
        });
        viewMenu.add(viewAll);

        ButtonGroup labelBy = new ButtonGroup();
        JRadioButtonMenuItem labelByName = new JRadioButtonMenuItem("Label by Name", true);
        labelByName.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    GeneaQuiltOldWindow.this.quiltPanel.setLabelBy("NAME");
                }
            }
        });
        labelBy.add(labelByName);
        viewMenu.add(labelByName);

        JRadioButtonMenuItem labelBySurname = new JRadioButtonMenuItem("Label by Surname", false);
        labelBySurname.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    GeneaQuiltOldWindow.this.quiltPanel.setLabelBy("NAME.SURN");
                }
            }
        });
        labelBy.add(labelBySurname);
        viewMenu.add(labelBySurname);

        JRadioButtonMenuItem labelByGiven = new JRadioButtonMenuItem("Label by Given Name", false);
        labelByGiven.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    GeneaQuiltOldWindow.this.quiltPanel.setLabelBy("NAME.GIVN");
                }
            }
        });
        labelBy.add(labelByGiven);
        viewMenu.add(labelByGiven);

        viewMenu.addSeparator();

        JCheckBoxMenuItem dotDebug = new JCheckBoxMenuItem("Keep DOT file", DOTLayersReader.isDebug());
        viewMenu.add(dotDebug);
        dotDebug.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                DOTLayersReader.setDebug(event.getStateChange() == ItemEvent.SELECTED);
            }
        });

        JCheckBoxMenuItem viewDebug = new JCheckBoxMenuItem("Frame Rate");
        viewMenu.add(viewDebug);
        viewDebug.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                if (event.getStateChange() == ItemEvent.SELECTED)
                {
                    PDebug.debugPrintFrameRate = true;
                }
                else
                {
                    PDebug.debugPrintFrameRate = false;
                }
            }
        });
        viewDebug.setSelected(PDebug.debugPrintFrameRate);

        JCheckBoxMenuItem threadDebug = new JCheckBoxMenuItem("Thread Bugs");
        viewMenu.add(threadDebug);
        threadDebug.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                PDebug.debugThreads = event.getStateChange() == ItemEvent.SELECTED;
            }
        });
        viewDebug.setSelected(PDebug.debugThreads);

        final JCheckBoxMenuItem textOutlines = new JCheckBoxMenuItem("Text outlines");
        viewMenu.add(textOutlines);
        textOutlines.addItemListener(new ItemListener()
        {
            /**
             * 
             */
            @Override
            public void itemStateChanged(final ItemEvent event)
            {
                // TODO
                // getQuilt().outlineManager.setEnabled(textOutlines.isSelected());
                GeneaQuiltOldWindow.this.quiltPanel.getCanvas().repaint();
            }
        });
        textOutlines.setSelected(false);// getQuilt().outlineManager.isEnabled());

        JMenu editMenu = new JMenu("Edit");
        mb.add(editMenu);
        // JMenuItem estimateDates = new JMenuItem("Estimate Dates");
        // editMenu.add(estimateDates);
        // estimateDates.addActionListener(new ActionListener() {
        // public void actionPerformed(ActionEvent ev) {
        // if (getQuilt() != null)
        // getQuilt().estimateDates();
        // }
        // });

        this.quiltPanel = new GeneaQuiltPanel(this.frame, this.network);
        this.frame.getContentPane().add(this.quiltPanel, BorderLayout.CENTER);

        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.pack();
        resizeAndCenterWindow(this.frame);
        this.frame.setVisible(true);
    }

    /**
     * @return the GeneaQuilt
     */
    public GeneaQuiltPanel getQuilt()
    {
        return this.quiltPanel;
    }

    /**
     * Computes statistics about the genealogy.
     * 
     * @return the stats.
     */
    public String getStats()
    {
        String result;

        if (this.currentFile == null)
        {
            result = "No file. No stats. Please, open a file.";
        }
        else
        {
            StringList buffer = new StringList();
            buffer.append("Stats for ").append(this.currentFile.getAbsolutePath()).appendln(":");
            buffer.appendln();

            int individuals = 0;
            int families = 0;
            for (Vertex vertex : this.network.getVertices())
            {
                PNode node = vertex.getNode();
                if (node instanceof PIndi)
                {
                    individuals += 1;
                }
                else if (node instanceof PFam)
                {
                    families += 1;
                }
            }
            int edges = this.network.getEdgeCount();
            int generations = this.quiltPanel.getQuiltManager().getIndiGenerations().length;

            buffer.append(generations).appendln(" generations");
            buffer.append(families).appendln(" families");
            buffer.append(individuals).appendln(" individuals");
            buffer.append(edges).appendln(" edges");

            result = buffer.toString();
        }

        //
        return result;
    }

    /**
	 * 
	 */
    void saveAsDOT()
    {
        saveAsDOT(this.network);
    }

    /**
     * 
     * @param network
     */
    void saveAsDOT(final Network network)
    {
        if (this.quiltPanel != null)
        {
            JFileChooser jexportfile = new JFileChooser(this.currentFile.getParentFile());

            jexportfile.addChoosableFileFilter(new FileFilter()
            {
                /**
                 * 
                 */
                @Override
                public boolean accept(final File file)
                {
                    boolean result;

                    if (file.isDirectory())
                    {
                        result = true;
                    }
                    else
                    {
                        result = file.getName().endsWith(".dot");
                    }

                    //
                    return result;
                }

                /**
                 * 
                 */
                @Override
                public String getDescription()
                {
                    return "Choose a DOT filename";
                }
            });

            String defaultFilename = this.currentFile.getAbsolutePath() + File.separator + this.currentFile.getName().substring(0, this.currentFile.getName().length() - 4) + ".dot";
            jexportfile.setSelectedFile(new File(defaultFilename));
            int ret = jexportfile.showSaveDialog(this.frame);
            if (ret == JFileChooser.APPROVE_OPTION)
            {
                String targetFilename = jexportfile.getSelectedFile().getAbsolutePath();
                DOTWriter writer = new DOTWriter(network);
                // writer.setBare(true);
                try
                {
                    writer.write(targetFilename);
                }
                catch (Exception exception)
                {
                    JOptionPane.showMessageDialog(this.frame, exception.getMessage());
                }
            }
        }
    }

    /**
	 * 
	 */
    private void saveAsJSON()
    {
        saveAsJSON(this.network);
    }

    /**
     * 
     * @param network
     */
    private void saveAsJSON(final Network network)
    {
        if (this.quiltPanel != null)
        {
            JFileChooser jexportfile = new JFileChooser(this.currentFile.getParentFile());
            jexportfile.addChoosableFileFilter(new FileFilter()
            {
                /**
                 * 
                 */
                @Override
                public boolean accept(final File file)
                {
                    boolean result;

                    if (file.isDirectory())
                    {
                        result = true;
                    }
                    else
                    {
                        result = file.getName().endsWith(".json");
                    }

                    //
                    return result;
                }

                /**
                 * 
                 */
                @Override
                public String getDescription()
                {
                    return "Choose a JSON filename";
                }
            });
            String defaultFilename = this.currentFile.getAbsolutePath() + File.separator + this.currentFile.getName().substring(0, this.currentFile.getName().length() - 4) + ".json";
            jexportfile.setSelectedFile(new File(defaultFilename));
            int ret = jexportfile.showSaveDialog(this.frame);
            if (ret == JFileChooser.APPROVE_OPTION)
            {
                String targetFilename = jexportfile.getSelectedFile().getAbsolutePath();
                JSONWriter writer = new JSONWriter(network);
                // writer.setBare(true);
                try
                {
                    writer.write(targetFilename);
                }
                catch (Exception exception)
                {
                    JOptionPane.showMessageDialog(this.frame, exception.getMessage());
                }
            }
        }
    }

    /**
	 * 
	 */
    private void saveLayer()
    {
        try
        {
            if (this.quiltPanel != null)
            {
                int last = this.currentFile.getAbsolutePath().lastIndexOf('.');
                String lyerfile;
                if (last == -1)
                {
                    lyerfile = this.currentFile.getAbsolutePath() + ".lyr";
                }
                else
                {
                    lyerfile = this.currentFile.getAbsolutePath().substring(0, last) + ".lyr";
                }
                File file = new File(lyerfile);
                if ((!file.exists())
                        || (JOptionPane.showConfirmDialog(this.frame, "File " + this.currentFile.getAbsolutePath() + " already exists.", "Replace", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION))
                {
                    LayerWriter writer = new LayerWriter(this.network);
                    writer.write(lyerfile);
                }
            }
        }
        catch (IOException exception)
        {
            this.logger.error("Cannot write layer file", exception);
        }
    }

    /**
	 * 
	 */
    private void saveSelectionAsDOT()
    {
        saveAsDOT(this.quiltPanel.getQuiltManager().getSelectionManager().getSelectedNetwork());
    }

    /**
	 * 
	 */
    private File showFileChooser()
    {
        File result;

        // if (quilt != null && quilt.bird != null)
        // quilt.bird.setVisible(false);

        JFileChooser jfile = null;
        jfile = new JFileChooser("data/");
        jfile.addChoosableFileFilter(new FileFilter()
        {
            /**
             * 
             */
            @Override
            public boolean accept(final File file)
            {
                boolean result;

                if (file.isDirectory())
                {
                    result = true;
                }
                else
                {
                    String name = file.getName().toLowerCase();
                    result = StringUtils.endsWithAny(name, ".ged", ".tip", ".ped");
                }

                //
                return result;
            }

            /**
             * 
             */
            @Override
            public String getDescription()
            {
                return "Choose a genealogical file";
            }
        });

        int ret = jfile.showOpenDialog(this.frame);
        if (ret == JFileChooser.APPROVE_OPTION)
        {
            result = jfile.getSelectedFile();
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Prints.
     */
    public void simplePrint()
    {
        GraphicsConstants.instance = new PrintConstants();
        this.quiltPanel.getQuiltManager().rebuild();
        PrintUtilities.printComponent(this.quiltPanel.getCanvas());
        GraphicsConstants.instance = new GraphicsConstants();
        this.quiltPanel.getQuiltManager().rebuild();
    }

    /**
     * 
     * @param source
     */
    private static Network load(final File source)
    {
        Network result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            if (StringUtils.endsWithIgnoreCase(source.getAbsolutePath(), ".tip"))
            {
                result = TIPReader.load(source.getAbsolutePath());
            }
            else if (StringUtils.endsWithIgnoreCase(source.getAbsolutePath(), ".ped"))
            {
                PEDReader reader = new PEDReader();
                result = reader.load(source.getAbsolutePath());
            }
            else
            { // if (lowercaseFilename.endsWith(".ged")) { // defaults to GED
                result = GEDReader.load(source.getAbsolutePath());
            }

            if (result != null)
            {
                result.setName(FilenameUtils.getBaseName(source.getName()));
            }
        }

        //
        return result;
    }

    /**
     * The main program
     * 
     * @param args
     */
    public static void main(final String[] args)
    {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            /**
             * 
             */
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\nTo fix this problem, run GeneaQuilts from the command line:\njava -jar -Xms256m geneaquilt-x.x.x.jar\n\nIf you still get the same error, increase the value 256 above.";
                }
                else
                {
                    message = "An error occured: " + exception.getClass() + "(" + exception.getMessage() + ")";
                }

                JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
                exception.printStackTrace();
            }
        });

        File loggerConfig = new File("log4j.properties");
        Logger logger;
        if (loggerConfig.exists())
        {
            PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
            logger = LoggerFactory.getLogger(GeneaQuiltOldWindow.class);
            logger.info("Dedicated log configuration done.");
            logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
        }
        else
        {
            BasicConfigurator.configure();
            logger = LoggerFactory.getLogger(GeneaQuiltOldWindow.class);
            logger.info("Basic log configuration done.");
            logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
        }

        //
        if (args.length > 1)
        {
            logger.warn("WARNING: Only one genealogy file can be loaded at a time. Loading the first one...");
        }

        //
        File file;
        if (ArrayUtils.isEmpty(args))
        {
            file = null;
        }
        else
        {
            file = new File(args[0]);

        }

        if ((file == null) || (file.exists()))
        {
            new GeneaQuiltOldWindow(file);
        }
        else
        {
            logger.error("The file {} does not exist. Quitting the application.", file.getAbsoluteFile());
            System.exit(0);
        }
    }

    /**
     * 
     * @param window
     */
    private static void resizeAndCenterWindow(final JFrame window)
    {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) (screenSize.width * GraphicsConstants.MAIN_WINDOW_SIZE);
        int height = (int) (screenSize.height * GraphicsConstants.MAIN_WINDOW_SIZE);

        if (width > GraphicsConstants.MAIN_WINDOW_MAX_WIDTH)
        {
            width = GraphicsConstants.MAIN_WINDOW_MAX_WIDTH;
        }
        if (height > GraphicsConstants.MAIN_WINDOW_MAX_HEIGHT)
        {
            height = GraphicsConstants.MAIN_WINDOW_MAX_HEIGHT;
        }

        window.setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2, width, height);
    }
}
