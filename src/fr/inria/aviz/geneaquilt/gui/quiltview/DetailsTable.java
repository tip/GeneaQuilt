/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.PEdge;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.PIndi;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.Selection;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.SelectionManager;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * The Class <b>DetailsTable</b> Visualize the details of selected items.
 * 
 * @author Jean-Daniel Fekete
 */
public class DetailsTable extends JTable implements ChangeListener
{
    /**
     * The Class TextAreaRenderer.
     */
    private static class TextAreaRenderer extends DefaultTableCellRenderer implements TableCellRenderer
    {
        private static final long serialVersionUID = 2080377988045527083L;

        /* (non-Javadoc)
         * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
         */
        @Override
        public Component getTableCellRendererComponent(final JTable jTable, final Object obj, final boolean isSelected, final boolean hasFocus, final int row, final int column)
        {
            String value = obj.toString();
            super.getTableCellRendererComponent(jTable, value, isSelected, hasFocus, row, column);
            if (value.indexOf('\n') != -1)
            {
                value = "<html>" + value.replace("\n", "<br>") + "</html>";
            }
            setToolTipText(value);
            return this;
        }
    }

    private static final long serialVersionUID = 4648869059634290177L;

    private SelectionManager selManager;
    private DefaultTableModel model;
    private JScrollPane scroll;

    private List<Color> cellColor = new ArrayList<Color>();
    private static Vector<String> emptyLine = new Vector<String>();

    static
    {
        emptyLine.add("");
        emptyLine.add("");
    }

    /**
     * Creates a details table looking at the selection.
     * 
     * @param selManager
     *            the selection manager
     */
    public DetailsTable(final SelectionManager selManager)
    {
        this.selManager = selManager;
        setFont(new Font("Helvetica", 0, 11));
        setRowHeight(13);
        if (selManager != null)
        {
            selManager.addChangeListener(this);
        }
        this.model = (DefaultTableModel) getModel();
        this.model.addColumn("Attribute");
        this.model.addColumn("Value");
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        TableColumnModel tcm = getColumnModel();
        tcm.getColumn(0).setPreferredWidth(75);
        tcm.getColumn(0).setMinWidth(75);
        tcm.getColumn(0).setCellRenderer(new DefaultTableCellRenderer()
        {
            private static final long serialVersionUID = -4500452132026687914L;

            /**
             * 
             */
            @Override
            public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column)
            {
                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setBackground(DetailsTable.this.cellColor.get(row));
                return this;
            }
        });
        tcm.getColumn(1).setPreferredWidth(150);
        tcm.getColumn(1).setMinWidth(150);
        tcm.getColumn(1).setCellRenderer(new DefaultTableCellRenderer()
        {
            private static final long serialVersionUID = -7365517749292104447L;

            /**
             * 
             */
            @Override
            public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column)
            {
                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

                String text;
                if (value == null)
                {
                    text = "";
                }
                else
                {
                    text = value.toString();
                }

                if (text.indexOf('\n') != -1)
                {
                    text = "<html>" + text.replace("\n", "<br>") + "</html>";
                }

                setToolTipText(text);

                //
                return this;
            }
        });

        setPreferredScrollableViewportSize(getPreferredSize());
    }

    /**
     * Gets the scroll pane.
     * 
     * @return a scroll pane on that table
     */
    public JScrollPane getScrollPane()
    {
        JScrollPane result;

        if (this.scroll == null)
        {
            this.scroll = new JScrollPane(this);
            JTableHeader header = getTableHeader();
            DefaultTableCellRenderer dcr = (DefaultTableCellRenderer) header.getDefaultRenderer();
            dcr.setHorizontalAlignment(SwingConstants.LEFT);
            header.setReorderingAllowed(false);

            this.scroll.setColumnHeaderView(header);
            this.scroll.setPreferredSize(new Dimension(200, 300));
        }

        result = this.scroll;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(final int row, final int column)
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stateChanged(final ChangeEvent event)
    {
        updateSelection();
    }

    /**
     * Update.
     * 
     * @param vertex
     *            the vertex
     * @param color
     *            the color
     */
    protected void update(final Vertex vertex, final Color color)
    {

        if (this.model.getRowCount() > 0)
        {
            this.model.addRow(emptyLine);
            this.cellColor.add(Color.white);
            setRowHeight(this.model.getRowCount() - 1, 6);
        }

        for (Entry<String, Object> entry : vertex.getProps().entrySet())
        {
            Vector row = new Vector();
            row.add(entry.getKey());
            row.add(entry.getValue());
            this.model.addRow(row);
            setRowHeight(this.model.getRowCount() - 1, getRowHeight());
            this.cellColor.add(color);
        }
    }

    /**
     * Update edge.
     * 
     * @param edge
     *            the edge
     * @param color
     *            the color
     */
    protected void updateEdge(final Edge edge, final Color color)
    {
    }

    /**
     * Update selection.
     */
    protected void updateSelection()
    {
        this.model.setNumRows(0);
        this.cellColor.clear();
        for (Selection selection : this.selManager.getSelections())
        {
            PNode node = selection.getSelectedObject();
            Color color = selection.getStrongColor();

            if (node instanceof PIndi)
            {
                PIndi pindi = (PIndi) node;
                Indi indi = pindi.getIndi();
                update(indi, color);
            }
            else if (node instanceof PFam)
            {
                PFam pfam = (PFam) node;
                Fam fam = pfam.getFam();
                update(fam, color);
            }

            if (node instanceof PEdge)
            {
                PEdge pedge = (PEdge) node;
                Edge edge = pedge.getEdge();
                updateEdge(edge, color);
            }
        }
    }
}
