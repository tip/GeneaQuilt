/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.hull;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.nodes.FamGeneration;
import fr.inria.aviz.geneaquilt.gui.nodes.IndiGeneration;
import fr.inria.aviz.geneaquilt.gui.nodes.PFam;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * 
 * The Class <b>Hull</b> quickly computes the hull (envelope) of a quilt. It is
 * used by the BirdsEyeView to constrain the panning to a predefined path and
 * for performing automatic zooming.
 * 
 * The hull is computed by first building a succession of vertical bins that are
 * laid out horizontally. Each bin is aligned with a PNode (PFam or
 * IndiGeneration). At first, bins are given the size of their PNode. They then
 * extend vertically to include every graphical object above and below,
 * including grid lines connecting distant nodes. Once bins have been computed,
 * two smooth curves are created that approximate the envelope above and below
 * the quilt. These two curves are averaged into a third curve that represents
 * the spine of the quilt.
 * 
 * Note: This class extends PNode for debugging purposes only. It does not need
 * to be added to the Piccolo scenegraph.
 * 
 * @author dragice
 * 
 */
public class Hull extends PNode
{
    private static final long serialVersionUID = 6790104184767759624L;

    private QuiltManager manager;

    // For debugging.
    private static final Color BINCOLOR = new Color(0, 0, 1, 0.1f);
    private static final Color TOPPATHCOLOR = new Color(0, 0, 1, 0.5f);
    private static final Color BOTTOMPATHCOLOR = new Color(1, 0, 0, 0.5f);

    // Bins.
    private HullBin[] bins;
    private double[] x0, x1;
    private double[] miny, maxy;

    // Outline.
    private Path topPath;
    private Path bottomPath;
    private Path midPath;
    private Path tmpPath;

    /**
     * Creates a new Hull.
     * 
     * @param manager
     *            the manager
     */
    public Hull(final QuiltManager manager)
    {
        this.manager = manager;
        setPickable(false);
    }

    /**
     * Adds the Y coordinate.
     * 
     * @param binIndex
     *            the bin index
     * @param y
     *            the y
     */
    private void addYCoordinate(final int binIndex, final double y)
    {
        if (y < this.miny[binIndex])
        {
            this.miny[binIndex] = y;
        }
        if (y > this.maxy[binIndex])
        {
            this.maxy[binIndex] = y;
        }
    }

    /**
     * Creates the vertical bins, once the QuiltManager has added all its
     * children.
     */
    public void createBins()
    {
        ArrayList<HullBin> tmpbins = new ArrayList<HullBin>();

        FamGeneration[] famGenerations = this.manager.getFamGenerations();
        IndiGeneration[] indiGenerations = this.manager.getIndiGenerations();
        for (int generationIndex = 0; generationIndex < famGenerations.length; generationIndex++)
        {
            if (indiGenerations[generationIndex] != null)
            {
                tmpbins.add(indiGenerations[generationIndex]);
            }
            if (famGenerations[generationIndex] != null)
            {
                for (Object childReference : famGenerations[generationIndex].getChildrenReference())
                {
                    if (childReference instanceof PFam)
                    {
                        tmpbins.add((PFam) childReference);
                    }
                }
            }
        }

        int nbBins = tmpbins.size();
        this.bins = new HullBin[nbBins];
        this.miny = new double[nbBins];
        this.maxy = new double[nbBins];
        this.x0 = new double[nbBins];
        this.x1 = new double[nbBins];
        this.topPath = new Path(nbBins + 2);
        this.bottomPath = new Path(nbBins + 2);
        this.midPath = new Path(nbBins + 2);
        this.tmpPath = new Path(nbBins + 2);
        for (int i = 0; i < nbBins; i++)
        {
            this.bins[i] = tmpbins.get(i);
            this.bins[i].setHullBinIndex(i);
        }
    }

    /**
     * Gets the area to zoom.
     * 
     * @param point
     *            the point
     * @return the bounding rectangle associated to the closest point on the
     *         middle path (spine). The center of the rectangle belongs to the
     *         path. Its size is large enough to contain all graphical objects
     *         around.
     */
    public Rectangle2D getAreaToZoom(final Point2D point)
    {
        Rectangle2D result;

        if (this.midPath == null)
        {
            result = null;
        }
        else
        {
            int index = this.midPath.getClosestPointIndex(point);
            result = new Rectangle2D.Double(this.bottomPath.x[index], this.topPath.y[index], this.topPath.x[index] - this.bottomPath.x[index], this.bottomPath.y[index] - this.topPath.y[index]);
        }

        //
        return result;
    }

    /**
     * Return the next point.
     * 
     * @param p
     *            point close
     * @return the next point
     */
    public Point2D getNextPoint(final Point2D p)
    {
        Point2D result;

        if (this.midPath == null)
        {
            result = null;
        }
        else
        {
            result = this.midPath.getControlPoint(this.midPath.getClosestPointIndex(p) + 1);
        }

        //
        return result;
    }

    /**
     * Gets the point on path.
     * 
     * @param point
     *            the point
     * @return the closest control point on the middle path (spine).
     */
    public Point2D getPointOnPath(final Point2D point)
    {
        Point2D result;

        if (this.midPath == null)
        {
            result = null;
        }
        else
        {
            result = this.midPath.getClosestPoint(point);
        }

        //
        return result;
    }

    /**
     * Return the previous point.
     * 
     * @param point
     *            the point close
     * @return the previous point
     */
    public Point2D getPreviousPoint(final Point2D point)
    {
        Point2D result;

        if (this.midPath == null)
        {
            result = null;
        }
        else
        {
            result = this.midPath.getControlPoint(this.midPath.getClosestPointIndex(point) - 1);
        }

        //
        return result;
    }

    /**
     * For debugging.
     * 
     * @param paintContext
     *            the paint context
     */
    @Override
    protected void paint(final PPaintContext paintContext)
    {
        final Graphics2D graphic = paintContext.getGraphics();

        if (this.bins != null)
        {
            graphic.setColor(BINCOLOR);
            for (int binIndex = 0; binIndex < this.bins.length; binIndex++)
            {
                graphic.fillRect((int) this.x0[binIndex], (int) this.miny[binIndex], (int) (this.x1[binIndex] - this.x0[binIndex]), (int) (this.maxy[binIndex] - this.miny[binIndex]));
            }
        }

        if (this.topPath != null)
        {
            this.topPath.paint(paintContext, TOPPATHCOLOR);
        }

        if (this.bottomPath != null)
        {
            this.bottomPath.paint(paintContext, TOPPATHCOLOR);
        }

        if (this.midPath != null)
        {
            this.midPath.paint(paintContext, BOTTOMPATHCOLOR);
        }

        graphic.setColor(new Color(0, 1, 0, 0.8f));
        for (int i = 0; i < this.midPath.x.length; i++)
        {
            if (i % 10 == 0)
            {
                graphic.drawLine((int) this.topPath.x[i], (int) this.topPath.y[i], (int) this.bottomPath.x[i], (int) this.bottomPath.y[i]);
            }
        }
    }

    /**
     * Update bins.
     */
    private void updateBins()
    {

        PBounds bounds;
        for (int binIndex = 0; binIndex < this.bins.length; binIndex++)
        {
            bounds = this.bins[binIndex].getFullBoundsReference();
            this.miny[binIndex] = bounds.getY();
            this.maxy[binIndex] = bounds.getY() + bounds.getHeight();
            if (binIndex == 0)
            {
                this.x0[binIndex] = bounds.getX();
            }
            else
            {
                // connect bins horizontally
                this.x0[binIndex] = (this.x1[binIndex - 1] + bounds.getX()) / 2;
                this.x1[binIndex - 1] = this.x0[binIndex];
            }
            this.x1[binIndex] = bounds.getX() + bounds.getWidth();
        }

        Network network = this.manager.getNetwork();
        for (Edge edge : network.getEdges())
        {
            PBounds nb = edge.getNode().getFullBoundsReference();
            if (nb.getWidth() == 0)
            {
                continue;
            }
            Vertex from = edge.getFromVertex();
            Vertex to = edge.getToVertex();
            PFam fam;
            PBounds fb;

            if (to instanceof Fam)
            {
                // Propagate the bounds to the right
                double xmin = nb.getX();
                double ymax = nb.getY() + nb.getHeight();
                fam = (PFam) to.getNode();
                int i = fam.getHullBinIndex();
                do
                {
                    fb = this.bins[i].getFullBoundsReference();
                    addYCoordinate(i, ymax);
                    i += 1;
                } while (i < this.bins.length && fb.getX() + fb.getWidth() >= xmin && fb.getY() <= ymax);
            }
            else if (from instanceof Fam)
            {
                // Propagate the bounds to the left
                double xmax = nb.getX() + nb.getWidth();
                double ymin = nb.getY();
                fam = (PFam) from.getNode();
                int i = fam.getHullBinIndex();
                do
                {
                    fb = this.bins[i].getFullBoundsReference();
                    addYCoordinate(i, ymin);
                    i -= 1;
                } while (i > 0 && fb.getX() <= xmax && fb.getY() + fb.getHeight() >= ymin);
            }
        }
    }

    /**
     * Update paths.
     */
    private void updatePaths()
    {
        this.topPath.x[0] = this.x0[0];
        this.topPath.y[0] = this.miny[0];
        this.bottomPath.x[0] = this.x0[0];
        this.bottomPath.y[0] = this.maxy[0];

        for (int binIndex = 0; binIndex < this.bins.length; binIndex++)
        {
            this.topPath.x[binIndex + 1] = this.x1[binIndex];
            this.topPath.y[binIndex + 1] = this.miny[binIndex];
            this.bottomPath.x[binIndex + 1] = this.x0[binIndex];
            this.bottomPath.y[binIndex + 1] = this.maxy[binIndex];
        }

        int i1 = this.bins.length - 1;
        this.topPath.x[i1 + 2] = this.x1[i1];
        this.topPath.y[i1 + 2] = this.miny[i1];
        this.bottomPath.x[i1 + 2] = this.x1[i1];
        this.bottomPath.y[i1 + 2] = this.maxy[i1];

        // Here we try to smoothen the curves while at the same time ensuring
        // they will still contain
        // all the graphical objects.

        this.topPath.copy(this.tmpPath);
        this.topPath.translate(200, -200);
        this.topPath.smoothen(5, 6, true);
        this.topPath.minY(this.tmpPath);
        this.topPath.translate(60, -60);
        this.topPath.smoothen(1, 6, true);

        this.bottomPath.copy(this.tmpPath);
        this.bottomPath.translate(-200, 200);
        this.bottomPath.smoothen(5, 6, true);
        this.bottomPath.maxY(this.tmpPath);
        this.bottomPath.translate(-60, 60);
        this.bottomPath.smoothen(1, 6, true);

        this.bottomPath.copy(this.midPath);
        this.midPath.average(this.topPath);
    }

    /**
     * Recomputes the shape of the hull, once the QuiltManager has updated its
     * layout.
     */
    public void updateShape()
    {
        // Benchmark.beginTask("Update bins");
        updateBins();
        // Benchmark.endTask();

        // Benchmark.beginTask("Update outline");
        updatePaths();
        // Benchmark.endTask();
    }
}
