/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview.hull;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Point2D;

import edu.umd.cs.piccolo.util.PPaintContext;

/**
 * A 2D path approximated by a polyline.
 * 
 * @author dragice
 */
public class Path
{
    /** For debugging. */
    private static Stroke pathstroke = new BasicStroke(2);

    protected double[] x;
    protected double[] y;
    private double[] tmpx;
    private double[] tmpy;

    /**
     * Instantiates a new path.
     * 
     * @param countOfPoints
     *            the count of points
     */
    public Path(final int countOfPoints)
    {
        this.x = new double[countOfPoints];
        this.y = new double[countOfPoints];
        this.tmpx = new double[countOfPoints];
        this.tmpy = new double[countOfPoints];
    }

    /**
     * Average.
     * 
     * @param path
     *            the path
     */
    public void average(final Path path)
    {
        for (int i = 0; i < this.x.length; i++)
        {
            this.x[i] = (this.x[i] + path.x[i]) / 2;
            this.y[i] = (this.y[i] + path.y[i]) / 2;
        }
    }

    /**
     * Copy.
     * 
     * @param to
     *            the to
     */
    public void copy(final Path to)
    {
        for (int i = 0; i < this.x.length; i++)
        {
            to.x[i] = this.x[i];
            to.y[i] = this.y[i];
        }
    }

    /**
     * Gets the closest point.
     * 
     * @param point
     *            the point
     * @return the closest point
     */
    public Point2D getClosestPoint(final Point2D point)
    {
        Point2D result;

        int closest = getClosestPointIndex(point);
        result = new Point2D.Double(this.x[closest], this.y[closest]);

        //
        return result;
    }

    /**
     * Gets the closest point index.
     * 
     * @param point
     *            the point
     * @return the closest point index
     */
    public int getClosestPointIndex(final Point2D point)
    {
        int result;

        result = 0;
        double min_dist = Double.MAX_VALUE;
        double dist;
        for (int i = 0; i < this.x.length; i++)
        {
            dist = (point.getX() - this.x[i]) * (point.getX() - this.x[i]) + (point.getY() - this.y[i]) * (point.getY() - this.y[i]);
            if (dist < min_dist)
            {
                min_dist = dist;
                result = i;
            }
        }

        //
        return result;
    }

    /**
     * Gets the control point.
     * 
     * @param index
     *            the index
     * @return the control point
     */
    public Point2D getControlPoint(final int index)
    {
        Point2D result;

        int targetIndex;
        if (index < 0)
        {
            targetIndex = 0;
        }
        else if (index > this.x.length - 1)
        {
            targetIndex = this.x.length - 1;
        }
        else
        {
            targetIndex = index;
        }

        result = new Point2D.Double(this.x[targetIndex], this.y[targetIndex]);

        //
        return result;
    }

    /**
     * Gets the y coord.
     * 
     * @param xcoord
     *            the xcoord
     * @return the y coord
     */
    public double getYCoord(final double xcoord)
    {
        double result;

        int i = 0;
        for (; i < this.x.length && this.x[i] < xcoord; i++)
        {
            ;
        }

        if (i == 0)
        {
            result = this.y[0];
        }
        else if (i == this.x.length)
        {
            result = this.y[this.x.length - 1];
        }
        else
        {
            result = this.y[i - 1] + (xcoord - this.x[i - 1]) / (this.x[i] - this.x[i - 1]) * (this.y[i] - this.y[i - 1]);
        }

        //
        return result;
    }

    /**
     * Max Y.
     * 
     * @param path
     *            the path
     */
    public void maxY(final Path path)
    {
        double ymax;
        for (int i = 0; i < this.x.length; i++)
        {
            ymax = path.getYCoord(this.x[i]);
            if (ymax > this.y[i])
            {
                this.y[i] = ymax;
            }
        }
    }

    /**
     * Min Y.
     * 
     * @param path
     *            the path
     */
    public void minY(final Path path)
    {
        double ymin;
        for (int i = 0; i < this.x.length; i++)
        {
            ymin = path.getYCoord(this.x[i]);
            if (ymin < this.y[i])
            {
                this.y[i] = ymin;
            }
        }
    }

    /**
     * For debugging.
     * 
     * @param paintContext
     *            the paint context
     * @param pathcolor
     *            the pathcolor
     */
    public void paint(final PPaintContext paintContext, final Color pathcolor)
    {
        final Graphics2D graphic = paintContext.getGraphics();
        graphic.setColor(pathcolor);
        graphic.setStroke(pathstroke);
        for (int i = 0; i < this.x.length - 1; i++)
        {
            graphic.drawLine((int) this.x[i], (int) this.y[i], (int) this.x[i + 1], (int) this.y[i + 1]);
        }
        final int r = 3;
        for (int i = 0; i < this.x.length; i++)
        {
            graphic.fillRect((int) this.x[i] - r, (int) this.y[i] - r, r * 2 + 1, r * 2 + 1);
        }
    }

    /**
     * Smoothen.
     * 
     * @param radius
     *            the radius
     * @param preserveExtremities
     *            the preserve extremities
     */
    private void smoothen(final int radius, final boolean preserveExtremities)
    {
        for (int i = 0; i < this.x.length; i++)
        {
            this.tmpx[i] = this.x[i];
            this.tmpy[i] = this.y[i];
        }
        double sumx, sumy;
        int j0, j1, r;
        for (int i = 0; i < this.x.length; i++)
        {
            sumx = 0;
            sumy = 0;
            if (preserveExtremities)
            {
                int distanceFromExtremity = Math.min(i, this.x.length - 1 - i);
                r = Math.min(distanceFromExtremity, radius);
            }
            else
            {
                r = radius;
            }
            j0 = Math.max(0, i - r);
            j1 = Math.min(this.x.length - 1, i + r);
            if (j1 > j0)
            {
                for (int j = j0; j < j1; j++)
                {
                    sumx += this.tmpx[j];
                    sumy += this.tmpy[j];
                }
                double dx = sumx / (j1 - j0) - this.x[i];
                double dy = sumy / (j1 - j0) - this.y[i];
                this.x[i] += dx;
                this.y[i] += dy;
                // x[i] = sumx / (j1 - j0);
                // y[i] = sumy / (j1 - j0);
            }
        }
    }

    /**
     * Smoothen the curve.
     * 
     * @param radius
     *            the radius
     * @param iterations
     *            the iterations
     * @param preserveExtremities
     *            the preserve extremities
     */
    public void smoothen(final int radius, final int iterations, final boolean preserveExtremities)
    {
        for (int iterationIndex = 0; iterationIndex < iterations; iterationIndex++)
        {
            smoothen(radius, preserveExtremities);
        }
    }

    /**
     * Translate.
     * 
     * @param dx
     *            the dx
     * @param dy
     *            the dy
     */
    public void translate(final int dx, final int dy)
    {
        for (int i = 0; i < this.x.length; i++)
        {
            this.x[i] += dx;
            this.y[i] += dy;
        }
    }
}
