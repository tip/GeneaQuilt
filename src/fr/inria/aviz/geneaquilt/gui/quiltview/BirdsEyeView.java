/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.awt.Color;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PLayer;
import edu.umd.cs.piccolo.activities.PActivity;
import edu.umd.cs.piccolo.activities.PTransformActivity;
import edu.umd.cs.piccolo.event.PDragSequenceEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PDimension;
import edu.umd.cs.piccolo.util.PPaintContext;
import fr.inria.aviz.geneaquilt.gui.nodes.PFlatRect;
import fr.inria.aviz.geneaquilt.gui.quiltview.hull.Hull;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils;
import fr.inria.aviz.geneaquilt.gui.util.GUIUtils.AdvancedKeyListener;

/**
 * The Class BirdsEyeView.
 * 
 * @author Jean-Daniel Fekete
 * @version $Revision$
 */
public class BirdsEyeView extends PCanvas implements PropertyChangeListener, AdvancedKeyListener
{
    private static final long serialVersionUID = 1L;

    /**
     * This is the node that shows the viewed area.
     */
    private PFlatRect areaVisiblePNode;

    /**
     * This is the canvas that is being viewed
     */
    private PCanvas viewedCanvas;

    /**
     * The change listener to know when to update the birds eye view.
     */
    private PropertyChangeListener changeListener;

    private int layerCount;

    private final Hull hull;

    private PDragSequenceEventHandler dragHandler;

    private PTransformActivity currentCameraAnimation = null;

    private boolean smoothPanning = true;

    private boolean keyScroll = false;

    private float currentScale = 1;

    /**
     * Creates a new instance of a BirdsEyeView.
     * 
     * @param hull
     *            the hull manager
     */
    public BirdsEyeView(final Hull hull)
    {

        this.hull = hull;

        // create the PropertyChangeListener for listening to the viewed
        // canvas
        this.changeListener = new PropertyChangeListener()
        {
            /**
             * 
             */
            @Override
            public void propertyChange(final PropertyChangeEvent evt)
            {
                updateFromViewed();
            }
        };

        // create the coverage node
        this.areaVisiblePNode = new PFlatRect();
        this.areaVisiblePNode.setPaint(new Color(0.5f, 0.5f, 1f, 0.3f));
        this.areaVisiblePNode.setTransparency(.5f);
        this.areaVisiblePNode.setBounds(0, 0, 100, 100);
        getCamera().addChild(this.areaVisiblePNode);

        // add the drag event handler
        this.dragHandler = new PDragSequenceEventHandler()
        {
            private Point2D dragOffset = new Point2D.Double();
            private Point2D p = new Point2D.Double();

            /**
			 * 
			 */
            @Override
            protected void drag(final PInputEvent event)
            {
                if (event.isShiftDown())
                {
                    final PDimension dim = event.getDelta();
                    BirdsEyeView.this.viewedCanvas.getCamera().translateView(0 - dim.getWidth(), 0 - dim.getHeight());
                }
                else
                {
                    Point2D pos = event.getPosition();
                    this.p.setLocation(pos.getX() - this.dragOffset.getX(), pos.getY() - this.dragOffset.getY());
                    moveCameraOnPath(this.p, false);
                }
            }

            /**
			 * 
			 */
            @Override
            protected void endDrag(final PInputEvent event)
            {
                BirdsEyeView.this.viewedCanvas.setInteracting(false);
                super.endDrag(event);
            }

            /**
			 * 
			 */
            @Override
            protected void startDrag(final PInputEvent event)
            {

                if (event.getPickedNode() == BirdsEyeView.this.areaVisiblePNode)
                {
                    PBounds bounds = BirdsEyeView.this.viewedCanvas.getCamera().getViewBounds();
                    this.dragOffset.setLocation(event.getPosition().getX() - bounds.getCenterX(), event.getPosition().getY() - bounds.getCenterY());
                    super.startDrag(event);
                }
                else
                {
                    if (event.isShiftDown())
                    {
                        PBounds b = BirdsEyeView.this.viewedCanvas.getCamera().getViewBounds();
                        BirdsEyeView.this.viewedCanvas.getCamera().translateView(b.x - event.getPosition().getX() + b.width / 2, b.y - event.getPosition().getY() + b.height / 2);
                        super.startDrag(event);
                    }
                    else
                    {
                        this.dragOffset.setLocation(0, 0);
                        moveCameraOnPath(event.getPosition(), true);
                        super.startDrag(event);
                    }
                }
                BirdsEyeView.this.viewedCanvas.setInteracting(true);
            }

        };

        getCamera().addInputEventListener(this.dragHandler);

        addComponentListener(new ComponentAdapter()
        {
            /**
			 * 
			 */
            @Override
            public void componentResized(final ComponentEvent event)
            {
                autoScale();
                updateFromViewed();
            }
        });

        // remove Pan and Zoom
        removeInputEventListener(getPanEventHandler());
        removeInputEventListener(getZoomEventHandler());

        // Scrolling with keys
        GUIUtils.addAdvancedKeyListener(null, this, true);

        setDefaultRenderQuality(PPaintContext.LOW_QUALITY_RENDERING);
    }

    /**
     * Add a layer to list of viewed layers.
     * 
     * @param newLayer
     *            the layer to add
     */
    public void addLayer(final PLayer newLayer)
    {
        getCamera().addLayer(newLayer);
        this.layerCount++;
    }

    /**
     * Auto scales the viewport.
     */
    public void autoScale()
    {
        getCamera().setViewBounds(getCamera().getUnionOfLayerFullBounds());
    }

    /**
     * Connects to a canvas and some layers.
     * 
     * @param canvas
     *            the canvas
     * @param viewed_layers
     *            the layers
     */
    public void connect(final PCanvas canvas, final PLayer[] viewed_layers)
    {

        this.viewedCanvas = canvas;
        this.layerCount = 0;

        this.viewedCanvas.getCamera().addPropertyChangeListener(this.changeListener);

        for (this.layerCount = 0; this.layerCount < viewed_layers.length; ++this.layerCount)
        {
            getCamera().addLayer(this.layerCount, viewed_layers[this.layerCount]);
        }

    }

    /**
     * Stop the birds eye view from receiving events from the viewed canvas and
     * remove all layers.
     */
    public void disconnect()
    {
        this.viewedCanvas.getCamera().removePropertyChangeListener(this.changeListener);

        for (int layerIndex = 0; layerIndex < getCamera().getLayerCount(); ++layerIndex)
        {
            getCamera().removeLayer(layerIndex);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final KeyEvent event)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressedOnce(final KeyEvent event)
    {
        if ((!this.viewedCanvas.getInteracting()) && (event.getKeyCode() == KeyEvent.VK_LEFT || event.getKeyCode() == KeyEvent.VK_RIGHT))
        {
            this.keyScroll = true;
            this.viewedCanvas.setInteracting(true);
            this.smoothPanning = false;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyReleased(final KeyEvent event)
    {
        if (this.keyScroll)
        {
            this.viewedCanvas.setInteracting(false);
            this.keyScroll = false;
            this.smoothPanning = true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyRepeated(final KeyEvent event)
    {
        // scrolling with arrow keys
        if (this.keyScroll)
        {
            PBounds bounds = this.viewedCanvas.getCamera().getViewBounds();
            Point2D point = new Point2D.Double(bounds.getCenterX(), bounds.getCenterY());
            Point2D targetPoint;
            if (event.getKeyCode() == KeyEvent.VK_LEFT)
            {
                targetPoint = this.hull.getPreviousPoint(point);
                moveCameraOnPath(targetPoint, false);
            }
            else if (event.getKeyCode() == KeyEvent.VK_RIGHT)
            {
                targetPoint = this.hull.getNextPoint(point);
                moveCameraOnPath(targetPoint, false);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyTyped(final KeyEvent arg0)
    {
    }

    /**
     * Moves along a path.
     * 
     * @param point
     *            the point
     * @param jump
     *            true if jumping
     */
    public void moveCameraOnPath(final Point2D point, final boolean jump)
    {
        Rectangle2D area = this.hull.getAreaToZoom(point);
        int minZoomHeight = (int) this.viewedCanvas.getCamera().getHeight();
        if (area.getHeight() < minZoomHeight)
        {
            area.setFrame(area.getX(), area.getCenterY() - minZoomHeight / 2, area.getWidth(), minZoomHeight);
        }
        if (jump || this.smoothPanning)
        {
            // The following allows a camera animation to start while another
            // animation
            // is already running.
            if (this.currentCameraAnimation != null && this.currentCameraAnimation.isStepping())
            {
                this.currentCameraAnimation.terminate(PActivity.TERMINATE_WITHOUT_FINISHING);
            }
            if (jump)
            {
                this.currentCameraAnimation = this.viewedCanvas.getCamera().animateViewToCenterBounds(area, true, 300);
                this.currentCameraAnimation.setSlowInSlowOut(true);
            }
            else
            {
                this.currentCameraAnimation = this.viewedCanvas.getCamera().animateViewToCenterBounds(area, true, 150);
                this.currentCameraAnimation.setSlowInSlowOut(false);
            }
            // Play one step right now, otherwise nothing will happen while
            // dragging the mouse since animations
            // won't have time to play.
            this.currentCameraAnimation.setStartTime(System.currentTimeMillis() - this.currentCameraAnimation.getStepRate());
            this.currentCameraAnimation.processStep(System.currentTimeMillis());
        }
        else
        {
            this.viewedCanvas.getCamera().setViewBounds(area);
        }
    }

    /**
     * This method will get called when the viewed canvas changes.
     * 
     * @param event
     *            the event
     */
    @Override
    public void propertyChange(final PropertyChangeEvent event)
    {
        updateFromViewed();
    }

    /**
     * Remove the layer from the viewed layers.
     * 
     * @param old_layer
     *            the layer to remove
     */
    public void removeLayer(final PLayer old_layer)
    {
        getCamera().removeLayer(old_layer);
        this.layerCount -= 1;
    }

    /**
     * This method gets the state of the viewed canvas and updates the
     * BirdsEyeViewer This can be called from outside code.
     */
    public void updateFromViewed()
    {
        double viewedX;
        double viewedY;
        double viewedHeight;
        double viewedWidth;

        final double ul_camera_x = this.viewedCanvas.getCamera().getViewBounds().getX();
        final double ul_camera_y = this.viewedCanvas.getCamera().getViewBounds().getY();
        final double lr_camera_x = ul_camera_x + this.viewedCanvas.getCamera().getViewBounds().getWidth();
        final double lr_camera_y = ul_camera_y + this.viewedCanvas.getCamera().getViewBounds().getHeight();

        // final Rectangle2D drag_bounds =
        // getCamera().getUnionOfLayerFullBounds();

        // final double ul_layer_x = drag_bounds.getX();
        // final double ul_layer_y = drag_bounds.getY();
        // final double lr_layer_x = drag_bounds.getX() +
        // drag_bounds.getWidth();
        // final double lr_layer_y = drag_bounds.getY() +
        // drag_bounds.getHeight();

        // find the upper left corner

        // set to the lesser value
        // if (ul_camera_x < ul_layer_x) {
        // viewedX = ul_layer_x;
        // }
        // else {
        viewedX = ul_camera_x;
        // }

        // same for y
        // if (ul_camera_y < ul_layer_y) {
        // viewedY = ul_layer_y;
        // }
        // else {
        viewedY = ul_camera_y;
        // }

        // find the lower right corner

        // set to the greater value
        // if (lr_camera_x < lr_layer_x) {
        viewedWidth = lr_camera_x - viewedX;
        // }
        // else {
        // viewedWidth = lr_layer_x - viewedX;
        // }

        // same for height
        // if (lr_camera_y < lr_layer_y) {
        viewedHeight = lr_camera_y - viewedY;
        // }
        // else {
        // viewedHeight = lr_layer_y - viewedY;
        // }

        Rectangle2D bounds = new Rectangle2D.Double(viewedX, viewedY, viewedWidth, viewedHeight);
        bounds = getCamera().viewToLocal(bounds);
        this.areaVisiblePNode.setBounds(bounds);
    }
}
