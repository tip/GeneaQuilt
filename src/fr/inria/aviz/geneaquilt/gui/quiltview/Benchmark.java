/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015,2016 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.quiltview;

import java.util.Collection;
import java.util.regex.Pattern;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import fr.inria.aviz.geneaquilt.gui.nodes.QuiltManager;
import fr.inria.aviz.geneaquilt.gui.quiltview.selection.SelectionManager;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * Computes a <b>Benchmark</b>
 * 
 * @author Pierre Dragicevic
 */
public class Benchmark
{
    static String taskname = null;
    static long t0 = 0;

    /**
     * Begin a long task
     * 
     * @param taskname_
     *            the task name
     */
    public static void beginTask(final String taskname_)
    {
        t0 = now();
        taskname = taskname_;
    }

    /**
	 * 
	 */
    private static void doBenchmark()
    {
        wait(2);

        // JFrame frame = GeneaQuilt.getFrame();
        GeneaQuiltOldWindow quilt = GeneaQuiltOldWindow.getQuilt();
        QuiltManager root = quilt.getManager();
        SelectionManager selection = root.getSelectionManager();
        PCanvas canvas = quilt.getCanvas();
        BirdsEyeView bev = quilt.getBev();

        // beginTask("search 1");
        Collection<Vertex> v = quilt.search("I2018", "ID", Pattern.LITERAL | Pattern.CASE_INSENSITIVE);
        PNode n = v.iterator().next().getNode();
        quilt.search("", "", Pattern.LITERAL);
        // endTask();

        wait(6);

        beginTask("select 1");
        // Selection s = selection.select(n);
        endTask();

        wait(1);

        /*beginTask("highlight 1");
        s.highlightPredecessors();
        s.highlightSuccessors();
        s.highlightSelection();
        frame.repaint();
        endTask();*/

        wait(2);

        // beginTask("search 2");
        v = quilt.search("I2961", "ID", Pattern.LITERAL | Pattern.CASE_INSENSITIVE);
        n = v.iterator().next().getNode();
        quilt.search("", "", Pattern.LITERAL);
        // endTask();

        wait(1);

        beginTask("select 2");
        // s =
        selection.select(n);
        endTask();

        wait(1);

        /*beginTask("highlight 2");
        s.highlightPredecessors();
        s.highlightSuccessors();
        s.highlightSelection();
        frame.repaint();
        endTask();*/

        wait(2);

        beginTask("paint canvas");
        canvas.paintImmediately(canvas.getBounds());
        endTask();

        wait(1);

        beginTask("paint bev");
        bev.paintImmediately(bev.getBounds());
        endTask();

        wait(1);

        beginTask("paint bev");
        bev.paintImmediately(bev.getBounds());
        endTask();

        wait(1);

        // System.exit(0);
    }

    // ///////////////////

    /**
     * Ends the current long task.
     */
    public static void endTask()
    {
        if (t0 == 0 || taskname == null)
        {
            System.out.println("Error: attempt to end a task that has not started.");
            return;
        }
        int timems = (int) ((now() - t0) / 1000000);
        System.out.println("Task \"" + taskname + "\" done in " + timems + " ms.");
        t0 = 0;
        taskname = null;
    }

    /**
     * 
     * This class is used to compare the performance of different rendering
     * methods.
     * 
     * @param args
     */
    public static void main(final String[] args)
    {

        // beginTask("load");
        GeneaQuiltOldWindow.main(new String[] { "data/royal92.ged" });
        // endTask();

        GeneaQuiltOldWindow.getFrame().setLocation(GeneaQuiltOldWindow.getFrame().getX(), 0);

        final Thread t = new Thread()
        {
            @Override
            public void run()
            {
                doBenchmark();
            }
        };
        t.start();
    }

    /**
     * 
     * @return
     */
    private static long now()
    {
        return System.nanoTime();
    }

    /**
     * Sleeps for a specified amount os seconds.
     * 
     * @param secondCount
     *            the number of seconds
     */
    public static void wait(final int secondCount)
    {
        try
        {
            Thread.sleep(1000 * secondCount);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
