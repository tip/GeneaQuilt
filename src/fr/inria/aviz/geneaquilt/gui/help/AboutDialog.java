/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.FileTools;

/**
 * The Class AboutDialog.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AboutDialog extends JDialog
{
    private static final long serialVersionUID = -2211494927893500376L;

    private final JPanel contentPanel = new JPanel();

    /**
     * Create the dialog.
     */
    public AboutDialog()
    {
        setModal(true);
        setTitle("About GeneaQuilt");
        setIconImage(Toolkit.getDefaultToolkit().getImage(AboutDialog.class.getResource("/fr/inria/aviz/geneaquilt/gui/favicon-32x32.png")));
        setBounds(100, 100, 718, 487);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(this.contentPanel, BorderLayout.CENTER);
        this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
        {
            JPanel panel = new JPanel();
            this.contentPanel.add(panel, "2, 2, center, fill");
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            {
                JLabel lblAvizLogo = new JLabel("");
                lblAvizLogo.setAlignmentX(Component.CENTER_ALIGNMENT);
                panel.add(lblAvizLogo);
                lblAvizLogo.setIcon(new ImageIcon(AboutDialog.class.getResource("/fr/inria/aviz/geneaquilt/gui/help/logo-aviz-200x.png")));
            }
            {
                Component verticalStrut = Box.createVerticalStrut(15);
                panel.add(verticalStrut);
            }
            {
                JLabel lblInriaLogo = new JLabel("");
                lblInriaLogo.setAlignmentX(Component.CENTER_ALIGNMENT);
                panel.add(lblInriaLogo);
                lblInriaLogo.setIcon(new ImageIcon(AboutDialog.class.getResource("/fr/inria/aviz/geneaquilt/gui/help/logo-Inria-nb-125x.png")));
            }
        }
        {
            JLabel lblGeneaQuiltLogo = new JLabel("");
            this.contentPanel.add(lblGeneaQuiltLogo, "4, 2, center, default");
            lblGeneaQuiltLogo.setIcon(new ImageIcon(AboutDialog.class.getResource("/fr/inria/aviz/geneaquilt/gui/help/logo-geneaquilt-336x.png")));
        }
        {
            JScrollPane scrollPane = new JScrollPane();
            this.contentPanel.add(scrollPane, "2, 4, 3, 1, fill, fill");
            {
                JEditorPane txtpnAbout = new JEditorPane("text/html", "");
                txtpnAbout.setEditable(false);
                txtpnAbout.setText("");
                scrollPane.setViewportView(txtpnAbout);
                // ///////////////////////////:
                try
                {
                    txtpnAbout.setText(FileTools.load(AboutDialog.class.getResource("/fr/inria/aviz/geneaquilt/gui/help/about.html")));
                }
                catch (IOException exception)
                {
                    exception.printStackTrace();
                }
                txtpnAbout.setCaretPosition(0);
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(final ActionEvent e)
                    {
                        dispose();
                    }
                });
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
        }
    }

    /**
     * Launch the application.
     * 
     * @param args
     *            the arguments
     */
    public static void main(final String[] args)
    {
        try
        {
            AboutDialog dialog = new AboutDialog();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
