/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015-2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui;

import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.gui.util.GUIToolBox;

/**
 * The Class GeneaQuiltGUI.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeneaQuiltGUI
{
    /** Lazy-loading with holder */
    private static class SingletonLoader
    {
        private static final GeneaQuiltGUI instance = new GeneaQuiltGUI();
    }

    private static final Logger logger = LoggerFactory.getLogger(GeneaQuiltGUI.class);

    private File homeDirectory;

    /**
     * Launch the application.
     */
    private GeneaQuiltGUI()
    {
        //
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            /**
             * 
             */
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\nTo fix this problem, run GeneaQuilts from the command line:\njava -jar -Xms256m geneaquilt-x.x.x.jar\n\nIf you still get the same error, increase the value 256 above.";
                }
                else
                {
                    message = "An error occured: " + exception.getClass() + "(" + exception.getMessage() + ")";
                }

                JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
                exception.printStackTrace();
            }
        });

        // Set home directory.
        this.homeDirectory = new File(System.getProperty("user.home") + File.separator + ".geneaquilt");
        if (!this.homeDirectory.exists())
        {
            //
            this.homeDirectory.mkdir();
            logger.info("Home directory created: " + this.homeDirectory.getAbsolutePath());
        }

        // Remove BOLD on default font.
        UIManager.put("swing.boldMetal", Boolean.FALSE);

        // Set LookAndFeel.
        System.out.println("System lookAndFeel property:" + System.getProperty("swing.defaultlaf"));
        System.out.println("Available lookAndFeel: " + GUIToolBox.availableLookAndFeels().toString());
        System.out.println("System lookAndFeel: " + UIManager.getSystemLookAndFeelClassName());
        System.out.println("Current lookAndFeel: " + UIManager.getLookAndFeel().getName());

        if (!StringUtils.equals(UIManager.getSystemLookAndFeelClassName(), "javax.swing.plaf.metal.MetalLookAndFeel"))
        {
            try
            {
                System.out.println("Metal LAF setted and system LAF detected, try to set system LAF.");
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
            catch (final Exception exception)
            {
                System.out.println("Failed to set the system LookAndFeel.");
            }
        }
        else if (GUIToolBox.availableLookAndFeels().toString().contains("GTK+"))
        {
            try
            {
                System.out.println("Metal LAF setted and GTK+ LAF detected, try to set GTK+ LAF.");
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            }
            catch (final Exception exception)
            {
                System.out.println("Failed to set the system LookAndFeel.");
            }
        }

        System.out.println("Activated lookAndFeel: " + UIManager.getLookAndFeel().getName());
    }

    /**
     * Exit.
     */
    public void exit()
    {
        //
        System.exit(0);
    }

    /**
     * Run.
     */
    public void run()
    {
        //
        new GeneaQuiltWindow().setVisible(true);
    }

    /**
     * Sets the file.
     * 
     * @param source
     *            the new file
     */
    public void setFile(final File source)
    {
        // TODO
    }

    /**
     * Instance.
     * 
     * @return the genea quilt GUI
     */
    public static GeneaQuiltGUI instance()
    {
        GeneaQuiltGUI result;

        result = SingletonLoader.instance;

        //
        return result;
    }
}
