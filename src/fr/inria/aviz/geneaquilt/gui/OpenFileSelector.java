/*
 * Copyright (c) 2010-2014, Jean-Daniel Fekete, Pierre Dragicevic, and INRIA.
 * Copyright 2015,2017 Klaus Hamberger, Christian Pierre Momon and UMR 7186 LESC.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inria.aviz.geneaquilt.gui;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.aviz.geneaquilt.gui.util.GenericFileFilter;

/**
 * The Class OpenFileSelector.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class OpenFileSelector extends JFileChooser
{
    private static final long serialVersionUID = 3782597353602048214L;

    private static final Logger logger = LoggerFactory.getLogger(OpenFileSelector.class);

    /**
     * Instantiates a new open file selector.
     * 
     * @param targetFile
     *            the target file
     */
    public OpenFileSelector(final File targetFile)
    {
        super(targetFile);

        //
        setDialogTitle("Open File Selector");
        setFileSelectionMode(JFileChooser.FILES_ONLY);
        setAcceptAllFileFilterUsed(true);
        setApproveButtonText("Open");
        setDialogType(CUSTOM_DIALOG);

        GenericFileFilter defaultFileFilter = new GenericFileFilter("Genealogic files (*.ged, *.tip, *.txt)", "ged", "tip", "txt");

        addChoosableFileFilter(new GenericFileFilter("GEDCOM (*.ged)", "ged"));
        addChoosableFileFilter(new GenericFileFilter("PED (*.ped)", "ped"));
        addChoosableFileFilter(new GenericFileFilter("TIP (*.tip)", "tip"));
        setFileFilter(defaultFileFilter);
    }

    /* (non-Javadoc)
     * @see javax.swing.JFileChooser#approveSelection()
     */
    @Override
    public void approveSelection()
    {
        File targetFile = getSelectedFile();

        logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
        logger.debug("selectedFile={}", targetFile);

        super.approveSelection();
    }

    /* (non-Javadoc)
     * @see javax.swing.JFileChooser#cancelSelection()
     */
    @Override
    public void cancelSelection()
    {
        logger.debug("Cancel selection.");
        super.cancelSelection();
    }

    /* (non-Javadoc)
     * @see javax.swing.JFileChooser#setSelectedFile(java.io.File)
     */
    @Override
    public void setSelectedFile(final File file)
    {
        super.setSelectedFile(file);

        System.out.println("==== SET SELECTED FILE=================");
        System.out.println("SELECED FILE " + file);
    }

    /* (non-Javadoc)
     * @see javax.swing.JComponent#setVisible(boolean)
     */
    @Override
    public void setVisible(final boolean visible)
    {
        super.setVisible(visible);

        if (!visible)
        {
            resetChoosableFileFilters();
        }
    }

    /**
     * This method is the main one of the selector.
     * 
     * @param parent
     *            the parent
     * @param targetFile
     *            the target file
     * @return the file
     */
    public static File showSelectorDialog(final Component parent, final File targetFile)
    {
        File result;

        //
        OpenFileSelector selector = new OpenFileSelector(targetFile);

        //
        if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION)
        {
            logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
            logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
            result = selector.getSelectedFile();
        }
        else
        {
            result = null;
        }

        //
        return result;
    }
}
